﻿<%@ WebHandler Language="C#" Class="Feedback" %>
using System;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
public class Feedback : IHttpHandler, IReadOnlySessionState, IRequiresSessionState {

    public void ProcessRequest (HttpContext context) {
        string JSONVal = "";
        int employeeid = 0;
        string FBSubject = "";
        string FBComments = "";
        ApiResponceData GER = new ApiResponceData();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        try
        {
            if (context.Request.QueryString.Count > 0)
            {
                employeeid = Convert.ToInt32(context.Request.QueryString["EID"]);
                FBSubject = context.Request.QueryString["FBSubject"].ToString();
                FBComments = context.Request.QueryString["FBComments"].ToString();
            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                employeeid = Convert.ToInt32(context.Request.Params["EID"]);
                FBSubject = context.Request.Params["FBSubject"].ToString();
                FBComments = context.Request.Params["FBComments"].ToString();
            }

            if (employeeid > 0 && FBSubject!="" && FBComments!="" )
            {
                DataSet ds = new DataSet();
                using (var command = new SqlCommand("ShowReport_ProfileSal", con)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    con.Open();
                    command.Parameters.AddWithValue("@Transtype", "Feedback");
                    command.Parameters.AddWithValue("@employeeid", employeeid);
                    command.Parameters.AddWithValue("@FBSubject", FBSubject);
                    command.Parameters.AddWithValue("@FBComments", FBComments);
                    command.Parameters.AddWithValue("@UserID", employeeid);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    sda.Fill(ds);
                    con.Close();
                }
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    List<Dictionary<string, object>> rowss = new List<Dictionary<string, object>>();
                    if (dt.Rows.Count > 0)
                    {

                        Dictionary<string, object> row1;

                        foreach (DataRow r in dt.Rows)
                        {
                            row1 = new Dictionary<string, object>();
                            foreach (DataColumn col in dt.Columns)
                            {
                                row1.Add(col.ColumnName, r[col].ToString());
                            }
                            rowss.Add(row1);
                        }
                    }
                    

                    GER.Data = rowss;
                    GER.Status = "Y";
                    GER.Message = "SUCCESS";
                    JSONVal = GER.ToJSON();
                }
                else
                {
                    GER.Status = "N";
                    GER.Message = "No records found ";
                    JSONVal = GER.ToJSON();
                }
            }
            else
            {
                GER.Status = "N";
                GER.Message = "Provide all the parameters ";
                JSONVal = GER.ToJSON();
            }

        }
        catch (Exception ex)
        {
            GER.Status = "N";
            GER.Message = ex.Message;
            JSONVal = GER.ToJSON();
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(JSONVal);
    }


    public bool IsReusable {
        get {
            return false;
        }
    }

}
