﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Account.Logins;

namespace wbEcsc.App_Codes.Utils
{
    public class CommonUtils
    {
        public static string GetFinYear(DateTime date)
        {
            string finYear = "";
            int curMonth = date.Month;
            if (curMonth > 3 && curMonth <= 12)
            {
                finYear = string.Format("{0}-{1}", date.Year, date.Year + 1);
            }
            else if (curMonth > 1 && curMonth <= 3)
            {
                finYear = string.Format("{0}-{1}", date.Year - 1, date.Year);
            }
            return finYear;
        }
        public static int GetMonthDiffFromTwoDates(DateTime bigDate, DateTime smallDate)
        {
            return (bigDate.Year * 12 + bigDate.Month) - (smallDate.Year * 12 + smallDate.Month);
        }
       
    }
}