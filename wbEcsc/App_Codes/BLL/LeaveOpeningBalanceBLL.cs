﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using wbEcsc.Models.Application;
using System.Configuration;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL
{
    public class LeaveOpeningBalanceBLL:BllBase
    {
        SqlCommand cmd;
        SessionData sds;
        public LeaveOpeningBalanceBLL()
        {
            sds = SessionContext.SessionData;
        }
        public DataTable Save(LeaveOpeningBalance_MD md)
        {
            try
            {
                cmd = new SqlCommand("check_leaveOpBal", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OpDate", md.OpeningDate ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@SectorID", md.SectorID ?? 0);
                cmd.Parameters.AddWithValue("@EmployeeID", md.EmployeeID ?? 0);
                cmd.Parameters.AddWithValue("@LvId", md.AbbraviationID ?? 0);
                cmd.Parameters.AddWithValue("@OpBal", md.OpeningAmount ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@FieldID", md.FieldID ?? 0);               
                cmd.Parameters.AddWithValue("@InsertedBy", sds.UserID);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Delete(LeaveOpeningBalance_MD md)
        {
            try
            {
                cmd = new SqlCommand("Delete_leaveOpBal", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OpDate",md.OpeningDate);
                cmd.Parameters.AddWithValue("@EmployeeID", md.EmployeeID);
                cmd.Parameters.AddWithValue("@LvId", md.AbbraviationID);
                cmd.Parameters.AddWithValue("@CanBy", sds.UserID);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LeaveAbbraviation> Get_LeaveAbbraviation(string abbraviation)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Get_LeaveType", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@LeaveAbbv", abbraviation);
                cmd.Parameters.AddWithValue("@IsLeave", 1);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                sda.Fill(dt);
                return convert_Abbraviation(dt);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Employee> Get_Employee(string SectorID,string empaname)
        {
            try
            {

                SqlCommand cmd = new SqlCommand("GetEmployee", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SecID", SectorID);
                cmd.Parameters.AddWithValue("@UserType", sds.UserType);
                cmd.Parameters.AddWithValue("@EmpName", empaname);
                cmd.Parameters.AddWithValue("@EmpId", 0);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                sda.Fill(dt);
                return convert_Employee(dt);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Field> GetField()
        {
            try
            {
                cmd = new SqlCommand("Check_Mand", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfID", 18);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return Converter_Field(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LeaveOpeningBalance_MD> GetData(string sectorid)
        {
            try
            {
                cmd = new SqlCommand("Load_leaveOpBal", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SecID", sectorid);
                cmd.Parameters.AddWithValue("@UserType", sds.UserType);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return Converter_Data(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Field> Converter_Field(DataTable dt)
        {
            try
            {
                List<Field> lst = new List<Field>();
                lst = dt.AsEnumerable().Select(t =>
                {
                    var obj = new Field()
                    {
                        FieldID = t.Field<int>("FieldID"),
                        FieldName = t.Field<string>("FieldName"),
                        IsMand = t.Field<int>("IsMand")
                    };
                    return obj;
                }
                ).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LeaveAbbraviation> convert_Abbraviation(DataTable dt)
        {
            List<LeaveAbbraviation> lst = new List<LeaveAbbraviation>();
            try
            {
               
                lst = dt.AsEnumerable().Select(t => {
                    var obj = new LeaveAbbraviation() { 
                        LeaveID=t.Field<int?>("LvId"),
                        Abbraviation=t.Field<string>("LeaveAbbv")
                    };
                    return obj;
                }).ToList();
                return lst;
            }
            catch (Exception)
            {

                throw;
            }         

        }
        public List<Employee> convert_Employee(DataTable dt)
        {
            List<Employee> lst = new List<Employee>();
            try
            {

                lst = dt.AsEnumerable().Select(t => {
                    var obj = new Employee()
                    {
                        EmployeeID = t.Field<int?>("EmployeeID"),
                        employeeName = t.Field<string>("EmpName")
                    };
                    return obj;
                }).ToList();
                return lst;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public List<LeaveOpeningBalance_MD> Converter_Data(DataTable dt)
        {
            try
            {                
                List<LeaveOpeningBalance_MD> lst = new List<LeaveOpeningBalance_MD>();
                lst = dt.AsEnumerable().Select(t => {
                    var obj = new LeaveOpeningBalance_MD()
                    {
                        EmployeeID = t.Field<int?>("EmployeeID"),
                        EmployeeName = t.Field<string>("EmpName"),
                        SectorID = t.Field<int?>("SectorID"),
                        OpeningDate = t.Field<string>("OpDate"),
                        AbbraviationID = t.Field<int?>("LvId"),
                        Abbraviation = t.Field<string>("LeaveAbbv"),
                        OpeningAmount = t.Field<decimal?>("OpBal")                       
                    };
                    return obj;
                }).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

  public class LeaveAbbraviation
    {
        public int? LeaveID { get; set; }
        public string Abbraviation { get; set; }
    }

    public class Employee
    {
        public int? EmployeeID { get; set; }
        public string employeeName { get; set; }
    }
}