﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Account;
using wbEcsc.Models.AppResource;

namespace wbEcsc.App_Codes.BLL
{
 
    class UrlBLL
    {
        SqlConnection cn;

        public UrlBLL()
        {
            cn = new SqlConnection();
            cn.ConnectionString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        }
        //public bool hasUrlInCurRole(AppUrl filterModel, object currentUser)
        //{
        //    try
        //    {
        //        SqlCommand cmd = new SqlCommand();
        //        IUserType userType = (IUserType)currentUser;
        //        int actionID = filterModel.UrlID;
        //        if (userType.UserType == UserType.Administrative )
        //        {
        //            IInternalUser iObj = (IInternalUser)currentUser;
        //            cmd.Parameters.AddWithValue("@actionID", actionID);
        //            cmd.Parameters.AddWithValue("@UsertTypeID", userType.UserType);
        //            cmd.Parameters.AddWithValue("@UserID", iObj.UserID);
        //        }
        //        cmd.CommandText = "Get_IsCurrentRequestInRole";
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Connection = cn;
        //        cn.Open();
        //        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //        DataTable dt = new DataTable();
        //        da.Fill(dt);
        //        if (Convert.ToInt32(dt.Rows[0]["isExist"]) == 1)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }catch (SqlException)
        //    {
        //        throw new Exception("Database Error");
        //    }
        //    finally
        //    {
        //        cn.Close();
        //    }
        //}

        public bool hasUrlInCurRole(AppUrl filterModel, object currentUser)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                //IUserType userType = (IUserType)currentUser;
                IInternalUser iObj = (IInternalUser)currentUser;
                int actionID = filterModel.UrlID;
                if (iObj.UserType == 1)
                {
                    cmd.Parameters.AddWithValue("@actionID", actionID);
                    cmd.Parameters.AddWithValue("@UsertTypeID", iObj.UserType);
                    cmd.Parameters.AddWithValue("@UserID", iObj.UserID);
                }
                cmd.CommandText = "Get_IsCurrentRequestInRole";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (Convert.ToInt32(dt.Rows[0]["isExist"]) == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (SqlException ex)
            {
                throw  new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }
        public AppUrl GetUrl(string actionName, string controllerName, string packageName)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Get_RegisteredUrl", cn);
                cn.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ActionName", actionName);
                cmd.Parameters.AddWithValue("@ControllerName", controllerName);
                cmd.Parameters.AddWithValue("@Packagename", packageName);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    AppUrl obj = ConvertObj(dt)[0];
                    return obj;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public AppUrl GetUrl(string actionName, string controllerName, string packageName,string formattedParameter)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Get_RegisteredUrl", cn);
                cn.Open();
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ActionName", actionName);
                cmd.Parameters.AddWithValue("@ControllerName", controllerName);
                cmd.Parameters.AddWithValue("@Packagename", packageName);
                //cmd.Parameters.AddWithValue("@formattedParameter",(string.IsNullOrEmpty( formattedParameter)?null: formattedParameter)??DBNull.Value as object);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    AppUrl obj = ConvertObj(dt)[0];
                    return obj;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        private List<AppUrl> ConvertObj(DataTable dt)
        {
            return dt.AsEnumerable().Select(t => {
                AppUrl ap = new AppUrl();
                ap.ActionName = t.Field<string>("ActionName");
                ap.ControllerName = t.Field<string>("ControllerName");
                ap.ControllerPackage = t.Field<string>("ControllerPackage");
                ap.IsAnonymousUrl = (t.Field<bool>("IsAnonymous"));
                ap.IsSharedUrl = t.Field<bool>("IsShared");
                ap.UrlID = t.Field<int>("ActionID");
                ap.ParamExpression = t.Field<string>("QueryStringValue");
                return ap;
            }).ToList();
        }
    }
}
