﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL
{
    public class LoanMaster:BllBase
    {
        public List<LoanMaster_MD> Get_LoanDescType()
        {
            SqlCommand cmd = new SqlCommand("Get_LoanDescType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Post_Loandesc(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LoanMaster_MD> Get_LoanDesc(string LoanType)
        {
            SqlCommand cmd = new SqlCommand("Get_LoanDesc", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LoanType", LoanType);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Loandesc_Data(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LoanMasterVM> Get_EmpNamebySec_Autocomplete(string secID, int? UserType, string Empname, int EmpId)
        {
            SqlCommand cmd = new SqlCommand("Get_EmployeebySectorandUserType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SecID", secID.Equals("") ? DBNull.Value : (object)secID);
            cmd.Parameters.AddWithValue("@UserType", UserType);
            cmd.Parameters.AddWithValue("@EmpName", Empname);
            cmd.Parameters.AddWithValue("@EmpId", EmpId);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return EmpDetails_Autocomplete(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LoanMasterVM> Get_LoanDedDate(string SecID, string SalFinYear, string LoanDeducDT)
        {
            SqlCommand cmd = new SqlCommand("Get_loanDate", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SecID", SecID.Equals("") ? DBNull.Value : (object)SecID);
            cmd.Parameters.AddWithValue("@SalFinYear", SalFinYear);
            cmd.Parameters.AddWithValue("@LoanSTDT", LoanDeducDT);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Loan_Deduction_Date(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LoanMasterVM> Get_LoanSancDate(int? EmpId, string Loandeddt, string Loansndt)
        {
            SqlCommand cmd = new SqlCommand("Get_loansancdt", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpId", EmpId.Equals("") ? DBNull.Value : (object)EmpId);
            cmd.Parameters.AddWithValue("@Loandeddt", Loandeddt.Equals("") ? DBNull.Value : (object)Loandeddt);
            cmd.Parameters.AddWithValue("@Loansndt", Loansndt.Equals("") ? DBNull.Value : (object)Loansndt);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return SaveData(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LoanMasterVM> Get_LoanInstallment(int? EmpId, int? LoanTypeID, int? LeftInst, int? TotalLoanInst, string LoanAmt)
        {
            SqlCommand cmd = new SqlCommand("Get_EmployeeInstNo", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpId", EmpId.Equals("") ? DBNull.Value : (object)EmpId);
            cmd.Parameters.AddWithValue("@Loantpid", LoanTypeID.Equals("") ? DBNull.Value : (object)LoanTypeID);
            cmd.Parameters.AddWithValue("@retmonth", LeftInst.Equals("") ? DBNull.Value : (object)LeftInst);
            cmd.Parameters.AddWithValue("@instno", TotalLoanInst.Equals("") ? DBNull.Value : (object)TotalLoanInst);
            cmd.Parameters.AddWithValue("@loanamt", LoanAmt.Equals("") ? DBNull.Value : (object)LoanAmt);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Get_LoanInstallmentList(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LoanMasterVM> Get_DisburseAmt(int? EmpId, int? Loantpid, string LoanAmt, string DisbAmt)
        {
            SqlCommand cmd = new SqlCommand("Get_DisbAmt", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpId", EmpId.Equals("") ? DBNull.Value : (object)EmpId);
            cmd.Parameters.AddWithValue("@Loantpid", Loantpid.Equals("") ? DBNull.Value : (object)Loantpid);
            cmd.Parameters.AddWithValue("@LoanAmt", LoanAmt.Equals("") ? DBNull.Value : (object)LoanAmt);
            cmd.Parameters.AddWithValue("@DisbAmt", DisbAmt.Equals("") ? DBNull.Value : (object)DisbAmt);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return SaveData(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LoanMasterVM> Save_Loan(string LoanTypeID, string EmployeeID, string LoanAmount, string LoanDeducStartDate, string TotLoanInstall
        , string CurLoanInstall, string LoanAmountPaid, string CloseInd, string AdjustAmount, string AdjustDate, string LoanSancDate, string ReferenceNo, string ReferenceDate
        , string DisburseAmt, string SancOrderNo, string inst_no1, string inst_amt1, string inst_no2, string inst_amt2, string inst_no3, string inst_amt3, int UserID
        , string SecID, string SalFinYear, string retmonth)
        {


            SqlCommand cmd = new SqlCommand("check_loan_mast", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@LoanTypeID", LoanTypeID.Equals("") ? DBNull.Value : (object)LoanTypeID);
            //cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID.Equals("") ? DBNull.Value : (object)EmployeeID);
            //cmd.Parameters.AddWithValue("@LoanAmount", LoanAmount.Equals("") ? DBNull.Value : (object)LoanAmount);
            //cmd.Parameters.AddWithValue("@DisburseAmt", DisburseAmt.Equals("") ? DBNull.Value : (object)DisburseAmt);
            //cmd.Parameters.AddWithValue("@LoanDeducStartDate", LoanDeducStartDate.Equals("") ? DBNull.Value : (object)LoanDeducStartDate);
            //cmd.Parameters.AddWithValue("@TotLoanInstall", TotLoanInstall.Equals("") ? DBNull.Value : (object)TotLoanInstall);
            //cmd.Parameters.AddWithValue("@CurLoanInstall", CurLoanInstall.Equals("") ? DBNull.Value : (object)CurLoanInstall);
            //cmd.Parameters.AddWithValue("@LoanAmountPaid", LoanAmountPaid.Equals("") ? DBNull.Value : (object)LoanAmountPaid);
            //cmd.Parameters.AddWithValue("@CloseInd", CloseInd.Equals("") ? DBNull.Value : (object)CloseInd);
            //cmd.Parameters.AddWithValue("@AdjustAmount", AdjustAmount.Equals("") ? DBNull.Value : (object)AdjustAmount);
            //cmd.Parameters.AddWithValue("@AdjustDate", AdjustDate.Equals("") ? DBNull.Value : (object)AdjustDate);
            //cmd.Parameters.AddWithValue("@LoanSancDate", LoanSancDate.Equals("") ? DBNull.Value : (object)LoanSancDate);
            //cmd.Parameters.AddWithValue("@ReferenceNo", ReferenceNo.Equals("") ? DBNull.Value : (object)ReferenceNo);
            //cmd.Parameters.AddWithValue("@ReferenceDate", ReferenceDate.Equals("") ? DBNull.Value : (object)ReferenceDate);
            //cmd.Parameters.AddWithValue("@SancOrderNo", SancOrderNo.Equals("") ? DBNull.Value : (object)SancOrderNo);
            //cmd.Parameters.AddWithValue("@inst_no1", inst_no1.Equals("") ? DBNull.Value : (object)inst_no1);
            //cmd.Parameters.AddWithValue("@inst_amt1", inst_amt1.Equals("") ? DBNull.Value : (object)inst_amt1);
            //cmd.Parameters.AddWithValue("@inst_no2", inst_no2.Equals("") ? DBNull.Value : (object)inst_no2);
            //cmd.Parameters.AddWithValue("@inst_amt2", inst_amt2.Equals("") ? DBNull.Value : (object)inst_amt2);
            //cmd.Parameters.AddWithValue("@inst_no3", inst_no3.Equals("") ? DBNull.Value : (object)inst_no3);
            //cmd.Parameters.AddWithValue("@inst_amt3", inst_amt3.Equals("") ? DBNull.Value : (object)inst_amt3);
            //cmd.Parameters.AddWithValue("@InsertedBy", UserID);

            cmd.Parameters.AddWithValue("@LoanTypeID", LoanTypeID.Equals("") ? DBNull.Value : (object)LoanTypeID);
            cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID.Equals("") ? DBNull.Value : (object)EmployeeID);
            cmd.Parameters.AddWithValue("@LoanAmount", LoanAmount.Equals("") ? DBNull.Value : (object)LoanAmount);
            cmd.Parameters.AddWithValue("@LoanDeducStartDate", LoanDeducStartDate.Equals("") ? DBNull.Value : (object)LoanDeducStartDate);
            cmd.Parameters.AddWithValue("@TotLoanInstall", TotLoanInstall.Equals("") ? DBNull.Value : (object)TotLoanInstall);
            cmd.Parameters.AddWithValue("@CurLoanInstall", CurLoanInstall.Equals("") ? DBNull.Value : (object)CurLoanInstall);
            cmd.Parameters.AddWithValue("@LoanAmountPaid", LoanAmountPaid.Equals("") ? DBNull.Value : (object)LoanAmountPaid);
            cmd.Parameters.AddWithValue("@AdjustDate", AdjustDate.Equals("") ? DBNull.Value : (object)AdjustDate);
            cmd.Parameters.AddWithValue("@AdjustAmount", AdjustAmount.Equals("") ? DBNull.Value : (object)AdjustAmount);
            cmd.Parameters.AddWithValue("@LoanSancDate", LoanSancDate.Equals("") ? DBNull.Value : (object)LoanSancDate);
            cmd.Parameters.AddWithValue("@ReferenceNo", ReferenceNo.Equals("") ? DBNull.Value : (object)ReferenceNo);
            cmd.Parameters.AddWithValue("@ReferenceDate", ReferenceDate.Equals("") ? DBNull.Value : (object)ReferenceDate);
            cmd.Parameters.AddWithValue("@DisburseAmt", DisburseAmt.Equals("") ? DBNull.Value : (object)DisburseAmt);
            cmd.Parameters.AddWithValue("@SancOrderNo", SancOrderNo.Equals("") ? DBNull.Value : (object)SancOrderNo);
            cmd.Parameters.AddWithValue("@inst_no1", inst_no1.Equals("") ? DBNull.Value : (object)inst_no1);
            cmd.Parameters.AddWithValue("@inst_amt1", inst_amt1.Equals("") ? DBNull.Value : (object)inst_amt1);
            cmd.Parameters.AddWithValue("@inst_no2", inst_no2.Equals("") ? DBNull.Value : (object)inst_no2);
            cmd.Parameters.AddWithValue("@inst_amt2", inst_amt2.Equals("") ? DBNull.Value : (object)inst_amt2);
            cmd.Parameters.AddWithValue("@inst_no3", inst_no3.Equals("") ? DBNull.Value : (object)inst_no3);
            cmd.Parameters.AddWithValue("@inst_amt3", inst_amt3.Equals("") ? DBNull.Value : (object)inst_amt3);
            cmd.Parameters.AddWithValue("@CloseInd", CloseInd.Equals("") ? DBNull.Value : (object)CloseInd);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            cmd.Parameters.AddWithValue("@SecID", SecID.Equals("") ? DBNull.Value : (object)SecID);
            cmd.Parameters.AddWithValue("@SalFinYear", SalFinYear.Equals("") ? DBNull.Value : (object)SalFinYear);
            cmd.Parameters.AddWithValue("@retmonth", retmonth.Equals("") ? DBNull.Value : (object)retmonth);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return SaveData(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LoanMasterVM> Search_Employee_Autocomplete(string secID, int? UserType, string Empname, int EmpId)
        {
            SqlCommand cmd = new SqlCommand("GetEmpSrch", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SecID", secID.Equals("") ? DBNull.Value : (object)secID);
            cmd.Parameters.AddWithValue("@UserType", UserType);
            cmd.Parameters.AddWithValue("@EmpName", Empname);
            cmd.Parameters.AddWithValue("@EmpId", EmpId.Equals("") ? 0 : (object)EmpId);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return AllEmpDetails_Autocomplete(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LoanMasterVM> Get_FieldName(int FormID)
        {
            SqlCommand cmd = new SqlCommand("Check_Mand", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ConfID", FormID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Get_FormDATA(dt);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public List<LoanMasterVM> Edit_Loan(int EmpId, int LoanID)
        {
            SqlCommand cmd = new SqlCommand("GetEmpEdit", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpId", EmpId);
            cmd.Parameters.AddWithValue("@LoanID", LoanID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Edit_Loan(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LoanMasterVM> Update_Loan(int EmpId, int LoanID, string Remarks, string CloseInd, int UserID)
        {
            SqlCommand cmd = new SqlCommand("GetLoanUpdt", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpId", EmpId);
            cmd.Parameters.AddWithValue("@LoanID", LoanID);
            cmd.Parameters.AddWithValue("@Reason", Remarks);
            cmd.Parameters.AddWithValue("@CloseInd", CloseInd);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return SaveData(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }









        List<LoanMaster_MD> Post_Loandesc(DataTable dt)
        {
            List<LoanMaster_MD> lst = new List<LoanMaster_MD>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new LoanMaster_MD();
                obj.LoanTypeID = s.Field<int>("LoanTypeID");
                obj.LoanDesc = s.Field<string>("LoanDesc");
                return obj;
            }).ToList();
            return lst;
        }

        List<LoanMaster_MD> Loandesc_Data(DataTable dt)
        {
            List<LoanMaster_MD> lst = new List<LoanMaster_MD>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new LoanMaster_MD();
                obj.LoanTypeID = s.Field<int>("LoanTypeID");
                obj.LoanDesc = s.Field<string>("LoanDesc");
                obj.IsRound = s.Field<int>("IsRound");
                return obj;
            }).ToList();
            return lst;
        }

        List<LoanMasterVM> EmpDetails_Autocomplete(DataTable dt)
        {
            List<LoanMasterVM> lst = new List<LoanMasterVM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new LoanMasterVM();
                obj.EmployeeID = t.Field<int?>("EmployeeID");
                obj.EmpNo = t.Field<string>("EmpNo");
                obj.EmpName = t.Field<string>("EmpName");
                obj.SectorID = t.Field<int?>("SectorID");
                obj.LeftMonth = t.Field<int?>("LeftMonth");
                obj.Messege = t.Field<string>("Messege");
                return obj;
            }).ToList();
            return lst;
        }

        List<LoanMasterVM> Loan_Deduction_Date(DataTable dt)
        {
            List<LoanMasterVM> lst = new List<LoanMasterVM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new LoanMasterVM();
                obj.LoanDeducDate = t.Field<string>("loanDedDt");
                return obj;
            }).ToList();
            return lst;
        }


        List<LoanMasterVM> Get_LoanInstallmentList(DataTable dt)
        {
            List<LoanMasterVM> lst = new List<LoanMasterVM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new LoanMasterVM();
                obj.Install1 = t.Field<int?>("Inst1");
                obj.InstallAmt1 = t.Field<decimal?>("InstAmt1").ToString();
                obj.Install2 = t.Field<int?>("Inst2");
                obj.InstallAmt2 = t.Field<decimal?>("InstAmt2").ToString();
                obj.Messege = t.Field<string>("Messege");
                return obj;
            }).ToList();
            return lst;
        }

        List<LoanMasterVM> SaveData(DataTable dt)
        {
            List<LoanMasterVM> lst = new List<LoanMasterVM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new LoanMasterVM();
                obj.Messege = s.Field<string>("Messege");
                obj.ErrorCode = s.Field<int?>("ErrorCode");
                return obj;
            }).ToList();
            return lst;
        }

        List<LoanMasterVM> AllEmpDetails_Autocomplete(DataTable dt)
        {
            List<LoanMasterVM> lst = new List<LoanMasterVM>();
            lst = dt.AsEnumerable().Select(s =>
                {
                    var obj = new LoanMasterVM();
                    obj.EmployeeID = s.Field<int>("EmployeeID");
                    obj.EmpName = s.Field<string>("EmpName");
                    obj.LoanTypeID = s.Field<int?>("LoanTypeID");
                    obj.LoanType = s.Field<string>("LoanType");
                    obj.LoanDeducDate = s.Field<string>("LoanDeducStartDate");
                    obj.LoanAmount = s.Field<decimal?>("LoanAmount").ToString();
                    obj.LoanDesc = s.Field<string>("LoanDesc");
                    obj.TotLoanInstall = s.Field<int?>("TotLoanInstall");
                    obj.CurLoanInstall = s.Field<int?>("CurLoanInstall");
                    obj.LoanAmountPaid = s.Field<decimal?>("LoanAmountPaid").ToString();
                    obj.LoanID = s.Field<int?>("LoanID");
                    obj.SectorID = s.Field<int?>("SectorID");
                    obj.InstallAmt1 = s.Field<decimal?>("inst_amt1").ToString();
                    obj.InstallAmt2 = s.Field<decimal?>("inst_amt2").ToString();
                    obj.InstallAmt3 = s.Field<decimal?>("inst_amt3").ToString();

                    //obj.LoanSancDate = s.Field<string>("LoanSancDate");
                    //obj.LoanTypeID = s.Field<int>("LoanTypeID");
                    //obj.LoanDescID = s.Field<int>("LoanDescID");
                    //obj.LoanDeducDate = s.Field<string>("LoanDeducStartDate");
                    //obj.TotLoanInstall = s.Field<int>("TotLoanInstall");
                    //obj.CurLoanInstall = s.Field<int>("CurLoanInstall");
                    //obj.LoanAmountPaid = s.Field<decimal?>("LoanAmountPaid");
                    //obj.AdjustDate = s.Field<string>("AdjustDate");
                    //obj.AdjustAmount = s.Field<decimal?>("AdjustAmount");
                    //obj.ReferenceNo = s.Field<string>("ReferenceNo");
                    //obj.Referencedate = s.Field<string>("Referencedate");
                    //obj.DisburseAmt = s.Field<decimal?>("DisburseAmt");
                    //obj.SancOrderNo = s.Field<string>("SancOrderNo");
                    //obj.Install1 = s.Field<int?>("inst_no1");
                    //obj.InstallAmt1 = s.Field<string>("inst_amt1");
                    //obj.Install2 = s.Field<int?>("inst_no2");
                    //obj.InstallAmt2 = s.Field<string>("inst_amt2");
                    //obj.Install3 = s.Field<int?>("inst_no3");
                    //obj.InstallAmt3 = s.Field<string>("inst_amt3");
                    return obj;
                }).ToList();
            return lst;
        }

        //select a.EmployeeID, a.EmpNo, a.EmpName + '' + '(' + a.EmpNO + ')'  EmpName,c.LoanTypeID,c.LoanDesc LoanType,
        //convert(varchar(10),b.LoanDeducStartDate,103) LoanDeducStartDate,b.LoanAmount,'('+d.LoanAbbv+')-'+d.LoanDesc LoanDesc,b.TotLoanInstall,
        //b.CurLoanInstall,b.LoanAmountPaid,convert(varchar(10),b.AdjustDate,103) AdjustDate,b.AdjustAmount, convert(varchar(10),b.LoanSancDate,103) LoanSancDate,b.ReferenceNo,
        //convert(varchar(10),b.Referencedate,103) Referencedate,b.DisburseAmt,b.SancOrderNo,b.inst_no1,b.inst_amt1,
        //isnull(b.inst_no2,0) inst_no2,isnull(b.inst_amt2,0) inst_amt2,
        //isnull(b.inst_no3,0) inst_no3,isnull(b.inst_amt3,0) inst_amt3,b.LoanID,a.SectorID

        List<LoanMasterVM> Get_FormDATA(DataTable dt)
        {
            List<LoanMasterVM> lst = new List<LoanMasterVM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new LoanMasterVM();
                obj.FieldID = s.Field<int>("FieldID");
                obj.FieldName = s.Field<string>("FieldName");
                obj.IsMand = s.Field<int>("IsMand");
                return obj;
            }).ToList();
            return lst;
        }

        List<LoanMasterVM> Edit_Loan(DataTable dt)
        {
            List<LoanMasterVM> lst = new List<LoanMasterVM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new LoanMasterVM();
                obj.EmployeeID = s.Field<int>("EmployeeID");
                obj.EmpName = s.Field<string>("EmpName");
                obj.LoanTypeID = s.Field<int?>("LoanTypeID");
                obj.LoanDeducDate = s.Field<string>("LoanDeducStartDate");
                obj.LoanAmount = s.Field<decimal?>("LoanAmount").ToString();
                obj.LoanDescID = s.Field<int?>("LoanDescID");
                obj.TotLoanInstall = s.Field<int?>("TotLoanInstall");
                obj.LeftMonth = s.Field<int?>("LeftMonth");
                obj.CurLoanInstall = s.Field<int?>("CurLoanInstall");
                obj.LoanAmountPaid = s.Field<decimal?>("LoanAmountPaid").ToString();
                obj.AdjustDate = s.Field<string>("AdjustDate");
                obj.AdjustAmount = s.Field<decimal?>("AdjustAmount").ToString();
                obj.LoanSancDate = s.Field<string>("LoanSancDate");
                obj.ReferenceNo = s.Field<string>("ReferenceNo");
                obj.Referencedate = s.Field<string>("ReferenceDate");
                obj.DisburseAmt = s.Field<decimal?>("DisburseAmt").ToString();
                obj.SancOrderNo = s.Field<string>("SancOrderNo");
                obj.LoanID = s.Field<int?>("LoanID");
                obj.SectorID = s.Field<int?>("SectorID");
                obj.Install1 = s.Field<int?>("inst_no1");
                obj.InstallAmt1 = s.Field<decimal?>("inst_amt1").ToString();
                obj.Install2 = s.Field<int?>("inst_no2");
                obj.InstallAmt2 = s.Field<decimal?>("inst_amt2").ToString();
                obj.Install3 = s.Field<int?>("inst_no3");
                obj.InstallAmt3 = s.Field<decimal?>("inst_amt3").ToString();
                return obj;
            }).ToList();
            return lst;
        }



    }//END
}