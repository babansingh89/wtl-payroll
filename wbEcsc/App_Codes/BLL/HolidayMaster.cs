﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL
{
    public class HolidayMaster:BllBase
    {


        internal List<HolidayMaster_MD> SaveUpdate_HolidayMaster(int HolidayID, string CalendarYear, string HolidayType, string FromDate, string ToDate, string Description, int SectorId, int UserID)
        {
            SqlCommand cmd = new SqlCommand("check_holiday_mast", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HdNo", HolidayID.ToString().Equals("") ? 0 : (object)HolidayID);
            cmd.Parameters.AddWithValue("@HType", HolidayType.Equals("") ? DBNull.Value : (object)HolidayType);
            cmd.Parameters.AddWithValue("@FromHDate", FromDate.Equals("") ? DBNull.Value : (object)FromDate);
            cmd.Parameters.AddWithValue("@ToHDate", ToDate.Equals("") ? DBNull.Value : (object)ToDate);
            cmd.Parameters.AddWithValue("@HDesc", Description.Equals("") ? DBNull.Value : (object)Description);
            cmd.Parameters.AddWithValue("@SectorID", SectorId.Equals("") ? 0 : (object)SectorId);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID.Equals("") ? 0 : (object)UserID);
            cmd.Parameters.AddWithValue("@Year", CalendarYear.Equals("") ? DBNull.Value : (object)CalendarYear);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return ThrowMessege(dt);
            }

            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

       

        List<HolidayMaster_MD> ThrowMessege(DataTable dt)
        {
            List<HolidayMaster_MD> lst = new List<HolidayMaster_MD>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new HolidayMaster_MD();
                obj.ErrorCode = s.Field<int?>("ErrorCode");
                obj.Messege = s.Field<string>("Messege");
                return obj;
            }).ToList();
            return lst;
        }

        internal List<HolidayMaster_MD> Delete_HolidayMaster(int hdnHolidayID, int SectorId)
        {
            SqlCommand cmd = new SqlCommand("Delete_HolidayMaster", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HdNo", hdnHolidayID.Equals("") ? 0 : (object)hdnHolidayID);
            cmd.Parameters.AddWithValue("@SectorID", SectorId.Equals("") ? 0 : (object)SectorId);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return ThrowMessege(dt);
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal DataTable Get_GridDATA(string hdnHolidayID)
        {
            SqlCommand cmd = new SqlCommand("Load_MST_Holiday", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HolidayID", hdnHolidayID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        internal DataSet Get_CalendarYearDATA(string txtYear, int SectorId)
        {
            SqlCommand cmd = new SqlCommand("Load_currentYear", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Year", txtYear);
            cmd.Parameters.AddWithValue("@SectorID", SectorId);
            cmd.Parameters.AddWithValue("@ConfID", 21);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        internal DataTable Get_MaxSalDt()
        {
            SqlCommand cmd = new SqlCommand("Get_MaxSalDt", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<LeaveMaster_MD> Get_FieldName(int FormID)
        {
            SqlCommand cmd = new SqlCommand("Check_Mand", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ConfID", FormID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Get_FormDATA(dt);
            }
            catch (Exception)
            {

                throw;
            }
        }

        List<LeaveMaster_MD> Get_FormDATA(DataTable dt)
        {
            List<LeaveMaster_MD> lst = new List<LeaveMaster_MD>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new LeaveMaster_MD();
                obj.FieldId = s.Field<int>("FieldID");
                obj.FieldName = s.Field<string>("FieldName");
                obj.IsMand = s.Field<int>("IsMand");
                return obj;
            }).ToList();
            return lst;
        }
    }
}