﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL
{
    public class District:BllBase
    {
        public List<District_MD> Get_District(int StateID)
        {

            SqlCommand cmd = new SqlCommand("Load_District", cn);
            cmd.Parameters.AddWithValue("@StateID", StateID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        #region Converter
        List<District_MD> Convert(DataTable dt)
        {
            List<District_MD> lst = new List<District_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new District_MD();
                obj.DistID = t.Field<int?>("DistID");
                obj.District = t.Field<string>("District");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(District v)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}