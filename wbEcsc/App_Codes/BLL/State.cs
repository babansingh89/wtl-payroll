﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL
{
    public class State:BllBase
    {
        public List<State_MD> Get_State()
        {

            SqlCommand cmd = new SqlCommand("Load_State", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }



        #region Converter
        List<State_MD> Convert(DataTable dt)
        {
            List<State_MD> lst = new List<State_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new State_MD();
                obj.StateId = t.Field<int?>("StateId");
                obj.State = t.Field<string>("State");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(State v)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}