﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Data;
using System.Data.SqlClient;
using wbEcsc.Models.Application;
using System.Text;
using NPOI.HSSF.UserModel;
using System.IO;

namespace wbEcsc.App_Codes.BLL.AppReport
{
    public  class DynamicReport:BllBase
    {
        public List<ReportBuilder_MD> Load_DynamicReport()
        {
            SqlCommand cmd = new SqlCommand("Load_ReportName", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_DynamicReportList(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public string Save_Header(string reportName, string reportHeader, string reportQuery, string ParameterTypes, long UserID)
        {
            int reportID = 0;
            SqlCommand cmd = new SqlCommand("Insert_DynamicReportHeader", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@reportName", reportName);
            cmd.Parameters.AddWithValue("@reportHeader", reportHeader);
            cmd.Parameters.AddWithValue("@reportQuery", reportQuery);
            cmd.Parameters.AddWithValue("@ParameterTypes", ParameterTypes);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    reportID = Convert.ToInt32(dt.Rows[0]["ReportID"].ToString());
                }
                if (reportID > 0)
                    return "success";
                else
                    return "fail";
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        List<ReportBuilder_MD> Convert_DynamicReportList(DataTable dt)
        {
            List<ReportBuilder_MD> lst = new List<ReportBuilder_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new ReportBuilder_MD();
                obj.ReportID = t.Field<int?>("ReportID");
                obj.ReportHeader = t.Field<string>("ReportHeader");
                return obj;
            }).ToList();
            return lst;
        }

       

       
    }



}