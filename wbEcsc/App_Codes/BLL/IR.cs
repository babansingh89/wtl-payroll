﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL
{
    public class IR:BllBase
    {
        public List<IR_MD> Bind_IR(string EmpType)
        {

            SqlCommand cmd = new SqlCommand("Load_IR", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpType", EmpType);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }



        #region Converter
        List<IR_MD> Convert(DataTable dt)
        {
            List<IR_MD> lst = new List<IR_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new IR_MD();
                obj.IRID = t.Field<int?>("IRID");
                obj.IRRate = t.Field<decimal>("IRRate");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(IR v)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}