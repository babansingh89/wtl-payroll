﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL
{
    public class Qualification:BllBase                                                                          
    {
        Qualification_MD objPayScale = new Qualification_MD();
        SessionData sds = new SessionData();
        public DataTable Save(Qualification_MD objqu)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("check_qualification_mast", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@QualificationID", objqu.QualificationID ?? 0);
                cmd.Parameters.AddWithValue("@Qualification", objqu.Qualification ?? (object)DBNull.Value);               
                cmd.Parameters.AddWithValue("@InsertedBy", sds.UserID);
                cn.Open();
                SqlDataAdapter ada = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                ada.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Delete(Qualification_MD objqu)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("DELETE_QualificationbyID", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", objqu.QualificationID ?? 0);       
                cn.Open();
                SqlDataAdapter ada = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                ada.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Qualification_MD> Get_Qualification()
        {
            SqlCommand cmd = new SqlCommand("Load_Qualification", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        #region Converter
        List<Qualification_MD> Convert(DataTable dt)
        {
            List<Qualification_MD> lst = new List<Qualification_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Qualification_MD();
                obj.QualificationID = t.Field<int?>("QualificationID");
                obj.Qualification = t.Field<string>("Qualification");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(Qualification v)
        {
            throw new NotImplementedException();
        }
        #endregion

        public List<Field> GetField()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Check_Mand", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfID", 10);
                cn.Open();
                SqlDataAdapter ada = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                ada.Fill(dt);
                return Convert_ListField(dt);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Field> Convert_ListField(DataTable dt)
        {
            try
            {
                List<Field> lstField = new List<Field>();
                lstField = dt.AsEnumerable().Select(t => {
                    var objfield = new Field();
                    objfield.FieldID = t.Field<int?>("FieldID");
                    objfield.FieldName = t.Field<string>("FieldName");
                    objfield.IsMand = t.Field<int>("IsMand");
                    return objfield;
                }).ToList();
                return lstField;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}