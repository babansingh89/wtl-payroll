﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL
{
    public class Religion: BllBase
    {

            public List<Religion_MD> Get_Religion()
            {

                SqlCommand cmd = new SqlCommand("Load_Religion", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                try
                {
                    cn.Open();
                    da.Fill(dt);
                    return Convert(dt);
                }
                catch (SqlException)
                {
                    throw new Exception("Database error");
                }
                finally
                {
                    cn.Close();
                }
            }



            #region Converter
            List<Religion_MD> Convert(DataTable dt)
            {
                List<Religion_MD> lst = new List<Religion_MD>();
                lst = dt.AsEnumerable().Select(t =>
                {
                    var obj = new Religion_MD();
                    obj.ReligionID = t.Field<int?>("ReligionID");
                    obj.ReligionCode = t.Field<string>("ReligionCode");
                    obj.Religion = t.Field<string>("Religion");
                    return obj;
                }).ToList();
                return lst;
            }

            public static implicit operator List<object>(Religion v)
            {
                throw new NotImplementedException();
            }
            #endregion
        }
    
}