﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL
{
    public class Bank:BllBase
    {
        public List<Field>  GetField()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Check_Mand", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfID", 2);
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return Convert_Field(dt);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally {
                cn.Close();
            }
        }
        public DataTable SaveData( Bank_MD bnk)
        {
            try
            {
                    SessionData sdt = new SessionData();                                 
                    SqlCommand cmd = new SqlCommand("check_bank_mast", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BankID", bnk.BankID ?? 0);
                    cmd.Parameters.AddWithValue("@BankName", bnk.BankName ?? DBNull.Value as object);
                    cmd.Parameters.AddWithValue("@InsertedBy", sdt.UserID);
                    cn.Open();
                    SqlDataAdapter sdp=new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                   sdp.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public Bank_MD GetBankByID(int id)
        {
            SqlCommand cmd = new SqlCommand("Get_BankByID", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BankID", id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                Bank_MD bnk_md = new Bank_MD() {BankID=(Int32)dt.Rows[0]["BankID"],BankName=dt.Rows[0]["BankName"].ToString() };
                return bnk_md;
            }
            catch (Exception ex)
            {
               
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }       
        }

        public List<Bank_MD> Get_Bank()
        {
            SqlCommand cmd = new SqlCommand("Load_Bank", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Field> Convert_Field(DataTable dt)
        {
            try
            {
                List<Field> lstField = new List<Field>();              

                lstField = dt.AsEnumerable().Select(t =>
                {
                    var lstf = new Field();
                    lstf.FieldID = t.Field<int?>("FieldID");
                    lstf.FieldName = t.Field<string>("FieldName");
                    lstf.IsMand = t.Field<int>("IsMand");
                    return lstf;
                }).ToList();

                return lstField;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region Converter
        List<Bank_MD> Convert(DataTable dt)
        {
            List<Bank_MD> lst = new List<Bank_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Bank_MD();
                obj.BankID = t.Field<int?>("BankID");
                obj.BankName = t.Field<string>("BankName");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(Bank v)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}