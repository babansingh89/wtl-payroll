﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL
{
    public class EarnDeduction:BllBase
    {
        public List<EarnDeduction_MD> Get_EarnDeduction(string EmpCategory, string StatusID)
        {

            SqlCommand cmd = new SqlCommand("Get_EarnDeduction", cn);
            cmd.Parameters.AddWithValue("@empCategory", EmpCategory);
            cmd.Parameters.AddWithValue("@empStatus", StatusID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        #region Converter
        List<EarnDeduction_MD> Convert(DataTable dt)
        {
            List<EarnDeduction_MD> lst = new List<EarnDeduction_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new EarnDeduction_MD();
                obj.EDID = t.Field<string>("EDID");
                obj.EarnDeduction = t.Field<string>("EarnDeduction");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(EarnDeduction v)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}