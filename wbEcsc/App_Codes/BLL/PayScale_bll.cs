﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;


namespace wbEcsc.App_Codes.BLL
{
    public class PayScale_bll:BllBase
    {
        PayScale_MD objPayScale = new PayScale_MD();
        SessionData sds = new SessionData();
        public PayScale_bll()
        {
            sds = SessionContext.SessionData;
        }

        public DataTable Save(PayScale_MD objPS)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("check_pay_scale", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PayScaleID", objPS.PayScaleID ?? 0);
                cmd.Parameters.AddWithValue("@PayScale", objPS.PayScale ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@EmpType", objPS.EmpTypeID ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Incre", objPS.IncrementPercentage ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@GradePay", objPS.GradePay ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@InsertedBy", sds.UserID);
                cn.Open();
                SqlDataAdapter ada = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                ada.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        internal DataSet GetPayScaleByEMPType(string EMPtype)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Get_PaybyEmpType", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmpType", EMPtype.Equals("")?DBNull.Value:(object)EMPtype);
                cn.Open();
                SqlDataAdapter sds = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                sds.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Field> GetField()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Check_Mand", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfID", 7);
                cn.Open();
                SqlDataAdapter ada = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                ada.Fill(dt);               
                return Convert_ListField(dt);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Field> Convert_ListField(DataTable dt)
        {
            try
            {
                List<Field> lstField = new List<Field>();
                lstField = dt.AsEnumerable().Select(t=> {
                    var objfield = new Field();
                    objfield.FieldID = t.Field<int?>("FieldID");
                    objfield.FieldName = t.Field<string>("FieldName");
                    objfield.IsMand = t.Field<int>("IsMand");
                    return objfield;
                }).ToList();
                return lstField;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}