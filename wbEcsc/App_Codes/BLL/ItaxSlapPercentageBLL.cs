﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using wbEcsc.Models.Application;
using System.Configuration;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL
{
    public class ItaxSlapPercentageBLL:BllBase
    {     
        SqlCommand cmd;
        SessionData sds;
        public ItaxSlapPercentageBLL()
        {
            sds = SessionContext.SessionData;         
        }
        public DataTable Save(ItaxSlapPercentage_MD ima)
        {
            try
            {
                cmd = new SqlCommand("check_itaxrateslab_mast", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RateSlabID", ima.RateSlabID ?? 0);
                cmd.Parameters.AddWithValue("@FinYear", ima.FinYear ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Gender", ima.GenderID ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@AddAmount", ima.AddAmount ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@AmountFrom", ima.AmountFrom ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@AmountTo", ima.AmountTo ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Percentage", ima.Percentage ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@InsertedBy", sds.UserID);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Delete(string RateSlabID)
        {
            try
            {
                cmd = new SqlCommand("Delete_itaxrateslab_mast", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RateSlabID", RateSlabID);
                cmd.Parameters.AddWithValue("@CanBy", sds.UserID);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Field> GetField()
        {
            try
            {
                cmd = new SqlCommand("Check_Mand", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfID", 17);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return Converter_Field(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }       

        public List<ItaxSlapPercentage_MD> GetData(string finyear)
        {
            try
            {
                cmd = new SqlCommand("Load_itaxrateslab_mast", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FinYear", finyear);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return Converter_Data(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Field> Converter_Field(DataTable dt)
        {
            try
            {
                List<Field> lst = new List<Field>();
                lst = dt.AsEnumerable().Select(t =>
                {
                    var obj = new Field()
                    {
                        FieldID = t.Field<int>("FieldID"),
                        FieldName = t.Field<string>("FieldName"),
                        IsMand = t.Field<int>("IsMand")
                    };
                    return obj;
                }
                ).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }  

        public List<ItaxSlapPercentage_MD> Converter_Data(DataTable dt)
        {
            try
            {
                List<ItaxSlapPercentage_MD> lst = new List<ItaxSlapPercentage_MD>();
                lst = dt.AsEnumerable().Select(t => {
                    var obj = new ItaxSlapPercentage_MD()
                    {
                        RateSlabID = t.Field<int?>("RateSlabID"),
                        FinYear = t.Field<string>("FinYear"),
                        GenderID = t.Field<string>("Gender"),
                        Gender = t.Field<string>("GenderDesc"),
                        AddAmount = t.Field<decimal?>("AddAmount"),
                        AmountFrom = t.Field<decimal?>("AmountFrom"),
                        AmountTo = t.Field<decimal?>("AmountTo"),
                        Percentage = t.Field<decimal?>("Percentage")
                    };
                    return obj;
                }).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}