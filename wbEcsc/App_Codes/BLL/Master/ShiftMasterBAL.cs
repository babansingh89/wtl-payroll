﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL.Master
{
    public class ShiftMasterBAL : BllBase
    {
        public DataSet InsertUpdateShiftMaster_DAL(ShiftMasterMD ShiftMasterMD)
        {
            SqlCommand cmd = new SqlCommand("MST_HrShift_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", ShiftMasterMD.TransactionType);
            cmd.Parameters.AddWithValue("@ShiftId", ShiftMasterMD.ShiftId);
            cmd.Parameters.AddWithValue("@ShiftDesc", ShiftMasterMD.ShiftDesc);
            cmd.Parameters.AddWithValue("@InTime", ShiftMasterMD.InTime);
            cmd.Parameters.AddWithValue("@OutTime", ShiftMasterMD.OutTime);
            cmd.Parameters.AddWithValue("@GraceIn", ShiftMasterMD.GraceIn);
            cmd.Parameters.AddWithValue("@GraceOut", ShiftMasterMD.GraceOut);
            cmd.Parameters.AddWithValue("@MinHalfDayHrs", ShiftMasterMD.MinHalfDayHrs);
            cmd.Parameters.AddWithValue("@MinFullDayHrs", ShiftMasterMD.MinFullDayHrs);
            cmd.Parameters.AddWithValue("@Wef", ShiftMasterMD.Wef);
            cmd.Parameters.AddWithValue("@ActiveFlag", ShiftMasterMD.ActiveFlag);
            cmd.Parameters.AddWithValue("@NShift", ShiftMasterMD.NShift);
            cmd.Parameters.AddWithValue("@UserID", ShiftMasterMD.UserID);
            cmd.Parameters.AddWithValue("@SectorID", ShiftMasterMD.SectorID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw;
            }
            finally { cn.Close(); }
        }

        public DataSet Select_DAL(ShiftMasterMD ShiftMasterMD)
        {
            SqlCommand cmd = new SqlCommand("MST_HrShift_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", ShiftMasterMD.TransactionType);
            cmd.Parameters.AddWithValue("@ShiftId", ShiftMasterMD.ShiftId);
            cmd.Parameters.AddWithValue("@SectorID", ShiftMasterMD.SectorID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw;
            }
            finally { cn.Close(); }
        }

    }//ALL END
}