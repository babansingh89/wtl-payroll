﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL.Master
{
    public class HRAMasterBLL:BllBase
    {

        public DataSet Get_Grid_HRABLL()
        {
            SqlCommand cmd = new SqlCommand("Get_HRA", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public DataSet Delete_HRABLL(string HRAID, int UserID)
        {
            SqlCommand cmd = new SqlCommand("Delete_HRAByID", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HRAID", HRAID);
            cmd.Parameters.AddWithValue("@CanBy", UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public DataSet Save_HRABLL(string ddlSector, string HRAID, string ddlEmpType, string txtHRA, string txtMaxAmt, string txtEffectFrom, string txtEffectTo, int UserID)
        {
            SqlCommand cmd = new SqlCommand("check_hra_mast", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SecID", ddlSector);
            cmd.Parameters.AddWithValue("@HRAID", HRAID);
            cmd.Parameters.AddWithValue("@EmpType", ddlEmpType);
            cmd.Parameters.AddWithValue("@HRA", txtHRA);
            cmd.Parameters.AddWithValue("@MaxAmount", txtMaxAmt);
            cmd.Parameters.AddWithValue("@EffectiveFrom", txtEffectFrom);
            cmd.Parameters.AddWithValue("@EffectiveTo", txtEffectTo);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<HRAMasterVM> Get_HRAData(string EmpType)
        {
            SqlCommand cmd = new SqlCommand("Get_HRAbyEmpType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpType", EmpType);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Get_GridDATA(dt);
            }
            catch (SqlException EX)
            {
                throw EX;
            }
            finally
            {
                cn.Close();
            }
        }

        List<HRAMasterVM> Get_GridDATA(DataTable dt)
        {
            List<HRAMasterVM> lst = new List<HRAMasterVM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new HRAMasterVM();
                obj.HRAID = s.Field<int?>("HRAID");
                obj.EmpType = s.Field<string>("EmpType");
                obj.EmpTypeDesc = s.Field<string>("EmpTypeDesc");
                obj.HRA = s["HRA"].ToString();
                obj.MaxAmount = s.Field<decimal?>("MaxAmount").ToString();
                obj.EffectiveFrom = s.Field<string>("EffectiveFrom");
                obj.EffectiveTo = s.Field<string>("EffectiveTo");
                obj.Edit = s.Field<string>("Edit");
                obj.Del = s.Field<string>("Del");
                return obj;
            }).ToList();
            return lst;
        }



    }
}