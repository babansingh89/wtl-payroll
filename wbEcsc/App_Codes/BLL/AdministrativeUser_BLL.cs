﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Account;

namespace wbEcsc.App_Codes.BLL
{
    public class AdministrativeUser_BLL:BllBase
    {
       
        public AdministrativeUser GetUser(string userName, string password)
        {
          
            SqlCommand cmd = new SqlCommand("Get_AdminstrativeUser(loginParam)",cn);
            cmd.Parameters.AddWithValue("@UserName", userName);
            cmd.Parameters.AddWithValue("@Password", password);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            try {
                cn.Open();
                da.Fill(dt);
                if(dt.Rows.Count>0)
                {
                    return ConvertDT(dt)[0];
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }



        #region Convert
        List<AdministrativeUser> ConvertDT(DataTable dt)
        {
            List<AdministrativeUser> lst = new List<AdministrativeUser>();
            foreach (DataRow dr in dt.Rows)
            {
                AdministrativeUser mn = new AdministrativeUser();
                mn.UserID = dr.Field<long>("UserID");
                mn.Email = dr.Field<string>("Email");
                mn.FirstName = dr.Field<string>("FirstName");
                mn.LastName = dr.Field<string>("LastName");
                mn.PhoneNo = dr.Field<string>("PhoneNo");
                mn.IsMailVerified = dr.Field<bool>("IsMailVerified");
                mn.IsAccountLocked = dr.Field<bool>("IsAccountLocked");
                mn.UserName = dr.Field<string>("UserName");
                mn.UserType = dr.Field<int>("UserType");

                lst.Add(mn);
            }
            return lst;
        }
        #endregion
    }
}