﻿<%@ WebHandler Language="C#" Class="Get_HolidayOffday" %>
using System;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
    public class Get_HolidayOffday : IHttpHandler, IReadOnlySessionState, IRequiresSessionState {

    public void ProcessRequest (HttpContext context) {
        string JSONVal = "";
        ApiResponceEmpReportPersonalInfo GER = new ApiResponceEmpReportPersonalInfo();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        try
        {
            DataSet ds = new DataSet();
            using (var command = new SqlCommand("Get_HolidayOffdayApp", con)
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(command);
                sda.Fill(ds);
                con.Close();
            }
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                List<Dictionary<string, object>> rowss = new List<Dictionary<string, object>>();
                    
                if (dt.Rows.Count > 0)
                {

                    Dictionary<string, object> row1;

                    foreach (DataRow r in dt.Rows)
                    {
                        row1 = new Dictionary<string, object>();
                        foreach (DataColumn col in dt.Columns)
                        {
                            row1.Add(col.ColumnName, r[col].ToString());
                        }
                        rowss.Add(row1);
                    }
                }
                    
                GER.Data = rowss;
                GER.Status = "Y";
                GER.Message = "SUCCESS";
                JSONVal = GER.ToJSON();
            }
            else
            {
                GER.Status = "N";
                GER.Message = "No records found ";
                JSONVal = GER.ToJSON();
            }
        }
        catch (Exception ex)
        {
            GER.Status = "N";
            GER.Message = ex.Message;
            JSONVal = GER.ToJSON();
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(JSONVal);
    }


    public bool IsReusable {
        get {
            return false;
        }
    }

}
public class ApiResponceEmpReportPersonalInfo
{
    public string Status { get; set; }

    public string Message { get; set; }

    public object Data { get; set; }
}
