﻿using System.Web.Mvc;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Application;
using System.Linq;
using System.Collections.Generic;
using wbEcsc.App_Start;
using wbEcsc.App_Codes;

namespace wbEcsc.Controllers
{
    [SecuredFilter]
    public class HomeController : Controller
    {
        public ActionResult AdministrativeLoginUI()
        {
            if(SessionContext.IsAuthenticated)
            {
                return RedirectToAction("HomeUI", "AdminHome");
            }

            return View();
        }


        
    }
}