﻿using System;
using System.Data.SqlClient;
using System.Security;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.Authenticator;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Account.Logins;
using wbEcsc.Models.Application;

namespace wbEcsc.Controllers
{
    [SecuredFilter]
    public class LoginManagerController : Controller
    {
        ClientJsonResult cj = new ClientJsonResult();
        AdministrativeUser_BLL bll = new AdministrativeUser_BLL();

        [HttpPost]
        public JsonResult AdministrativeLogin(AdministrativeLogin LoginParam)
        {
            #region
            Action<AdministrativeLogin> Validate = (_loginParam) =>
            {
                throw new NotImplementedException();
            };
            #endregion
            try
            {
                AdministrativeAuthenticator act = new AdministrativeAuthenticator(LoginParam);
                AdministrativeUser user = act.Authenticate();
                SessionContext.Login(user);

             
                cj.Data = new Models.AppResource.InOut() {
                    User = user,
                    LogoutUrl = "/Home/Logout",
                    LoginUIUrl = "/Home/AdministrativeLoginUI",
                    LoginUrl = "/LoginManager/AdministrativeLogin",
                    LandingPageUrl= "/Administration/AdminHome/HomeUI",
                    RedirectTo= "/Administration/AdminHome/HomeUI",
                    IsAuthenticated = SessionContext.IsAuthenticated
                };
                cj.Status = ResponseStatus.SUCCESS;
                cj.Message = "Successfully Logged In";
            }
            catch (SqlException)
            {
                cj.Message = "Database Error! Please try again later";
                cj.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }catch(SecurityException ex)
            {
                cj.Message = ex.Message;
                cj.Status = ResponseStatus.UNAUTHORIZED;
            }
            return Json(cj);
        }
        [HttpPost]
        public ActionResult LogOut()
        {
            try
            {
                App_Codes.SessionContext.Logout();
                cj.Data = new Models.AppResource.InOut()
                {
                    //LoginUIUrl= "/Home/LoginMenuUI",
                    //RedirectTo = "/Home/LoginMenuUI",
                    LoginUIUrl = "/Home/AdministrativeLoginUI",
                    RedirectTo = "/Home/AdministrativeLoginUI",
                    IsAuthenticated = SessionContext.IsAuthenticated
                };
                cj.Message = "Logged Out";
                cj.Status = ResponseStatus.SUCCESS;
            }
            catch (ArgumentException ex)
            {
                cj.Data = null;
                cj.Message = ex.Message;
                cj.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cj);
        }
    }
}