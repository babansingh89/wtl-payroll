﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace wbEcsc.Controllers.Report
{
    
    public class All_ReportController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        protected SqlConnection con;
        CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();
        ClientJsonResult cr = new ClientJsonResult();

        SessionData sData;
        public All_ReportController()
        {
            sData = SessionContext.SessionData;
        }

        [SecuredFilter]
        public ActionResult Index(int extension )
        {
            bool result = new ReportBll().HasReportAuthorization(extension, (SessionContext.CurrentUser as IInternalUser).UserID);

            if (result == true)
            {
                ViewBag.ReportTypeID = extension;
                if (string.Equals(Request.HttpMethod, "post", System.StringComparison.OrdinalIgnoreCase))
                {
                    TempData["ReportTypeID"] = extension;
                    TempData["ReportID"] = Request["ReportList"].ToString();
                }

                return View("~/Views/All_Report/ReportsMaster.cshtml");
            }
            else
            {
                return PartialView("~/Views/Error/Error401/index.cshtml");
            }
        }
        public ActionResult SearchPanel(int ReportTypeID)
        {
            List<wbEcsc.Models.Application.Report> reports = new List<wbEcsc.Models.Application.Report>();
            ViewBag.ReportTypeID = ReportTypeID;
            var user = SessionContext.CurrentUser as IInternalUser;
            reports = new ReportBll().GetReports(ReportTypeID, "" , user.UserID);
            return PartialView("~/Views/All_Report/CallingForms/searchPanel.cshtml", reports);
            
        }
        public ActionResult CallingForm()
        {
            if(TempData["ReportID"]==null || TempData["ReportID"] as string=="0")
            {
                return PartialView("~/Views/All_Report/CallingForms/callingForm_NotFound.cshtml");

            }
            else
            {
                return PartialView(string.Format("~/Views/All_Report/CallingForms/callingForm_{0}.cshtml", TempData["ReportID"]));
            }
        }



        //++++++++++++++++++++++++++++++++++++++++++++++++++++++= Plese don't touch the above code +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
        [HttpPost]
        public JsonResult Show_PDF_Excel(int ReportTypeID, string ReportID)
        {
            try
            {
                List<wbEcsc.Models.Application.Report> lstreports = new List<wbEcsc.Models.Application.Report>();
                var user = SessionContext.CurrentUser as IInternalUser;
                lstreports = new ReportBll().GetReports(ReportTypeID, ReportID, user.UserID);
                cr.Data = lstreports;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", lstreports.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);

        }

        [HttpPost]
        public JsonResult SaveReportData(int ReportID, Dictionary<string, object> Params)
        {
            try
            {
                string result = new ReportBll().Save_Report(ReportID, (SessionContext.CurrentUser as IInternalUser).UserID, Params["SectorID"].ToString(), Params["SalFinYear"].ToString(),
                                                            Params["SalMonthID"].ToString(), Params["SalMonth"].ToString(), Convert.ToInt32(Params["isPDFExcel"]));
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);

        }

        [HttpPost]
        public JsonResult Show_Grid(int ReportID, Dictionary<string, object> Params)
        {
            try
            {
                List<Bank_Report_MD> lstBank = new ReportBll().Show_BankData(Params["SalMonthID"].ToString(), Params["SectorID"].ToString(), sData.CurFinYear, (SessionContext.CurrentUser as IInternalUser).UserID, Params["BankID"].ToString());
                cr.Data = lstBank;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", lstBank.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);

        }

        [HttpPost]
        public JsonResult Update_BankSlip(Dictionary<string, object> Params)
        {
            try
            {
               string result = new ReportBll().Update_BankSlip(Params["SalMonthID"].ToString(), Params["SectorID"].ToString(), (SessionContext.CurrentUser as IInternalUser).UserID,
                                                                             Params["ChequeNo"].ToString(), Params["ChequeDate"].ToString());
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);

        }

        [HttpPost]
        public JsonResult Get_EmployeeBank()
        {
            try
            {
                List<Bank_MD> lst = new Bank().Get_Bank();
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Show_FInyear(string SectorID)
        {
            try
            {
                DataTable dt = new DataTable();
                con = new SqlConnection(conString);
                SqlCommand cmd = new SqlCommand("Load_SalFinYear", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@sectorID", SectorID);
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);

        }

        [HttpPost]
        public JsonResult Show_SalMonth(string FinYear, string SectorID)
        {
            try
            {
                DataTable dt = new DataTable();
                con = new SqlConnection(conString);
                SqlCommand cmd = new SqlCommand("Load_SalMonthForReport", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@finYear", FinYear);
                cmd.Parameters.AddWithValue("@sectorid", SectorID);
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);

        }
    }
}