﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL.Master;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class LevelUserMasterController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sd;

        public LevelUserMasterController()
        {
            sd = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Get_FieldName()
        {
            try
            {
                int FormID = 26;
                List<LoanMasterVM> lst = new LoanMaster().Get_FieldName(FormID);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_LevelName(string TransactionType, string FormName)
        {
            try
            {
                List<LevelUserMasterVM> lst = new LevelUserMasterBAL().Get_LevelName(TransactionType, FormName, Convert.ToString(sd.CurrSector));
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_UserName(string LevelID, string UserName)
        {
            try
            {
                List<LevelUserMasterVM> lst = new LevelUserMasterBAL().Get_UserName(LevelID, UserName, Convert.ToString(sd.CurrSector));
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GridLevel(string ConfID, string LevelSubID)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = new LevelUserMasterBAL().GridLevel_DAL(ConfID, LevelSubID, Convert.ToString(sd.CurrSector));
                if (ds.Tables.Count > 0)
                {
                    cr.Data = ds.GetXml();
                }
                else
                {
                    cr.Data = null;
                }
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult SaveLevel(string LevelID, string LevelSubID, string LevelUser, string LevelRank, string IsActive)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = new LevelUserMasterBAL().SaveLevel_DAL(LevelID, LevelSubID, LevelUser, LevelRank, IsActive, Convert.ToString(sd.CurrSector));
                if (ds.Tables.Count > 0)
                {
                    cr.Data = ds.GetXml();
                }
                else
                {
                    cr.Data = null;
                }
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }


    }//ALL END
}