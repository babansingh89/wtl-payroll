﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Codes.BLL.Transaction;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;
using System.Data.SqlClient;
using System.Configuration;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class NextMonthController : Controller
    {
        Models.ClientJsonResult cr = new ClientJsonResult();
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SessionData sData;
        public NextMonthController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            sData.Sectors = new EmployeeMaster().Get_Sector((SessionContext.CurrentUser as IInternalUser).UserID);
            SessionContext.SessionData.Sectors = sData.Sectors;
            SalaryGeneration_MD obj_SalMonthDetails = new SalaryGeneration_MD();
            if (sData.Sectors.Count == 1)
            {
                foreach (var sector in sData.Sectors)
                {
                    sData.CurrSector = sector.SectorID;
                }

                if (sData.CurrSector > 0)
                {
                    
                    obj_SalMonthDetails = new SalaryGeneration().Get_SalPayMonth(sData.CurrSector, SessionContext.SessionData.CurSalFinYear as string);

                    if (obj_SalMonthDetails != null)
                    {
                        ViewBag.MaxSalMonth = obj_SalMonthDetails.MaxSalMonth;
                        ViewBag.MaxSalMonthID = obj_SalMonthDetails.MaxSalMonthID;
                    }
                }
            }

            return View();
        }

        [HttpPost]
        public JsonResult GoForNextMonth(string salmonthid, string secid)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Sal_Generation", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            try
            {
                cmd.Parameters.AddWithValue("@sal_mon", salmonthid);
                cmd.Parameters.AddWithValue("@insertedby", sData.UserID);
                cmd.Parameters.AddWithValue("@fp", "Y");
                cmd.Parameters.AddWithValue("@SecID", secid);
                con.Open();
                cmd.CommandTimeout = 0;
                cmd.ExecuteNonQuery();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "success");
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetEncodedValue(string salmonth)
        {
            SalaryGeneration_MD obj_SalMonthDetails = new SalaryGeneration_MD();
            try
            {
                obj_SalMonthDetails = new SalaryGeneration().Get_SalPayMonth(sData.CurrSector, SessionContext.SessionData.CurSalFinYear as string);
                
                cr.Data = obj_SalMonthDetails.EncodeValue;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "success");
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_ForNextMonth(string salmonth, string SecID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Check_NextMonthProcess", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cmd.Parameters.AddWithValue("@Salmonth", salmonth);
                cmd.Parameters.AddWithValue("@SectorID", SecID);
                con.Open();
                da.Fill(dt);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "success");
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }
    }
}