﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;

namespace wbEcsc.Controllers.Administration.Master
{
    public class Form16DownloadController : Controller
    {
        // GET: Form16Download
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sd;

        public Form16DownloadController()
        {
            sd = SessionContext.SessionData;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Load()
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Upload_form16", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", "LoadByEmpID");
            cmd.Parameters.AddWithValue("@UserID", sd.UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Rows Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally { con.Close(); }
            return Json(cr);
        }
    }
}