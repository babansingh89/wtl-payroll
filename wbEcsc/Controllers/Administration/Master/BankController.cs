﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;
using System.Data;

namespace wbEcsc.Controllers.Master
{    
    public class BankController : Controller
    {
        // GET: Bank
        public ActionResult Index(int? id)
        {
         // Response.AddHeader("Refresh","10");
         
            Bank bnk_bll = new Bank();
            BankViewModel bnk_vm = new BankViewModel();
            bnk_vm.BankList= bnk_bll.Get_Bank();

            bnk_vm.FieldList = bnk_bll.GetField();
            bnk_vm.SelectedBank = new Models.Application.Bank_MD();
            if(id!=null)
            {
                bnk_vm.SelectedBank= bnk_bll.GetBankByID(id ?? 0);
            }

            return View(bnk_vm);
        }               
        
        // POST: Bank/Edit/5
        [HttpPost]
        public ActionResult Edit(Bank_MD bank)
        {
            try
            {
                // TODO: Add update logic here               
                    Bank bnk = new Bank();
                 DataTable dt= bnk.SaveData(bank);
                TempData["Massage"] = dt.Rows[0][1].ToString();
                TempData["indit"] = dt.Rows[0][0].ToString();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
           
    }
}
