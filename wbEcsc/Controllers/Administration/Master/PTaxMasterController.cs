﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL.Master;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class PTaxMasterController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sd;

        public PTaxMasterController()
        {
            sd = SessionContext.SessionData;
        }
        // GET: PTax
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Get_FieldName()
        {
            try
            {
                int FormID = 9;
                List<LoanMasterVM> lst = new LoanMaster().Get_FieldName(FormID);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_Grid(string hdnPTaxID)
        {
            try
            {
                DataSet ds = new PTaxMasterBLL().Populate_GridBLL(hdnPTaxID);
                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Delete_PTax(string PTaxID)
        {
            try
            {
                DataSet ds = new PTaxMasterBLL().Delete_PTaxBLL(PTaxID);
                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save_PTax(string SecID, string hdnPTaxID, string SalFrom, string SalTo, string PTax)
        {
            try
            {
                DataSet ds = new PTaxMasterBLL().SaveBLL(SecID, hdnPTaxID, SalFrom, SalTo, PTax, Convert.ToInt32(sd.UserID));
                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }




    }// ALL END
}