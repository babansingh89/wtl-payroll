﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;

namespace wbEcsc.Controllers.Administration.Master
{
    //[SecuredFilter]
    public class HolidayMasterController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sd;

        public HolidayMasterController()
        {
            sd = SessionContext.SessionData;
        }
        // GET: HolidayMaster
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]

        public JsonResult SaveUpdate_HolidayMaster(string hdnHolidayID, string txtYear, string ddlHolidayType, string txtFromDate, string txtToDate, string txtDescription, string SectorId, string UserID)
        {
            try
            {
                int id = 0;
                if(hdnHolidayID=="")
                {
                    id = 0;
                }
                else
                {
                    id = Convert.ToInt32(hdnHolidayID);
                }
                //string hdnLeaveTypeID, string txtLeaveDesc, string txtAbbv, string ddlGender, string txtMaxInYear, string txtMaxSave, string txtMaxInServicePeriod, int? UserID
                List<HolidayMaster_MD> lst = new HolidayMaster().SaveUpdate_HolidayMaster(id, txtYear, ddlHolidayType, txtFromDate, txtToDate, txtDescription, Convert.ToInt32(SectorId), Convert.ToInt32(sd.UserID));
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
        [HttpPost]
        public JsonResult Delete_HolidayMaster(string hdnHolidayID, string SectorId)
        {
            try
            {
                List<HolidayMaster_MD> lst = new HolidayMaster().Delete_HolidayMaster(Convert.ToInt32(hdnHolidayID), Convert.ToInt32(SectorId));
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_FieldName()
        {
            try
            {
                int FormID = 21;
                List<LeaveMaster_MD> lst = new LeaveMaster().Get_FieldName(FormID);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_GridDATA(string txtYear)
        {
            try
            {
                HolidayMaster grid = new HolidayMaster();
                DataTable ds = grid.Get_GridDATA(txtYear);
                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Load_CalendarYear(string txtYear, string SectorId)
        {
            try
            {
                HolidayMaster grid = new HolidayMaster();
                DataSet ds = grid.Get_CalendarYearDATA(txtYear, Convert.ToInt32(SectorId));
                if (ds!=null)
                {
                    DataTable dt = ds.Tables[0];
                    DataTable list = ds.Tables[1];
                    cr.YearList = (from DataRow row in dt.Rows
                                   select new YearList
                                   {
                                       Year = row["currentYears"].ToString()
                                   }).ToList();   
                    cr.holidayMaster = (from DataRow row in list.Rows select new HolidayResult
                       {
                             HdNo = row["HdNo"].ToString(),
                             FromHDate = row["FromHDate"].ToString(),
                             HDesc = row["HDesc"].ToString(),
                             ToHDate = row["ToHDate"].ToString(),
                             HType = row["HType"].ToString()

                       }).ToList();   
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Data Found", ds.Tables[0].ToString());
                }
                else
                {
                    cr.Data = null;
                    cr.Status = ResponseStatus.NOT_FOUND;
                    cr.Message = string.Format("{0} No Data Found", ds.Tables[0].ToString());
                }
                
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_MaxSalDt()
        {
            try
            {
                HolidayMaster grid = new HolidayMaster();
                DataTable dt = grid.Get_MaxSalDt();
                if(dt.Rows.Count>0)
                {
                    cr.Data = dt.Rows[0]["Vdate"].ToString();
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Data Found", dt.Rows.Count);
                }
                else
                {
                    cr.Data = null;
                    cr.Status = ResponseStatus.NOT_FOUND;
                    cr.Message = string.Format("{0} No Data Found", dt.Rows.Count);
                }
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}