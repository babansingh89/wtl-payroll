﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;

namespace wbEcsc.Controllers.Administration.Master
{
    public class Form16UploadController : Controller
    {
        // GET: Form16Upload
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sd;

        public Form16UploadController()
        {
            sd = SessionContext.SessionData;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Load(string FinYear)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Upload_form16", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", "Load");
            cmd.Parameters.AddWithValue("@FinYear", FinYear);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Rows Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally { con.Close(); }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult UploadPicture()
        {
            string fname = "", fileWithEXT = "", EXT = "", FinYear = "", EmpID ="", EmpNo="";
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            DataTable dt = new DataTable();

            try
            {
                if (Request.Files.Count > 0)
                {
                    try
                    {
                        HttpFileCollectionBase files = Request.Files;
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFileBase file = files[i];

                            // Checking for Internet Explorer  
                            if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                            {
                                string[] testfiles = file.FileName.Split(new char[] { '\\' });
                                fname = testfiles[testfiles.Length - 1];
                            }
                            else
                            {
                                string[] words = file.FileName.Split('#');
                                EXT = words[1];
                                FinYear = words[0];
                                EmpID = words[2];
                                EmpNo = words[3];
                                //fname = file.FileName;
                            }
                            fname = EmpNo + "_" + FinYear;

                            //deleting code starts here
                            string dfname = Path.Combine(Server.MapPath("~/Form16/"));
                            string[] dfiles = System.IO.Directory.GetFiles(dfname, fname + ".*");
                            foreach (string f in dfiles)
                            {
                                System.IO.File.Delete(f);
                            }

                            fname = Path.Combine(Server.MapPath("~/Form16/"), fname + EXT);
                            file.SaveAs(fname);


                            cmd = new SqlCommand("Upload_form16", dbConn, transaction);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@TransType", "Insert");
                            cmd.Parameters.AddWithValue("@FinYear", FinYear);
                            cmd.Parameters.AddWithValue("@EmployeeID", EmpID);
                            cmd.Parameters.AddWithValue("@Path", "Form16/" + EmpNo + "_" + FinYear + EXT);
                            cmd.Parameters.AddWithValue("@UserID", sd.UserID);
                            cmd.ExecuteNonQuery();

                            transaction.Commit();
                            cr.Data = 1;

                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        cr.Data = 0;
                    }
                }

                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", 0);
            }
            catch (SqlException ex)
            {
                transaction.Rollback();
                cr.Data = 0;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }
    }
}