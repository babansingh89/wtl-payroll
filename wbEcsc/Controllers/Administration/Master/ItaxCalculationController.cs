﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using wbEcsc.App_Codes.BLL;
using System.Web.Script.Serialization;
using wbEcsc.ViewModels;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class ItaxCalculationController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        int IntEmpItaxID;
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        public ItaxCalculationController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Search_EmpNoAutoComplete(string EmpNo)
        {
            try
            {
                List<EmployeeMasterDetails> listEmpNo = new EmployeeMaster().Get_EmpNoAuto_Complete(EmpNo);
                cr.Data = listEmpNo;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listEmpNo.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Load_Grid(string EmpNo, string SalFinYear, string SectorID )
        {
            string msg = "";
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Get_EmployeeDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;

            DataTable dt = new DataTable();
            DataSet dsGlobal = new DataSet();
            try
            {
                cmd.Parameters.AddWithValue("@EmpNo", EmpNo);
                cmd.Parameters.AddWithValue("@SectorID", SectorID);
                cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dtss = new DataTable();
                DataTable dts = new DataTable();
                DataTable dtresult = new DataTable();
                con.Open();
                da.Fill(dsGlobal);
                if(dsGlobal.Tables.Count>0)
                {
                     dtss = dsGlobal.Tables[0];  dts = dsGlobal.Tables[1];  dtresult = dsGlobal.Tables[2];
                   
                    if (dtss.Rows.Count == 0)
                    {
                        if (dtresult.Rows[0][0].ToString() == "Y")
                        {
                             msg = "Please Select the Appropriate Sector of this Employee.";
                        }
                        else
                        {
                             msg = "Sorry ! You are not Authorised.";
                        }
                    }
                    else
                    {
                        cmd = new SqlCommand("LoadEmpDetailsForItax", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@SalFinYear", SalFinYear);
                        cmd.Parameters.AddWithValue("@SectorID", SectorID);
                        cmd.Parameters.AddWithValue("@EmpNo", EmpNo);
                        cmd.Parameters.AddWithValue("@insertedBy", sData.UserID);
                        cmd.ExecuteNonQuery();

                        cmd = new SqlCommand("Get_ITaxEmployeeDetailsNew", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FinYear", SalFinYear);
                        cmd.Parameters.AddWithValue("@SecID", SectorID);
                        cmd.Parameters.AddWithValue("@EmpNo", EmpNo);
                        SqlDataAdapter daa = new SqlDataAdapter(cmd);
                        daa.Fill(dt);

                        dsGlobal.Tables.Add(dt);

                        ViewBag.HiddenCheckGrid = "1";
                    }
                }
                

                cr.Data = dsGlobal;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", msg);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult GetTaxBalAndChallan(string hdnEmpID)
        {
            string msg = "";
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Load_AllChallanAndTaxAmt", con);
            cmd.CommandType = CommandType.StoredProcedure;

            DataSet dsGlobal = new DataSet();
            try
            {
                cmd.Parameters.AddWithValue("@EmpID", hdnEmpID);
                cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                con.Open();
                da.Fill(dsGlobal);
                cr.Data = dsGlobal;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", msg);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Grid2ndGrid(string hdnEmpID)
        {
            string msg = "";
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Load_AllITaxType", con);
            cmd.CommandType = CommandType.StoredProcedure;

            DataTable dt = new DataTable();
            try
            {
                cmd.Parameters.AddWithValue("@EmpID", hdnEmpID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", msg);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult TaxLiabilies(string TaxableIncome, string FinYear, string Gender)
        {
            string msg = "";
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Get_ITaxRateSlab", con);
            cmd.CommandType = CommandType.StoredProcedure;

            DataTable dt = new DataTable();
            try
            {
                cmd.Parameters.AddWithValue("@TaxableIncome", TaxableIncome);
                cmd.Parameters.AddWithValue("@FinYear", FinYear);
                cmd.Parameters.AddWithValue("@Gender", Gender);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", msg);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Load_AllSavingType(string hdnEmpID)
        {
            string msg = "";
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Load_AllSavingType", con);
            cmd.CommandType = CommandType.StoredProcedure;

            DataTable dt = new DataTable();
            try
            {
                cmd.Parameters.AddWithValue("@EmpID", hdnEmpID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", msg);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult AddNewSection(string Section, int MaxAmount, string FinYear, string Gender, string hdnEmpID, string Type)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Insert_TaxType", con);
            cmd.CommandType = CommandType.StoredProcedure;

            DataTable dtGlobal = new DataTable();
            try
            {
                cmd.Parameters.AddWithValue("@Section", Section);
                cmd.Parameters.AddWithValue("@MaxAmt", MaxAmount);
                cmd.Parameters.AddWithValue("@Type", Type);
                cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                cmd.Parameters.AddWithValue("@FinYear", FinYear);
                cmd.Parameters.AddWithValue("@Gender", Gender);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                con.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    cmd = new SqlCommand("Load_AllITaxTypeByID", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TaxtypeID", dt.Rows[0][0].ToString());
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter daa = new SqlDataAdapter(cmd);
                    daa.Fill(dtGlobal);
                }
                cr.Data = dtGlobal;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Rows Found", dtGlobal.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult SaveData(ITaxCalculation_vm obj_ITaxCalculation)
        {
        //    object TableChild, object TableParents, object TableChallan, object TableOtherIncome, string FinYear, string AssessmentYear, string EmpID, string hdnEmpGender, string hdnTotalGrossPay, string hdnTotalPtax,
        //string hdnTotalITax, string hdnTotalGIS, string hdnTotalHRA, string hdnTotalGPF, string hdnTotalGPFRec, string hdnTotalIHBL, string hdnTotalHBL, string hdnTotalOtherDe, string hdnTotalNetPay,
        //string hdnTotalArrearOT, string IncomeFromSalary, string InuterestFromSaving, string InterestFromFD, string InterestFromOther, string TotDeduChapVIA, string TotTaxableIncome,
        //string TaxOnNetIncome, string RabateSec87A, string Surcharge, string EducationCess, string HSEducationCess, string TaxLiabilities, string ReliefUS89,
        //string TaxPayabl, string OtherITAX, string BalanceTax)
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            int SrNo = 1;
            try
            {
                try
                {
                    cmd = new SqlCommand("Save_DAT_EmpItax", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@SalaryFinYear", obj_ITaxCalculation.FinYear == null ? DBNull.Value : (object)obj_ITaxCalculation.FinYear);
                    cmd.Parameters.AddWithValue("@AssessmentYear", obj_ITaxCalculation.AssessmentYear == null ? DBNull.Value : (object)obj_ITaxCalculation.AssessmentYear);
                    cmd.Parameters.AddWithValue("@EmployeeID",  obj_ITaxCalculation.EmpID == null ? DBNull.Value : (object)obj_ITaxCalculation.EmpID);
                    cmd.Parameters.AddWithValue("@GrossPay", obj_ITaxCalculation.hdnTotalGrossPay == 0 ? DBNull.Value : (object)obj_ITaxCalculation.hdnTotalGrossPay);
                    cmd.Parameters.AddWithValue("@SalPTax", obj_ITaxCalculation.hdnTotalPtax == 0 ? DBNull.Value : (object)obj_ITaxCalculation.hdnTotalPtax);
                    cmd.Parameters.AddWithValue("@SalITax", obj_ITaxCalculation.hdnTotalITax == 0 ? DBNull.Value : (object)obj_ITaxCalculation.hdnTotalITax);
                    cmd.Parameters.AddWithValue("@SalGIS", obj_ITaxCalculation.hdnTotalGIS == 0 ? DBNull.Value : (object)obj_ITaxCalculation.hdnTotalGIS);
                    cmd.Parameters.AddWithValue("@SalHRA", obj_ITaxCalculation.hdnTotalHRA == 0 ? DBNull.Value : (object)obj_ITaxCalculation.hdnTotalHRA);
                    cmd.Parameters.AddWithValue("@SalGPF", obj_ITaxCalculation.hdnTotalGPF == 0 ? DBNull.Value : (object)obj_ITaxCalculation.hdnTotalGPF);
                    cmd.Parameters.AddWithValue("@SalGPFRec", obj_ITaxCalculation.hdnTotalGPFRec == 0 ? DBNull.Value : (object)obj_ITaxCalculation.hdnTotalGPFRec);
                    cmd.Parameters.AddWithValue("@SalHBL", obj_ITaxCalculation.hdnTotalHBL == 0 ? DBNull.Value : (object)obj_ITaxCalculation.hdnTotalHBL);
                    cmd.Parameters.AddWithValue("@SalIHBL", obj_ITaxCalculation.hdnTotalIHBL == 0 ? DBNull.Value : (object)obj_ITaxCalculation.hdnTotalIHBL);
                    cmd.Parameters.AddWithValue("@SalOtherDedu", obj_ITaxCalculation.hdnTotalOtherDe == 0 ? DBNull.Value : (object)obj_ITaxCalculation.hdnTotalOtherDe);
                    cmd.Parameters.AddWithValue("@SalNetPay", obj_ITaxCalculation.hdnTotalNetPay == 0 ? DBNull.Value : (object)obj_ITaxCalculation.hdnTotalNetPay);
                    cmd.Parameters.AddWithValue("@SalArrearOTBon", obj_ITaxCalculation.hdnTotalArrearOT == 0 ? DBNull.Value : (object)obj_ITaxCalculation.hdnTotalArrearOT);
                    cmd.Parameters.AddWithValue("@SalTotIncome", obj_ITaxCalculation.IncomeFromSalary == 0 ? DBNull.Value : (object)obj_ITaxCalculation.IncomeFromSalary);
                    cmd.Parameters.AddWithValue("@OtherIncomeIntSaving", obj_ITaxCalculation.InuterestFromSaving == 0 ? DBNull.Value : (object)obj_ITaxCalculation.InuterestFromSaving);
                    cmd.Parameters.AddWithValue("@OtherIncomeIntFD", obj_ITaxCalculation.InterestFromFD == 0 ? DBNull.Value : (object)obj_ITaxCalculation.InterestFromFD);
                    cmd.Parameters.AddWithValue("@OtherIncomeOther", obj_ITaxCalculation.InterestFromOther == 0 ? DBNull.Value : (object)obj_ITaxCalculation.InterestFromOther);
                    cmd.Parameters.AddWithValue("@TotDeduChapVIA", obj_ITaxCalculation.TotDeduChapVIA == 0 ? DBNull.Value : (object)obj_ITaxCalculation.TotDeduChapVIA);
                    cmd.Parameters.AddWithValue("@TotTaxableIncome", obj_ITaxCalculation.TotTaxableIncome == 0 ? DBNull.Value : (object)obj_ITaxCalculation.TotTaxableIncome);
                    cmd.Parameters.AddWithValue("@TaxOnNetIncome", obj_ITaxCalculation.TaxOnNetIncome == 0 ? DBNull.Value : (object)obj_ITaxCalculation.TaxOnNetIncome);
                    cmd.Parameters.AddWithValue("@RabateUS87A", obj_ITaxCalculation.RabateSec87A == 0 ? DBNull.Value : (object)obj_ITaxCalculation.RabateSec87A);
                    cmd.Parameters.AddWithValue("@Surcharge", obj_ITaxCalculation.Surcharge == 0 ? DBNull.Value : (object)obj_ITaxCalculation.Surcharge);
                    cmd.Parameters.AddWithValue("@EduCess", obj_ITaxCalculation.EducationCess == 0 ? DBNull.Value : (object)obj_ITaxCalculation.EducationCess);
                    cmd.Parameters.AddWithValue("@HSEduCess", obj_ITaxCalculation.HSEducationCess == 0 ? DBNull.Value : (object)obj_ITaxCalculation.HSEducationCess);
                    cmd.Parameters.AddWithValue("@TotTaxLiabilities", obj_ITaxCalculation.TaxLiabilities == 0 ? DBNull.Value : (object)obj_ITaxCalculation.TaxLiabilities);
                    cmd.Parameters.AddWithValue("@ReliefUS89", obj_ITaxCalculation.ReliefUS89 == 0 ? DBNull.Value : (object)obj_ITaxCalculation.ReliefUS89);
                    cmd.Parameters.AddWithValue("@TaxPayable", obj_ITaxCalculation.TaxPayabl == 0 ? DBNull.Value : (object)obj_ITaxCalculation.TaxPayabl);
                    cmd.Parameters.AddWithValue("@OtherITAX", obj_ITaxCalculation.OtherITAX == 0 ? DBNull.Value : (object)obj_ITaxCalculation.OtherITAX);
                    cmd.Parameters.AddWithValue("@BalanceTax", obj_ITaxCalculation.BalanceTax == 0 ? DBNull.Value : (object)obj_ITaxCalculation.BalanceTax);
                    cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);

                    SqlDataReader ReturnVal = cmd.ExecuteReader();
                    if (ReturnVal.Read())
                    {
                        IntEmpItaxID = (int)ReturnVal["strID"];
                    }
                    ReturnVal.Close();


                    cmd = new SqlCommand("Delete_TaxSavingType", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Type", "TaxType");
                    cmd.Parameters.AddWithValue("@EmpItaxID", IntEmpItaxID);
                    cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);
                    cmd.ExecuteNonQuery();

                    ////**************************************************************************************************************************************
                    for (int i = 0; i < obj_ITaxCalculation.TableParents.Count; i++)
                    {
                        cmd = new SqlCommand("Save_ItaxSavingType", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ItaxEmpID", IntEmpItaxID);
                        cmd.Parameters.AddWithValue("@ItaxEmpDetailsID", SrNo);
                        cmd.Parameters.AddWithValue("@ItaxSavingTypeID", DBNull.Value);
                        cmd.Parameters.AddWithValue("@ITaxTypeID", obj_ITaxCalculation.TableParents[i].ParentID);
                        cmd.Parameters.AddWithValue("@SavingAmount", obj_ITaxCalculation.TableParents[i].ParentAmt);
                        cmd.Parameters.AddWithValue("@ParentItaxTypeID", DBNull.Value);
                        cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);
                        cmd.ExecuteNonQuery();

                        SrNo++;
                    }
                    ////**************************************************************************************************************************************
                    SrNo = 1;
                    for (int i = 0; i < obj_ITaxCalculation.TableChild.Count; i++)
                    {
                        cmd = new SqlCommand("Save_ItaxSavingType", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ItaxEmpID", IntEmpItaxID);
                        cmd.Parameters.AddWithValue("@ItaxEmpDetailsID", SrNo);
                        cmd.Parameters.AddWithValue("@ItaxSavingTypeID", obj_ITaxCalculation.TableChild[i].ChildID);
                        cmd.Parameters.AddWithValue("@ITaxTypeID", DBNull.Value);
                        cmd.Parameters.AddWithValue("@SavingAmount", obj_ITaxCalculation.TableChild[i].ChildAmt);
                        cmd.Parameters.AddWithValue("@ParentItaxTypeID", 1);
                        cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);
                        cmd.ExecuteNonQuery();

                        SrNo++;
                    }

                    ////**************************************************************************************************************************************
                    cmd = new SqlCommand("Delete_TaxSavingType", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Type", "Challan");
                    cmd.Parameters.AddWithValue("@EmpItaxID", IntEmpItaxID);
                    cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);
                    cmd.ExecuteNonQuery();

                    ////**************************************************************************************************************************************
                    SrNo = 1;
                    for (int i = 0; i < obj_ITaxCalculation.TableChallan.Count; i++)
                    {
                        cmd = new SqlCommand("Save_ItaxOtherChallan", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ItaxEmpID", IntEmpItaxID);
                        cmd.Parameters.AddWithValue("@SubDetailID", SrNo);
                        cmd.Parameters.AddWithValue("@ChallanNo", obj_ITaxCalculation.TableChallan[i].ChallanNo);
                        cmd.Parameters.AddWithValue("@ChallanDate", obj_ITaxCalculation.TableChallan[i].ChallanDate);
                        cmd.Parameters.AddWithValue("@BSRNo", obj_ITaxCalculation.TableChallan[i].BSRNo);
                        cmd.Parameters.AddWithValue("@Amount", obj_ITaxCalculation.TableChallan[i].Amount);
                        cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);
                        cmd.ExecuteNonQuery();

                        SrNo++;
                    }
                    ////**************************************************************************************************************************************
                    cmd = new SqlCommand("Delete_TaxOtherIncomeSource", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@EmpItaxID", IntEmpItaxID);
                    cmd.Parameters.AddWithValue("@InsertedBy", sData.UserID);
                    cmd.ExecuteNonQuery();

                    SrNo = 1;
                    for (int i = 0; i < obj_ITaxCalculation.TableOtherIncome.Count; i++)
                    {
                        cmd = new SqlCommand("Save_ItaxOtherIncomeSource", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ItaxEmpID", IntEmpItaxID);
                        cmd.Parameters.AddWithValue("@DetailID", SrNo);
                        cmd.Parameters.AddWithValue("@Description", obj_ITaxCalculation.TableOtherIncome[i].Description);
                        cmd.Parameters.AddWithValue("@Amount", obj_ITaxCalculation.TableOtherIncome[i].Amount);
                        cmd.Parameters.AddWithValue("@insertedBy", sData.UserID);
                        cmd.ExecuteNonQuery();

                        SrNo++;
                    }

                    transaction.Commit();

                    cr.Data = "Record Saved Successfully !!";
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Rows Found", "success");
                }

                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }
    }
}