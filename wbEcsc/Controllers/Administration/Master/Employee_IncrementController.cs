﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Application;

namespace wbEcsc.Controllers.Administration.Master
{
    public class Employee_IncrementController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public Employee_IncrementController()
        {
            sData = SessionContext.SessionData;
        }

        public ActionResult Index()
        {
            Logo lObj = new Logo();
            Logo_MD modelObj = lObj.GetAppsInfo();
            ViewBag.logo = modelObj.LogoPath;
            ViewBag.header = modelObj.OfficeName;
            ViewBag.address = modelObj.Address;

            return View();
        }

        [HttpPost]
        public JsonResult EmployeeDetail(string SectorID, string SalMonth)
        {
            try
            {
                DataTable dt = new Employee_Increment_BLL().EmployeeDetail(SectorID, SalMonth);
                cr.Data = dt;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Empwise_UpdateIncrement(string EmpID, string OldBasic, string NewPayscaleID, string NewBasic, string IncrPer, string IncrOn, string Decline, string Remarks, string SectorID)
        {
            try
            {
                new Employee_Increment_BLL().Empwise_UpdateIncrement(EmpID, OldBasic, NewPayscaleID, NewBasic, IncrPer, IncrOn, Decline, Remarks, SectorID);
                cr.Data = "success";
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Apply(string EmpID, string OldBasic, string NewPayscaleID, string NewBasic, string IncrPer, string IncrOn, string Decline, string Remarks, string SectorID)
        {
            try
            {
                string result =  new Employee_Increment_BLL().Apply(EmpID, OldBasic, NewPayscaleID, NewBasic, IncrPer, IncrOn, Decline, Remarks, SectorID);
                cr.Data = result;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
    }
}