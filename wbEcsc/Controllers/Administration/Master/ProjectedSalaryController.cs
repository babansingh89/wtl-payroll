﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.Authenticator;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using wbEcsc.Models.Account.Logins;
using wbEcsc.Models.AppResource;
using wbEcsc.Utils.EncryptionDecrytion;
using Newtonsoft.Json;
using wbEcsc.ViewModels;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class ProjectedSalaryController : Controller
    {
        EmployeeMaster emp = new EmployeeMaster();
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public ProjectedSalaryController()
        {
            sData = SessionContext.SessionData;

        }
        public ActionResult Index()
        {
            string empNoLength = new Logo().Get_EmpNoLenth();
            ViewBag.EmpNoLength = empNoLength;

            return View();
        }

        [HttpPost]
        public JsonResult Search_EmpNoAutoComplete(string EmpNo)
        {
            try
            {
                List<EmployeeMasterDetails> listEmpNo = new EmployeeMaster().Get_EmpNoAuto_Complete(EmpNo);
                cr.Data = listEmpNo;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listEmpNo.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_Employee(string EmployeeNo, string SectorID)
        {
            try
            {
                DataTable listEmp = new SalaryProjection().Get_EmployeeDetails(EmployeeNo, SectorID);
                cr.Data = listEmp;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", listEmp.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Populate_EmpGrid(int SecID, string EmpNo, string FinYear)
        {
            try
            {
                DataTable listEmp = new SalaryProjection().Get_EmployeeSalProjection(EmpNo, SecID, FinYear, sData.UserID);
                cr.Data = listEmp;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", (listEmp.Rows.Count == 0 ? "No Record." : (listEmp.Rows.Count).ToString()));
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_FieldName()
        {
            try
            {
                int FormID = 13;
                List<LoanMasterVM> lst = new LoanMaster().Get_FieldName(FormID);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Report_Paravalue_Payslip(string EmployeeID, string SalMonth, string Status, string SecID, string FormName, string ReportName, string ReportType)
        {
            try
            {
                DataTable listEmp = new SalaryProjection().Get_Empwise_Hist_PaySlip(Status, SalMonth, EmployeeID, SecID, sData.UserID);
                cr.Data = listEmp;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", listEmp.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Original_Sal_Statement(string EmpNo, int SecID, string FinYear, string strReportType)
        {
            try
            {
                string result= new SalaryProjection().Original_Sal_Statement(EmpNo, SecID, FinYear, strReportType, sData.UserID);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}