﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;
using wbEcsc.App_Codes.BLL.Master;
using wbEcsc.App_Start;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class DA_HRA_MA_MasterController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sd;

        public DA_HRA_MA_MasterController()
        {
            sd = SessionContext.SessionData;
        }

        // GET: DA_HRA_MA_Master
        public ActionResult Index()
        {
            return View("~/Views/DA_HRA_MA_Master/Index.cshtml");
        }

        [HttpPost]
        public JsonResult Get_FieldName()
        {
            try
            {
                int FormID = 6;
                List<LoanMasterVM> lst = new LoanMaster().Get_FieldName(FormID);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_EarningType()
        {
            try
            {
                List<DA_HRA_MA_MaterVM> lst = new DA_HRA_MA_Master().Get_EarningTypeBLL();
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_EmpType()
        {
            try
            {
                List<DA_HRA_MA_MaterVM> lst = new DA_HRA_MA_Master().Get_EmpTypeBLL("");
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_DA_HRA_MA_Rate(string SecID, string EmpType, string EarningType, string PayMonthID)
        {
            try
            {
                List<DA_HRA_MA_MaterVM> lst = new DA_HRA_MA_Master().Get_DA_HRA_MA_RateBLL(SecID, EmpType, EarningType, PayMonthID);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_GridDATA(string SecID, string EarningCode, string EmpNo, string Emptype, string DaHraMa, string PayMonthID)
        {
            try
            {
                List<DA_HRA_MA_MaterVM> lst = new DA_HRA_MA_Master().Get_GridDataBLL(SecID, EarningCode, EmpNo, Emptype, DaHraMa, PayMonthID, Convert.ToInt32(sd.UserID));
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_Update(string EmpNo, string Decline, string Reasons)
        {
            try
            {
                List<DA_HRA_MA_MaterVM> lst = new DA_HRA_MA_Master().Get_UpdateBLL(EmpNo, Decline, Reasons, Convert.ToInt32(sd.UserID));
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_Generate(string SecID, string Daid, string EarningCode)
        {
            try
            {
                List<DA_HRA_MA_MaterVM> lst = new DA_HRA_MA_Master().Get_GenerateBLL(SecID, Daid, EarningCode, Convert.ToInt32(sd.UserID));
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }


    }//ALL END
}