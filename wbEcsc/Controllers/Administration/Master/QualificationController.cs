﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Application;
using wbEcsc.Models;
using System.Data;

namespace wbEcsc.Controllers.MasterControllers
{
    public class QualificationController : Controller
    {
        // GET: Qualification
        ClientJsonResult cr = new ClientJsonResult();
        Qualification qu_dal = new Qualification();
        Qualification_MD qu_model = new Qualification_MD();        
        
        public ActionResult Index()
        {
            return View();
        }

       [HttpGet]
        public JsonResult Get_FieldName()
        {
            try
            {
                cr.Data = qu_dal.GetField();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Get_Qualification()
        {
            try
            {
                cr.Data = qu_dal.Get_Qualification();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr,JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Save(Qualification_MD qu_model)
        {
            try
            {
                DataTable dt = qu_dal.Save(qu_model);
                cr.Data = dt.Rows[0][0];
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = (string)dt.Rows[0][1];
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Delete(Qualification_MD qu_model)
        {
            try
            {
                DataTable dt = qu_dal.Delete(qu_model);
                cr.Data = null;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = (string)dt.Rows[0][0];
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

    }
}