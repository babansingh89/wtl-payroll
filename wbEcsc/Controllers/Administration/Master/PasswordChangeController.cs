﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.Models.Application;

namespace wbEcsc.Controllers.Administration.Master
{
    //[SecuredFilter]
    public class PasswordChangeController : Controller
    {
        //
        // GET: /PasswordChange/

        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        protected SqlConnection con;
        SessionData sData;
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult ChangePassword(string OldPassword, string NewPassword)
        {
            con = new SqlConnection(conString);
            DataTable dt = new DataTable();
            sData = SessionContext.SessionData;
            int InsertedBy = Convert.ToInt32(sData.UserID);
            using (var command = new SqlCommand("ECSC.Change_UserPassword", con)
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                con.Open();
                command.Parameters.AddWithValue("@UserId", InsertedBy);
                command.Parameters.AddWithValue("@OldPassword", OldPassword);
                command.Parameters.AddWithValue("@NewPassword", NewPassword);
                SqlDataAdapter sda = new SqlDataAdapter(command);
                sda.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    if ((dt.Rows[0]["MESSEGE"]).ToString() == "success")
                    {
                        Session["session_Password"] = NewPassword;
                    }
                }
                con.Close();
            }
            
            return Json(dt);
        }
	}
}