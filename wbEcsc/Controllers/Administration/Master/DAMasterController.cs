﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;
using wbEcsc.App_Codes.BLL.Master;
using wbEcsc.App_Start;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class DAMasterController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sd;

        public DAMasterController()
        {
            sd = SessionContext.SessionData;
        }
        // GET: DAMaster
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Get_DAInGrid(string EmpType)
        {
            try
            {
                List<DAMasterVM> lst = new DAMaster().Get_DAData(EmpType);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_FieldName()
        {
            try
            {
                int FormID = 5;
                List<LoanMasterVM> lst = new LoanMaster().Get_FieldName(FormID);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Delete_DA(string DAID)
        {
            try
            {
                List<DAMasterVM> lst = new DAMaster().Delete_DA(DAID, Convert.ToInt32(sd.UserID));
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult SaveUpdate_DA(string SecID, string DAID, string EmpType, string OrderNo, string OrderDate
                                      , string DARate, string MaxAmount, string EffectiveFrom, string EffectiveTo)
        {
            try
            {
                List<DAMasterVM> lst = new DAMaster().SaveUpdate_DA(SecID, DAID, EmpType
                                                                    , OrderNo, OrderDate, DARate, MaxAmount, EffectiveFrom
                                                                    , EffectiveTo, Convert.ToInt32(sd.UserID));
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_EffectFromDT(string SecID, string EffectiveFrom)
        {
            try
            {
                List<DAMasterVM> lst = new DAMaster().Get_EffectFromDT(SecID, EffectiveFrom);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }



    }//ALL END
}