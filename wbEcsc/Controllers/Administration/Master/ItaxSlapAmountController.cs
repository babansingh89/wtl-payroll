﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Application;
using wbEcsc.Models;
using System.Data;
using wbEcsc.App_Start;

namespace wbEcsc.Controllers.MasterControllers
{
   [SecuredFilter]
    public class ItaxSlapAmountController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        ItaxMaxAmountSave qu_dal = new ItaxMaxAmountSave();
        ItaxMaxAmount qu_model = new ItaxMaxAmount();
        // GET: ItaxSlapAmount
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public JsonResult Get_FieldName()
        {
            try
            {
                cr.Data = qu_dal.GetField();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Get_Gender()
        {
            try
            {
                cr.Data = qu_dal.GetGender();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Get_Data(ItaxMaxAmount qu_model)
        {
            try
            {
                cr.Data = qu_dal.GetData(qu_model);
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save(ItaxMaxAmount qu_model)
        {
            try
            {
                DataTable dt = qu_dal.Save(qu_model);
                cr.Data = dt.Rows[0][0];
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = (string)dt.Rows[0][1];
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Delete(ItaxMaxAmount qu_model)
        {
            try
            {
                DataTable dt = qu_dal.Delete(qu_model);
                cr.Data = null;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = (string)dt.Rows[0][1];
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}