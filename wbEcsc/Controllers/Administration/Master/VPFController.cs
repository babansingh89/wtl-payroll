﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Codes.BLL.Transaction;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;
using System.Data.SqlClient;
using System.Configuration;

namespace wbEcsc.Controllers.Administration.Master
{
    public class VPFController : Controller
    {
        Models.ClientJsonResult cr = new ClientJsonResult();
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        SessionData sData;
        public VPFController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Load()
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("calCu_vpf", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", "PageLoad");
            cmd.Parameters.AddWithValue("@SalmonthID", sData.SalMonthID);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "success");
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save(List<Master> param)
        {
            SqlConnection dbConn = new SqlConnection(conString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;

            try
            {
                try
                {
                    DataTable DT = new DataTable();
                    DT.Columns.Add("EmployeeID", typeof(String));
                    DT.Columns.Add("ApplicableGross", typeof(String));
                    DT.Columns.Add("Percentage", typeof(String));
                    DT.Columns.Add("CalcuAmt", typeof(String));
                    DT.Columns.Add("InputAmount", typeof(String));
                    DT.Columns.Add("CheckBox", typeof(String));
                    DT.Columns.Add("SalMonthID", typeof(String));
                    DT.Columns.Add("InsertedBy", typeof(String));

                    if(param != null)
                    {
                        if(param.Count >0)
                        {
                            for(int i=0; i< param.Count; i++)
                            {
                                DataRow dr1 = DT.NewRow();
                                dr1["EmployeeID"] = param[i].EmployeeID;
                                dr1["ApplicableGross"] = param[i].appgross;
                                dr1["Percentage"] = param[i].perct;
                                dr1["CalcuAmt"] = param[i].calamt;
                                dr1["InputAmount"] = param[i].inputamt;
                                dr1["CheckBox"] = param[i].chk;
                                dr1["SalMonthID"] = sData.SalMonthID;
                                dr1["InsertedBy"] = sData.UserID;
                                DT.Rows.Add(dr1);
                            }

                            cmd = new SqlCommand("calCu_vpf", dbConn, transaction);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@SalmonthID", sData.SalMonthID);
                            cmd.Parameters.AddWithValue("@TransType", "Save");
                            cmd.Parameters.AddWithValue("@typ_emp_vpf", DT);
                            cmd.ExecuteNonQuery();
                        }
                    }


                    transaction.Commit();
                    cr.Data = "success";
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Found", "success");
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = sqlError.Message;
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        public class Master
        {
            public string EmployeeID { get; set; }
            public string appgross { get; set; }
            public string perct { get; set; }
            public string calamt { get; set; }
            public string inputamt { get; set; }
            public string chk { get; set; }
        }
    }
}