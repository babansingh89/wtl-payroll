﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Application;
using wbEcsc.Models;
using System.Data;
using wbEcsc.App_Start;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class ItaxSlapPercentageController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        ItaxMaxAmountSave qu_dal = new ItaxMaxAmountSave();       
        ItaxSlapPercentageBLL bll = new ItaxSlapPercentageBLL();
        // GET: ItaxSlapPercentage
        public ActionResult Index()
        {
            return View();
        }           
     
        [HttpGet]
        public JsonResult Get_FieldName()
        {
            try
            {
                cr.Data = bll.GetField();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Get_Gender()
        {
            try
            {
                cr.Data = qu_dal.GetGender();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Get_Data(string finyear)
        {
            try
            {
                cr.Data = bll.GetData(finyear);
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save(ItaxSlapPercentage_MD md)
        {
            try
            {
                DataTable dt = bll.Save(md);

                var data = new {FocusCode=dt.Rows[0][0],IsSuccess=dt.Rows[0][1] };

                cr.Data = data;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = (string)dt.Rows[0][2];
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Delete(string RateSlabID)
        {
            try
            {
                DataTable dt = bll.Delete(RateSlabID);
                cr.Data = null;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = (string)dt.Rows[0][1];
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}