﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.Authenticator;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using wbEcsc.Models.Account.Logins;
using wbEcsc.Models.AppResource;
using wbEcsc.Utils.EncryptionDecrytion;
using Newtonsoft.Json;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class EmployeeMasterController : Controller
    {
      
        EmployeeMaster emp = new EmployeeMaster();
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData ;
        public EmployeeMasterController()
        {
            sData = SessionContext.SessionData;

        }
        public ActionResult Index()
        {
           

            List<Caste_MD> lstCaste = new Caste().Get_Caste();
            List<Classification_MD> lstClassification = new Classification().Get_Classification();
            List<Religion_MD> lstReligion = new Religion().Get_Religion();
            List<Qualification_MD> lstQualification = new Qualification().Get_Qualification();
            List<State_MD> lstState = new State().Get_State();
            List<Bank_MD> lstBank = new Bank().Get_Bank();
           // List<EarnDeduction_MD> lstEarnDeduction = new EarnDeduction().Get_EarnDeduction();
            List<Designation_MD> lstDesignation = new Designation().Get_Designation();
            List<Group_MD> lstGroup = new Group().Get_Group();
            List<Cooperative_MD> lstCooperative = new Cooperative().Get_Cooperative();
            List<Category_MD> lstCategory = new Category().Get_Category();
            List<House_MD> lstHouse = new House().Get_House();
            //List<Emp_Type_MD> lstEmpType = new EmployeeMaster().Get_EmpType();
            string  empNoLength = new Logo().Get_EmpNoLenth();

            sData.Sectors = new EmployeeMaster().Get_Sector((SessionContext.CurrentUser as IInternalUser).UserID);
            SessionContext.SessionData.Sectors = sData.Sectors;
            List<Location_MD> lstCenter = new List<Location_MD>();
            if (sData.Sectors.Count == 1)
            {
                foreach (var sector in sData.Sectors)
                {
                    sData.CurrSector = sector.SectorID;
                }
               
                if (sData.CurrSector > 0)
                {
                    lstCenter = new Location().Get_Center(sData.CurrSector);
                }
            }



            ViewBag.lstCaste = lstCaste;
            ViewBag.lstClassification = lstClassification;
            ViewBag.lstReligion = lstReligion;
            ViewBag.lstQualification = lstQualification;
            ViewBag.lstState = lstState;
            ViewBag.lstBank = lstBank;
            //ViewBag.lstEarnDeduction = lstEarnDeduction;
            ViewBag.lstDesignation = lstDesignation;
            ViewBag.lstGroup = lstGroup;
            ViewBag.lstCooperative = lstCooperative;
            ViewBag.lstCategory = lstCategory;
            ViewBag.lstHouse = lstHouse;
            ViewBag.EmpNoLength = empNoLength;

            ViewBag.lstCenter = lstCenter;
            //ViewBag.lstEmpType = lstEmpType;

            return View();
        }

        [HttpPost]
        public JsonResult Get_Center(int SectorID)
        {
            try
            {
                List<Location_MD> lstCenter= new Location().Get_Center(SectorID);
                cr.Data = lstCenter;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", lstCenter.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_District(int StateID)
        {
            try
            {
                List<District_MD> lstDistrict = new District().Get_District(StateID);
                cr.Data = lstDistrict;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", lstDistrict.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_BankBranch(int BankID)
        {
            try
            {
                List<Branch_MD> lstBranch = new Branch().Get_Branch(BankID);
                cr.Data = lstBranch;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", lstBranch.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_IFSCMICR(int BranchID)
        {
            try
            {
                List<Branch_MD> listIFSCMICR = new EmployeeMaster().Get_IFSCMICR(BranchID);
                cr.Data = listIFSCMICR;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listIFSCMICR.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_DA(string EmpType)
        {
            try
            {
                List<DA_MD> listDA = new EmployeeMaster().Get_DA(EmpType);
                cr.Data = listDA;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listDA.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_HRA(string EmpType)
        {
            try
            {
                List<HRA_MD> listHRA = new EmployeeMaster().Get_HRA(EmpType);
                cr.Data = listHRA;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listHRA.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_PFPercentage(string EmpType)
        {
            try
            {
                List<PF_Percentage_Value> listPFPercentage = new EmployeeMaster().Get_PFPercentage(EmpType);
                cr.Data = listPFPercentage;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", listPFPercentage.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_PayScaleType(string EmpType)
        {
            try
            {
                List<PayScaleType_MD> listPayScaleType = new EmployeeMaster().Get_PayScaleType(EmpType);
                cr.Data = listPayScaleType;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listPayScaleType.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_PayScale(string PayScaleType)
        {
            try
            {
                List<PayScale_MD> listPayScale = new EmployeeMaster().Get_PayScale(PayScaleType);
                cr.Data = listPayScale;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listPayScale.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Search_IFSCAutoComplete(string IFSC)
        {
            try
            {
                List<Branch_MD> listBranch = new Branch().Get_Auto_IFSC(IFSC);
                cr.Data = listBranch;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listBranch.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_BankandBranch(string BranchID)
        {
            try
            {
                List<Branch_MD> listBranch = new Branch().Get_BankandBranch(BranchID);
                cr.Data = listBranch;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listBranch.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Search_EmpNoAutoComplete(string EmpNo)
        {
            try
            {
                List<EmployeeMasterDetails> listEmpNo = new EmployeeMaster().Get_EmpNoAuto_Complete(EmpNo);
                cr.Data = listEmpNo;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listEmpNo.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Search_EmpNameAutoComplete(string EmpName)
        {
            try
            {
                List<EmployeeMasterDetails> listEmpName = new EmployeeMaster().Get_EmpNameAuto_Complete(EmpName);
                cr.Data = listEmpName;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listEmpName.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_Employee(string EmployeeNo, string SectorID)
        {
            try
            {
                DataSet listEmp = new EmployeeMaster().Get_Employee(EmployeeNo, SectorID, sData.UserID);
                cr.Data = listEmp;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listEmp.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult HideShow(string HRACat)
        {
            try
            {
                DataTable dt = new House().Get_HideShow(HRACat);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save(Employee_SaveData Master, string Detail)
        {
            try
            {
                EmployeeMaster emp = new EmployeeMaster();
                string result = emp.Save(Master, Detail, sData.UserID, sData.CurSalFinYear);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Update(Employee_SaveData Master, string Detail)
        {
            try
            {
                EmployeeMaster emp = new EmployeeMaster();
                string result = emp.Update(Master, Detail, sData.UserID, sData.CurSalFinYear);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Show_PayDetails(string EmpNo)
        {
            try
            {
                List<Employee_PayDetails> lstEmployee_PayDetails = new EmployeeMaster().Show_PayDetails(EmpNo);
                cr.Data = lstEmployee_PayDetails;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", lstEmployee_PayDetails.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Check_PayScaleRange(string Amount, string PayScaleID)
        {
            try
            {
                DataTable dt = new EmployeeMaster().Check_PayScaleRange(Amount, PayScaleID);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Add_PayDetails(string EmpNo, string EmpType, string EmpCategory, string PayScaleID, string Amount, string DAID,
                                         string hraCategory, string HRAID, string HRARent, string FixedHRAAmount, string SpouseAmount, string isHealth,
                                         string HealthFrom, string HealthTo, string IRID, string PFID, string PFAmount, string PhysicalHandicapped)
        {
            try
            {
                DataTable dt = new EmployeeMaster().Add_PayDetails(EmpNo, EmpType, EmpCategory, PayScaleID, Amount, DAID, hraCategory, HRAID, HRARent, FixedHRAAmount, 
                                            SpouseAmount, isHealth, HealthFrom, HealthTo, sData.UserID, IRID, PFID, PFAmount, PhysicalHandicapped);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_IR(string EmpType)
        {
            try
            {
                List<IR_MD> lstIR = new IR().Bind_IR(EmpType);
                cr.Data = lstIR;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", lstIR.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_Status(string CategoryID)
        {
            try
            {
                List<Status_MD> lstStatus = new EmployeeMaster().Get_Status(CategoryID);
                cr.Data = lstStatus;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", lstStatus.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_EarnDeduction(string EmpCategory, string StatusID)
        {
            try
            {
                List<EarnDeduction_MD> lstEarnDeduction = new EarnDeduction().Get_EarnDeduction(EmpCategory, StatusID);
                cr.Data = lstEarnDeduction;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", lstEarnDeduction.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_FieldName()
        {
            try
            {
                int FormID = 20;
                DataTable lst = new EmployeeMaster().Get_FieldName(FormID);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Check_EmpMasterValidation(Employee_SaveData Master)
        {
            try
            {
                EmployeeMaster emp = new EmployeeMaster();
                DataTable result = emp.Check_EmpMasterValidation(Master);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", result.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}