﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Application;
using wbEcsc.Models;
using System.Data;
using wbEcsc.App_Start;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class LoanTypeMasterController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        LoanTypeBll qu_dal = new LoanTypeBll();
        // GET: LoanTypeMaster
        public ActionResult Index()
        {
            return View();
        }         
      
        [HttpGet]
        public JsonResult Get_FieldName()
        {
            try
            {
                cr.Data = qu_dal.GetField();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

       [HttpGet]
        public JsonResult Get_Type()
        {
            try
            {
                List<LoanMaster_MD> lst = new LoanMaster().Get_LoanDescType();
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr,JsonRequestBehavior.AllowGet);
        }          
        
        [HttpGet]
        public JsonResult Get_Data()
        {
            try
            {
                cr.Data = qu_dal.GetData();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr,JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Save(LoanType_MD qu_model)
        {
            try
            {
                DataTable dt = qu_dal.Save(qu_model);
                object dataobj = new
                {
                    data1 = dt.Rows[0][0],
                    data2 = dt.Rows[0][1]
                };
                cr.Data = dataobj;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = (string)dt.Rows[0][2];
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Delete(string LoanTypeID)
        {
            try
            {
                DataTable dt = qu_dal.Delete(LoanTypeID);
                cr.Data = null;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = (string)dt.Rows[0][1];
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}