﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL.Master;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.BLL;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Script.Serialization;
using wbEcsc.Models.Account;

namespace wbEcsc.Controllers.Administration.Master
{
    public class ITaxCalculatorController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sd;
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;

        public ITaxCalculatorController()
        {
            sd = SessionContext.SessionData;
        }

        // GET: ITaxCalculator
        public ActionResult Index()
        {
            if (sd == null)
            {
                return RedirectToAction("AdministrativeLoginUI", "Home");
            }
            List<Caste_MD> lstCaste = new Caste().Get_Caste();
            List<Classification_MD> lstClassification = new Classification().Get_Classification();
            List<Religion_MD> lstReligion = new Religion().Get_Religion();
            List<Qualification_MD> lstQualification = new Qualification().Get_Qualification();
            List<State_MD> lstState = new State().Get_State();
            List<Bank_MD> lstBank = new Bank().Get_Bank();
            // List<EarnDeduction_MD> lstEarnDeduction = new EarnDeduction().Get_EarnDeduction();
            List<Designation_MD> lstDesignation = new Designation().Get_Designation();
            List<Group_MD> lstGroup = new Group().Get_Group();
            List<Cooperative_MD> lstCooperative = new Cooperative().Get_Cooperative();
            List<Category_MD> lstCategory = new Category().Get_Category();
            List<House_MD> lstHouse = new House().Get_House();
            //List<Emp_Type_MD> lstEmpType = new EmployeeMaster().Get_EmpType();
            string empNoLength = new Logo().Get_EmpNoLenth();

            sd.Sectors = new EmployeeMaster_ITAX().Get_Sector((SessionContext.CurrentUser as IInternalUser).UserID);
            SessionContext.SessionData.Sectors = sd.Sectors;
            List<Location_MD> lstCenter = new List<Location_MD>();
            if (sd.Sectors.Count == 1)
            {
                foreach (var sector in sd.Sectors)
                {
                    sd.CurrSector = sector.SectorID;
                }

                if (sd.CurrSector > 0)
                {
                    lstCenter = new Location().Get_Center(sd.CurrSector);
                }
            }



            ViewBag.lstCaste = lstCaste;
            ViewBag.lstClassification = lstClassification;
            ViewBag.lstReligion = lstReligion;
            ViewBag.lstQualification = lstQualification;
            ViewBag.lstState = lstState;
            ViewBag.lstBank = lstBank;
            //ViewBag.lstEarnDeduction = lstEarnDeduction;
            ViewBag.lstDesignation = lstDesignation;
            ViewBag.lstGroup = lstGroup;
            ViewBag.lstCooperative = lstCooperative;
            ViewBag.lstCategory = lstCategory;
            ViewBag.lstHouse = lstHouse;
            ViewBag.EmpNoLength = empNoLength;

            ViewBag.lstCenter = lstCenter;
            //ViewBag.lstEmpType = lstEmpType;

            return View();
        }

        [HttpPost]
        public JsonResult Get_ITaxData_onPageLoad(string TransType, string SalFinYear, string EmpNo, string SectorID)
        {
            try
            {
                DataSet ds = new ITaxCalculator().Get_ITaxData_onPageLoadBLL(TransType, SalFinYear, EmpNo, SectorID, Convert.ToString(sd.UserID));
                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_ITaxDataCalculation(string TransType, string SalFinYear, string EmpNo, string SectorID, string AssesmentYear, string TaxableIncome)
        {
            try
            {
                DataSet ds = new ITaxCalculator().Get_ITaxDataCalculationBLL(TransType, SalFinYear, EmpNo, SectorID, Convert.ToString(sd.UserID), AssesmentYear, TaxableIncome);
                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult SaveNewSubSavingType(string ItaxSaveID, string Description, string TaxTypeId, string Abbre, string MaxAmount, string type, string Percentage)
        {
            try
            {
                DataSet ds = new ITaxCalculator().SaveNewSubSavingTypeBLL(ItaxSaveID, Description, TaxTypeId, Abbre, MaxAmount, type, Percentage, Convert.ToString(sd.UserID));
                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        //-------------------------------------------------------
        [HttpPost]
        public JsonResult Get_AllOtherSourceIncome(string ITaxCalcID)
        {
            try
            {
                DataSet ds = new ITaxCalculator().Get_OtherSourceIncomeBLL(ITaxCalcID);
                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save_OtherSourceIncome(string ITaxCalcID, string OtherSourceName)
        {
            try
            {
                DataSet ds = new ITaxCalculator().Save_OtherSourceIncomeBLL(ITaxCalcID, OtherSourceName, Convert.ToString(sd.UserID));
                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Search_EmpNoAutoComplete(string EmpNo)
        {
            try
            {
                List<EmployeeMasterDetails> listEmpNo = new EmployeeMaster_ITAX().Get_EmpNoAuto_Complete(EmpNo);
                cr.Data = listEmpNo;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listEmpNo.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult AddNewSection(string Section, int MaxAmount, string FinYear, string Gender, string Type, string Percentage)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Insert_TaxType", con);
            cmd.CommandType = CommandType.StoredProcedure;

            DataTable dtGlobal = new DataTable();
            try
            {
                cmd.Parameters.AddWithValue("@Section", Section);
                cmd.Parameters.AddWithValue("@Abbv", "");
                cmd.Parameters.AddWithValue("@MaxAmt", MaxAmount);
                cmd.Parameters.AddWithValue("@Type", Type);
                cmd.Parameters.AddWithValue("@UserID", sd.UserID);
                cmd.Parameters.AddWithValue("@FinYear", FinYear);
                cmd.Parameters.AddWithValue("@Gender", Gender);
                cmd.Parameters.AddWithValue("@Percentage", Percentage.Equals("") ? DBNull.Value : (object)Percentage);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                con.Open();
                da.Fill(ds);

                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Rows Found", dtGlobal.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally { con.Close(); }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult FinalSave(string TransType, string EmpID, string SalFinYear, string AssessmentYear
            , string ArrayMainTable, string ArrayChallan, string ArrayOtherIncome, string ArrayParentDeduction6, string ArrayChildDeduction6)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Itax_allPurpose", con);
            cmd.CommandType = CommandType.StoredProcedure;

            var data = new JavaScriptSerializer();
            List<MainTable> lstMainTable = data.Deserialize<List<MainTable>>(ArrayMainTable);
            List<Challan> lstChallan = data.Deserialize<List<Challan>>(ArrayChallan);
            List<OtherIncomeTable> lstOtherIncome = data.Deserialize<List<OtherIncomeTable>>(ArrayOtherIncome);
            List<ParentDeduction6> lstParentDeduction6 = data.Deserialize<List<ParentDeduction6>>(ArrayParentDeduction6);
            List<ChildDeduction6> lstChildDeduction6 = data.Deserialize<List<ChildDeduction6>>(ArrayChildDeduction6);

            DataTable dtMainTable = new DataTable();
            dtMainTable.Columns.Add("ITCalcID", typeof(String));
            dtMainTable.Columns.Add("Amount", typeof(String));
            if (lstMainTable != null)
            {
                foreach (var r in lstMainTable)
                {
                    DataRow dr = dtMainTable.NewRow();
                    dr["ITCalcID"] = r.ITCalcID;
                    dr["Amount"] = r.Amount;
                    dtMainTable.Rows.Add(dr);
                }
            }

            DataTable dtChallan = new DataTable();
            dtChallan.Columns.Add("SubDetailID", typeof(String));
            dtChallan.Columns.Add("ChallanNo", typeof(String));
            dtChallan.Columns.Add("ChallanDate", typeof(String));
            dtChallan.Columns.Add("BSRNo", typeof(String));
            dtChallan.Columns.Add("TaxAmount", typeof(String));
            if (lstChallan != null)
            {
                foreach (var r in lstChallan)
                {
                    DataRow dr = dtChallan.NewRow();
                    dr["SubDetailID"] = r.SubDetailID;
                    dr["ChallanNo"] = r.ChallanNo;
                    dr["ChallanDate"] = r.ChallanDate;
                    dr["BSRNo"] = r.BSRNo;
                    dr["TaxAmount"] = r.TaxAmount;
                    dtChallan.Rows.Add(dr);
                }
            }

            DataTable dtOtherIncome = new DataTable();
            dtOtherIncome.Columns.Add("ITCalcID", typeof(Int32));
            dtOtherIncome.Columns.Add("OtherSourceId", typeof(String));
            dtOtherIncome.Columns.Add("OtherIncome", typeof(String));
            if (lstOtherIncome != null)
            {
                foreach (var r in lstOtherIncome)
                {
                    DataRow dr = dtOtherIncome.NewRow();
                    dr["ITCalcID"] = r.ITCalcID;
                    dr["OtherSourceId"] = r.OtherSourceId;
                    dr["OtherIncome"] = r.OtherIncome;
                    dtOtherIncome.Rows.Add(dr);
                }
            }
            //ItaxEmpDetailsID, 'ITaxTypeID' LabelAmount, 'SavingAmount'

            DataTable dtParentDeduction6 = new DataTable();
            dtParentDeduction6.Columns.Add("ItaxEmpDetailsID", typeof(String));
            dtParentDeduction6.Columns.Add("ITaxTypeID", typeof(String));
            dtParentDeduction6.Columns.Add("LabelAmount", typeof(String));
            dtParentDeduction6.Columns.Add("SavingAmount", typeof(String));
            if (lstParentDeduction6 != null)
            {
                foreach (var r in lstParentDeduction6)
                {
                    DataRow dr = dtParentDeduction6.NewRow();
                    dr["ItaxEmpDetailsID"] = r.ItaxEmpDetailsID;
                    dr["ITaxTypeID"] = r.ITaxTypeID;
                    dr["LabelAmount"] = r.LabelAmount;
                    dr["SavingAmount"] = r.SavingAmount;
                    dtParentDeduction6.Rows.Add(dr);
                }
            }

            DataTable dtChildDeduction6 = new DataTable();
            dtChildDeduction6.Columns.Add("ItaxEmpDetailsID", typeof(String));
            dtChildDeduction6.Columns.Add("ITaxTypeID", typeof(String));
            dtChildDeduction6.Columns.Add("ItaxSubSavingTypeID", typeof(String));
            dtChildDeduction6.Columns.Add("LabelAmount", typeof(String));
            dtChildDeduction6.Columns.Add("SavingAmount", typeof(String));
            if (lstChildDeduction6 != null)
            {
                foreach (var r in lstChildDeduction6)
                {
                    DataRow dr = dtChildDeduction6.NewRow();
                    dr["ItaxEmpDetailsID"] = r.ItaxEmpDetailsID;
                    dr["ITaxTypeID"] = r.ITaxTypeID;
                    dr["ItaxSubSavingTypeID"] = r.ItaxSubSavingTypeID;
                    dr["LabelAmount"] = r.LabelAmount;
                    dr["SavingAmount"] = r.SavingAmount;
                    dtChildDeduction6.Rows.Add(dr);
                }
            }

            DataTable dtGlobal = new DataTable();
            try
            {
                cmd.Parameters.AddWithValue("@TransType", TransType);
                cmd.Parameters.AddWithValue("@salFinYear", SalFinYear);
                cmd.Parameters.AddWithValue("@EmpNo", EmpID);
                cmd.Parameters.AddWithValue("@SectorID", "");
                cmd.Parameters.AddWithValue("@UserID", sd.UserID);
                cmd.Parameters.AddWithValue("@AssesmentYear", AssessmentYear);
                cmd.Parameters.AddWithValue("@TaxableIncome", 0.00);
                cmd.Parameters.AddWithValue("@Typ_EmpItax_ID_AMT", dtMainTable);
                cmd.Parameters.AddWithValue("@Typ_EmpItaxOtherChallan_details", dtChallan);
                cmd.Parameters.AddWithValue("@TYP_EmpItaxOtherIncome", dtOtherIncome);
                cmd.Parameters.AddWithValue("@TYP_EmpItaxParentSaving", dtParentDeduction6);
                cmd.Parameters.AddWithValue("@TYP_EmpItaxChildSaving", dtChildDeduction6);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                con.Open();
                da.Fill(ds);

                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Rows Found", dtGlobal.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally { con.Close(); }
            return Json(cr);
        }





        class MainTable
        {
            public string ITCalcID { get; set; }
            public string Amount { get; set; }
        }
        class Challan
        {
            public string SubDetailID { get; set; }
            public string ChallanNo { get; set; }
            public string ChallanDate { get; set; }
            public string BSRNo { get; set; }
            public string TaxAmount { get; set; }
        }

        class OtherIncomeTable
        {
            public int? ITCalcID { get; set; }
            public string OtherSourceId { get; set; }
            public string OtherIncome { get; set; }
        }

        class ParentDeduction6
        {
            
            public string ItaxEmpDetailsID { get; set; }
            public string ITaxTypeID { get; set; }
            public string LabelAmount { get; set; }
            public string SavingAmount { get; set; }
        }

        class ChildDeduction6
        {

            public string ItaxEmpDetailsID { get; set; }
            public string ITaxTypeID { get; set; }
            public string ItaxSubSavingTypeID { get; set; }
            public string LabelAmount { get; set; }
            public string SavingAmount { get; set; }
        }

        //====================================== Employee Master ===============================================

        //EmployeeMaster emp = new EmployeeMaster();

        [HttpPost]
        public JsonResult Get_Center(int SectorID)
        {
            try
            {
                List<Location_MD> lstCenter = new Location().Get_Center(SectorID);
                cr.Data = lstCenter;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", lstCenter.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_District(int StateID)
        {
            try
            {
                List<District_MD> lstDistrict = new District().Get_District(StateID);
                cr.Data = lstDistrict;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", lstDistrict.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_BankBranch(int BankID)
        {
            try
            {
                List<Branch_MD> lstBranch = new Branch().Get_Branch(BankID);
                cr.Data = lstBranch;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", lstBranch.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_IFSCMICR(int BranchID)
        {
            try
            {
                List<Branch_MD> listIFSCMICR = new EmployeeMaster_ITAX().Get_IFSCMICR(BranchID);
                cr.Data = listIFSCMICR;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listIFSCMICR.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_DA(string EmpType)
        {
            try
            {
                List<DA_MD> listDA = new EmployeeMaster_ITAX().Get_DA(EmpType);
                cr.Data = listDA;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listDA.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_HRA(string EmpType)
        {
            try
            {
                List<HRA_MD> listHRA = new EmployeeMaster_ITAX().Get_HRA(EmpType);
                cr.Data = listHRA;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listHRA.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_PFPercentage(string EmpType)
        {
            try
            {
                List<PF_Percentage_Value> listPFPercentage = new EmployeeMaster_ITAX().Get_PFPercentage(EmpType);
                cr.Data = listPFPercentage;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", listPFPercentage.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_PayScaleType(string EmpType)
        {
            try
            {
                List<PayScaleType_MD> listPayScaleType = new EmployeeMaster_ITAX().Get_PayScaleType(EmpType);
                cr.Data = listPayScaleType;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listPayScaleType.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_PayScale(string PayScaleType)
        {
            try
            {
                List<PayScale_MD> listPayScale = new EmployeeMaster_ITAX().Get_PayScale(PayScaleType);
                cr.Data = listPayScale;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listPayScale.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Search_IFSCAutoComplete(string IFSC)
        {
            try
            {
                List<Branch_MD> listBranch = new Branch().Get_Auto_IFSC(IFSC);
                cr.Data = listBranch;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listBranch.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_BankandBranch(string BranchID)
        {
            try
            {
                List<Branch_MD> listBranch = new Branch().Get_BankandBranch(BranchID);
                cr.Data = listBranch;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listBranch.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }



        [HttpPost]
        public JsonResult Search_EmpNameAutoComplete(string EmpName)
        {
            try
            {
                List<EmployeeMasterDetails> listEmpName = new EmployeeMaster_ITAX().Get_EmpNameAuto_Complete(EmpName);
                cr.Data = listEmpName;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listEmpName.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_Employee(string EmployeeNo, string SectorID, string SalMonth)
        {
            try
            {
                DataSet listEmp = new EmployeeMaster_ITAX().Get_Employee(EmployeeNo, SectorID, SalMonth, sd.UserID);
                cr.Data = listEmp;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listEmp.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult HideShow(string HRACat)
        {
            try
            {
                DataTable dt = new House().Get_HideShow(HRACat);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save(Employee_SaveData Master, string Detail)
        {
            try
            {
                EmployeeMaster_ITAX emp = new EmployeeMaster_ITAX();
                string result = emp.Save(Master, Detail, sd.UserID, sd.CurSalFinYear);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Update(Employee_SaveData Master, string Detail)
        {
            try
            {
                EmployeeMaster_ITAX emp = new EmployeeMaster_ITAX();
                string result = emp.Update(Master, Detail, sd.UserID, sd.CurSalFinYear);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Show_PayDetails(string EmpNo)
        {
            try
            {
                List<Employee_PayDetails> lstEmployee_PayDetails = new EmployeeMaster_ITAX().Show_PayDetails(EmpNo);
                cr.Data = lstEmployee_PayDetails;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", lstEmployee_PayDetails.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Check_PayScaleRange(string Amount, string PayScaleID)
        {
            try
            {
                DataTable dt = new EmployeeMaster_ITAX().Check_PayScaleRange(Amount, PayScaleID);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Add_PayDetails(string EmpNo, string EmpType, string EmpCategory, string PayScaleID, string Amount, string DAID,
                                         string hraCategory, string HRAID, string HRARent, string FixedHRAAmount, string SpouseAmount, string isHealth,
                                         string HealthFrom, string HealthTo, string IRID, string PFID, string PFAmount, string PhysicalHandicapped)
        {
            try
            {
                DataTable dt = new EmployeeMaster_ITAX().Add_PayDetails(EmpNo, EmpType, EmpCategory, PayScaleID, Amount, DAID, hraCategory, HRAID, HRARent, FixedHRAAmount,
                                            SpouseAmount, isHealth, HealthFrom, HealthTo, sd.UserID, IRID, PFID, PFAmount, PhysicalHandicapped);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_IR(string EmpType)
        {
            try
            {
                List<IR_MD> lstIR = new IR().Bind_IR(EmpType);
                cr.Data = lstIR;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", lstIR.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_Status(string CategoryID)
        {
            try
            {
                List<Status_MD> lstStatus = new EmployeeMaster_ITAX().Get_Status(CategoryID);
                cr.Data = lstStatus;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", lstStatus.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_EarnDeduction(string EmpCategory, string StatusID)
        {
            try
            {
                List<EarnDeduction_MD> lstEarnDeduction = new EarnDeduction().Get_EarnDeduction(EmpCategory, StatusID);
                cr.Data = lstEarnDeduction;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", lstEarnDeduction.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_FieldName()
        {
            try
            {
                int FormID = 20;
                DataTable lst = new EmployeeMaster_ITAX().Get_FieldName(FormID);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Check_EmpMasterValidation(Employee_SaveData Master)
        {
            try
            {
                EmployeeMaster_ITAX emp = new EmployeeMaster_ITAX();
                DataTable result = emp.Check_EmpMasterValidation(Master);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", result.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult MedicalAllowance(string TransactionType, string EmployeeID, string FinYear, string MedicalAllowance)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("ecsc.MST_EmpWiseMediAllowance_SP", con);
            cmd.CommandType = CommandType.StoredProcedure;

            DataTable dtGlobal = new DataTable();
            try
            {
                cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
                cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                cmd.Parameters.AddWithValue("@FinYear", FinYear);
                cmd.Parameters.AddWithValue("@MedicalAllowance", MedicalAllowance);
                cmd.Parameters.AddWithValue("@InsertedBy", sd.UserID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                con.Open();
                da.Fill(ds);

                cr.Data = ds.GetXml();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Rows Found", dtGlobal.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally { con.Close(); }
            return Json(cr);
        }






    }//ALL END
}