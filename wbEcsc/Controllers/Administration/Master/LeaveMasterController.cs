﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Codes.BLL.Master;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;


namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class LeaveMasterController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sd;

        public LeaveMasterController()
        {
            sd = SessionContext.SessionData;
        }
        // GET: LeaveMaster
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SaveUpdate_LeaveMaster(string LeaveTypeID, string leaveDesc,string abbv,string gender,string maxInYear,string maxSave,string maxInServicePeriod,int? isActive, int? isRound, int? isLeaveTour)
        {
            try
            {
                //string hdnLeaveTypeID, string txtLeaveDesc, string txtAbbv, string ddlGender, string txtMaxInYear, string txtMaxSave, string txtMaxInServicePeriod, int? UserID
                List<LeaveMaster_MD> lst = new LeaveMaster().SaveUpdate_LeaveMaster(LeaveTypeID, leaveDesc, abbv, gender, maxInYear, maxSave, maxInServicePeriod, isActive, isRound, isLeaveTour, Convert.ToInt32(sd.UserID));
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Delete_LeaveMaster(string LeaveTypeID)
        {
            try
            {
                List<LeaveMaster_MD> lst = new LeaveMaster().Delete_LeaveMaster(LeaveTypeID, Convert.ToInt32(sd.UserID));
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_FieldName()
        {
            try
            {
                int FormID = 16;
                List<LeaveMaster_MD> lst = new LeaveMaster().Get_FieldName(FormID);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }


        [HttpPost]
        public JsonResult Get_GridDATA(string LVID)
        {
            try
            {
                LeaveMaster grid = new LeaveMaster();
                DataTable ds = grid.Get_GridDATA(LVID);
                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

    } 
}