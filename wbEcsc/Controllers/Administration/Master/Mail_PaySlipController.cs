﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.BLL.Transaction;
using wbEcsc.ViewModels;
using wbEcsc.Models;


namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class Mail_PaySlipController : Controller
    {
        Models.ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public Mail_PaySlipController()
        {
            sData = SessionContext.SessionData;
        }


        public ActionResult Index()
        {
            sData.Sectors = new EmployeeMaster().Get_Sector((SessionContext.CurrentUser as IInternalUser).UserID);
            SessionContext.SessionData.Sectors = sData.Sectors;
            SalaryGeneration_MD obj_SalMonthDetails = new SalaryGeneration_MD();
            if (sData.Sectors.Count == 1)
            {
                foreach (var sector in sData.Sectors)
                {
                    sData.CurrSector = sector.SectorID;
                }

                if (sData.CurrSector > 0)
                {
                    obj_SalMonthDetails = new SalaryGeneration().Get_SalPayMonth(sData.CurrSector, SessionContext.SessionData.CurSalFinYear as string);

                    if (obj_SalMonthDetails != null)
                    {
                        ViewBag.MaxSalMonth = obj_SalMonthDetails.MaxSalMonth;
                        ViewBag.MaxSalMonthID = obj_SalMonthDetails.MaxSalMonthID;
                    }

                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult Populate_Employee(int SalMonthID, string SectorID)
        {
            try
            {
                DataTable dtlist = new EmployeeMaster().Get_Email_Eligible_Employee(SalMonthID, SectorID, sData.UserID);
                cr.Data = dtlist;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", dtlist.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Start_Email(int SalMonthID, string SectorID)
        {
            try
            {
                MailPaySlip obj = new MailPaySlip();
                string result = obj.Generate_PaySlip(SalMonthID, SectorID);

                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}