﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.Authenticator;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using wbEcsc.Models.Account.Logins;
using wbEcsc.Models.AppResource;
using wbEcsc.Utils.EncryptionDecrytion;

namespace wbEcsc.Controllers.Administration
{
    [SecuredFilter]
    public class AdminHomeController : Controller
    {
        ClientJsonResult cj = new ClientJsonResult();

        IEncryptDecrypt chiperAlgorithm;
        public AdminHomeController()
        {

            
        }
        // GET: AdminHome
        public ActionResult HomeUI()
        {
            return View("~/Views/Administration/AdminHome/HomeUI.cshtml");
        }

        [HttpPost]
        public JsonResult GetApplicationApprovalUrl()
        {
            
            
            try
            {
                EcscSetting_Bll bll = new EcscSetting_Bll();
                EcscSetting setting = bll.GetSettings();
                if (setting == null)
                {
                    throw new NullReferenceException("Ecsc Settings not properly configured");
                }
                else if (string.IsNullOrEmpty(setting.ApprovalServerAddress))
                {
                    throw new NullReferenceException("Approval Address not configured on Ecsc Settings");
                }
                else if (string.IsNullOrEmpty(setting.ChiperKey))
                {
                    throw new NullReferenceException("Chiper Key not configured on Ecsc Settings");
                }

                chiperAlgorithm = new AES_Algorithm(setting.ChiperKey);


                AdministrativeUser user= SessionContext.CurrentUser as AdministrativeUser;
                
                var obj = new { UserID = user.UserID, DateTime = DateTime.Now.ToString() };
                JavaScriptSerializer js = new JavaScriptSerializer();
                string serializedData = js.Serialize(obj);
                string encryptedUserID = chiperAlgorithm.Encrypt(serializedData);
                string urlEncodedData = HttpUtility.UrlEncode(encryptedUserID);

                cj.Data = new 
                {
                    Url = string.Format("{0}/Main/ProcurementApproval.aspx?Data={1}", setting.ApprovalServerAddress, urlEncodedData)
                };
                cj.Status = ResponseStatus.SUCCESS;
                cj.Message = "Url successfully  generated";
            }
            catch (SqlException)
            {
                cj.Message = "Database Error! Please try again later";
                cj.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            catch (SecurityException ex)
            {
                cj.Message = ex.Message;
                cj.Status = ResponseStatus.UNAUTHORIZED;
            }
            catch (NullReferenceException ex)
            {
                cj.Message = ex.Message;
                cj.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            catch (Exception)
            {
                cj.Message = "Unknown exception occured! Please contact with vendor";
                cj.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cj);
        }
    }
}