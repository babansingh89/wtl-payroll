﻿using System;
using System.Configuration;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions;
using CrystalDecisions.Shared;
using System.Web.Services;
using wbEcsc.App_Codes;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using System.Net;
using System.Net.Mail;
using System.Net.Security;

namespace wbEcsc
{
    public partial class MailPaySlip : System.Web.UI.Page
    {
        string SalMonth = "";
        string SalMonthID = "";
        string SectorID = "";
        string ExportFileName = "";

        ExportFormatType formatType = ExportFormatType.NoFormat;
        CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        IInternalUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!SessionContext.IsAuthenticated)
            {
                Response.Redirect("/Error/Error401/Index", true);
                return;
            }
            user = SessionContext.CurrentUser as IInternalUser;
            if (HttpContext.Current.Request.QueryString["SectorID"] != null)
                SectorID = HttpContext.Current.Request.QueryString["SectorID"].ToString();

            if (HttpContext.Current.Request.QueryString["SalMonthID"] != null)
                SalMonthID = HttpContext.Current.Request.QueryString["SalMonthID"].ToString();

            if (HttpContext.Current.Request.QueryString["SalMonth"] != null)
                SalMonth = HttpContext.Current.Request.QueryString["SalMonth"].ToString();



            //Generate_PaySlip();
        }

        public string Generate_PaySlip(int SalMonthID, string SectroID)
        {
            try
            {
                SqlConnection con = new SqlConnection(conString);
                SqlCommand cmd = new SqlCommand("Get_Employee_forEmpNo", con);
  cmd.Parameters.AddWithValue("@SalMonthID", SalMonthID);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                DataTable dtEmailSetting = Connection_Details.Get_EmailSettings();

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string rptName = "PaySlip_new_LocationWISE_A5.rpt";
                        string ReportName = "PaySlip_";

                        crystalReport.Load(Server.MapPath("~/Reports/" + rptName.Trim()));
                        crystalReport.RecordSelectionFormula = "{MST_Employee.EmployeeID}=" + dt.Rows[i]["EmployeeID"];

                        string[] str = dt.Rows[i]["SalMonth"].ToString().Split('/');

                        ExportFileName = ReportName + dt.Rows[i]["EmpNo"] + "_" + Get_Month(str[0]) + "-" + str[1] + ".pdf";

                        crystalReport.Refresh();

                        string server = Connection_Details.ServerName();
                        string database = Connection_Details.DatabaseName();
                        string userid = Connection_Details.UserID();
                        string password = Connection_Details.Password();


                        crystalReport.DataSourceConnections[0].SetConnection(server, database, userid, password);
                        crystalReport.DataSourceConnections[0].IntegratedSecurity = false;
                        crystalReport.SetDatabaseLogon(server, database, userid, password);

                        crystalReport.VerifyDatabase();
                       // CrystalReportViewer1.ReportSource = crystalReport;

                        ExportAndEmail(crystalReport, ExportFileName, dt.Rows[i]["Email"].ToString(), dtEmailSetting, dt.Rows[i]["EmployeeID"].ToString());
                    }
                   
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return "success";
        }

        protected void ExportAndEmail(ReportDocument crystalReport, string FileName, string Email, DataTable dtEmailSetting, string EmployeeID)
        {
            try
            {
                if (Email != "")
                {
                    if (dtEmailSetting != null)
                    {
                        SmtpClient client = new SmtpClient();
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.EnableSsl = true;
                        client.Host = dtEmailSetting.Rows[0]["SMTP"].ToString(); //"smtp.gmail.com";
                        client.Port = Convert.ToInt32(dtEmailSetting.Rows[0]["Port"].ToString()); //587;

                        System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(dtEmailSetting.Rows[0]["UserName"].ToString(), dtEmailSetting.Rows[0]["Password"].ToString());
                        client.UseDefaultCredentials = false;
                        client.Credentials = credentials;
                        MailMessage msg = new MailMessage();
                        msg.From = new MailAddress(dtEmailSetting.Rows[0]["FromEmail"].ToString());

                        msg.To.Add(new MailAddress(Email));

                        string[] str = FileName.Split('.');
                        msg.Subject = str[0];
                        msg.IsBodyHtml = true;
                        msg.Body = "";

                        msg.Attachments.Add(new Attachment(crystalReport.ExportToStream(ExportFormatType.PortableDocFormat), FileName));
                        client.Send(msg);
                        client.Timeout = 1000;

                        Connection_Details.Update_EmailSetting(EmployeeID, "Y", "success");
                    }
                    else
                    {
                        Connection_Details.Update_EmailSetting(EmployeeID, "N", "Email Sending Credentials are not available.");
                    }
                }
                else
                {
                    Connection_Details.Update_EmailSetting(EmployeeID, "N", "Email-ID is not available.");
                }
            }
            catch (Exception ex)
            {
                Connection_Details.Update_EmailSetting(EmployeeID, "N", ex.Message);
            }
            //////https://myaccount.google.com/lesssecureapps
        }

        public string Get_Month(string SalMonthID)
        {
            string Month = "";
            switch (SalMonthID)
            {
                case "01":
                    Month = "Jan";
                    break;
                case "02":
                    Month = "Feb";
                    break;
                case "03":
                    Month = "Mar";
                    break;
                case "04":
                    Month = "Apr";
                    break;
                case "05":
                    Month = "May";
                    break;
                case "06":
                    Month = "June";
                    break;
                case "07":
                    Month = "Jul";
                    break;
                case "08":
                    Month = "Aug";
                    break;
                case "09":
                    Month = "Sep";
                    break;
                case "10":
                    Month = "Oct";
                    break;
                case "11":
                    Month = "Nov";
                    break;
                case "12":
                    Month = "Dec";
                    break;
            }

            return Month;
        }

    }
}