﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;

namespace wbEcsc.Controllers.Administration.Master
{
    public class EDInputController : Controller
    {
      
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public EDInputController()
        {
            sData = SessionContext.SessionData;
        }
        [SecuredFilter]
        public ActionResult Index(int extension)
        {
            ViewBag.EDID = extension;
            sData.Sectors = new EmployeeMaster().Get_Sector((SessionContext.CurrentUser as IInternalUser).UserID);
            SessionContext.SessionData.Sectors = sData.Sectors;
            SalaryGeneration_MD obj_SalMonthDetails = new SalaryGeneration_MD();
            EDDetail_MD obj_EDDetails = new EDDetail_MD();
            List<Location_MD> lstCenter = new List<Location_MD>();

            if (sData.Sectors.Count == 1)
            {
                foreach (var sector in sData.Sectors)
                {
                    sData.CurrSector = sector.SectorID;
                }

                if (sData.CurrSector > 0)
                {
                    obj_SalMonthDetails = new App_Codes.BLL.Transaction.SalaryGeneration().Get_SalPayMonth(sData.CurrSector, SessionContext.SessionData.CurSalFinYear as string);

                    if (obj_SalMonthDetails != null)
                    {
                        ViewBag.MaxSalMonth = obj_SalMonthDetails.MaxSalMonth;
                        ViewBag.MaxSalMonthID = obj_SalMonthDetails.MaxSalMonthID;
                    }

                    lstCenter = new Location().Get_Center(sData.CurrSector);
                    ViewBag.lstCenter = lstCenter;
                }
            }

            obj_EDDetails = new App_Codes.EDInputs().EDDetails(extension);
            ViewBag.EDName = obj_EDDetails.EDName;
            ViewBag.MenuID = obj_EDDetails.MenuID;



            return View();
        }

        [HttpPost]
        public JsonResult Search_Details(string SectorID, string LocationID, string EDID, string SalFinYear, string SalMonthID, string MenuID)
        {
            try
            {
                List<EDInputs_MD> listEmp = new EDInputs().Get_EmpDetails(SectorID, LocationID, EDID, SalFinYear, SalMonthID, MenuID, sData.UserID);
                cr.Data = listEmp;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listEmp.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Update_Details_RowWise(string EmpNo, string EDID, string Amount ,string SectorID, string FinYear)
        {
            try
            {
                string Result = new EDInputs().Update_Details_RowWise(EmpNo, EDID, Amount, SectorID, FinYear, sData.UserID);
                cr.Data = Result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", Result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}