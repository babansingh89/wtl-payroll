﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace wbEcsc.Controllers.Administration
{
    [SecuredFilter]
    public class MenuManagerController : Controller
    {
        ClientJsonResult cj = new ClientJsonResult();
        // GET: MenuManager
        [HttpPost]
       public JsonResult GetMenus()
        {
            Menu_BLL bll = new Menu_BLL();
            List<Menu> lst = new List<Menu>();
            try
            {
                AdministrativeUser user = SessionContext.CurrentUser as AdministrativeUser;
                lst = bll.GetMenus(user.UserID);
                cj.Data = lst;
                cj.Message = string.Format("{0} Menu Item Found",lst.Count);
                cj.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception)
            {
                cj.Data = lst;
                cj.Message = string.Format("{0} Menu Item Found", lst.Count);
                cj.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cj);
        }
    }
}