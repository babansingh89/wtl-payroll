﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.BLL.Transaction;
using wbEcsc.ViewModels;
using wbEcsc.Models;




namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class SalaryGenerationController : Controller
    {
        Models.ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public SalaryGenerationController()
        {
            sData = SessionContext.SessionData;
        }
        
        public ActionResult Index()
        {
            sData.Sectors = new EmployeeMaster().Get_Sector((SessionContext.CurrentUser as IInternalUser).UserID);
            SessionContext.SessionData.Sectors = sData.Sectors;
            SalaryGeneration_MD obj_SalMonthDetails = new SalaryGeneration_MD();
            if (sData.Sectors.Count == 1)
            {
                foreach (var sector in sData.Sectors)
                {
                    sData.CurrSector = sector.SectorID;
                }

                if (sData.CurrSector > 0)
                {
                    obj_SalMonthDetails = new SalaryGeneration().Get_SalPayMonth(sData.CurrSector, SessionContext.SessionData.CurSalFinYear as string);

                    ViewBag.MaxSalMonth = obj_SalMonthDetails.MaxSalMonth;
                    ViewBag.MaxSalMonthID = obj_SalMonthDetails.MaxSalMonthID;
                }
            }

            return View("~/Views/Shared/SalaryGeneration/SalaryGeneration.cshtml");
        }

        [HttpPost]
        public JsonResult Salary_Process(int SalMonthID, int SectorID, string SalFinYear)
        {
            try
            {
                string result = new SalaryGeneration().Salary_Process(SalMonthID, SectorID, SalFinYear, sData.UserID);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "success");
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}