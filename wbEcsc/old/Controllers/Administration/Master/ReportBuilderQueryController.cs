﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.BLL.Transaction;
using wbEcsc.ViewModels;
using wbEcsc.Models;
using wbEcsc.App_Codes.BLL.AppReport;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class ReportBuilderQueryController : Controller
    {
        DynamicReport obj = new DynamicReport();
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public ReportBuilderQueryController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Save_Header(string reportName, string reportHeader, string reportQuery, string ParameterTypes)
        {

            try
            {
                string result = new wbEcsc.App_Codes.BLL.AppReport.DynamicReport().Save_Header(reportName, reportHeader, reportQuery, ParameterTypes, sData.UserID);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}