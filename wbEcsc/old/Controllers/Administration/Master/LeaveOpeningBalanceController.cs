﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Application;
using wbEcsc.Models;
using System.Data;
using wbEcsc.App_Start;
using wbEcsc.App_Codes;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class LeaveOpeningBalanceController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();       
        LeaveOpeningBalanceBLL bll = new LeaveOpeningBalanceBLL();
        EmployeeMaster empbll = new EmployeeMaster();


        SessionData sd;
        public LeaveOpeningBalanceController()
        {
            sd = SessionContext.SessionData;
        }



        // GET: LeaveOpeningBalance
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public JsonResult Get_FieldName()
        {
            try
            {
                cr.Data = bll.GetField();
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AutocompleteEmployeeName(string SectorID, string empaname)
        {
            try
            {
                cr.Data = bll.Get_Employee(SectorID, empaname);
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult AutocompleteAbbravition(string Abbraviation)
        {
            try
            {
                cr.Data = bll.Get_LeaveAbbraviation(Abbraviation);
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_Data(string sectorid, string openingDate, string EmpID, string AbbraviationID)
        {
            try
            {
                cr.Data = bll.GetData(sectorid);
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save(LeaveOpeningBalance_MD md)
        {
            try
            {
                DataTable dt = bll.Save(md);

                var data = new { FocusCode = dt.Rows[0][0], IsSuccess = dt.Rows[0][1] };

                cr.Data = data;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = (string)dt.Rows[0][2];
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Delete(LeaveOpeningBalance_MD md)
        {
            try
            {
                DataTable dt = bll.Delete(md);
                cr.Data = null;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = (string)dt.Rows[0][1];
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}