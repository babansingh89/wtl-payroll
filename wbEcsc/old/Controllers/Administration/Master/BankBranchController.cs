﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.BLL;
using wbEcsc.ViewModels;
using System.Data;
using wbEcsc.Models;
using wbEcsc.App_Start;

namespace wbEcsc.Controllers.Master
{
    [SecuredFilter]
    public class BankBranchController : Controller
    {
        // GET: BankBranch
        Branch bra = new Branch();
        Bank bnk = new Bank();
        ClientJsonResult cr = new ClientJsonResult();
        public ActionResult Index()
        {
            try
            {
                //Branch_MD brch = new Branch_MD();
                //BankBranchViewModel brch_vm = new BankBranchViewModel();
                //brch_vm.SelectedBranch = brch;
                //brch_vm.lstField = bra.GetField();
                //brch_vm.BranchList = bra.Get_Branch();      
                //if(id!=null)
                //{
                //    brch_vm.SelectedBranch = bra.Get_BankandBranch(id ?? 0);
                //}
                return View();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }          
        }

        [HttpPost]
        public JsonResult GetFieldName()
        {
            try
            {
                List<Field> lstField = new List<Field>();
                lstField= bra.GetField();
                cr.Data = lstField;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = "";
                return Json(cr);
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
                return Json(cr);
            }
        }

        [HttpPost]
        public JsonResult Get_Bank(string strprefix)
        {
            try
            {
                List<Bank_MD> bnklist = new List<Bank_MD>();              
                bnklist = bnk.Get_Bank();
                var objbank = (from b in bnklist
                               where b.BankName.ToUpper().StartsWith(strprefix.ToUpper())
                               select new { b.BankID,b.BankName }).ToList();                

                return Json(objbank);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpPost]
        public JsonResult Save(Branch_MD obj)
        {
            try
            {             
               DataTable dt=bra.Save(obj);
                cr.Data = dt.Rows[0][0];
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = dt.Rows[0][1].ToString();
                return Json(cr);
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
                return Json(cr);
            }          
        }

        [HttpPost]
        public JsonResult GetBankBranch()
        {
            try
            {
                List<Branch_MD> lst = new List<Branch_MD>();
                lst = bra.Get_Branch();
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = "";
                return Json(cr);
            }
            catch (Exception ex)
            {                
                cr.Data = null;
                cr.Status =ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message =ex.Message;
                return Json(cr);
            }

        }
    }
}