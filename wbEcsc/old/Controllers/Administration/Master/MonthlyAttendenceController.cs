﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Codes.BLL.Master;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Configuration;
using System.IO;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class MonthlyAttendenceController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();

        SessionData sd;

        public MonthlyAttendenceController()
        {
            sd = SessionContext.SessionData;
        }

        // GET: MonthlyAttendence
        public ActionResult Index()
        {
            Logo lObj = new Logo();
            Logo_MD modelObj = lObj.GetAppsInfo();
            ViewBag.Logo = modelObj.LogoPath;
            ViewBag.Header = modelObj.OfficeName;
            ViewBag.address = modelObj.Address;
            
            return View();
        }

        [HttpPost]
        public JsonResult SelectMonthYearData(string TransactionType, string Year)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = new MonthlyAttendenceBAL().SelectMonthYearData_DAL(TransactionType, Year, Convert.ToString(sd.CurrSector), Convert.ToString(sd.UserID));
                if (ds.Tables.Count > 0)
                {
                    cr.Data = ds.GetXml();
                }
                else
                {
                    cr.Data = null;
                }
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult SelectDetailData(string TransactionType, string Year, string SalMonthId, string FromDate, string ToDate)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = new MonthlyAttendenceBAL().SelectDetailData_DAL(TransactionType, Year, Convert.ToString(sd.CurrSector), SalMonthId, Convert.ToString(sd.UserID), FromDate, ToDate);
                if (ds.Tables.Count > 0)
                {
                    cr.Data = ds.GetXml();
                }
                else
                {
                    cr.Data = null;
                }
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult UpdateDetailData(string TransactionType, string Year, string SalMonthID, string EmployeeID, string AttDays, string TourDays
            , string ClDays, string MlDays, string ElDays, string OthLvDays, string ActOffDays, string ActHoliday, string LwpDays
            , string AdjustTiffinDays, string TiffinAllowance, string LateAtt)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = new MonthlyAttendenceBAL().UpdateDetailData_DAL(TransactionType, Year, Convert.ToString(sd.CurrSector), SalMonthID, Convert.ToString(sd.UserID)
                    , EmployeeID, AttDays, TourDays, ClDays, MlDays, ElDays, OthLvDays, ActOffDays, ActHoliday, LwpDays, AdjustTiffinDays, TiffinAllowance, LateAtt);
                if (ds.Tables.Count > 0)
                {
                    cr.Data = ds.GetXml();
                }
                else
                {
                    cr.Data = null;
                }
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult LeaveEdit(string TransactionType, string SalMonthID, string EmployeeID, string yr)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = new MonthlyAttendenceBAL().LeaveEdit_DAL(TransactionType, Convert.ToString(sd.CurrSector), SalMonthID, EmployeeID, yr, Convert.ToString(sd.UserID));
                if (ds.Tables.Count > 0)
                {
                    cr.Data = ds.GetXml();
                }
                else
                {
                    cr.Data = null;
                }
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult LeaveBalance(string TransactionType, string Year, string SalMonthID, string EmployeeID)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = new MonthlyAttendenceBAL().LeaveBalance_DAL(TransactionType, Year, Convert.ToString(sd.CurrSector), SalMonthID, Convert.ToString(sd.UserID), EmployeeID);
                if (ds.Tables.Count > 0)
                {
                    cr.Data = ds.GetXml();
                }
                else
                {
                    cr.Data = null;
                }
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult LeaveTypeDetails(string TransactionType, string Year, string SalMonthID, string EmployeeID, string LvId)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = new MonthlyAttendenceBAL().LeaveDetails_DAL(TransactionType, Year, Convert.ToString(sd.CurrSector), SalMonthID, Convert.ToString(sd.UserID), EmployeeID, LvId);
                if (ds.Tables.Count > 0)
                {
                    cr.Data = ds.GetXml();
                }
                else
                {
                    cr.Data = null;
                }
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult CompanyDetails()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = new MonthlyAttendenceBAL().CompanyDetails_DAL();
                if (ds.Tables.Count > 0)
                {
                    cr.Data = ds.GetXml();
                }
                else
                {
                    cr.Data = null;
                }
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult ChangeHoliday(string TransactionType, string Year, string SalMonthID, string EmployeeID, string AttDays, string ActHoliday, string ChangeHoliday, string FromDate, string ToDate)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = new MonthlyAttendenceBAL().ChangeHoliday_DAL(TransactionType, Year, Convert.ToString(sd.CurrSector), SalMonthID, Convert.ToString(sd.UserID), EmployeeID, AttDays, ActHoliday, ChangeHoliday, FromDate, ToDate);
                if (ds.Tables.Count > 0)
                {
                    cr.Data = ds.GetXml();
                }
                else
                {
                    cr.Data = null;
                }
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult ChangeOffDays(string TransactionType, string Year, string SalMonthID, string EmployeeID, string AttDays, string ActOffDays, string ChOffDays, string FromDate, string ToDate)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = new MonthlyAttendenceBAL().ChangeOffDays_DAL(TransactionType, Year, Convert.ToString(sd.CurrSector), SalMonthID, Convert.ToString(sd.UserID), EmployeeID, AttDays, ActOffDays, ChOffDays, FromDate, ToDate);
                if (ds.Tables.Count > 0)
                {
                    cr.Data = ds.GetXml();
                }
                else
                {
                    cr.Data = null;
                }
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult UpdateSanction(string TransactionType, string Year, string SalMonthID, string LevelRemarks)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = new MonthlyAttendenceBAL().UpdateSanction_DAL(TransactionType, Year, Convert.ToString(sd.CurrSector), SalMonthID, Convert.ToString(sd.UserID), LevelRemarks);
                if (ds.Tables.Count > 0)
                {
                    cr.Data = ds.GetXml();
                }
                else
                {
                    cr.Data = null;
                }
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpGet]
        public ActionResult Print(string SalMonthID, string EmployeeID, string SectorID)
        {
            string reportName = ""; string fileName = "";

            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            try
            {

                reportName = "RPT_MonthlyAttendance.rpt";
                fileName = "MonthlyAttendance";

                crystalReport.Load(Server.MapPath("~/Reports/" + reportName));

                crystalReport.Refresh();

                string server = Connection_Details.ServerName();
                string database = Connection_Details.DatabaseName();
                string userid = Connection_Details.UserID();
                string password = Connection_Details.Password();


                crystalReport.DataSourceConnections[0].SetConnection(server, database, userid, password);
                crystalReport.DataSourceConnections[0].IntegratedSecurity = false;
                crystalReport.SetDatabaseLogon(server, database, userid, password);

                crystalReport.VerifyDatabase();
                crystalReport.SetParameterValue("@SalMonthID", SalMonthID);
                crystalReport.SetParameterValue("@EmployeeID", EmployeeID);
                crystalReport.SetParameterValue("@SectorID", SectorID);

                Stream stream = crystalReport.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/pdf", fileName + ".pdf");

            }
            catch (Exception ex)
            {
              
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
                return View("~/Views/Error/Error404/Index.cshtml");
            }
            finally
            {
                dbConn.Close();
            }
        }


    }//ALL END
}