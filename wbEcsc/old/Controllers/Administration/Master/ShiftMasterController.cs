﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL.Master;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class ShiftMasterController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sd;

        public ShiftMasterController()
        {
            sd = SessionContext.SessionData;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Get_FieldName()
        {
            try
            {
                int FormID = 23;
                List<LoanMasterVM> lst = new LoanMaster().Get_FieldName(FormID);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult InsertUpdateShiftMaster(ShiftMasterMD ShiftMasterMD)
        {
            try
            {
                ShiftMasterMD.UserID = Convert.ToInt32(sd.UserID);
                ShiftMasterMD.SectorID = sd.CurrSector;
                DataSet ds = new DataSet();
                ds = new ShiftMasterBAL().InsertUpdateShiftMaster_DAL(ShiftMasterMD);
                if (ds.Tables.Count > 0)
                {
                    cr.Data = ds.GetXml();
                }
                else
                {
                    cr.Data = null;
                }
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult SelectShift(ShiftMasterMD ShiftMasterMD)
        {
            try
            {
                ShiftMasterMD.SectorID = sd.CurrSector;
                DataSet ds = new DataSet();
                ds = new ShiftMasterBAL().Select_DAL(ShiftMasterMD);
                if (ds.Tables.Count > 0)
                {
                    cr.Data = ds.GetXml();
                }
                else
                {
                    cr.Data = null;
                }
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", ds.Tables.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }



    }//ALL END
}