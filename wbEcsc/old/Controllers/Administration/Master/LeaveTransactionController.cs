﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;
using System.Data;

namespace wbEcsc.Controllers.Administration.Master
{
    //[SecuredFilter]
    public class LeaveTransactionController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public LeaveTransactionController()
        {
            sData = SessionContext.SessionData;
        }

        public ActionResult Index()
        {
            List<LeaveTransaction_VM> lstReason = new LeaveTransaction_Bll().Get_Reasons();
            ViewBag.lstReason = lstReason;

            //Request.QueryString["BillNo"] == "" ? DBNull.Value : (object)Request.QueryString["BillNo"]
            //ViewState["RedirectEmpID"]
            ViewBag.EmpID = Request.QueryString["emp"];
            ViewBag.SalMonthID = Request.QueryString["sal"];
            ViewBag.LvFlag = Request.QueryString["flag"];
            ViewBag.Year = Request.QueryString["yr"];

            return View();
        }

        [HttpPost]
        public JsonResult Get_FieldName()
        {
            try
            {
                int FormID = 19;
                List<LeaveTransaction_VM> lst = new LeaveTransaction_Bll().Get_FieldName(FormID);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Search_EmpNameAutoComplete(string EmpName, string SectorID, string EmpNoName, string SalMonthId)
        {
            try
            {
                List<LeaveTransaction_VM> listEmpName = new LeaveTransaction_Bll().Get_EmpNameAuto_Complete(EmpName, SectorID, EmpNoName, sData.UserType, SalMonthId);
                cr.Data = listEmpName;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listEmpName.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Search_EmpNameAutoComplete_Search(string EmpName, string SectorID, string EmpId, string SalMonthId)
        {
            try
            {
                List<LeaveTransaction_VM> listEmpName = new LeaveTransaction_Bll().Get_EmpNameAuto_Complete_Search(EmpName, SectorID, sData.UserType, EmpId, SalMonthId);
                cr.Data = listEmpName;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", listEmpName.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Bind_TourLeave(int isTourLeave)
        {
            try
            {
                // 0- for Tour  // 1-for Leave
                List<LeaveTransaction_VM> listleave = new LeaveTransaction_Bll().Get_TourLeave(isTourLeave);
                cr.Data = listleave;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", listleave.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_LeaveBalance(string ApplicationNo, string ApplicationDate, string EmpID, string LeaveID, int isTourLeave)
        {
            try
            {
              DataTable dt = new LeaveTransaction_Bll().Get_LeaveBalance(ApplicationNo, ApplicationDate,  EmpID,  LeaveID,  isTourLeave);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save_LeaveTransaction(string LVAppNo, string LVAppDate, string EmployeeID, string LeaveID, string FromDate, string ToDate, string LeaveBal, string isRound, 
                                                string isLeaveTour, string NoofDays, string Remarks, string SalMonthID, string ReasonID, string LvFlag)
        {
            try
            {
                DataTable dt = new LeaveTransaction_Bll().Save_LeaveTransaction(LVAppNo, LVAppDate, EmployeeID,  LeaveID,  FromDate,  ToDate,  LeaveBal,  isRound, isLeaveTour,  NoofDays, Remarks, SalMonthID, ReasonID, sData.UserID, LvFlag);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Check_From_Date(string ApplicationNo,  string ApplicationDate, string EmpID, string FromDate, string ToDate)
        {
            try
            {
                DataTable dt = new LeaveTransaction_Bll().Check_From_Date(ApplicationNo, ApplicationDate, EmpID, FromDate, ToDate);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Check_To_Date(string ApplicationNo, string ApplicationDate, string EmpID, string FromDate, string ToDate, string LeaveBal, string isRound, string isLeave, string LeaveID)
        {
            try
            {
                DataTable dt = new LeaveTransaction_Bll().Check_To_Date(ApplicationNo, ApplicationDate, EmpID, FromDate, ToDate, LeaveBal, isRound, isLeave, LeaveID);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_Date_MinMax_Date()
        {
            try
            {
                DataTable dt = new LeaveTransaction_Bll().Get_Date_MinMax_Date();
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Delete_LeaveDetails(string ApplicationNo, string EmployeeID, string FromDate, string ToDate)
        {
            try
            {
                DataTable dt = new LeaveTransaction_Bll().Delete_LeaveDetails(ApplicationNo, EmployeeID, FromDate, ToDate, sData.UserID);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Center Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}