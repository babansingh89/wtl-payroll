﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Application;

namespace wbEcsc.Controllers.Administration.Master
{
    public class Employee_Increment_PerctController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public Employee_Increment_PerctController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Empwise_UpdateIncrement(string EmpNo, string EmpID, string Perct)
        {
            try
            {
                string result = new Employee_Increment_BLL().Empwise_UpdateIncrementPerct(EmpNo, EmpID, Perct, sData.UserID);
                cr.Data = result;
                cr.Message = result;
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
    }
}