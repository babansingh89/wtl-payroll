﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.BLL.Transaction;
using wbEcsc.ViewModels;
using wbEcsc.Models;
using wbEcsc.App_Codes.BLL.AppReport;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class ReportBuilderController : Controller
    {
        DynamicReport obj = new DynamicReport();
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public ReportBuilderController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            List<ReportBuilder_MD> lstReport = new wbEcsc.App_Codes.BLL.AppReport.DynamicReport().Load_DynamicReport();




            ViewBag.lstReport = lstReport;

            return View();
        }

        [HttpPost]
        public ActionResult Open_Excel(int ReportID, string Parameter, string ReportHeader)
        {
            //AllExcel objExcel = new AllExcel();
            //obj.Print(ReportID, Parameter, ReportHeader, sData.UserID);
            //try
            //{
            //    obj.Print(ReportID, Parameter, ReportHeader, sData.UserID);
            //    cr.Data = "";
            //    cr.Status = ResponseStatus.SUCCESS;
            //    cr.Message = string.Format("{0} Data Found", "");
            //}
            //catch (Exception ex)
            //{
            //    cr.Data = false;
            //    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            //    cr.Message = ex.Message;
            //}
            return Json(cr);
        }
    }
}