﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models;

namespace wbEcsc.Controllers.MasterControllers
{
    public class PayScaleController : Controller
    {
        // GET: PayScale
        PayScale_MD objPSmd = new PayScale_MD();
        PayScaleViewModel objpsVM = new PayScaleViewModel();
        PayScale_bll objpsbll = new PayScale_bll();
        ClientJsonResult cr = new ClientJsonResult();

        public ActionResult Index()
        {
            //objpsVM.SelectedPayScale = objPSmd;
            //objpsVM.ListField = objpsbll.GetField();
            //objpsVM.ListPayScale = objpsbll.getPayScale();
            //if(id!=null)
            //{

            //}
            return View();
        }

        [HttpPost]        
        public JsonResult Get_FieldName()
        {
            try
            {
                cr.Data = objpsbll.GetField(); 
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_EmployeeType(string EMPTYPE)
        {
            try
            {
                DataSet ds = new DataSet();              
                ds = objpsbll.GetPayScaleByEMPType(EMPTYPE);

                cr.Data = ds.GetXml() ;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = null;              
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }     

        [HttpPost]
        public JsonResult SaveData(PayScale_MD objpaymd)
        {
            try
            {
                DataTable dt = objpsbll.Save(objpaymd);               
                cr.Data = dt.Rows[0][0];
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = (string)dt.Rows[0][1];                
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }
    }
}