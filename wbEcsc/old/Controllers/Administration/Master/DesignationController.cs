﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;
using System.Data;
using wbEcsc.App_Start;

namespace wbEcsc.Controllers.Master
{
    [SecuredFilter]
    public class DesignationController : Controller
    {
        // GET: Designation
        Designation desig_bll = new Designation();
        Designation_MD desig = new Designation_MD();
        DesignationViewModel desig_viewmodel = new DesignationViewModel();
        public ActionResult Index(int? id)
        {
            desig_viewmodel.Designation_list = desig_bll.Get_Designation();
            desig_viewmodel.SelectedDesignation = desig;
            desig_viewmodel.LsitField = desig_bll.GetField();
            if(id!=null)
            {
                var selectedDesig =(from d in desig_viewmodel.Designation_list where d.DesignationID.Equals(id) select new { d.DesignationID, d.Designation });
                foreach (var d in selectedDesig)
                {
                    desig_viewmodel.SelectedDesignation.Designation = d.Designation;
                    desig_viewmodel.SelectedDesignation.DesignationID = d.DesignationID;
                }           
            }                
            return View(desig_viewmodel);
        }
        public ActionResult Save(Designation_MD desig)
        {
            try
            {               
                DataTable dt= desig_bll.Save(desig);
                TempData["ErrorCode"] = dt.Rows[0][0].ToString();
                TempData["Massage"] = dt.Rows[0][1].ToString();               
               
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}