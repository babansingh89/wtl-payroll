﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL.Master;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;

namespace wbEcsc.Controllers.Administration.Master
{
    [SecuredFilter]
    public class LoanMasterController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sd;

        public LoanMasterController()
        {
            sd = SessionContext.SessionData;
        }


        // GET: LoanMaster
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Get_LoanDescType()
        {
            try
            {
                List<LoanMaster_MD> lst = new LoanMaster().Get_LoanDescType();
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_LoanDesc(string ddlLoanType)
        {
            try
            {
                List<LoanMaster_MD> lst = new LoanMaster().Get_LoanDesc(ddlLoanType);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Search_EmpNameAutoComplete(string sectID, string EmpName, string EmpID)
        {
            try
            {
                List<LoanMasterVM> listEmpName = new LoanMaster().Get_EmpNamebySec_Autocomplete(sectID, Convert.ToInt32(sd.UserType), EmpName, Convert.ToInt32(EmpID));
                cr.Data = listEmpName;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", listEmpName.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_LoanDedDate(string sectID, string LoanDeducDT)  //, string SalFinYear
        {
            try
            {
                List<LoanMasterVM> list = new LoanMaster().Get_LoanDedDate(sectID, LoanDeducDT);   //SalFinYear, 
                cr.Data = list;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", list.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_LoanSancDate(string EmpID, string LoanDeducStrt, string LoanSancDate)
        {
            try
            {
                List<LoanMasterVM> list = new LoanMaster().Get_LoanSancDate(Convert.ToInt32(EmpID), LoanDeducStrt, LoanSancDate);
                cr.Data = list;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", list.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_LoanInstallment(string EmpId, string LoanTypeID, string LeftInst, string TotalLoanInst, string LoanAmt)
        {
            try
            {
                List<LoanMasterVM> list = new LoanMaster().Get_LoanInstallment(Convert.ToInt32(EmpId), Convert.ToInt32(LoanTypeID), Convert.ToInt32(LeftInst), Convert.ToInt32(TotalLoanInst), LoanAmt);
                cr.Data = list;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", list.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_DisburseAmt(string EmpId, string Loantpid, string LoanAmt, string DisbAmt)
        {
            try
            {
                List<LoanMasterVM> list = new LoanMaster().Get_DisburseAmt(Convert.ToInt32(EmpId), Convert.ToInt32(Loantpid), LoanAmt, DisbAmt);
                cr.Data = list;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", list.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]   //, string SalFinYear
        public JsonResult Save_Loan(string hdnLoanID, string LoanTypeID, string EmployeeID, string LoanAmount, string LoanDeducStartDate, string TotLoanInstall
        , string CurLoanInstall, string LoanAmountPaid, string CloseInd, string AdjustAmount, string AdjustDate, string LoanSancDate, string ReferenceNo, string ReferenceDate
        , string DisburseAmt, string SancOrderNo, string inst_no1, string inst_amt1, string inst_no2, string inst_amt2, string inst_no3, string inst_amt3
        , string SecID, string retmonth)
        {
            try
            {
                List<LoanMasterVM> list = new LoanMaster().Save_Loan(hdnLoanID, LoanTypeID, EmployeeID, LoanAmount
                    , LoanDeducStartDate, TotLoanInstall, CurLoanInstall, LoanAmountPaid
                    , CloseInd, AdjustAmount, AdjustDate, LoanSancDate, ReferenceNo, ReferenceDate, DisburseAmt, SancOrderNo
                    , inst_no1, inst_amt1, inst_no2, inst_amt2, inst_no3, inst_amt3, Convert.ToInt32(sd.UserID), SecID, retmonth);   //, SalFinYear
                cr.Data = list;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", list.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Search_Employee_Autocomplete(string secID, string Empname, string EmpId)
        {
            try
            {
                List<LoanMasterVM> list = new LoanMaster().Search_Employee_Autocomplete(secID, Convert.ToInt32(sd.UserType), Empname, Convert.ToInt32(EmpId));
                cr.Data = list;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", list.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_FieldName()
        {
            try
            {
                int FormID = 1;
                List<LoanMasterVM> lst = new LoanMaster().Get_FieldName(FormID);
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Loan_Edit(string EmpId, string LoanID)
        {
            try
            {
                List<LoanMasterVM> lst = new LoanMaster().Edit_Loan(Convert.ToInt32(EmpId), Convert.ToInt32(LoanID));
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Update_Loan(string EmpId, string LoanID, string Remarks, string CloseInd)
        {
            try
            {
                List<LoanMasterVM> lst = new LoanMaster().Update_Loan(Convert.ToInt32(EmpId), Convert.ToInt32(LoanID), Remarks, CloseInd, Convert.ToInt32(sd.UserID));
                cr.Data = lst;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", lst.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }







    }//END
}