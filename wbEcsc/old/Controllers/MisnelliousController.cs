﻿using System;
using System.Data.SqlClient;
using System.Security;
using System.Web.Mvc;
using System.Data;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.Authenticator;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Account.Logins;
using wbEcsc.Models.Application;

namespace wbEcsc.Controllers
{
    [SecuredFilter]
    public class MisnelliousController : Controller
    {

        ClientJsonResult cj = new ClientJsonResult();
        SessionData sData;
        public MisnelliousController()
        {
            sData = SessionContext.SessionData;
        }

        public ActionResult _DisplaySetting()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Save_UserColor(string Header, string Background)
        {
            try
            {
                string result = new Miscellenous().Save_UserColor(Header, Background, sData.UserID);
                cj.Data = result;
                cj.Status = ResponseStatus.SUCCESS;
                cj.Message = string.Format("{0} Data Found", result);
            }
            catch (ArgumentException ex)
            {
                cj.Data = null;
                cj.Message = ex.Message;
                cj.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cj);
        }

        [HttpPost]
        public ActionResult Get_UserColor()
        {
            try
            {
               DataTable  dtlist = new Miscellenous().Get_UserColor(sData.UserID);
                cj.Data = dtlist;
                cj.Status = ResponseStatus.SUCCESS;
                cj.Message = string.Format("{0} Data Found", dtlist.Rows.Count);
            }
            catch (ArgumentException ex)
            {
                cj.Data = null;
                cj.Message = ex.Message;
                cj.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cj);
        }
    }
}