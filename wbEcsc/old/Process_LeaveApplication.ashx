﻿<%@ WebHandler Language="C#" Class="Process_LeaveApplication" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Data.SqlClient;
public class Process_LeaveApplication : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
{

    public void ProcessRequest (HttpContext context) {
        string JSONVal = "";
        string LtNo = "";
        string LtDate = "";
        int EmployeeID = 0;
        int LvId = 0;
        string FromDate = "";
        string ToDate = "";
        decimal Bal = 0;
        int IsRound = 0;
        int IsLeave = 0;
        int Sanctioned = 0;
        string NoOfDays = "";
        int InsertedBy = 0;
        string Remarks = "";
        int RCode = 0;
        int LvFlag = 0;
        int LocCode = 0;
        ApiResponceData GER = new ApiResponceData();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        try
        {
            if (context.Request.QueryString.Count > 0)
            {
                LtNo = context.Request.QueryString["LtNo"].ToString();
                LtDate = context.Request.QueryString["LtDate"].ToString();
                EmployeeID = Convert.ToInt32(context.Request.QueryString["EmployeeID"]);
                LvId = Convert.ToInt32(context.Request.QueryString["LvId"]);
                FromDate = context.Request.QueryString["FromDate"].ToString();
                ToDate = context.Request.QueryString["ToDate"].ToString();
                Bal = Convert.ToDecimal(context.Request.QueryString["Bal"]);
                IsRound = Convert.ToInt32(context.Request.QueryString["IsRound"]);
                IsLeave = Convert.ToInt32(context.Request.QueryString["IsLeave"]);
                Sanctioned = Convert.ToInt32(context.Request.QueryString["Sanctioned"]);
                NoOfDays = context.Request.QueryString["NoOfDays"].ToString();
                InsertedBy = Convert.ToInt32(context.Request.QueryString["InsertedBy"]);
                Remarks = context.Request.QueryString["Remarks"].ToString();
                RCode = Convert.ToInt32(context.Request.QueryString["RCode"]);
                LvFlag = Convert.ToInt32(context.Request.QueryString["LvFlag"]);
                LocCode = Convert.ToInt32(context.Request.QueryString["LocCode"]);
            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                LtNo = context.Request.Params["LtNo"].ToString();
                LtDate = context.Request.Params["LtDate"].ToString();
                EmployeeID = Convert.ToInt32(context.Request.Params["EmployeeID"]);
                LvId = Convert.ToInt32(context.Request.Params["LvId"]);
                FromDate = context.Request.Params["FromDate"].ToString();
                ToDate = context.Request.Params["ToDate"].ToString();
                Bal = Convert.ToDecimal(context.Request.Params["Bal"]);
                IsRound = Convert.ToInt32(context.Request.Params["IsRound"]);//1
                IsLeave = Convert.ToInt32(context.Request.Params["IsLeave"]);//1
                Sanctioned = Convert.ToInt32(context.Request.Params["Sanctioned"]);//1
                NoOfDays = context.Request.Params["NoOfDays"].ToString();
                InsertedBy = Convert.ToInt32(context.Request.Params["InsertedBy"]);
                Remarks = context.Request.Params["Remarks"].ToString();
                RCode = Convert.ToInt32(context.Request.Params["RCode"]);
                LvFlag = Convert.ToInt32(context.Request.Params["LvFlag"]);//1
                LocCode = Convert.ToInt32(context.Request.Params["LocCode"]);//1
            }
            if (EmployeeID >0 && RCode >0)
            {
                DataTable dtSignInfo = new DataTable();
                List<Dictionary<string, object>> rowss = new List<Dictionary<string, object>>();
                //DataTable dtSignInfo = DBHandler.GetResult("payroll.GET_LoginDetails", UserName, UPassword);
                using (var command = new SqlCommand("check_leaveTransactionApp", con)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    con.Open();
                    command.Parameters.AddWithValue("@LtNo", LtNo==""?DBNull.Value:(Object)LtNo);
                    command.Parameters.AddWithValue("@LtDate", LtDate);
                    command.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                    command.Parameters.AddWithValue("@LvId", LvId);
                    command.Parameters.AddWithValue("@FromDate", FromDate);
                    command.Parameters.AddWithValue("@ToDate", ToDate);
                    command.Parameters.AddWithValue("@Bal", Bal);
                    command.Parameters.AddWithValue("@IsRound", IsRound);
                    command.Parameters.AddWithValue("@IsLeave", IsLeave);
                    command.Parameters.AddWithValue("@Sanctioned", Sanctioned);
                    command.Parameters.AddWithValue("@NoOfDays", NoOfDays);
                    command.Parameters.AddWithValue("@InsertedBy", InsertedBy);
                    command.Parameters.AddWithValue("@Remarks", Remarks);
                    command.Parameters.AddWithValue("@RCode", RCode);
                    command.Parameters.AddWithValue("@LvFlag", LvFlag);
                    command.Parameters.AddWithValue("@LocCode", LocCode);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    sda.Fill(dtSignInfo);
                    con.Close();
                }
                if (dtSignInfo.Rows.Count > 0)
                {
                    Dictionary<string, object> row1;
                    foreach (DataRow r in dtSignInfo.Rows)
                    {
                        row1 = new Dictionary<string, object>();
                        foreach (DataColumn col in dtSignInfo.Columns)
                        {
                            row1.Add(col.ColumnName, r[col]);
                        }
                        rowss.Add(row1);
                    }

                    GER.Data = rowss;
                    GER.Status = "Y";
                    GER.Message = "SUCCESS";
                    JSONVal = GER.ToJSON();
                }
                else
                {
                    GER.Status = "N";
                    GER.Message = "No records found ";
                    JSONVal = GER.ToJSON();
                }
            }
            else
            {
                GER.Status = "N";
                GER.Message = "Provide all the parameters ";
                JSONVal = GER.ToJSON();
            }

        }
        catch(Exception ex)
        {
            GER.Status = "N";
            GER.Message = ex.Message;
            JSONVal = GER.ToJSON();
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(JSONVal);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}