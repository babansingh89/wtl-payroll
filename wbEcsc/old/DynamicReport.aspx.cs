﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Data.SqlClient;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;
using NPOI.HSSF.UserModel;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using System.Configuration;
using wbEcsc.App_Codes;
using wbEcsc.Models.Application;
using wbEcsc.Models.Account;

namespace wbEcsc
{
    public partial class DynamicReport : System.Web.UI.Page
    {
        string ReportID = "";
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
        IInternalUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!SessionContext.IsAuthenticated)
            {
                Response.Redirect("/Error/Error401/Index", true);
                return;
            }

            user = SessionContext.CurrentUser as IInternalUser;
            if (HttpContext.Current.Request.QueryString["ReportID"] != null)
                ReportID = HttpContext.Current.Request.QueryString["ReportID"].ToString();

            if(ReportID !="")
            Print_Excel(ReportID);
        }


        public void Print_Excel(string ReportID)
        {
            if (ReportID == "1")
                print_1();
        }

        public void print_1()
        {
            string ddlReportID = ""; string Parameter = ""; string ReportHeader = "";
            if (HttpContext.Current.Request.QueryString["ddlReportID"] != null)
               ddlReportID = HttpContext.Current.Request.QueryString["ddlReportID"].ToString();
            if (HttpContext.Current.Request.QueryString["Parameter"] != null)
                Parameter = HttpContext.Current.Request.QueryString["Parameter"].ToString();
            if (HttpContext.Current.Request.QueryString["ReportHeader"] != null)
                ReportHeader = HttpContext.Current.Request.QueryString["ReportHeader"].ToString();



            int RowsforPrint = 10; int MinusRows = 0; int NextRowCount = 0; int isFirstPage = 0; int rows = 0; int ForNotCount = 0;
            string ParamValue = "";
            DataTable dtNextRecord = new DataTable();
            StringBuilder html = new StringBuilder();

            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Get_ReportQuery", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReportID", ddlReportID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet dsRepQry = new DataSet();
            con.Open();
            da.Fill(dsRepQry);

            DataTable dtRepQry1 = dsRepQry.Tables[0];
            DataTable dtRepQry2 = dsRepQry.Tables[1];

            if (dtRepQry1.Rows.Count > 0)
            {
                ParamValue = dtRepQry1.Rows[0]["ParameterFlag"].ToString();
                if (ParamValue == "N")
                {
                    if (Parameter == "R")
                    {
                        //html.Append("<div id='tabEmpdetail'  runat='server'>");
                        //html.Append("<table  style=' border-collapse: collapse;  border: solid 1px black; ' width='100%'>");
                        //html.Append("<tr>");
                        //html.Append("<td style='padding: 3px;'>");
                        //html.Append("<table align='center' width='100%'>");
                        //html.Append("<tr>");
                        //html.Append("<td style='text-align:left;'>");
                        //html.Append("<div style='width:7%; float:left;'>");
                        //html.Append("<a href='javascript:void(0);' id='A1' style='color: red;'> ");
                        //html.Append("<img src='images/Kmda-logo.gif' alt='Close' title='Close' style='Height: 80px; Width: 120px;'/></a> ");
                        //html.Append("</div>");
                        //html.Append("</td>");
                        //html.Append("<td style='width: 100%; text-align: left;'>");
                        //html.Append("<table style='width: 100%;'>");
                        //html.Append("<tr>");
                        //html.Append("<td colspan='4' style='width: auto;'>");
                        //html.Append("<hr style='border: dashed 1px black;'>");
                        //html.Append("<hr style='border: dashed 1px black;'>");
                        //html.Append("</td>");
                        //html.Append("</tr>");
                        //html.Append("<tr>");
                        //html.Append("<td colspan='4' style='width: auto; text-align: center; background-color:white;'>");
                        //html.Append("<span ID='lblHeader' runat='server' style='font-family:Verdana; font-size:15px;font-weight:bold;'> * KOLKATA METROPOLITAN DEVELOPMENT AUTHORITY * </span>");
                        //html.Append("</td>");
                        //html.Append("</tr>");
                        //html.Append("<tr>");
                        //html.Append("<td colspan='4' style='width: auto;'>");
                        //html.Append("<hr style='border: dashed 1px black;'>");
                        //html.Append("<hr style='border: dashed 1px black;'>");
                        //html.Append("</td>");
                        //html.Append("</tr>");
                        //html.Append("</table>");
                        //html.Append("</td>");
                        //html.Append("</tr>");
                        //html.Append("</table>");
                        //html.Append("</td>");
                        //html.Append("</tr>");
                        //html.Append("<tr>");
                        //html.Append("<td>");
                        //html.Append("<table style='width: 100%; background-color:white;'>");
                        //html.Append("<td style='text-align:center; font-family:Verdana;font-weight:bold; font-size:20px; border-top:inset 1px black;border-bottom:inset 1px black;background-color:white; '>Employee Details");
                        //html.Append("</td>");
                        //html.Append("</tr>");
                        //html.Append("</table>");
                        //html.Append("</td>");
                        //html.Append("</tr>");
                        //html.Append("<tr>");
                        //html.Append("<td>");

                        //string RepParam = "";
                        //if (ParameterFlag == "N")
                        //    RepParam = "";
                        //else
                        //    RepParam = "";

                        //DataSet dsRepQry = DBHandler.GetResults("returnReportQuery", ReportID, ParameterFlag, RepParam, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                        //DataTable dtRepQry = dsRepQry.Tables[0];

                        //dtNextRecord = dtRepQry;
                        //int rowCount = dtNextRecord.Rows.Count;
                        //NextRowCount = rowCount;
                        //MinusRows = NextRowCount - RowsforPrint;
                        //if (MinusRows < 0)
                        //{
                        //    ForNotCount = MinusRows;
                        //    MinusRows = RowsforPrint = NextRowCount;
                        //}


                        //while (MinusRows <= NextRowCount)
                        //{
                        //    if (MinusRows < RowsforPrint)
                        //    {
                        //        break;
                        //    }
                        //    else
                        //    {
                        //        if (dtNextRecord.Rows.Count > 0)
                        //        {
                        //            if (isFirstPage == 0)
                        //            {
                        //                rows = RowsforPrint;
                        //                RowsforPrint = rows;
                        //            }
                        //            else
                        //            {
                        //                RowsforPrint = rows + 7;
                        //            }

                        //            DataTable dtPrintData = (DataTable)dtNextRecord.Rows.Cast<DataRow>().Take(RowsforPrint).CopyToDataTable();
                        //            if (dtPrintData.Rows.Count > 0)
                        //            {
                        //                isFirstPage = 1;
                        //                //=====================================================================================================

                        //                //place holder

                        //                html.Append("<div>");
                        //                html.Append("<table border='1' style='border-collapse: collapse; page-break-after: always; border-bottom:solid 1px black;' width='100%'>");

                        //                html.Append("<tr>");
                        //                foreach (DataColumn column in dtPrintData.Columns)
                        //                {
                        //                    html.Append("<th  style='background-color:LightGray; text-transform:uppercase;'>");
                        //                    html.Append(column.ColumnName);
                        //                    html.Append("</th>");
                        //                }
                        //                html.Append("</tr>");

                        //                foreach (DataRow row in dtPrintData.Rows)
                        //                {
                        //                    html.Append("<tr>");
                        //                    foreach (DataColumn column in dtPrintData.Columns)
                        //                    {
                        //                        html.Append("<td>");
                        //                        html.Append(row[column.ColumnName]);
                        //                        html.Append("</td>");
                        //                    }
                        //                    html.Append("</tr>");
                        //                }

                        //                html.Append("</div>");
                        //                html.Append("</table>");



                        //                //place holder
                        //                //PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });

                        //                //=====================================================================================================
                        //                if (ForNotCount < 0)
                        //                {
                        //                    MinusRows = -1;
                        //                }
                        //                else
                        //                {
                        //                    dtNextRecord = (DataTable)dtNextRecord.AsEnumerable().Skip(RowsforPrint).CopyToDataTable<DataRow>();
                        //                    NextRowCount = dtNextRecord.Rows.Count;
                        //                    MinusRows = NextRowCount - RowsforPrint;
                        //                }
                        //            }

                        //        }
                        //    }
                        //}
                        //html.Append("</td>");
                        //html.Append("</tr>");
                        //html.Append("</table>");
                        //html.Append("</div>");
                        //PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
                    }
                    else
                    {
                        string RepParam = "";
                        if (ParamValue == "N")
                            RepParam = "";
                        else
                            RepParam = "";

                       
                        SqlCommand cmds = new SqlCommand("returnReportQuery", con);
                        cmds.CommandType = CommandType.StoredProcedure;
                        cmds.Parameters.AddWithValue("@ReportID", ddlReportID);
                        cmds.Parameters.AddWithValue("@Parameterflag", ParamValue);
                        cmds.Parameters.AddWithValue("@ParameterValue", RepParam);
                        cmds.Parameters.AddWithValue("@InsertedBy", user.UserID);
                        SqlDataAdapter daa = new SqlDataAdapter(cmds);
                        DataSet dsRepQrys = new DataSet();

                        daa.Fill(dsRepQrys);
                        DataTable dt = dsRepQrys.Tables[0];

                        if (dt.Rows.Count > 0)
                        {
                            string Columns = "";
                            GenerateExcels(dt, Columns, ReportHeader);
                        }
                    }
                }
            }
        }
        public void GenerateExcels(DataTable dt, string SumFieldName, string ReportName)
        {
            DataTable dtTotalPosition = new DataTable();

            decimal Total = 0; string Position = "";
            if (dt.Rows.Count > 0)
            {
                var workbook = new HSSFWorkbook();
                var sheet = workbook.CreateSheet();

                //Create a header row
                var headerRow = sheet.CreateRow(0);

                string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                for (int j = 0; j < columnNames.Length; j++)
                {
                    headerRow.CreateCell(j).SetCellValue(columnNames[j]);
                }
                int rowNumber = 1;
                var row = sheet.CreateRow(rowNumber);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //Create a new Row
                    row = sheet.CreateRow(rowNumber++);

                    string[] columnNamess = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    for (int j = 0; j < columnNamess.Length; j++)
                    {
                        row.CreateCell(j).SetCellValue(dt.Rows[i][columnNamess[j]].ToString());
                    }
                }

                if (SumFieldName != "")
                {
                    string col = "";
                    string[] lines = SumFieldName.Split(',');
                    for (int i = 0; i < lines.Length; i++)
                    {
                        col = lines[i].Trim().ToString();
                        DataColumnCollection columns = dt.Columns;
                        if (columns.Contains(col))
                        {
                            Total = 0;
                            for (int k = 0; k < dt.Rows.Count; k++)
                            {
                                string value = dt.Rows[k][col].ToString();
                                Total = Total + Convert.ToDecimal(value);
                                Position = dt.Columns.IndexOf(col).ToString();
                            }
                            //Add Total amount and Column Position in a DataTable
                            if (dtTotalPosition != null)
                            {
                                if (dtTotalPosition.Rows.Count > 0)
                                {
                                    DataRow dtNewRow = dtTotalPosition.NewRow();
                                    dtNewRow["Position"] = Position;
                                    dtNewRow["Total"] = Total;
                                    dtTotalPosition.Rows.Add(dtNewRow);
                                }
                                else
                                {
                                    dtTotalPosition.Columns.Add("Position");
                                    dtTotalPosition.Columns.Add("Total");

                                    DataRow dtNewRow = dtTotalPosition.NewRow();
                                    dtNewRow["Position"] = Position;
                                    dtNewRow["Total"] = Total;
                                    dtTotalPosition.Rows.Add(dtNewRow);
                                }
                            }
                        }
                        else
                        {
                            if (dtTotalPosition != null)
                            {
                                if (dtTotalPosition.Rows.Count > 0)
                                {

                                }
                                else
                                {
                                    dtTotalPosition.Columns.Add("Position");
                                    dtTotalPosition.Columns.Add("Total");
                                }
                            }
                        }
                    }
                }

                if (SumFieldName != "")
                {
                    // Add a row for the detail row
                    row = sheet.CreateRow(dt.Rows.Count + 1);
                    //var cell = row.CreateCell(Convert.ToInt32(Position) - 1);
                    //cell.SetCellValue("Total:");

                    if (dtTotalPosition.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtTotalPosition.Rows.Count; j++)
                        {
                            string FinalPosition = dtTotalPosition.Rows[j]["Position"].ToString();
                            string FinalAmt = dtTotalPosition.Rows[j]["Total"].ToString();

                            row.CreateCell(Convert.ToInt32(FinalPosition)).SetCellValue(FinalAmt);
                        }
                    }
                    else
                    {

                    }
                }


                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);
                    string saveAsFileName = string.Format(ReportName + "-{0:d}.xls", DateTime.Now).Replace("/", "-");
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
                    Response.Clear();
                    Response.BinaryWrite(exportData.GetBuffer());
                    Response.End();
                }
            }
        }
    }
}