

$(document).ready(function () {
    $(document).delegate('.expander', 'click', function () {
        $(this).toggleClass('expanded')
            .nextAll('ul:first').toggleClass('expanded');
        return true;
    });

    var getTreeData = function () {
        return JSON.parse('[{"MenuID":1,"MenuName":"Collection","ParentMenuID":null,"ActionMethodID":null,"Rank":1,"Url":"///","IsActive":true,"List":[{"MenuID":4,"MenuName":"Property Tax","ParentMenuID":1,"ActionMethodID":16,"Rank":1,"Url":"/Municipality/Collection/Index/","IsActive":true,"List":[]}]},{"MenuID":2,"MenuName":"Configuration","ParentMenuID":null,"ActionMethodID":1,"Rank":2,"Url":"/Home/Index/","IsActive":true,"List":[{"MenuID":5,"MenuName":"Counters","ParentMenuID":2,"ActionMethodID":24,"Rank":1,"Url":"/Municipality/MnCounter/Index/","IsActive":true,"List":[]},{"MenuID":6,"MenuName":"Map Counter & User","ParentMenuID":2,"ActionMethodID":37,"Rank":2,"Url":"/Municipality/MnUserCounterAssociate/Index/","IsActive":true,"List":[]},{"MenuID":7,"MenuName":"Pay Head Config","ParentMenuID":2,"ActionMethodID":42,"Rank":3,"Url":"/Municipality/MnPayHead/Configure_PayHead/","IsActive":true,"List":[]},{"MenuID":8,"MenuName":"Map Counter & Payment Mode","ParentMenuID":2,"ActionMethodID":45,"Rank":4,"Url":"/Municipality/MnPaymentModeConfiguration/Index/","IsActive":true,"List":[]}]},{"MenuID":3,"MenuName":"Reporting","ParentMenuID":null,"ActionMethodID":null,"Rank":3,"Url":"///","IsActive":true,"List":[{"MenuID":14,"MenuName":"Collection Report","ParentMenuID":3,"ActionMethodID":54,"Rank":1,"Url":"/Municipality/ReportCalls/MnCollectionReport/Index/","IsActive":true,"List":[]},{"MenuID":15,"MenuName":"Demand Report","ParentMenuID":3,"ActionMethodID":58,"Rank":2,"Url":"/Municipality/ReportCalls/MnDemandReport/Index/","IsActive":true,"List":[]}]},{"MenuID":9,"MenuName":"User","ParentMenuID":null,"ActionMethodID":1,"Rank":4,"Url":"/Home/Index/","IsActive":true,"List":[{"MenuID":10,"MenuName":"New User","ParentMenuID":9,"ActionMethodID":34,"Rank":1,"Url":"/Municipality/MnUserRegistration/Index/","IsActive":true,"List":[]},{"MenuID":12,"MenuName":"Reset Password","ParentMenuID":9,"ActionMethodID":55,"Rank":2,"Url":"/Municipality/MnUserPassword/ResetPasswordUI/","IsActive":true,"List":[]},{"MenuID":11,"MenuName":"Change Password","ParentMenuID":9,"ActionMethodID":28,"Rank":3,"Url":"/Municipality/MnUserPassword/Index/","IsActive":true,"List":[]},{"MenuID":13,"MenuName":"Manage Role","ParentMenuID":9,"ActionMethodID":30,"Rank":4,"Url":"/Municipality/MnRoleUsersMapping/Index/","IsActive":true,"List":[]}]},{"MenuID":9,"MenuName":"User","ParentMenuID":null,"ActionMethodID":1,"Rank":4,"Url":"/Home/Index/","IsActive":true,"List":[{"MenuID":10,"MenuName":"New User","ParentMenuID":9,"ActionMethodID":34,"Rank":1,"Url":"/Municipality/MnUserRegistration/Index/","IsActive":true,"List":[]},{"MenuID":12,"MenuName":"Reset Password","ParentMenuID":9,"ActionMethodID":55,"Rank":2,"Url":"/Municipality/MnUserPassword/ResetPasswordUI/","IsActive":true,"List":[]},{"MenuID":11,"MenuName":"Change Password","ParentMenuID":9,"ActionMethodID":28,"Rank":3,"Url":"/Municipality/MnUserPassword/Index/","IsActive":true,"List":[]},{"MenuID":13,"MenuName":"Manage Role","ParentMenuID":9,"ActionMethodID":30,"Rank":4,"Url":"/Municipality/MnRoleUsersMapping/Index/","IsActive":true,"List":[]}]},{"MenuID":3,"MenuName":"Reporting","ParentMenuID":null,"ActionMethodID":null,"Rank":3,"Url":"///","IsActive":true,"List":[{"MenuID":14,"MenuName":"Collection Report","ParentMenuID":3,"ActionMethodID":54,"Rank":1,"Url":"/Municipality/ReportCalls/MnCollectionReport/Index/","IsActive":true,"List":[]},{"MenuID":15,"MenuName":"Demand Report","ParentMenuID":3,"ActionMethodID":58,"Rank":2,"Url":"/Municipality/ReportCalls/MnDemandReport/Index/","IsActive":true,"List":[]}]}]');
    };

    var ConvertToTreeFormat = function (data) {
        var formattedSource = [];
        var formattedClass = function (nodeText, nodeID, childrens, isExpanded, additionalData, parentID,link) {
            if (childrens.constructor !== Array) {
                throw 'childrens must be type of array';
            }
            return {
                text: nodeText,
                id: nodeID,
                nodes: childrens,
                hasChildren: childrens.length > 0,
                isExpanded: isExpanded,
                data: additionalData,
                parentID: parentID,
                link: link
            };
        };
        var drillDownNodes = function (data) {
            var d = [];
            $.each(data, function (index, value) {
                if (value.List.length == 0) {
                    d.push(formattedClass(value.MenuName, value.MenuID, [], true, null, value.ParentMenuID === null ? 0 : value.ParentMenuID, value.Url));
                } else {
                    d.push(formattedClass(value.MenuName, value.MenuID, drillDownNodes(value.List), true, null, value.ParentMenuID === null ? 0 : value.ParentMenuID, value.Url));
                }
            });
            return d;
        };
        return (function () {
            return formattedClass('Super Parent', 0, drillDownNodes(data), true, null, null,null);
        }());
    };
    var treeData = ConvertToTreeFormat(getTreeData());

    //return tree dom
    var CreateTreeDom = function (data) {
        var superParent = $("<ul></ul>");

        $.each(data, function (index, value) {
            
            if (value.hasChildren === false) {//no child
                superParent.addClass((value.isExpanded == true ? "expanded" : "")).append("<li><a class='link' href='"+value.link+"'>" + value.text + "</a></li>");
            } else {
                superParent.addClass((value.isExpanded == true ? "expanded" : "")).append($("<li></li>").append(CreateTreeDom(value.nodes)).prepend("<span class='node-header-text'>" + value.text + "</span><div class='expander " + (value.isExpanded == true ? "expanded" : "")+"'></div>"));
                var x = $("<div/>").append(superParent).html();
                //d.push(formattedClass(value.MenuName, value.MenuID, drillDownNodes(value.List), false, null, value.ParentMenuID === null ? 0 : value.ParentMenuID));
            }
        });
        return superParent;
    };
    var x = CreateTreeDom(treeData.nodes);
    $("#tree").append(CreateTreeDom(treeData.nodes).addClass('tree'));
});