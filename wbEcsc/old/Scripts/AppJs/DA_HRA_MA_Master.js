﻿$(document).ready(function () {
    FieldNameData();
    Populate_EarningType();
    Populate_EmpType();
    $(document).on('change', '#ddlEmpType, #ddlErnType', function () {
        Populate_NewDARate();
    });

    $("#btnRefresh").click(ClearAllField);
    $("#btnUpdate").click(Update);
    //$("#btnGenerate").click(Generate);
    $(document).on('click', '#btnShowChanges', function () {
        $('#lblCountEmp').text('');
        $('#tbl tr.trclass').empty();
        Populate_Grid();
        jQuery.noConflict();
        jQuery(document).ready(function ($) {
            $('#Div1').modal('show');
        });
        
    });

    $("#txtSearchEmpNo").on("keyup", function () {
        if ($(this).val().trim() == "") {
            $("#tbl tbody tr").show();
            return;
        }
        var value = $(this).val();
        var s = $("#tbl tbody tr").not(".EmpNo:contains('" + value.toUpperCase() + "')");
        var s1 = $("#tbl tbody tr").find(".EmpNo:contains('" + value.toUpperCase() + "')");
        s.hide();
        s1.parent().show();
        //$('#lblCountEmp').text('');
        //$('#lblCountEmp').text($('#tbl tr.trclass').length);
    });

    $(document).on('change', '.chkSelectcls', function () {
        $('.chkSelectcls').not(this).prop('checked', false);
    });

    $(document).on('change', '#ddlDecline', function () {
        if ($('#ddlDecline').val() == 'N') {
            $('#txtRemarks').val(''); $('#txtRemarks').attr('disabled', true);
        }
        else {
            $('#txtRemarks').attr('disabled', false);
        }
    });

    $(document).on('click', '#btnGenerate', function () {
        if ($("#tbl input[type = checkbox]:checked").length > 0)
        {
            alert("One DATA is checked ! Please Update your checked DATA or Unchecked that for Final Generate " + $('#ddlErnType option:selected').text());
            return false;
        }
        else {
            Generate();
        }
    });
    jQuery.noConflict();
    $("#Div1").on("hidden.bs.modal", function () {
        
        $('#tbl tr.trclass').empty();
        $('#txtSearchEmpNo').val(''); $('#ddlDecline').val(''); $('#txtRemarks').val('');
        $('#txtRemarks').attr('disabled', false); $('#lblCountEmp').text('');
    });
    $('#btnPrint').click(Print);
});

function FieldNameData() {
    $.ajax({
        type: "POST",
        url: "/Administration/Master/DA_HRA_MA_Master/Get_FieldName",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                $.each(D.Data, function (index, value) {
                    var MandatoryStyle = '*';
                    var Id = '#' + value.FieldID;
                    $(Id).text(value.FieldName);
                    $('#spn' + value.FieldID).text(value.IsMand == 1 ? '   ' + MandatoryStyle : '');
                });
            }
        }
    });
}

function ClearAllField()
{
    $("#ddlDARate, #ddlEmpType, #ddlErnType").empty();
    $("#ddlDARate, #ddlEmpType, #ddlErnType").append("<option value=''>--- (Select) ---</option>");
    $('#tbl tr.trclass').empty();
    $('#txtSearchEmpNo').val(''); $('#ddlDecline').val(''); $('#txtRemarks').val('');
    $('#txtRemarks').attr('disabled', false); $('#lblCountEmp').text('');
    Populate_EarningType(); Populate_EmpType();
}

function Populate_EarningType()
{
    var options = {};
    options.url = "/Administration/Master/DA_HRA_MA_Master/Get_EarningType";
    options.type = "POST";
    options.data = "";
    options.dataType = "json";
    options.contentType = "application/json";
    options.success = function (list) {
        var a = list.Data.length;
        if (a > 0) {
            $("#ddlErnType").empty();
            $("#ddlErnType").append("<option value=''>--- (Select) ---</option>");
            $.each(list.Data, function (index, value) {
                var label = value.EarningType;
                var val = value.EarningCode;

                $("#ddlErnType").append($("<option></option>").val(val).html(label));
            });
        }
        else {
            $("#ddlErnType").empty();
            $("#ddlErnType").append("<option value=''>--- (Select) ---</option>");
        }
    };
    options.error = function () { alert("Error in retrieving DATA !"); };
    $.ajax(options);
}

function Populate_EmpType() {
    
    var options = {};
    options.url = "/Administration/Master/DA_HRA_MA_Master/Get_EmpType";
    options.type = "POST";
    options.data = "";
    options.dataType = "json";
    options.contentType = "application/json";
    options.success = function (list) {
        var a = list.Data.length;
        if (a > 0) {
            $("#ddlEmpType").empty();
            $("#ddlEmpType").append("<option value=''>--- (Select) ---</option>");
            $.each(list.Data, function (index, value) {
                var label = value.EmpTypeDesc;
                var val = value.EmpType;

                $("#ddlEmpType").append($("<option></option>").val(val).html(label));
            });
        }
        else {
            $("#ddlEmpType").empty();
            $("#ddlEmpType").append("<option value=''>--- (Select) ---</option>");
        }
    };
    options.error = function () { alert("Error in retrieving DATA !"); };
    $.ajax(options);
}

function Populate_NewDARate()
{
    var S = "{SecID:'" + $.trim($('#ddlSector').val()) + "', Emptype:'" + $.trim($('#ddlEmpType').val()) + "', EarningType:'" + $.trim($('#ddlErnType').val()) + "', PayMonthID:'" + $.trim($('#txtSalMonthID').val()) + "'}";
    var options = {};
    options.url = "/Administration/Master/DA_HRA_MA_Master/Get_DA_HRA_MA_Rate";
    options.type = "POST";
    options.data = S;
    options.dataType = "json";
    options.contentType = "application/json";
    options.success = function (list) {
        var a = list.Data;
        if (a.length > 0) {
            $("#ddlDARate").empty();
            $("#ddlDARate").append("<option value=''>--- (Select) ---</option>");
            var label = a[0].VALS;
            var val = a[0].IDS;
            if (a[0].ErrorCode == 1) {
                if (a[0].DA_HRA_MA_ID == 1) {
                    $("#ddlDARate").append($("<option></option>").val(val).html(label));
                    $("#ddlDARate").val(val);
                }
                else {

                }
            }
            else {
                alert(a[0].Messege);
                $('#ddlEmpType').val('');
            }
        }
        else {
            $("#ddlDARate").empty();
            $("#ddlDARate").append("<option value=''>--- (Select) ---</option>");
        }
    };
    options.error = function () { alert("Error in retrieving DATA !"); };
    $.ajax(options);
}

function Populate_Grid()
{
    var trcount = 1;
    var S = "{SecID:'" + $.trim($('#ddlSector').val()) + "', EarningCode:'" + $.trim($('#ddlErnType').val()) + "', EmpNo:'" + $.trim($('#txtSearchEmpNo').val()) + 
        "', Emptype:'" + $.trim($('#ddlEmpType').val()) + "', DaHraMa:'" + $.trim($("#ddlDARate option:selected").text()) + "', PayMonthID:'" + $.trim($('#hdnSalMonthID').val()) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/DA_HRA_MA_Master/Get_GridDATA",
        data: S,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                $('#tbl tr.trclass').remove();
                $.each(D.Data, function (index, value) {
                    $("#tbl").append("<tr class='trclass'><td>" +   // style='display:none;'
                        "<input type='CheckBox' id='chkSelect' class='chkSelectcls' style='align:center' />" + "</td><td style='text-align:center'>" +
                        trcount + "</td><td style='text-align:center'>" +
                        value.EmpNo + "</td><td style='text-align:center'>" +
                        value.EmpName + "</td><td style='text-align:center'>" +
                        value.CenterName + "</td><td style='text-align:center'>" +
                        value.OldDa + "</td><td style='text-align:center'>" +
                        value.NewDa + "</td><td style='text-align:center'>" +
                        value.Declined + "</td></tr>");
                    trcount++;
                });
                $('#lblCountEmp').text('');
                $('#lblCountEmp').text($('#tbl tr.trclass').length);
            }
        }
    });
}

function Update()
{
    var conf = confirm('Are you sure want to Update');
    if (conf == true) {
        var EmpNo = '';
        $("input[id*='chkSelect']:checkbox").each(function (index) {
            if ($(this).is(":checked")) {
                EmpNo = $(this).closest('tr').find('td:eq(2)').text().trim();
            }
        });

        var S = "{EmpNo:'" + EmpNo + "', Decline:'" + $.trim($('#ddlDecline').val()) + "', Reasons:'" + $.trim($("#txtRemarks").val()) + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/DA_HRA_MA_Master/Get_Update",
            data: S,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0) {
                    if (t[0]["ErrorCode"] == 1) {
                        $('#txtSearchEmpNo').val('');
                        $('#ddlDecline').val('');
                        $('#tbl tr.trclass').empty();
                        Populate_Grid();
                    }
                    else {
                        alert(t[0]["Messege"]);
                    }
                }
            }
        });
    }
}

function Generate()
{
    var conf = confirm('Are you sure want to Final Generate ' + $('#ddlErnType option:selected').text());
    if (conf == true) {
        var S = "{SecID:'" + $.trim($('#ddlSector').val()) + "', Daid:'" + $.trim($('#ddlDARate').val()) + "', EarningCode:'" + $.trim($('#ddlErnType').val()) + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/DA_HRA_MA_Master/Get_Generate",
            data: S,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0) {
                    if (t[0]["ErrorCode"] == 1) {
                        alert(t[0]["Messege"]);
                        ClearAllField();
                    }
                    else {
                        alert(t[0]["Messege"]);
                        return false;
                    }
                }
            }
        });
    }
}

function Print()
{
    var prnt = document.getElementById('tbl');
    prnt.border = 1;
    var p = window.open('', 'Print', 'left=100,top=100,width=1000,height=1000,tollbar=0,scrollbars=1,status=0,resizable=1');
    $('tr').children().eq(0).hide();
    $('#tbl tr').find('td:eq(0)').hide();
    p.document.write(prnt.outerHTML);
    p.document.close();
    p.focus();
    $('tr').children().eq(0).show();
    $('#tbl tr').find('td:eq(0)').show();
    p.print();
    p.close();
}
