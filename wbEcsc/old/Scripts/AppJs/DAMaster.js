﻿$(document).ready(function () {
    FieldNameData();
    PopulateEmpType('');
    $('#txtOrderDate').datepicker({
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        maxDate: '+0d'
    });
    $('#txtEffectTo').datepicker({
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
    $('#txtEffectFrom').datepicker({
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function (dates) {
            //var date = $("#txtEffectFrom").val().split('/');
            //var LoanDeducDT = date[2] + '-' + date[1] + '-' + date[0];
            var options = {};
            options.url = "/Administration/Master/DAMaster/Get_EffectFromDT";
            options.type = "POST";
            options.data = "{SecID:'" + $("#ddlSector").val() + "', EffectiveFrom:'" + $.trim($("#txtEffectFrom").val()) + "'}";  //, SalFinYear:'" + '2017-2018' + "'
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (D) {
                var a = D.Data;
                if (a.length > 0) {
                    if (a[0].ErrorCode == 0) {
                        alert(a[0].Messege);
                        $('#txtEffectFrom').val('');
                    } else {
                        $('#txtEffectFrom').val(a[0].EffectiveFrom);
                    }
                }
            };
            options.error = function () { alert("Error in retrieving Data !"); };
            $.ajax(options);
        }
    });

    $(document).on('change', '#ddlEmpType', function () {
        if ($('#ddlEmpType').val() == '') {
            $("#tbl tr.trclass").empty();
            ClearAllFields()
        }
        else {
            PopulateGridDA();
        }
    })

    $('#txtDARate, #txtMaxAmt').keypress(function (event, element) {
        if (event.which != 46 && (event.which <= 47 || event.which >= 59)) {
            event.preventDefault();
            if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
                event.preventDefault();
            }
        }
    });

    $('#btnSave').click(SaveUpdate);
    $('#btnRefresh').click(ClearAllFields);
});

function FieldNameData() {
    $.ajax({
        type: "POST",
        url: "/Administration/Master/DAMaster/Get_FieldName",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                $.each(D.Data, function (index, value) {
                    var MandatoryStyle = '*';
                    var Id = '#' + value.FieldID;
                    $(Id).text(value.FieldName);
                    $('#spn' + value.FieldID).text(value.IsMand == 1 ? '   ' + MandatoryStyle : '');
                });
            }
        }
    });
}

function ClearAllFields() {
    $("#hdnDAID").val(''); $("#ddlEmpType").val(''); $("#txtOrderNo").val(''); $("#txtOrderDate").val(''); $("#txtDARate").val('');
    $("#txtMaxAmt").val('10000000'); $("#txtEffectFrom").val('');
    PopulateEmpType('');
    $("#tbl tr.trclass").empty();
    $("#divEffectiveTo").hide();
    $('#btnSave').html('Save');
}

function PopulateEmpType(EmpType)
{
    var E = "{EmpType:'" + EmpType + "'}";
    var options = {};
    options.url = "/Administration/Master/DAMaster/Get_DAInGrid";
    options.type = "POST";
    options.data = E;
    options.dataType = "json";
    options.contentType = "application/json";
    options.success = function (list) {
        var a = list.Data.length;
        if (a > 0) {
            $("#ddlEmpType").empty();
            $("#ddlEmpType").append("<option value=''>--- (Select) ---</option>");
            $.each(list.Data, function (index, value) {
                var label = value.EmpTypeDesc;
                var val = value.EmpType;
                
                $("#ddlEmpType").append($("<option></option>").val(val).html(label));
            });
        }
        else {
            $("#ddlEmpType").empty();
            $("#ddlEmpType").append("<option value=''>--- (Select) ---</option>");
        }
    };
    options.error = function () { alert("Error in retrieving Employee Type !"); };
    $.ajax(options);
}

function PopulateGridDA() {
    var E = "{EmpType:'" + $.trim($('#ddlEmpType').val()) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/DAMaster/Get_DAInGrid",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                $("#tbl tr.trclass").empty();
                $.each(D.Data, function (index, value) {
                    $("#tbl").append("<tr class='trclass'><td>" +   // style='display:none;'
                        value.EmpTypeDesc + "</td><td>" +
                        value.OrderNo + "</td><td style='text-align:center'>" +
                        value.OrderDate + "</td><td style='text-align:center'>" +
                        value.DARate + "</td><td style='text-align:center'>" +
                        value.MaxAmount + "</td><td style='text-align:center'>" +
                        value.EffectiveFrom + "</td><td style='text-align:center'>" +
                        value.EffectiveTo + "</td><td onclick='EditDA(this)' style='text-align:center; cursor:pointer; color:blue'>" +
                        value.Edit + "</td><td onclick='DeleteDA(this)' style='text-align:center; cursor:pointer; color:blue'>" +
                        value.Del + "</td><td style='display:none;'>" +
                        value.EmpType + "</td><td style='display:none;'>" +
                        value.DAID + "</td></tr>");
                });
            }
        }
    });
}

function EditDA(t) {
    $('#divEffectiveTo').show(); $('#btnSave').html('Update');
    $('#ddlEmpType').val($(t).closest('tr').find('td:eq(9)').text().trim());
    $('#txtOrderNo').val($(t).closest('tr').find('td:eq(1)').text().trim());
    $('#txtOrderDate').val($(t).closest('tr').find('td:eq(2)').text().trim());
    $('#txtDARate').val($(t).closest('tr').find('td:eq(3)').text().trim());
    $('#txtMaxAmt').val($(t).closest('tr').find('td:eq(4)').text().trim());
    $('#txtEffectFrom').val($(t).closest('tr').find('td:eq(5)').text().trim());
    $('#hdnDAID').val($(t).closest('tr').find('td:eq(10)').text().trim());
}

function DeleteDA(t) {
    var conf = confirm("Are you sure want to Delete this Record ?")
    if (conf == true) {
        var E = "{DAID:'" + $(t).closest('tr').find('td:eq(10)').text().trim() + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/DAMaster/Delete_DA",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0) {
                    if (t[0].ErrorCode == 1) {
                        alert(t[0].Messege);
                        ClearAllFields();
                    }
                    else {
                        alert(t[0].Messege);
                    }
                }
            }
        });
    }
}

function SaveUpdate()
{
    var S = "{SecID:'" + $.trim($('#ddlSector').val()) + "', DAID:'" + $.trim($('#hdnDAID').val()) + "', EmpType:'" + $.trim($('#ddlEmpType').val()) +
        "', OrderNo:'" + $.trim($('#txtOrderNo').val()) + "', OrderDate:'" + $.trim($('#txtOrderDate').val()) + "', DARate:'" + $.trim($('#txtDARate').val()) +
        "', MaxAmount:'" + $.trim($('#txtMaxAmt').val()) + "', EffectiveFrom:'" + $.trim($('#txtEffectFrom').val()) + "', EffectiveTo:'" + $.trim($('#txtEffectTo').val()) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/DAMaster/SaveUpdate_DA",
        data: S,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                if (t[0].ErrorCode == 1) {
                    alert(t[0].Messege);
                    ClearAllFields();
                }
                else {
                    alert(t[0].Messege);
                }
            }
        }
    });
}

