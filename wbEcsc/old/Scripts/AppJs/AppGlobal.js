﻿var APP_GLOBAL = {};
$(document).ready(function () {
    APP_GLOBAL.SERVER_OBJ = {};
    APP_GLOBAL.APP_OBJ = {};
    APP_GLOBAL.SERVER_OBJ.LOGIN_LOGOUT = {
        LogOut: function (callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/LoginManager/LogOut",
                data: null,
                dataType: "json",
                success: function (data) {
                    callBack(data);
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
    };
    APP_GLOBAL.APP_OBJ.LOGIN_LOGOUT = {
        LogOut: function () {
            APP_GLOBAL.SERVER_OBJ.LOGIN_LOGOUT.LogOut(function (response) {
                if (response.Status === 200)
                {
                    var data = response.Data;
                    location.href = data.RedirectTo;
                }
                else {
                    window.location.href = "/Home/AdministrativeLoginUI";
                    //alert('Error: Can not logout successfully');
                }
            });
        }
    };
    APP_GLOBAL.APP_OBJ.BIND_EVENTS = function () {
        $(".btnLogOut").on('click', function () {
            APP_GLOBAL.APP_OBJ.LOGIN_LOGOUT.LogOut();
        });
    };

    (function () {
        APP_GLOBAL.APP_OBJ.BIND_EVENTS();
    }());
});