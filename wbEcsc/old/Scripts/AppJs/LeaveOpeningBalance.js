﻿$(function () {
    var Edit = "";
    var Delete = "";
    var Update = "";

    var saveID = "";
    var UpdateID = "";

    var Selector = {
        ddlSector:$("#ddlSector"),
        txtOpeningDate: $("#txtOpeningDate"),
        txtEmployeeName: $("#txtEmployeeName"),
        hdnEmployeeID: $("#hdnEmployeeID"),
        txtLeaveAbbraviation: $("#txtLeaveAbbraviation"),
        hdnLeaveAbbraviationID: $("#hdnLeaveAbbraviationID"),
        txtLeaveBalance: $("#txtLeaveBalance"),      
        btnSave: $("#btnSave"),
        btnRefresh: $("#btnRefresh"),
        tblData: $(".clstbl")      
    };
    var objServerSide = {
        GetMethod: function (URL, callback) {
            $.ajax({
                type: "get",
                contentType: "application/json;charset=utf-8;",
                data: null,
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    callback(responce);
                },
                error: function () {
                    callback("error");
                }
            });
        },
        PostMehtod: function (URL, Data, callback) {
            $.ajax({
                type: "post",
                contentType: "application/json;charset=utf-8;",
                data: JSON.stringify(Data),
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    callback(responce);
                },
                error: function () {
                    callback("error");
                }
            });
        }
    };
    var objClientSide = {
        Initialization: function () {
            objClientSide.Event.onLoad();
            objClientSide.Event.onClick();
            objClientSide.Event.onKeydown();
            objClientSide.Event.onKeyUp();
            objClientSide.Event.onBlur();
            objClientSide.Event.onChange();
            objClientSide.Event.onAutocomplete();
        },
        Event: {
            onLoad: function () {
                // Selector.tbl.DataTable();
               // Selector.txtOpeningDate.datepicker();
                Selector.txtOpeningDate.datepicker({
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    closeText: 'X',
                    showAnim: 'drop',
                    showButtonPanel: true,
                    duration: 'slow',
                    dateFormat: 'dd/mm/yy',
                    onSelect: function () {
                        objClientSide.Method.Search();
                    }
                });
                objClientSide.Method.GetFieldName();
                objClientSide.Method.GetData();              
            },
            onClick: function () {
                Selector.btnSave.click(function () {
                    objClientSide.Method.Save();
                });
                Selector.btnRefresh.click(function () {
                    objClientSide.Method.Refresh();
                });
                $(document).on("click", ".clsUpdate", function () {
                    saveID = UpdateID;
                    var OpeningDate = $(this).closest(".row").find(".clsOpeningDate").text();
                    var Abbraviation = $(this).closest(".row").find(".clsAbbraviation").text();
                    var AbbraviationID = $(this).closest(".row").find(".clsAbbraviationID").text();
                    var EmployeeName = $(this).closest(".row").find(".clsEmployeeName").text();
                    var EmployeeID = $(this).closest(".row").find(".clsEmployeeID").text();
                    var OpeningAmount = $(this).closest(".row").find(".clsOpeningAmount").text();

                    Selector.txtOpeningDate.prop('disabled', true);
                    Selector.txtEmployeeName.prop('disabled', true);
                    Selector.txtLeaveAbbraviation.prop('disabled', true);

                    Selector.txtOpeningDate.val(OpeningDate);
                    Selector.txtEmployeeName.val(EmployeeName);
                    Selector.hdnEmployeeID.val(EmployeeID);
                    Selector.txtLeaveAbbraviation.val(Abbraviation);
                    Selector.hdnLeaveAbbraviationID.val(AbbraviationID);
                    Selector.txtLeaveBalance.val(OpeningAmount);
                    Selector.btnSave.val(Update);
                });
                $(document).on("click", ".clsDelete", function () {
                    var OpeningDate = $(this).closest(".row").find(".clsOpeningDate").text();
                    var Abbraviation = $(this).closest(".row").find(".clsAbbraviation").text();
                    var AbbraviationID = $(this).closest(".row").find(".clsAbbraviationID").text();
                    var EmployeeName = $(this).closest(".row").find(".clsEmployeeName").text();
                    var EmployeeID = $(this).closest(".row").find(".clsEmployeeID").text();
                    var OpeningAmount = $(this).closest(".row").find(".clsOpeningAmount").text();

                    var Data = {
                        SectorID: Selector.ddlSector.val(), OpeningDate: OpeningDate,
                        AbbraviationID: AbbraviationID, Abbraviation: "", EmployeeName: "",
                        EmployeeID: EmployeeID, OpeningAmount: OpeningAmount,
                        FieldID: ''
                    };

                    if (confirm("Are you sure want to Delete this Record ??")) {
                        objClientSide.Method.Delete(Data);
                    }
                });
            },
            onKeydown: function () {
                $('#txtOpeningDate,#txtEmployeeName,#txtLeaveAbbraviation').keypress(function () {
                    objClientSide.Method.Search();
                });              
                $(document).on('keypress', '.money', function (e) {
                    if (!(e.keyCode > 45 && e.keyCode < 58) || e.keyCode == 47 || (e.keyCode == 46 && $(this).val().indexOf('.') != -1)) {
                        return false;
                    }
                });
            },
            onKeyUp: function () {
                Selector.txtEmployeeName.keyup(function () {
                    if ($(this).val().trim()=="") {
                        Selector.hdnEmployeeID.val("");
                    }
                });
                Selector.txtLeaveAbbraviation.keyup(function () {
                    if ($(this).val().trim() == "") {
                        Selector.hdnLeaveAbbraviationID.val("");
                    }
                });
            },
            onBlur: function () {
                $('#txtOpeningDate, #txtEmployeeName, #txtLeaveAbbraviation').blur(function () {
                    objClientSide.Method.Search();
                });
            },
            onChange: function () {
                Selector.ddlSector.change(function () {
                    objClientSide.Method.GetData();
                });
            },
            onAutocomplete: function () {
                Selector.txtEmployeeName.autocomplete({
                    source: function (request, responce) {
                     //   string SectorID, string empaname
                        var data = { SectorID: Selector.ddlSector.val(), empaname: Selector.txtEmployeeName.val().trim() };
                        var url = "/Administration/Master/LeaveOpeningBalance/AutocompleteEmployeeName";
                        var array = [];
                        objServerSide.PostMehtod(url, data, function (responce) {
                            var dt = responce.Data;
                         //   console.log(dt);
                            $.each(dt, function (index, value) {
                                var obj = {
                                    label: value.employeeName,
                                    value:value.EmployeeID
                                }
                                array.push(obj);
                            });                           
                        });
                        responce(array);                       
                    },
                    select: function (e, i) {
                        e.preventDefault();
                        Selector.txtEmployeeName.val(i.item.label);
                        Selector.hdnEmployeeID.val(i.item.value);
                        objClientSide.Method.Search();
                    },
                    focus: function (e,i) {
                        e.preventDefault();
                        Selector.txtEmployeeName.val(i.item.label);
                        Selector.hdnEmployeeID.val(i.item.value);
                        objClientSide.Method.Search();
                    }
                });
                Selector.txtLeaveAbbraviation.autocomplete({
                    source: function (request,responce ) {
                        var data = { Abbraviation: Selector.txtLeaveAbbraviation.val().trim() };
                        var url = "/Administration/Master/LeaveOpeningBalance/AutocompleteAbbravition";
                        var array = [];
                        objServerSide.PostMehtod(url, data, function (responce) {
                            var dt = responce.Data;
                            $.each(dt, function (index, value) {
                             //   console.log(value);
                                var obj = {
                                    label: value.Abbraviation,
                                    value: value.LeaveID
                                }
                                array.push(obj);
                            });
                        });
                        responce(array);
                    },
                    select: function (e, i) {
                        e.preventDefault();
                        Selector.txtLeaveAbbraviation.val(i.item.label);
                        Selector.hdnLeaveAbbraviationID.val(i.item.value);
                        objClientSide.Method.Search();
                    },
                    focus: function (e, i) {
                        e.preventDefault();
                        Selector.txtLeaveAbbraviation.val(i.item.label);
                        Selector.hdnLeaveAbbraviationID.val(i.item.value);
                        objClientSide.Method.Search();
                    }
                });
            }
        },
        Method: {            
            GetFieldName: function () {
                var URL = "/Administration/Master/LeaveOpeningBalance/Get_FieldName";
                objServerSide.GetMethod(URL, function (responce) {
                    var jsdata = responce.Data;
                    $.each(jsdata, function (index, value) {
                        index = index + 1;
                        var star = value.IsMand == 1 ? '*' : "";
                        $("#" + index + "").text(value.FieldName);
                        $("#10" + index + "").text(star);
                        if (value.FieldID == 5) {
                            saveID= value.FieldID;
                            Selector.btnSave.val(value.FieldName);
                        }
                        if (value.FieldID == 6) {
                            Edit = value.FieldName;
                        }
                        if (value.FieldID == 7) {
                            UpdateID= value.FieldID;
                            Update = value.FieldName;
                        }
                        if (value.FieldID == 8) {
                            Delete = value.FieldName;
                        }
                    });
                });
            },           
            GetData: function () {
                var Data = { sectorid: Selector.ddlSector.val(), openingDate: Selector.txtOpeningDate, EmpID: Selector.hdnEmployeeID, AbbraviationID: Selector.hdnLeaveAbbraviationID.val() };
                var url = "/Administration/Master/LeaveOpeningBalance/Get_Data";
              //  console.log(Data);
                objServerSide.PostMehtod(url, Data, function (responce) {
                    var jsdata = responce.Data;
               //     console.log(jsdata);
                    objClientSide.Method.AppendTable(jsdata);
                });
            },
            AppendTable: function (jsdata) {
            //    console.log(jsdata);               
                Selector.tblData.empty();
                $.each(jsdata, function (index, value) {
                    index = index + 1;
                    Selector.tblData.append("<div class='row border-top'>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1'>" + index + "</div>"
                        + "<div class='col-lg-0 col-md-0 col-sm-0 clsSectorID hide'>" + value.SectorID + "</div>"
                        + "<div class='col-lg-2 col-md-2 col-sm-2 clsOpeningDate'>" + value.OpeningDate + "</div>"
                        + "<div class='col-lg-0 col-md-0 col-sm-0 clsEmployeeID hide'>" + value.EmployeeID + "</div>"
                        + "<div class='col-lg-3 col-md-3 col-sm-3 clsEmployeeName'>" + value.EmployeeName + "</div>"
                        + "<div class='col-lg-0 col-md-0 col-sm-0 clsAbbraviationID hide'>" + value.AbbraviationID + "</div>"
                        + "<div class='col-lg-2 col-md-2 col-sm-2 clsAbbraviation'>" + value.Abbraviation + "</div>"                     
                        + "<div class='col-lg-2 col-md-2 col-sm-2 clsOpeningAmount'>" + value.OpeningAmount + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsUpdate text-center text-info' style='cursor:pointer' >" + Edit + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsDelete text-center text-default' style='cursor:pointer'>" + Delete + "</div>"
                        + "</div>");
                });
               // Selector.tbl.DataTable();
            },
            Save: function () {
                var Data = {
                    SectorID: Selector.ddlSector.val(), OpeningDate: Selector.txtOpeningDate.val().trim(),
                    AbbraviationID: Selector.hdnLeaveAbbraviationID.val(), Abbraviation: "", EmployeeName: "",
                    EmployeeID: Selector.hdnEmployeeID.val(), OpeningAmount: Selector.txtLeaveBalance.val().trim(),
                    FieldID: saveID
                };
             //   console.log(Data);
                var URL = "/Administration/Master/LeaveOpeningBalance/Save";
                objServerSide.PostMehtod(URL, Data, function (responce) {
                    var Message = responce.Message;
                    var code = responce.Data;
                    alert(Message);
                    if (code.IsSuccess == 1) {
                        objClientSide.Method.Refresh();
                    } else {
                        objClientSide.Method.Focus(code.FocusCode);
                    }
                });
            },
            Delete: function (data) {              
                var URL = "/Administration/Master/LeaveOpeningBalance/Delete";
                objServerSide.PostMehtod(URL, data, function (responce) {
                    var Message = responce.Message;
                    alert(Message);
                    objClientSide.Method.Refresh();
                });
            },
            Refresh: function () {
                location.reload();
            },
            Focus: function (num) {
                if (num == 1) {
                    Selector.txtOpeningDate.focus();                   
                }
                if (num == 2) {
                    Selector.txtEmployeeName.focus();
                }
                if (num == 3) {
                    Selector.txtLeaveAbbraviation.focus();
                }
                if (num == 4) {
                    Selector.txtLeaveBalance.focus();
                }               
            },
            Search: function (obj) {
                var date = Selector.txtOpeningDate.val().trim();
                var employee = Selector.txtEmployeeName.val().trim().toUpperCase();
                var lvabb = Selector.txtLeaveAbbraviation.val().trim().toUpperCase();
                var tbl = Selector.tblData.find('.row');
                console.log(employee);
                $.each(tbl, function (index, value) {
                    var dt = $(this).find('.clsOpeningDate').text();
                    var emp = $(this).find('.clsEmployeeName').text().toUpperCase();
                    var abr = $(this).find('.clsAbbraviation').text().toUpperCase();

                    if(dt.indexOf(date)>-1 && emp.indexOf(employee)>-1 && abr.indexOf(lvabb)>-1)
                    {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }

                });
                //Selector.clstbl.find('.row').find('.clsGenderID:contains(' + GenderID + ')').closest('.row').show();
                //Selector.clstbl.find('.row').find('.clsGenderID:not(:contains(' + GenderID + '))').closest('.row').hide();
            }
        }
    };

    objClientSide.Initialization();
});

