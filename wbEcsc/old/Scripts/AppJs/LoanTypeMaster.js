﻿
$(function () {
    var Edit = "";
    var Delete = "";
    var Update = "";

    var Selector = {
        ddlLoanType: $("#ddlLoanType"),
        hdnLoanTypeID: $("#hdnLoanTypeID"),
        txtDescription: $("#txtDescription"),
        txtAbbreviation: $("#txtAbbreviation"),
        txtRank: $("#txtRank"),
        chckIsActive: $("#chckIsActive"),
        chckIsRound: $("#chckIsRound"),       
        btnSave: $("#btnSave"),
        btnRefresh: $("#btnRefresh"),
        clstbl: $(".clstbl")
    };
    var objServerSide = {
        GetMethod: function (URL, callback) {
            $.ajax({
                type: "get",
                contentType: "application/json;charset=utf-8;",
                data: null,
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    callback(responce);
                },
                error: function () {
                    callback("error");
                }
            });
        },
        PostMehtod: function (URL, Data, callback) {
            $.ajax({
                type: "post",
                contentType: "application/json;charset=utf-8;",
                data: JSON.stringify(Data),
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    callback(responce);
                },
                error: function () {
                    callback("error");
                }
            });
        }
    }
    var objClientSide = {
        Initialization: function () {
            objClientSide.Event.onLoad();
            objClientSide.Event.onClick();
            objClientSide.Event.onKeydown();
            objClientSide.Event.onBlur();
        },
        Event: {
            onLoad: function () {               
                objClientSide.Method.GetFieldName();
                objClientSide.Method.GetType();
                objClientSide.Method.GetLoanTypeData();               
            },
            onClick: function () {
                Selector.btnSave.click(function () {
                    objClientSide.Method.Save();
                });
                Selector.btnRefresh.click(function () {
                    objClientSide.Method.Refresh();
                });
                $(document).on("click", ".clsUpdate", function () {
                    var ID = $(this).closest(".row").find(".clsLoanTypeID").text();
                    var TypeID = $(this).closest(".row").find(".clsTypeID").text();
                    var Description = $(this).closest(".row").find(".clsDescription").text();
                    var Abbreviation = $(this).closest(".row").find(".clsAbbreviation").text();
                    var Rank = $(this).closest(".row").find(".clsRank").text();
                    var IsActive = $(this).closest(".row").find(".clsIsActive").text();
                    var IsRound = $(this).closest(".row").find(".clsIsRound").text();
                    objClientSide.Method.UncheckedCheckBox();
                    Selector.chckIsActive.prop("disabled", false);
                    if (IsActive=='Y') {
                        Selector.chckIsActive.prop('checked', true);
                    }
                    if (IsRound == 1) {
                        Selector.chckIsRound.prop('checked', true);
                    }
                    Selector.hdnLoanTypeID.val(ID);
                    Selector.txtDescription.val(Description);
                    Selector.txtAbbreviation.val(Abbreviation);
                    Selector.txtRank.val(Rank);
                    Selector.ddlLoanType.val(TypeID);
                    Selector.btnSave.val(Update);
                });
                $(document).on("click", ".clsDelete", function () {
                    var ID = $(this).closest(".row").find(".clsLoanTypeID").text();
                    if (Selector.hdnLoanTypeID.val() == ID) {
                        alert("this Record in Delete Mode. You cannt Delete this record now..");
                        return false;
                    }

                    if (confirm("Are you sure want to Delete this Record ??")) {

                        objClientSide.Method.Delete(ID);
                    }
                });
            },
            onKeydown: function () {
                Selector.txtRank.keypress(function (e) {
                    if (!(e.keyCode > 47 && e.keyCode < 58)) {
                        return false;
                    }
                });
                Selector.txtAbbreviation.keypress(function () {
                  //  objClientSide.Method.Search();
                });
                Selector.txtDescription.keypress(function () {
                 //   objClientSide.Method.Search();
                });
            },
            onBlur: function () {                
                Selector.txtDescription.blur(function () {
                  //  objClientSide.Method.Search();
                });               
            }
        },
        Method: {           
            GetFieldName: function () {
                var URL = "/Administration/Master/LoanTypeMaster/Get_FieldName";
                objServerSide.GetMethod(URL, function (responce) {
                    var jsdata = responce.Data;
                    $.each(jsdata, function (index, value) {
                        index = index + 1;
                        var star = value.IsMand == 1 ? '*' : "";
                        $("#" + index + "").text(value.FieldName);
                        $("#10" + index + "").text(star);
                        if (value.FieldID == 7) {
                            Selector.btnSave.val(value.FieldName);
                        }
                        if (value.FieldID == 8) {
                            Edit = value.FieldName;
                        }
                        if (value.FieldID == 9) {
                             Delete = value.FieldName;
                        }
                        if (value.FieldID == 10) {
                            Update = value.FieldName;
                        }
                    });
                });
            },
            GetType: function () {
                var URL = "/Administration/Master/LoanTypeMaster/Get_Type";
                objServerSide.GetMethod(URL, function (responce) {
                    var jsdata = responce.Data;
                    Selector.ddlLoanType.empty();
                    Selector.ddlLoanType.append("<option value=''>Select Type</option>")
                    $.each(jsdata, function (index, value) {
                        Selector.ddlLoanType.append("<option value='" + value.LoanTypeID + "'>" + value.LoanDesc + "</option>");
                    });
                });
            },
            GetLoanTypeData: function () {                
                var url = "/Administration/Master/LoanTypeMaster/Get_Data";
                //  console.log(Data);
                objServerSide.GetMethod(url, function (responce) {
                    var jsdata = responce.Data;
                    console.log(jsdata);
                    objClientSide.Method.AppendTable(jsdata);
                });
            },
            AppendTable: function (jsdata) {
                console.log(jsdata);
                Selector.clstbl.empty();
                $.each(jsdata, function (index, value) {
                    index = index + 1;                 

                    Selector.clstbl.append("<div class='row border-top'>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsLoanTypeID hide'>" + value.LoanTypeID + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1'>" + index + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 hide clsTypeID'>" + value.TypeID + "</div>"
                        + "<div class='col-lg-2 col-md-2 col-sm-2 clsTypeName'>" + value.TypeName + "</div>"
                        + "<div class='col-lg-2 col-md-2 col-sm-2 clsDescription'>" + value.Description + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsAbbreviation'>" + value.Abbreviation + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsRank'>" + value.Rank + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 hide clsIsActive'>" + value.IsActive + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1'>" + value.IsActiveText + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 hide clsIsRound'>" + value.IsRound + "</div>"
                        + "<div class='col-lg-2 col-md-2 col-sm-2'>" + value.IsRoundText + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsUpdate text-center text-info' style='cursor:pointer' >" + Edit + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsDelete text-center text-default' style='cursor:pointer'>" + Delete + "</div>"
                        + "</div>");
                });
            },
            Save: function () {
                var IsActive = 'Y';
                var IsRound = 1;

                if (!Selector.chckIsActive.is(':checked')) {
                    IsActive = 'N';
                }

                if (!Selector.chckIsRound.is(':checked')) {
                    IsRound = 0;
                }

                var Data = {
                    LoanTypeID: Selector.hdnLoanTypeID.val(), TypeID: Selector.ddlLoanType.val(),
                    TypeName:"", Description:Selector.txtDescription.val().trim() ,
                    Abbreviation: Selector.txtAbbreviation.val().trim(), Rank:Selector.txtRank.val().trim() , IsActive: IsActive,
                    IsActiveText: "", IsRound: IsRound,
                    IsRoundText: ""
                };
                console.log(Data);
                var URL = "/Administration/Master/LoanTypeMaster/Save";
                objServerSide.PostMehtod(URL, Data, function (responce) {
                    var Message = responce.Message;
                    var code = responce.Data;
                    alert(Message);
                    console.log(code);
                    if (code.data2 == 0) {
                        objClientSide.Method.Focus(code.data1);
                    }
                    if (code.data2 == 1) {
                        objClientSide.Method.Refresh();
                    }
                });
            },
            Delete: function (ID) {
                var Data = { LoanTypeID: ID };
                var URL = "/Administration/Master/LoanTypeMaster/Delete";
                objServerSide.PostMehtod(URL, Data, function (responce) {
                    var Message = responce.Message;
                    alert(Message);
                    objClientSide.Method.Refresh();
                });
            },
            UncheckedCheckBox: function () {
                Selector.chckIsActive.prop('disabled', true);
                $("input[type=checkbox]").prop('checked', true);
            },
            Refresh: function () {             
                Selector.txtDescription.val("");
                Selector.txtAbbreviation.val("");
                Selector.txtRank.val("");
                objClientSide.Method.GetFieldName();
                objClientSide.Method.GetType();
                objClientSide.Method.UncheckedCheckBox();
                objClientSide.Method.GetLoanTypeData();
            },
            Focus: function (num) {
                if(num==1)
                {
                    Selector.ddlLoanType.focus();
                }
                if (num == 2) {
                    Selector.txtDescription.focus();
                }
                if (num == 3) {
                    Selector.txtAbbreviation.focus();
                }
                if (num == 4) {
                    Selector.txtRank.focus();
                }
                if (num == 5) {
                    Selector.chckIsActive.focus();
                }               
            },
            Search: function () {
                var input = Selector.txtDescription.val().trim().toUpperCase();
                var tr = Selector.clstbl.find('.row');
                $.each(tr, function (index, value) {
                    var description = $(this).find('.clsDescription').text();
                    if (description.toUpperCase().indexOf(input) > -1) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            }
        }
    };

    objClientSide.Initialization();
});