﻿
var dniDate = "";
var main = {};

$(document).ready(function () {

    main.Populate();

});

main = {

    Populate: function () {
        var V = "{SectorID: '" + $("#ddlSector").val() + "', SalMonth: '" + $("#txtSalMonth").val() + "'}";
        $.ajax({
            url: '/Administration/Master/Employee_Increment/EmployeeDetail',
            type: 'POST',
            data: V,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (D) {
                var t = D.Data;
                if (t.length > 0 && D.Status == 200) {
                    var table = $('#tbl');

                    table.find('tr.allData').empty();

                    var totalEmp = 0; var totalNotEmp = 0; var color = "";

                    $.each(t, function (index, item) {
                        if (index % 2 == 0) color = "#DEFAF5"; else color = "white";

                        var declinedValue = item.declined;
                        if (declinedValue == 'Y') {
                            color = "#FADBD8";
                            totalNotEmp++;
                        }
                        else
                            totalEmp++;

                        table.append("<tr class='allData' style=' cursor:pointer ;background-color:" + color + "'>"
                            + "<td style='display:none' class='employeeID'>" + item.EmployeeID + "</td>"
                            + "<td class='empNo' style='width:100px; border-bottom:1px solid black;'>" + item.EmpNo + "</td>"
                            + "<td class='empName' style='border-bottom:1px solid black;'>" + item.EmpName + "</td>"
                            + "<td style='width:200px' class='remarks'><input type='text' value='' class='form-control' onchange='return main.ChangeValue(this, \"" + '' + "\")' onkeydown='return !((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) && event.keyCode!=8 && event.keyCode!=46);' /></td>"
                            + "</tr>");

                    });
                }

            }
        });
    },
    ChangeValue: function (txt) {
       
        var EmpID = $(txt).closest('tr').find('.employeeID').text();
        var EmpNo = $(txt).closest('tr').find('.empNo').text();

        var Perct = $(txt).val();

        var C = "{ EmpNo: '" + EmpNo + "',  EmpID : '" + EmpID + "',  Perct: '" + Perct + "'}"; 

       
        $.ajax({
            type: "POST",
            url: '/Administration/Master/Employee_Increment_Perct/Empwise_UpdateIncrement',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var result = D.Data;
                 if (result == "fail") {
                    alert('Sorry ! There is some problem with database !');
                    return false;
                }
                else {
                    $(txt).css("background-color", "lightgreen");
                }
            }
        });

    }

}
function SearchTable() {
    var forSearchprefix = $("#txtSearch").val().trim().toUpperCase();
    var tablerow = $('#tbl').find('.allData');
    $.each(tablerow, function (index, value) {
        var empNo = $(this).find('.empNo').text().toUpperCase();
        var empName = $(this).find('.empName').text().toUpperCase();
        if (empName.indexOf(forSearchprefix) > -1 || empNo.indexOf(forSearchprefix) > -1) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}

