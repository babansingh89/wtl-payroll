﻿
$(document).ready(function () {
    FieldNameData();
    PopulateEmpType();
    Populate_Grid();
    

    $('#txtEffectFrom').datepicker({
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function (dates) {
        var options = {};
        options.url = "/Administration/Master/DAMaster/Get_EffectFromDT";
        options.type = "POST";
        options.data = "{SecID:'" + $("#ddlSector").val() + "', EffectiveFrom:'" + $.trim($("#txtEffectFrom").val()) + "'}";  //, SalFinYear:'" + '2017-2018' + "'
        options.dataType = "json";
        options.contentType = "application/json";
        options.success = function (D) {
            var a = D.Data;
            if (a.length > 0) {
                if (a[0].ErrorCode == 0) {
                    alert(a[0].Messege);
                    $('#txtEffectFrom').val('');
                } else {
                    $('#txtEffectFrom').val(a[0].EffectiveFrom);
                }
            }
        };
        options.error = function () { alert("Error in retrieving Data !"); };
        $.ajax(options);
    }
    });
    $('#txtEffectTo').datepicker({
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });

    $('#btnRefresh').click(ClearAllFields);
    $('#btnSave').click(Save);
    $('#ddlEmpType').change(Populate_Grid);
});

function ClearAllFields()
{
    $('#ddlEmpType').val(''); $('#txtHRA').val(''); $('#txtMaxAmt').val('10000000'); $('#txtEffectFrom').val(''); $('#txtEffectTo').val(''); $('#hdnHRAID').val('');
    $('#btnSave').html('Save'); $("#tbl tr.trclass").empty(); $('#divEffectiveTo').hide();
}

function FieldNameData() {
    $.ajax({
        type: "POST",
        url: "/Administration/Master/HRAMaster/Get_FieldName",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                $.each(D.Data, function (index, value) {
                    var MandatoryStyle = '*';
                    var Id = '#' + value.FieldID;
                    $(Id).text(value.FieldName);
                    $('#spn' + value.FieldID).text(value.IsMand == 1 ? '   ' + MandatoryStyle : '');
                });
            }
        }
    });
}

function PopulateEmpType() {
    var E = "{EmpType:'" + $('#ddlEmpType').val() + "'}";
    var options = {};
    options.url = "/Administration/Master/HRAMaster/Get_HRAInGrid";
    options.type = "POST";
    options.data = E;
    options.dataType = "json";
    options.contentType = "application/json";
    options.success = function (list) {
        var a = list.Data.length;
        if (a > 0) {
            $("#ddlEmpType").empty();
            $("#ddlEmpType").append("<option value=''>--- (Select) ---</option>");
            $.each(list.Data, function (index, value) {
                var label = value.EmpTypeDesc;
                var val = value.EmpType;

                $("#ddlEmpType").append($("<option></option>").val(val).html(label));
            });
        }
        else {
            $("#ddlEmpType").empty();
            $("#ddlEmpType").append("<option value=''>--- (Select) ---</option>");
        }
    };
    options.error = function () { alert("Error in retrieving Employee Type !"); };
    $.ajax(options);
}

function Populate_Grid()
{
    var E = "{EmpType:'" + $('#ddlEmpType').val() + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/HRAMaster/Get_HRAInGrid",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
               
                $("#tbl tr.trclass").empty();
                $.each(D.Data, function (index, value) {
                    if (value.HRAID != null) {
                       
                        $("#tbl").append("<tr class='trclass'><td>" +   // style='display:none;'
                            value.EmpTypeDesc + "</td><td style='text-align:center'>" +
                            value.HRA + "</td><td style='text-align:center'>" +
                            value.MaxAmount + "</td><td style='text-align:center'>" +
                            value.EffectiveFrom + "</td><td style='text-align:center'>" +
                            value.EffectiveTo + "</td><td onclick='EditHRA(this)' style='text-align:center; cursor:pointer; color:blue'>" +
                            value.Edit + "</td><td onclick='DeleteHRA(this)' style='text-align:center; cursor:pointer; color:blue'>" +
                            value.Del + "</td><td style='display:none;'>" +
                            value.HRAID + "</td></tr>");
                    }
                });
            }
        }
    });
}

function EditHRA(t)
{
    var EmpType = "";
    if ($(t).closest('tr').find('td:eq(0)').text().trim() == "State Employee") {
        EmpType = "S";
    }
    if ($(t).closest('tr').find('td:eq(0)').text().trim() == "Central Employee") {
        EmpType = "C";
    }
    $('#ddlEmpType').val(EmpType);
    $('#txtHRA').val($(t).closest('tr').find('td:eq(1)').text().trim());
    $('#txtMaxAmt').val($(t).closest('tr').find('td:eq(2)').text().trim());
    $('#txtEffectFrom').val($(t).closest('tr').find('td:eq(3)').text().trim());
    //$('#txtEffectTo').val($(t).closest('tr').find('td:eq(4)').text().trim());
    $('#hdnHRAID').val($(t).closest('tr').find('td:eq(7)').text().trim());

    $('#btnSave').html('Update'); $('#divEffectiveTo').show();
}

function DeleteHRA(t)
{
    var conf = confirm("Are sure want to delete this HRA ?");
    if (conf == true) {
        var S = "{HRAID:'" + $(t).closest('tr').find('td:eq(7)').text() + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/HRAMaster/Delete_HRA",
            data: S,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var xmlDoc = $.parseXML(D.Data);
                var xml = $(xmlDoc);
                var Table = xml.find("Table");
                if (Table.length > 0) {
                    if ($(Table).find("ErrorCode").text() == 1) {
                        alert($(Table).find("Messege").text());
                        ClearAllFields();
                    }
                    else {
                        alert($(Table).find("Messege").text());
                    }
                }
            }
        });
    }
}

function Save()
{
    var SaveUpdate = "";
    if ($('#hdnHRAID').val() == '') { SaveUpdate = "Save"; } else { SaveUpdate = "Update"; }
    var conf = confirm("Are sure want to " +SaveUpdate+ " this HRA ?");
    if (conf == true) {
        var S = "{ddlSector:'" + $.trim($('#ddlSector').val()) + "', HRAID:'" + $.trim($('#hdnHRAID').val()) +
            "', ddlEmpType:'" + $.trim($('#ddlEmpType').val()) + "', txtHRA:'" + $.trim($('#txtHRA').val()) +
            "', txtMaxAmt:'" + $.trim($('#txtMaxAmt').val()) + "', txtEffectFrom:'" + $.trim($('#txtEffectFrom').val()) +
            "', txtEffectTo:'" + $.trim($('#txtEffectTo').val()) + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/HRAMaster/Save_HRA",
            data: S,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var xmlDoc = $.parseXML(D.Data);
                var xml = $(xmlDoc);
                var Table = xml.find("Table");
                if (Table.length > 0) {
                    if ($(Table).find("ErrorCode").text() == 1) {
                        alert($(Table).find("Messege").text());
                        ClearAllFields();
                    }
                    else {
                        alert($(Table).find("Messege").text());
                    }
                }
            }
        });
    }
}