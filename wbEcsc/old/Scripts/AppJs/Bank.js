﻿
$(function () {
    myFunction();

    $("#btnRefresh").click(function () {
        window.location.href = "Index";
    });
});

function myFunction() {
    var input, tr;
    input = $('#txtBankName').val().toUpperCase();
    tr = $('#myTable').find('.clsrow');
    $.each(tr, function (index, value) {
        var bankname = $(this).find('.clsbankname').text();
        if (bankname.toUpperCase().indexOf(input) > -1) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}