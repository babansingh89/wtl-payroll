﻿$(document).ready(function ()
{

    FieldNameData();
    //GridData(0);
    CalendarYear();
    LoadCalender();



    $('#btnSave').click(SaveUpdate);
    $('#btnRefresh').click(ClearAllFields);
    $('#btndo').click(doFucntion);
});

function LoadCalender() {
    var MaxSalDt = $('#MaxSalDt').val();
    var dateObj = new Date(MaxSalDt);
    var dbyear = dateObj.getUTCFullYear(); 
    var year = $("#txtYear").val();
    
    var yearrange = year + ':' + year;
    var setDate = "1/1/"+ year;

    $('#txtFromDate, #txtToDate').datepicker({
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        //changeYear: true,
        changeMonth: true,
        yearRange: ''+year+'',
        defaultDate: ""+setDate+"",
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onClose: function () {
            var FromDate = $("#txtFromDate").val();
            var ToDate = $("#txtToDate").val();
            if (FromDate != "" & ToDate != "") {
                if (FromDate <= ToDate)
                { }
                else {
                    $("#txtToDate").val("");
                    alert("FromDate Must be greater than ToDate");
                }
            }
        }
    });
    if (year == dbyear)
    {
        $('#txtFromDate').datepicker("option", "minDate", MaxSalDt);
        $("#txtFromDate").datepicker("option", "maxDate", "31/12/" + year);
        $('#txtToDate').datepicker("option", "minDate", MaxSalDt);
        $("#txtToDate").datepicker("option", "maxDate", "31/12/" + year);
    }
    else {
        $('#txtFromDate').datepicker("option", "minDate", new Date(year, 1 - 1, 1));
        $("#txtFromDate").datepicker("option", "maxDate", "31/12/" + year);
        $('#txtToDate').datepicker("option", "minDate", new Date(year, 1 - 1, 1));
        $("#txtToDate").datepicker("option", "maxDate", "31/12/" + year);
    }
    
}

function FieldNameData() {
    $.ajax({
        type: "POST",
        url: "/Administration/Master/HolidayMaster/Get_FieldName",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                $.each(D.Data, function (index, value) {
                    var MandatoryStyle = '*';
                    var Id = '#' + value.FieldId;
                    $(Id).text(value.FieldName);
                    $('#spn' + value.FieldId).text(value.IsMand == 1 ? '   ' + MandatoryStyle : '');
                });
            }
        }
    });
}

function CalendarYear() {
    var E = "{txtYear:'" + $.trim($("#txtYear").val()) + "', SectorId:'" + $.trim($('#ddlSector').val()) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/HolidayMaster/Load_CalendarYear",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            if (D.Status == 200) {
                             
                var t = "";
                if (t != null) {
                    $.each(D.YearList, function (index, value) {
                        t = t + '<option value="' + value.Year + '">' + value.Year + '</option>'
                    })
                    $('#txtYear').empty().attr('disabled', false).append("<option >--- (Select) ---</option>")
                    $('#txtYear').append(t);
                }
            }
            else {
                alert('No data found');
            }
        }
    });

    $.ajax({
        type: "POST",
        url: "/Administration/Master/HolidayMaster/Get_MaxSalDt",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            if (D.Status == 200) {
                var t = D.Data;
                if (t.length > 0) {
                    $('#MaxSalDt').val(t);
                }
            }
            else {
                alert('No data found');
            }
        }
    });
}

function LoadData() {
    LoadCalender();
    var MaxSalDt = new Date($('#MaxSalDt').val());
    var E = "{txtYear:'" + $.trim($("#txtYear").val()) + "', SectorId:'" + $.trim($('#ddlSector').val()) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/HolidayMaster/Load_CalendarYear",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            if (D.Status == 200) {
            
                $("#tbl tr.trclass").empty();
                $.each(D.holidayMaster, function (index, value) {
                    var i = 0;
                    var fromdart = new Date(value.FromHDate);
                    if (MaxSalDt > fromdart)
                    {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    $('#tbl').append("<tr class='trclass'> <td style='display:none'>" +
                           value.HdNo +                 //0
                        "</td><td style='text-align:center; display:none;'>" +
                            value.HType +            //1
                        "</td><td style='text-align:center'>" +
                            value.FromHDate +        //2
                        "</td><td style='text-align:center'>" +
                            value.ToHDate +         //3
                        "</td><td style='text-align:center'>" +
                            value.HDesc +            //4
                        "</td><td style='text-align:center; cursor:pointer; color:blue' " + ((i == 1) ? "" : "onclick='EditHolidayMaster(this)'") + " >" +
                             "" + ((i == 1) ? "" : "Edit") + "" +
                        "</td><td style='text-align:center; cursor:pointer; color:blue' onclick='DeleteHolidayMaster(this)'>" +
                            "Delete" +
                        "</td></tr>"
                    );
                });
            }
            else {
                alert('No data found');
            }
        }
    });
    
}

function EditHolidayMaster(t) {

    //$('#divEffectiveTo').show();
    $('#btnSave').html('Update');
    $('#hdnHolidayID').val($(t).closest('tr').find('td:eq(0)').text().trim());
    $('#ddlHolidayType').val($(t).closest('tr').find('td:eq(1)').text().trim());
    $('#txtFromDate').val($(t).closest('tr').find('td:eq(2)').text().trim());
    $('#txtToDate').val($(t).closest('tr').find('td:eq(3)').text().trim());
    $('#txtDescription').val($(t).closest('tr').find('td:eq(4)').text().trim());
}

function DeleteHolidayMaster(t) {
    var conf = confirm("Are you sure want to Delete this Record ?");
    if (conf == true) {
        var HolidayID = $(t).closest('tr').find('td:eq(0)').text().trim();
        
        var E = "{hdnHolidayID:'" + HolidayID + "', SectorId:'" + $.trim($('#ddlSector').val()) + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/HolidayMaster/Delete_HolidayMaster",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {

                var t = D.Data;
                if (t.length > 0) {
                    if (t[0].ErrorCode == 1) {
                        alert(t[0].Messege);
                        ClearAllFields();
                        LoadData();
                    }
                    else {
                        alert(t[0].Messege);
                    }
                }
            },
            failure: function (D) {
                alert(D.Message)
            }
        });
    }
}

function SaveUpdate() {

    var S = "{hdnHolidayID:'" + $.trim($('#hdnHolidayID').val()) +
        "', txtYear:'" + $.trim($('#txtYear').val()) +
        "', ddlHolidayType:'" + $.trim($('#ddlHolidayType').val()) +
        "', txtFromDate:'" + $.trim($('#txtFromDate').val()) +
        "', txtToDate:'" + $.trim($('#txtToDate').val()) +
        "', txtDescription:'" + $.trim($('#txtDescription').val()) +
        "', SectorId:'" + $.trim($('#ddlSector').val()) +
        "', UserID:'" + $.trim($('#UserID').val()) + "'}";

    $.ajax({
        type: "POST",
        url: "/Administration/Master/HolidayMaster/SaveUpdate_HolidayMaster",
        data: S,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                if (t[0].ErrorCode == 1) {
                    alert(t[0].Messege);
                    LoadData();
                    ClearAllFields();                   
                }
                else {
                    alert(t[0].Messege);
                }
            }
        }
    });
}

function ClearAllFields() {
    $("#hdnHolidayID").val('');
    $("#ddlHolidayType").val('');
    $("#txtFromDate").val('');
    $("#txtToDate").val('');
    $("#txtDescription").val('');
    $('#btnSave').html('Save');
}

function doFucntion() {
    $("#txtToDate").val($("#txtFromDate").val());
}