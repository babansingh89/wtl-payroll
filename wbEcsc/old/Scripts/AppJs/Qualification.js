﻿//new
$(function () {
  
    var Edit = "";
    var Delete = "";
    var Update = "";
    var Selector = {
        txtQualification: $("#txtQualification"),
        hdnQualificationID:$("#hdnQualificationID"),
        btnSave: $("#btnSave"),
        btnRefresh: $("#btnRefresh"),
        clstbl: $(".clstbl"),
    };
    var objServerSide = {
        GetMethod: function (URL, callback) {
            $.ajax({
                type: "get",
                contentType: "application/json;charset=utf-8;",
                data: null,
                dataType: "json",
                async:false,
                url: URL,
                success: function (responce) {                   
                    callback(responce);
                },
                error: function () {
                    callback("error");
                }
            });
        },
        PostMehtod: function (URL, Data, callback) {
            $.ajax({
                type: "post",
                contentType: "application/json;charset=utf-8;",
                data: JSON.stringify(Data),
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    callback(responce);
                },
                error: function () {
                    callback("error");
                }
            });
        }
    }
    var objClientSide = {
        Initialization: function () {
            objClientSide.Event.onLoad();
            objClientSide.Event.onClick();
            objClientSide.Event.onKeyUp();
        },
        Event: {           
            onLoad: function () {
                objClientSide.Method.GetFieldName();
                objClientSide.Method.GetQualification();               
            },
            onClick: function () {
                Selector.btnSave.click(function () {
                    objClientSide.Method.Save();
                });
                Selector.btnRefresh.click(function () {
                    objClientSide.Method.Refresh();
                });
                $(document).on("click", ".clsUpdate", function () {
                    var ID = $(this).closest(".row").find(".clsID").text();
                    var Qualification = $(this).closest(".row").find(".clsQualification").text();
                    Selector.hdnQualificationID.val(ID);
                    Selector.txtQualification.val(Qualification);
                    Selector.btnSave.val(Update);
                });
                $(document).on("click", ".clsDelete", function () {
                    var QualificationID = $(this).closest(".row").find(".clsID").text();
                    if (Selector.hdnQualificationID.val() == QualificationID)
                    {
                        alert("this Record in Delete Mode. You cannt Delete this record now..");
                        return false;
                    }

                    if (confirm("Are you sure want to Delete this Record ??"))
                    {
                       
                        objClientSide.Method.Delete(QualificationID);
                    }                   
                });
            },            
            onKeyUp: function () {
                Selector.txtQualification.keyup(function () {
                    objClientSide.Method.Search();
                });
            }
        },
        Method: {
            GetFieldName: function () {
                var URL = "/Administration/Master/Qualification/Get_FieldName";
                objServerSide.GetMethod(URL, function (responce) {
                    var jsdata = responce.Data;
                    $.each(jsdata, function (index, value) {
                        index = index + 1;
                        var star = value.IsMand == 1 ? '*' : "";
                        $("#" + index + "").text(value.FieldName);
                        $("#" + index + "0").text(star);
                        if (value.FieldID == 2) {
                            Selector.btnSave.val(value.FieldName);
                        }
                        if (value.FieldID == 3) {
                            Edit = value.FieldName;
                        }
                        if (value.FieldID == 4) {
                            Update = value.FieldName;
                        }
                        if (value.FieldID == 5) {
                            Delete = value.FieldName;
                        }
                    });
                });
            },
            GetQualification: function () {
                var url = "/Administration/Master/Qualification/Get_Qualification"; 
                objServerSide.GetMethod(url, function (responce) {
                    var jsdata = responce.Data;
                    objClientSide.Method.AppendTable(jsdata);
                });
            },
            AppendTable: function (jsdata) {
                Selector.clstbl.empty();
                $.each(jsdata, function (index, value) {
                    index = index + 1;
                    Selector.clstbl.append("<div class='row border-top'>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsID hide'>" + value.QualificationID + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1'>" + index + "</div>"
                        + "<div class='col-lg-7 col-md-7 col-sm-7 clsQualification'>" + value.Qualification + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsUpdate text-center text-info' style='cursor:pointer' >" + Edit + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsDelete text-center text-default' style='cursor:pointer'>" + Delete + "</div>"
                        +"</div>");
                });
            },
            Save: function () {
                var data = { QualificationID: Selector.hdnQualificationID.val(), Qualification: Selector.txtQualification.val().trim() };
                var URL = "/Administration/Master/Qualification/Save";
                objServerSide.PostMehtod(URL, data, function (responce) {
                    var Message = responce.Message;
                    var code=responce.Data;
                    alert(Message);
                    if (code == 1)
                    {
                        objClientSide.Method.Refresh();
                    }                  
                });
            },
            Delete: function (QualificationID) {
                var data = { QualificationID: QualificationID, Qualification: "" };
                var URL = "/Administration/Master/Qualification/Delete";
                objServerSide.PostMehtod(URL, data, function (responce) {
                    var Message = responce.Message;                  
                    alert(Message);                   
                    objClientSide.Method.Refresh();                   
                });
            },
            Search:function(){
                var quliPrefix = Selector.txtQualification.val().trim().toUpperCase();
                var tbl = $('.clstbl').find('.row');
                $.each(tbl, function (index, value) {
                    var quli = $(this).find('.clsQualification').text().toUpperCase();
                    if (quli.indexOf(quliPrefix) > -1) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            },
            Refresh: function () {
                Selector.txtQualification.val("");
                Selector.hdnQualificationID.val("");
                objClientSide.Method.GetFieldName();
                objClientSide.Method.GetQualification();
            }
        }
    };

    objClientSide.Initialization();
});