﻿var isPDFExcel = "";

$(document).ready(function () {

   
    FieldNameData();

    $('#radioPDF').change(function () {
        isPDFExcel = "1";
    });
    $('#radioExcel').change(function () {
        isPDFExcel = "2";
    });

    $("#txtSearchEmpNo").autocomplete({
        source: function (request, response) {

            var E = "{EmpNo: '" + $.trim($("#txtSearchEmpNo").val()) + "'}";

            $.ajax({
                type: "POST",
                url: "/Administration/Master/ProjectedSalary/Search_EmpNoAutoComplete",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.EmpNo,
                                EmpNo: item.EmployeeID
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            var EmpNoID = i.item.EmpNo;
            var a = EmpNoID.split('#');
            var EmployeeID = a[0];
            var EmpNo = a[1];

            $("#hdnEmployeeID").val(EmployeeID);
            Populate_EmployeeDetails(EmpNo);
        },
        minLength: 0
    }).click(function () {
        var SectorID = $("#ddlSector").val();
        if (SectorID == "") {
            alert('Please Select Sector !'); $('#txtSearchEmpNo').val(''); $("#hdnEmployeeID").val(''); $("#ddlSector").focus(); return false;
        }
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtSearchEmpNo').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtSearchEmpNo').val('');
            $('#hdnEmployeeID').val('');
        }
        if (iKeyCode == 46) {
            $('#txtSearchEmpNo').val('');
            $('#hdnEmployeeID').val('');
        }
    });

    $('#btnOriginal').click(function () {
        if (isPDFExcel == "") {
            alert("Please Select Report Print Option !"); return false;
        }
        Orginal();
    });

    $('#btnProjected').click(function () {
        if (isPDFExcel == "") {
            alert("Please Select Report Print Option !"); return false;
        }
        Projected();
    });

    $('#btnRefresh').click(function () {
        location.reload();
    });
});




function Populate_EmployeeDetails(EmpNo)
{
    var EmployeeNo = $("#hdnEmployeeID").val();
    var SectorID = $("#ddlSector").val();


    if (EmpNo != '') {
        var E = "{EmployeeNo: '" + EmpNo + "', SectorID: '" + SectorID + "'}"; 
        $.ajax({
            type: "POST",
            url: "/Administration/Master/ProjectedSalary/Get_Employee",
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0) {
                    $('#txtEmpName').val(t[0].EmpName); 
                    $('#txtDesignation').val(t[0].Designation);
                    $('#txtDOB').val(t[0].DOB);
                    $('#txtDOR').val(t[0].DOR);
                    $('#txtPFNo').val(t[0].PFCode);
                    $('#txtPAN').val(t[0].PAN);
                    $('#txtPayScale').val(t[0].PayScale);
                    $('#txtSector').val(t[0].SectorName);
                    PopulateGrid(EmpNo, SectorID);
                }
            }
        });
    }
}
function PopulateGrid(EmpNo, SectorID) {

    var a = "";
    var E = "{SecID: " + SectorID + ", EmpNo:'" + EmpNo + "', FinYear:'" + $('#txtSalFinYear').val() + "'}";

    $.ajax({
        type: "POST",
        url: "/Administration/Master/ProjectedSalary/Populate_EmpGrid",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;  
            var a = D.Status; 
            if(t.length>0 && a == 200)
            {
                $("#tbl").empty();
                $("#tbl").append("<tr><th style='border: 1px solid;'>Month</th><th style='border: 1px solid;'>Gross Pay</th><th style='border: 1px solid;'>Ptax</th><th style='border: 1px solid;'>Itax</th>"+
                "<th style='border: 1px solid;'>GIS</th><th style='border: 1px solid;'>HRA</th><th style='border: 1px solid;'>GPF</th><th style='border: 1px solid;'>GPF Recover</th><th style='border: 1px solid;'>HBL(Principal)</th>"+ 
                "<th style='border: 1px solid;'>HBL(Interest)</th><th style='border: 1px solid;'>Other Deduction</th><th style='border: 1px solid;'>Net Pay</th><th style='border: 1px solid;'>Arrear/OT/Bonus</th>"+
                "<th style='border: 1px solid;'>Status</th><th style='border: 1px solid;'>Print</th></tr>");
                $("#tbl").show();

                var Gross = 0;var Ptax = 0;var Itax = 0;var GPF = 0; var Netpay = 0; var IHBL = 0; var GPFRecov = 0; var HBL = 0; var ArrearBonus = 0; var HRA = 0; var OtherDedu = 0;var GIS = 0;

                for (var i = 0; i < t.length; i++) {
                    var status = t[i].sStatus;
                    var sal = t[i].SalMonth;
                    var empID = t[i].EmployeeID;
                    if (status == "Projection") {
                        var re = "Unrealised";
                        $("#tbl").append("<tr style='background-color:#D3F9FA; height:30px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td></td></tr>");
                    }
                    else if (status == "H") {
                        var re = "Realised";
                        $("#tbl").append("<tr style='background-color:#F9F8D3; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td style='text-align:center; cursor:pointer;'><img src='/Content/Images/print.png' height=22px width=25px onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                    }
                    else if (status == "C") {
                        var re = "Completed and Bank Generated";
                        $("#tbl").append("<tr style='background-color:lightgreen; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td style='text-align:center; cursor:pointer;'><img src='/Content/Images/print.png' height=22px width=25px onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                    }
                    else if (status = "B") {
                        var re = 'Bank Not Yet Generated';
                        $("#tbl").append("<tr style='background-color:lightgreen; height:20px;'><td>" + t[i].SalMonth + "</td><td>" + t[i].GrossPay + "</td><td>" + t[i].Ptax + "</td><td>" + t[i].Itax + "</td><td>" + t[i].GIS + "</td><td>" + t[i].HRA + "</td><td>" + t[i].GPF + "</td><td>" + t[i].GPFRecov + "</td><td>" + t[i].HBL + "</td><td>" + t[i].IHBL + "</td><td>" + t[i].OtherDedu + "</td><td>" + t[i].NetPay + "</td><td>" + t[i].ArrearOT + "</td><td>" + re + "</td><td style='text-align:center; cursor:pointer;'><img src='/Content/Images/print.png' height=22px width=25px onclick=GenReport('" + empID + "','" + sal + "','" + status + "') /></td></tr>");
                    }

                    if (t[i].GrossPay != "") {
                        Gross = parseInt(Gross) + parseInt(t[i].GrossPay);
                    }
                    if (t[i].Ptax != "") {
                        Ptax = parseInt(Ptax) + parseInt(t[i].Ptax);
                    }

                    if (t[i].Itax != "") {
                        Itax = parseInt(Itax) + parseInt(t[i].Itax);
                    }
                    if (t[i].GPF != "") {
                        GPF = parseInt(GPF) + parseInt(t[i].GPF);
                    }
                    if (t[i].NetPay != "") {
                        Netpay = parseInt(Netpay) + parseInt(t[i].NetPay);
                    }
                    if (t[i].IHBL != "") {
                        IHBL = parseInt(IHBL) + parseInt(t[i].IHBL);
                    }
                    if (t[i].GPFRecov != "") {
                        GPFRecov = parseInt(GPFRecov) + parseInt(t[i].GPFRecov);
                    }
                    if (t[i].HBL != "") {
                        HBL = parseInt(HBL) + parseInt(t[i].HBL);
                    }
                    if (t[i].ArrearOT != "") {
                        ArrearBonus = parseInt(ArrearBonus) + parseInt(t[i].ArrearOT);
                    }
                    if (t[i].HRA != "") {
                        HRA = parseInt(HRA) + parseInt(t[i].HRA);
                    }
                    if (t[i].OtherDedu != "") {
                        OtherDedu = parseInt(OtherDedu) + parseInt(t[i].OtherDedu);
                    }
                    if (t[i].GIS != "") {
                        GIS = parseInt(GIS) + parseInt(t[i].GIS);
                    }
                    if (i == t.length - 1) {
                        $("#tbl").append("<tr style='background-color:lightpink; height:30px;Font-Bold=true'><td style='font-weight:bold;' height='30px'>Total</td><td>" + Gross + "</td><td>" + Ptax + "</td><td>" + Itax + "</td><td>" + GIS + "</td><td>" + HRA + "</td><td>" + GPF + "</td><td>" + GPFRecov + "</td><td>" + HBL + "</td><td>" + IHBL + "</td><td>" + OtherDedu + "</td><td>" + Netpay + "</td><td>" + ArrearBonus + "</td><td></td><td></td></tr>");
                    }
                }
            }
            else
            {
                alert(D.Message); return false;
            }
        }
    });
}

function GenReport(EmpID, SalMonth, status) {

    var FormName = '';
    var ReportName = '';
    var ReportType = '';
    var StrFinYr = '';
    var StrSecID;
    var StrSalMonth;
    var StrSalMonthID;
    var StrCenID;
    var StrReportTyp = '';
    ReportName = "History_PaySlip_new_LocationWISE_A5";
    var EmployeeID = EmpID;
    var SalMonth = SalMonth;
    var Status = status;
    var SecID = $('#ddlSector').val()
    FormName = "EmployeeWisePayReport.aspx";

    ReportType = "PaySlipReport";
  
    var E = '';
    E = "{EmployeeID:'" + EmployeeID + "',SalMonth:'" + SalMonth + "',Status:'" + Status + "',SecID:'" + SecID + "', FormName:'" + FormName + "', ReportName:'" + ReportName + "',ReportType:'" + ReportType + "'}";
   
    $.ajax({

        type: "POST",
        url: "/Administration/Master/ProjectedSalary/Report_Paravalue_Payslip",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (msg) {
            var left = ($(window).width() / 2) - (900 / 2),
           top = ($(window).height() / 2) - (600 / 2),
           popup = window.open("/ReportView.aspx?ReportTypeID=" + "8" + "&ReportID=" + "8" + "&SalMonth=" + $("#txtSalMonth").val(), "popup", "width=900, height=600, top=" + top + ", left=" + left);
           //reporttypeid and reportid see from table mst_reportconfig and dat_reportconfig
           popup.focus();
        }
    });

}


function Projected() {
    var SecID = $('#ddlSector').val().trim();
    var EmpNo = $('#txtSearchEmpNo').val().trim();
    var strReportType = 1;
    if (isPDFExcel == "1") {
        var result = confirm("Do you want to view the report ?");
        if (result == true) {
            var E = "{EmpNo:'" + EmpNo + "',SecID:" + SecID + ", FinYear:'" + $('#txtSalFinYear').val() + "', strReportType:'" + strReportType + "'}";
            $.ajax({
                type: "POST",
                url: "/Administration/Master/ProjectedSalary/Original_Sal_Statement",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    var left = ($(window).width() / 2) - (900 / 2),
                     top = ($(window).height() / 2) - (600 / 2),
                     popup = window.open("/ReportView.aspx?ReportTypeID=" + "10" + "&ReportID=" + "10" + "&SalMonth=" + $("#txtSalMonth").val(), "popup", "width=900, height=600, top=" + top + ", left=" + left);
                    popup.focus();
                }

            });
        }
    }
    else {
        var ReportName = "Salary_Projection_Report";

        window.open("/AllExcel.aspx?ReportName=" + ReportName + "&SecID=" + SecID + "&EmpNo=" + EmpNo + "&SalFinYear=" + $("#txtSalFinYear").val());
    }

}

function Orginal() {
    var SecID = $('#ddlSector').val().trim();
    var EmpNo = $('#txtSearchEmpNo').val().trim();
    var strReportType = 2;
    if (isPDFExcel == "1") {
        var result = confirm("Do you want to view the report ?"); 
        if (result == true) {
            var E = "{EmpNo:'" + EmpNo + "', SecID:" + SecID + ", FinYear:'" + $('#txtSalFinYear').val() + "', strReportType:'" + strReportType + "'}";
            $.ajax({
                type: "POST",
                url: "/Administration/Master/ProjectedSalary/Original_Sal_Statement",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    var left = ($(window).width() / 2) - (900 / 2),
                    top = ($(window).height() / 2) - (600 / 2),
                    popup = window.open("/ReportView.aspx?ReportTypeID=" + "9" + "&ReportID=" + "9" + "&SalMonth=" + $("#txtSalMonth").val(), "popup", "width=900, height=600, top=" + top + ", left=" + left);
                    popup.focus();
                }

            });
        } 
    }
    else {
        var ReportName = "EMP_Orginal_Sal_Statement";

        window.open("/AllExcel.aspx?ReportName=" + ReportName + "&SecID=" + SecID + "&EmpNo=" + EmpNo + "&SalFinYear=" + $("#txtSalFinYear").val());

    }

}

function FieldNameData() {
    $.ajax({
        type: "POST",
        url: "/Administration/Master/ProjectedSalary/Get_FieldName",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                $.each(D.Data, function (index, value) {
                    var MandatoryStyle = '*';
                    var Id = '#' + value.FieldID;
                    $(Id).text(value.FieldName);
                    $('#spn' + value.FieldID).text(value.IsMand == 1 ? '   ' + MandatoryStyle : '');
                });
            }
        }
    });
}