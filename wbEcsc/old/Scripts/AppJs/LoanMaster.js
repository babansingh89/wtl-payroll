﻿$(document).ready(function () {
    //$('#loader-3').show();
    
    FieldNameData();
    
    Populate_LoanType();
    $('[data-toggle="tooltip"]').tooltip();
    $('#txtAdjDate, #txtRefDate').datepicker({
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
    $('#txtLoanDeducStrt').datepicker({
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function (dates) {
            var date = $("#txtLoanDeducStrt").val().split('/');
            var LoanDeducDT = date[2] + '-' + date[1] + '-' + date[0];
            var options = {};
            options.url = "/Administration/Master/LoanMaster/Get_LoanDedDate";
            options.type = "POST";
            options.data = "{sectID:'" + $("#ddlSector").val() + "', LoanDeducDT:'" + LoanDeducDT + "'}";  //, SalFinYear:'" + '2017-2018' + "'
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (list) {
                var a = list.Data.length;
                if (a > 0) {
                    $.each(list.Data, function (index, value) {
                        var val = value.LoanDeducDate;
                        $("#txtLoanDeducStrt").val(val);
                    });
                }
            };
            options.error = function () { alert("Error in retrieving Data !"); };
            $.ajax(options);
        }
    });

    $('#txtLoanSancDate').datepicker({
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function (dates) {
            var options = {};
            options.url = "/Administration/Master/LoanMaster/Get_LoanSancDate";
            options.type = "POST";
            options.data = "{EmpID:'" + $("#hdnEmpNo").val() + "', LoanDeducStrt:'" + $.trim($("#txtLoanDeducStrt").val()) + "', LoanSancDate:'" + $.trim($("#txtLoanSancDate").val()) + "'}";
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (list) {
                var a = list.Data.length;
                if (a > 0) {
                    $.each(list.Data, function (index, value) {
                        if (value.Messege != null) {
                            alert(value.Messege);
                            $("#txtLoanSancDate").val('');
                            $("#txtLoanSancDate").focus();
                        }
                    });
                }
            };
            options.error = function () { alert("Error in retrieving Data !"); };
            $.ajax(options);
        }
    });

    $("#ddlLoanType").change(function () {
        Populate_LoanDesc();
    });

    $("#txtSearchEmpName").autocomplete({
        source: function (request, response) {
            var SectorID = $("#ddlSector").val();
            var EmpID = 0;   /// 0 for first time when employee search

            var E = "{sectID:'" + SectorID + "', EmpName: '" + $.trim($("#txtSearchEmpName").val()) + "', EmpID:'" + EmpID + "'}";
            $.ajax({
                type: "POST",
                url: "/Administration/Master/LoanMaster/Search_EmpNameAutoComplete",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.EmpName,
                                EmployeeID: item.EmployeeID
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $("#hdnEmpNo").val(i.item.EmployeeID); //alert($("#hdnEmpNo").val());
            var SectorID = $("#ddlSector").val();
            var E = "{sectID:'" + SectorID + "', EmpName: '" + $.trim($("#txtSearchEmpName").val()) + "', EmpID:'" + $("#hdnEmpNo").val().trim() + "'}";
            $.ajax({
                type: "POST",
                url: "/Administration/Master/LoanMaster/Search_EmpNameAutoComplete",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    if (t.length > 0) {
                        $.each(D.Data, function (index, value) {
                            if (value.Messege == null) {
                                var SecID = value.SectorID;
                                var LeftMonth = value.LeftMonth;
                                var LeftMonthwithText = LeftMonth + ' ' + 'Left Installment';
                                $('#ddlSector').val(SecID);
                                $('#lblLeftLoanInstall').text(LeftMonthwithText);
                            }
                            else {
                                $.each(D.Data, function (index, value) {
                                    alert(value.Messege);
                                    $("#txtSearchEmpName").val('');
                                    $("#hdnEmpNo").val('');
                                });
                            }
                        });
                    }
                }
            });
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });

    $('#txtSearchEmpName').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtSearchEmpName').val('');
            $('#hdnEmpNo').val('');
            $('#lblLeftLoanInstall').text('');
        }
        if (iKeyCode == 46) {
            $('#txtSearchEmpName').val('');
            $('#hdnEmpNo').val('');
            $('#lblLeftLoanInstall').text('');
        }
    });

    $('#txtLoanAmt').keyup(function (evt) {
        $('#txtDisbAmt').val($('#txtLoanAmt').val());
    })

    $("#ddlLoanDesc").change(function () {
        if ($("#ddlLoanDesc").val() == "") { $("#hdnIsRound").val(''); }
        var ss = $("#ddlLoanDesc").val().split('#');
        $("#hdnIsRound").val(ss[1]);
    })

    $('#txtLoanAmt').keypress(function (event, element) {
        if ($("#hdnIsRound").val() == 1) {
            if (event.which <= 47 || event.which >= 59) {
                event.preventDefault();
                
            }
            //return !((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode != 8 && event.keyCode != 46);
        }
        if ($("#hdnIsRound").val() == 0) {
            if (event.which != 46 && (event.which <= 47 || event.which >= 59)) {
                event.preventDefault();
                if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
                    event.preventDefault();
                }
            }
        }
        $('#txtDisbAmt').val($('#txtLoanAmt').val());
    })

    $('#txtTotLoanInstl').keydown(function (event, element) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode == 13 || charCode == 9) {
            CalculateTotalLoanInstall();
        }
    })

    $('#txtDisbAmt').keydown(function (event, element) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode == 13 || charCode == 9) {
            CheckDisburseAmt();
        }
    })

    $("#txtSearchEmpNo").autocomplete({
        source: function (request, response) {
            var SectorID = $("#ddlSector").val();
            var EmpID = 0;   /// 0 for first time when employee search

            var E = "{secID:'" + SectorID + "', Empname: '" + $.trim($("#txtSearchEmpNo").val()) + "', EmpId:'" + EmpID + "'}";
            $.ajax({
                type: "POST",
                url: "/Administration/Master/LoanMaster/Search_Employee_Autocomplete",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.EmpName,
                                EmployeeID: item.EmployeeID
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $("#hdnEmpNo").val(i.item.EmployeeID);
            var SectorID = $("#ddlSector").val();
            var E = "{secID:'" + SectorID + "', Empname: '" + $.trim($("#txtSearchEmpNo").val()) + "', EmpId:'" + $.trim($('#hdnEmpNo').val()) + "'}";
            $.ajax({
                type: "POST",
                url: "/Administration/Master/LoanMaster/Search_Employee_Autocomplete",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    if (t.length > 0) {
                        $("#tbl tr.trclass").empty();
                        $.each(D.Data, function (index, value) {
                            $("#tbl").append("<tr class='trclass'><td>" +   // style='display:none;'
                                value.LoanDesc + "</td><td>" +
                                value.EmpName + "</td><td>" +
                                value.LoanAmount + "</td><td>" +
                                value.LoanDeducDate + "</td><td>" +
                                value.TotLoanInstall + "</td><td>" +
                                value.CurLoanInstall + "</td><td>" +
                                value.LoanAmountPaid + "</td><td>" +
                                value.InstallAmt1 + "</td><td>" +
                                value.InstallAmt2 + "</td><td>" +
                                value.InstallAmt3 + "</td><td style='display:none;'>" +
                                value.LoanID + "</td></tr>");

                        });
                        OpenPopUpGrid();
                    }
                }
            });
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });

    $('#txtSearchEmpNo').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtSearchEmpNo').val('');
            $('#hdnEmployeeNo').val('');
            $('#hdnLoanID').text('0');
        }
        if (iKeyCode == 46) {
            $('#txtSearchEmpNo').val('');
            $('#hdnEmployeeNo').val('');
            $('#hdnLoanID').text('0');
        }
    });

    $('#btnSave').click(Save); 
    $("#btnRefresh").on("click", function () {
        ClearAllFields(); FieldEnable();
    });

    $("#tbl").delegate("tr.trclass", "click", function () {
        var LoanID = $(this).closest('tr').find('td:eq(10)').text().trim();
        EditGridRowLoan(LoanID);
    });

    $("#chkCloseInd").on("click", function () {
        if ($(this).is(":checked")) {
            RemarksOpen();
        }
        else {
            
        }
    });

    $('#btnRemarksSave').click(Update);
});

function Populate_LoanType()
{
        var options = {};
        options.url = "/Administration/Master/LoanMaster/Get_LoanDescType";
        options.type = "POST";
        options.data = "";
        options.dataType = "json";
        options.contentType = "application/json";
        options.success = function (list) {
            var a = list.Data.length;
            if (a > 0) {
                $.each(list.Data, function (index, value) {
                    var label = value.LoanDesc;
                    var val = value.LoanTypeID;
                    $("#ddlLoanType").append($("<option></option>").val(val).html(label));
                });
            }
            else {
                $("#ddlLoanType").empty();
                $("#ddlLoanType").append("<option value=''>--- (Select) ---</option>")
            }
        };
        options.error = function () { alert("Error in retrieving Loan Type !"); };
        $.ajax(options);
}

function Populate_LoanDesc()
{
    var s = $('#ddlLoanType').val();

    if (s != '') {
        var E = "{ddlLoanType: '" + $('#ddlLoanType').val() + "'}";
        var options = {};
        options.url = "/Administration/Master/LoanMaster/Get_LoanDesc";
        options.type = "POST";
        options.async = false;
        options.data = E;
        options.dataType = "json";
        options.contentType = "application/json";
        options.success = function (list) {
            var a = list.Data.length;
            if (a > 0) {
 $("#ddlLoanDesc").empty();
                $("#ddlLoanDesc").append("<option value=''>--- (Select) ---</option>")
                $.each(list.Data, function (index, value) {
                    var label = value.LoanDesc;
                    var val = value.LoanTypeID + '#' + value.IsRound;
                    $("#ddlLoanDesc").append($("<option></option>").val(val).html(label));
                });
            }
            else {
                $("#ddlLoanDesc").empty();
                $("#ddlLoanDesc").append("<option value=''>--- (Select) ---</option>")
            }
        };
        options.error = function () { alert("Error in retrieving Loan Description !"); };
        $.ajax(options);
    }
    else {
        $("#ddlLoanDesc").empty();
        $("#ddlLoanDesc").append("<option value=''>--- (Select) ---</option>")
    }
}

function CalculateTotalLoanInstall()
{
    var LoanTypeID = $("#ddlLoanDesc").val().split('#')[0];
    var LeftInst = $.trim($('#lblLeftLoanInstall').text()).split(' Left Installment')[0];
    var S = "{EmpId:'" + $.trim($("#hdnEmpNo").val()) + "', LoanTypeID:'" + $.trim(LoanTypeID) + "', LeftInst:'" + $.trim(LeftInst) +
            "', TotalLoanInst: '" + $.trim($('#txtTotLoanInstl').val()) + "', LoanAmt:'" + $.trim($("#txtLoanAmt").val()) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/LoanMaster/Get_LoanInstallment",
        data: S,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                $.each(D.Data, function (index, value) {
                    if (value.Messege == null) {
                        var Install1 = value.Install1;
                        var InstallAmt1 = value.InstallAmt1;
                        var Install2 = value.Install2;
                        var InstallAmt2 = value.InstallAmt2;
                        $('#txtInstNo1').val(Install1); $('#txtInstAmt1').val(InstallAmt1);
                        $('#txtInstNo2').val(Install2); $('#txtInstAmt2').val(InstallAmt2);
                    }
                    else {
                        $.each(D.Data, function (index, value) {
                            alert(value.Messege);
                            $('#txtTotLoanInstl').val('');
                            $('#txtInstNo1').val('0'); $('#txtInstAmt1').val('0');
                            $('#txtInstNo2').val('0'); $('#txtInstAmt2').val('0');
                            $('#txtInstNo3').val('0'); $('#txtInstAmt3').val('0');
                        });
                    }
                });
            }
        }
    });
}

function Save()
{
    if ($('#txtInstNo1').val() != '' && $('#txtInstAmt1').val() != '') {
        var CloseInd = "";
        if (document.getElementById('chkCloseInd').checked == true) { CloseInd = "Y"; }
        if (document.getElementById('chkCloseInd').checked == false) { CloseInd = "N"; }
        var S = "{hdnLoanID:'" + $.trim($("#hdnLoanID").val()) + "', LoanTypeID:'" + $.trim($("#ddlLoanDesc").val().split('#')[0]) +
                "', EmployeeID:'" + $.trim($('#hdnEmpNo').val()) + "', LoanAmount:'" + $.trim($('#txtLoanAmt').val()) +
                "', LoanDeducStartDate:'" + $.trim($("#txtLoanDeducStrt").val()) +
                "', TotLoanInstall: '" + $.trim($('#txtTotLoanInstl').val()) + "', CurLoanInstall:'" + $.trim($("#txtCurLoanInst").val()) +
                "', LoanAmountPaid: '" + $.trim($('#txtLoanAmtPaid').val()) + "', CloseInd:'" + CloseInd +
                "', AdjustAmount: '" + $.trim($('#txtAdjAmt').val()) + "', AdjustDate:'" + $.trim($("#txtAdjDate").val()) +
                "', LoanSancDate: '" + $.trim($('#txtLoanSancDate').val()) + "', ReferenceNo:'" + $.trim($("#txtRefNo").val()) +
                "', ReferenceDate: '" + $.trim($('#txtRefDate').val()) + "', DisburseAmt:'" + $.trim($('#txtDisbAmt').val()) +
                "', SancOrderNo:'" + $.trim($('#txtSancOrderNo').val()) +
                "', inst_no1:'" + $.trim($("#txtInstNo1").val()) + "', inst_amt1: '" + $.trim($('#txtInstAmt1').val()) +
                "', inst_no2: '" + $.trim($('#txtInstNo2').val()) + "', inst_amt2:'" + $.trim($("#txtInstAmt2").val()) +
                "', inst_no3: '" + $.trim($('#txtInstNo3').val()) + "', inst_amt3:'" + $.trim($("#txtInstAmt3").val()) +
                "', SecID:'" + $("#ddlSector").val() + "', retmonth:'" + $('#lblLeftLoanInstall').val().split(' Left Installment')[0] + "'}";  //, SalFinYear:'" + '2017-2018' + "'

        $.ajax({
            type: "POST",
            url: "/Administration/Master/LoanMaster/Save_Loan",
            data: S,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0) {
                    $.each(D.Data, function (index, value) {
                        if (value.Messege == null) {

                        }
                        else {
                            if (value.ErrorCode == 1) {
                                alert(value.Messege);
                                ClearAllFields();
                            }
                            else {
                                alert(value.Messege);
                            }
                        }
                    });
                }
            }
        });
    }

}

function CheckDisburseAmt()
{
    var S = "{EmpId:'" + $.trim($("#hdnEmpNo").val()) + "', Loantpid:'" + $.trim($('#ddlLoanDesc').val().split('#')[0]) +
                "', LoanAmt:'" + $.trim($('#txtLoanAmt').val()) + "', DisbAmt:'" + $.trim($("#txtDisbAmt").val()) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/LoanMaster/Get_DisburseAmt",
        data: S,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                $.each(D.Data, function (index, value) {
                    if (value.Messege != null) {
                        alert(value.Messege);
                        $('#txtDisbAmt').val($.trim($('#txtLoanAmt').val()));
                        $('#txtDisbAmt').focus();
                    }
                    else {

                    }
                });
            }
        }
    });
}

function ClearAllFields()
{
    $("#txtSearchEmpNo").val(''); $("#hdnEmployeeNo").val(''); $("#ddlLoanType").val(''); $("#ddlLoanDesc").val(''); $("#txtSearchEmpName").val('');
    $("#hdnEmpNo").val(''); $("#txtLoanAmt").val('0'); $("#txtLoanDeducStrt").val(''); $("#txtTotLoanInstl").val(''); $("#lblLeftLoanInstall").text('');
    $("#txtCurLoanInst").val('0'); $("#txtLoanAmtPaid").val('0'); $("#txtAdjDate").val(''); $("#txtAdjAmt").val(''); $("#txtLoanSancDate").val('');
    $("#txtRefNo").val(''); $("#txtRefDate").val(''); $("#txtDisbAmt").val('0'); $("#txtSancOrderNo").val(''); $("#txtInstNo1").val('0');
    $("#txtInstAmt1").val('0'); $("#txtInstNo2").val('0'); $("#txtInstAmt2").val('0'); $("#txtInstNo3").val('0'); $("#txtInstAmt3").val('0');
    $('#hdnLoanID').val('0');
    $("#ddlLoanDesc").empty();
    $("#ddlLoanDesc").append("<option value=''>--- (Select) ---</option>");
    document.getElementById('chkCloseInd').checked = false;
    $("#btnSave").html('Save');
    FieldEnable();
}

function FieldNameData()
{
    $.ajax({
        type: "POST",
        url: "/Administration/Master/LoanMaster/Get_FieldName",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                $.each(D.Data, function (index, value) {
                    var MandatoryStyle='*';
                    var Id = '#' + value.FieldID;
                    $(Id).text(value.FieldName);
                    $('#spn' + value.FieldID).text(value.IsMand == 1 ? '   '+MandatoryStyle:'');
                });
            }
        }
    });
}

function OpenPopUpGrid()
{
    //jQuery.noConflict();
    $('#Div1').modal('show');
}

function EditGridRowLoan(LoanID)
{
    var E = "{EmpId:'" + $.trim($('#hdnEmpNo').val()) + "', LoanID:'" + LoanID + "'}"; //alert(E);
    $.ajax({
        type: "POST",
        url: "/Administration/Master/LoanMaster/Loan_Edit",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                //ClearAllFields();
                $.each(D.Data, function (index, value) {
                    $("#ddlLoanType").val(value.LoanTypeID);
                });
                Populate_LoanDesc();
                $.each(D.Data, function (index, value) {
                    //alert(value.LoanDescID);
//alert(value.LoanDescID);
                    $("#ddlLoanDesc").val(value.LoanDescID + '#' + value.IsRound); $("#txtSearchEmpName").val(value.EmpName); $("#hdnEmpNo").val(value.EmployeeID);
                    $("#txtLoanAmt").val(value.LoanAmount); $("#txtLoanDeducStrt").val(value.LoanDeducDate); $("#txtTotLoanInstl").val(value.TotLoanInstall); $("#lblLeftLoanInstall").text(value.LeftMonth + ' ' + 'Left Installment');
                    $("#txtCurLoanInst").val(value.CurLoanInstall); $("#txtLoanAmtPaid").val(value.LoanAmountPaid); $("#txtAdjDate").val(value.AdjustDate); $("#txtAdjAmt").val(value.AdjustAmount); $("#txtLoanSancDate").val(value.LoanSancDate);
                    $("#txtRefNo").val(value.ReferenceNo); $("#txtRefDate").val(value.Referencedate); $("#txtDisbAmt").val(value.DisburseAmt); $("#txtSancOrderNo").val(value.SancOrderNo);
                    $("#txtInstNo1").val(value.Install1); $("#txtInstAmt1").val(value.InstallAmt1); $("#txtInstNo2").val(value.Install2); $("#txtInstAmt2").val(value.InstallAmt2);
                    $("#txtInstNo3").val(value.Install3); $("#txtInstAmt3").val(value.InstallAmt3); $('#hdnLoanID').val(value.LoanID);
                    
                });
                
                FieldDisable();
                $('#Div1').modal('hide');
            }
        }
    });
}

function FieldDisable()
{
    $("#ddlLoanType").attr('disabled', true); $("#ddlLoanDesc").attr('disabled', true); $("#txtSearchEmpName").attr('disabled', true);
    $("#txtLoanAmt").attr('disabled', true); $("#txtLoanDeducStrt").attr('disabled', true); //$("#txtTotLoanInstl").attr('disabled', true); 
    $("#txtCurLoanInst").attr('disabled', true); $("#txtLoanAmtPaid").attr('disabled', true); $("#txtAdjDate").attr('disabled', true); $("#txtAdjAmt").attr('disabled', true); $("#txtLoanSancDate").attr('disabled', true);
    $("#txtRefNo").attr('disabled', true); $("#txtRefDate").attr('disabled', true); $("#txtDisbAmt").attr('disabled', true); $("#txtSancOrderNo").attr('disabled', true); 
    //$("#txtInstNo1").attr('disabled', true);$("#txtInstAmt1").attr('disabled', true); $("#txtInstNo2").attr('disabled', true); $("#txtInstAmt2").attr('disabled', true); $("#txtInstNo3").attr('disabled', true); $("#txtInstAmt3").attr('disabled', true);
    $("#chkCloseInd").attr('disabled', false); $("#btnSave").html('Update');
}

function FieldEnable()
{
    $("#ddlLoanType").attr('disabled', false); $("#ddlLoanDesc").attr('disabled', false); $("#txtSearchEmpName").attr('disabled', false);
    $("#txtLoanAmt").attr('disabled', false); $("#txtLoanDeducStrt").attr('disabled', false); $("#txtTotLoanInstl").attr('disabled', false);
    $("#txtCurLoanInst").attr('disabled', false); $("#txtLoanAmtPaid").attr('disabled', false); $("#txtAdjDate").attr('disabled', false); $("#txtAdjAmt").attr('disabled', false); $("#txtLoanSancDate").attr('disabled', false);
    $("#txtRefNo").attr('disabled', false); $("#txtRefDate").attr('disabled', false); $("#txtDisbAmt").attr('disabled', false); $("#txtSancOrderNo").attr('disabled', false); $("#txtInstNo1").attr('disabled', false);
    $("#txtInstAmt1").attr('disabled', false); $("#txtInstNo2").attr('disabled', false); $("#txtInstAmt2").attr('disabled', false); $("#txtInstNo3").attr('disabled', false); $("#txtInstAmt3").attr('disabled', false);
    $("#chkCloseInd").attr('disabled', true); $("#btnSave").attr('disabled', false);
}

function RemarksOpen()
{
    jQuery.noConflict();
    $('#Div2').modal('show');
}

function Update()
{
    var CloseInd = "N";
    if (document.getElementById('chkCloseInd').checked == true) { CloseInd = "Y"; }
    if (document.getElementById('chkCloseInd').checked == false) { CloseInd = "N"; }
    var E = "{EmpId:'" + $.trim($('#hdnEmpNo').val()) + "', LoanID:'" + $.trim($('#hdnLoanID').val()) + "', Remarks:'" + $.trim($('#txtRemarks').val()) + "', CloseInd:'" + CloseInd + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/LoanMaster/Update_Loan",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                //ClearAllFields();
                $.each(D.Data, function (index, value) {
                    if (value.ErrorCode == 1) {
                        alert(value.Messege);
                        ClearAllFields();
                        FieldEnable();
                    } else {
                        alert(D.Message);
                    }
                });
                $('#Div2').modal('hide');
            }
        }
    });
}