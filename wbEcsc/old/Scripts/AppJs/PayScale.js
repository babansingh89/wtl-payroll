﻿
$(function () {
    var EditButton = "";
    var Control = {
        ddlEmployeeType: $("#ddlEmployeeType"),
        txtPayScale: $("#txtPayScale"),
        hdnPayScaleID:$("#hdnPayScaleID"),
        txtGradePay: $("#txtGradePay"),
        txtIncrementPercentage: $("#txtIncrementPercentage"),
        btnSubmit: $("#btnSubmit"),
        btnReset: $("#btnReset"),
        clstbl: $(".clstbl")      
    };
    var objServer = {
        GetFieldName: function (callback) {
            $.ajax({
                type: 'post',
                contentType: 'application/json;churset=utf-8;',
                data: null,
                dataType: 'json',
                url: '/PayScale/Get_FieldName',
                success: function (responce) {                   
                    var js = responce.Data;                
                    callback(js);
                },
                error: function (responce) {
                    callback("error");
                }
            });
        },
        GetEmployeeType: function (data, callback) {
            $.ajax({
                type: 'post',
                contentType: 'application/json;charset=utf-8;',
                data: JSON.stringify(data),
                dataType: 'json',               
                url: '/PayScale/Get_EmployeeType',
                success: function (response) {                
                    var xml = $.parseXML(response.Data);
                    callback(xml);
                },
                error: function (response) {
                    callback("error");
                }
            });
        },
        GetPayByEmployeeType: function (data, callback) {
            $.ajax({
                type: 'post',
                contentType: 'application/json',
                data: JSON.stringify(data),
                dataType: 'json',
                url: '/PayScale/Get_varietyTypeMSTdata',
                success: function (responce) {
                    var xml = $.parseXML(responce.d);
                    callback(xml);
                },
                error: function () {
                    callback("error");
                }
            });
        },
        SaveData: function (data, callback) {
            $.ajax({
                type: 'post',
                contentType: 'application/json',
                data: JSON.stringify(data),
                dataType: 'json',
                url: '/PayScale/SaveData',
                success: function (responce) {                    
                    callback(responce);
                },
                error: function () {
                    callback("error");
                }
            });
        }
    };
    var objClient = {
        Initialization: function () {
            objClient.Event.onLoad();
            objClient.Event.onClick();
            objClient.Event.onBlur();
            objClient.Event.onChange();
        },
        Event: {
            onLoad: function () {
                objClient.Method.GetFieldName();
                objClient.Method.GetEmployeeType();
            },
            onClick: function () {
                Control.btnSubmit.click(function () {
                    objClient.Method.SaveData();
                });
                $(document).on( 'click','.clsEdit', function () {                  
                    var PayScaleID = $(this).closest(".clsrow").find(".clsPayScaleID").text();
                    var PayScale = $(this).closest(".clsrow").find(".clsPayScale").text();
                    var GradePay = $(this).closest(".clsrow").find(".clsGradePay").text();
                    var incre_parct = $(this).closest(".clsrow").find(".clsincre").text();
                    Control.hdnPayScaleID.val(PayScaleID);                   
                    Control.txtPayScale.val(PayScale);
                    Control.txtGradePay.val(GradePay);
                    Control.txtIncrementPercentage.val(incre_parct)
                    Control.btnSubmit.val("Update");
                });
            },
            onBlur: function () {

            },
            onChange: function () {
                Control.ddlEmployeeType.change(function () {
                    Control.clstbl.empty();
                    var emptp = $(this).val();
                    if(emptp=="")
                    {
                        return false;
                    }
                    objClient.Method.GetPayByEmployeeType(emptp);
                });
            }
        },
        Method: {
            GetFieldName: function () {
                objServer.GetFieldName(function (jsdata) {
                    $.each(jsdata, function (index,value) {                       
                        index = index + 1;
                        var star = value.IsMand==1 ? '*' : "";
                        $("#"+ index+"").text(value.FieldName);
                        $("#" + index + "0").text(star);
                        if(value.FieldID==7)
                        {
                            EditButton = value.FieldName;
                        }
                    });
                });
            },
            GetEmployeeType: function () {
                var data = { EMPTYPE: "" };
                objServer.GetEmployeeType(data, function (dataxml) {
                    var xmlTable = $(dataxml).find("Table");
                    objClient.Method.PopulateDropdown(xmlTable);
                });
            },
            GetPayByEmployeeType: function (EMPtype) {
                var data = { EMPTYPE: EMPtype };
                objServer.GetEmployeeType(data, function (dataxml) {
                    var xmlTable = $(dataxml).find("Table");
                    objClient.Method.PopulateTableByEMPtype(xmlTable);
                });
            },          
            PopulateTableByEMPtype: function (xmlTable) {
                Control.clstbl.empty();
                //    PayScaleID,EmpType,EmpTypeDesc,null PayScale,null GradePay,null incre_parct     
                $.each(xmlTable, function (index, value) {
                    index = index + 1;
                    Control.clstbl
                           .append("<div class='row border-top clsrow'>"
                                 + "<div class='col-md-1 col-sm-1'>" + index + "</div>"
                                 + "<div class='col-md-1 col-sm-1 clsPayScaleID'>" + $(value).find("PayScaleID").text() + "</div>"
                               //+ "<div class='col-md-3 col-sm-3 clsEmpTypeDesc'>" + $(value).find("EmpTypeDesc").text() + "</div>"
                                 + "<div class='col-md-3 col-sm-3 clsPayScale text-center'>" + $(value).find("PayScale").text() + "</div>"
                               + "<div class='col-md-2 col-sm-2 clsGradePay text-right'>" + $(value).find("GradePay").text() + "</div>"
                               + "<div class='col-md-2 col-sm-2 clsincre text-right'>" + $(value).find("incre_parct").text() + "</div>"
                               + "<div class='col-md-1 col-sm-1 clsEdit text-center text-info' style='cursor:pointer;' title='click to Edit payscale " + $(value).find("PayScale").text() + "'> " + EditButton + " </div>"
                           +"</div>");
                });
            },
            PopulateDropdown: function (xmlTable) {
                Control.ddlEmployeeType.empty();
                Control.ddlEmployeeType.append("<option value=''>Select Employee Type</option>");
                $.each(xmlTable, function (index, value) {
                    Control.ddlEmployeeType.append("<option value='" + $(value).find("EmpType").text() + "'>" + $(value).find("EmpTypeDesc").text() + "</option>");
                });
            },
            SaveData: function () {
                var objpaymd = {
                    PayScaleID: Control.hdnPayScaleID.val() == "" ? null : parseInt(Control.hdnPayScaleID.val()),
                    EmpTypeID: Control.ddlEmployeeType.val(),
                    EmpType: "",
                    PayScale: Control.txtPayScale.val().trim(),
                    GradePay: Control.txtGradePay.val().trim(),
                    IncrementPercentage: Control.txtIncrementPercentage.val().trim()
                };

                console.log(objpaymd);
                objServer.SaveData(objpaymd, function (responce) {
                    if(responce=="error")
                    {
                        alert("Internal server error");
                        return false;
                    }
                    alert(responce.Message);
                    if(responce.Data==1)
                    {
                        window.location.reload();
                    }                    
                });
            }
        }
    };

    objClient.Initialization();
});
