﻿using System.Web.Mvc;
using System.Web.Routing;

namespace wbEcsc.App_Start.Routes
{
    public class DefaultRoute : IRoute
    {
        private RouteCollection _routes { get; set; }
        public DefaultRoute(RouteCollection routes)
        {
            this._routes = routes;
        }
        public void MapRoute()
        {
            _routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "AdministrativeLoginUI", id = UrlParameter.Optional }   //  LoginMenuUI
                //,constraints: new { controller = "(Home|LoginManager)" }//(Home|Other)
                , namespaces: new[] { "wbEcsc.Controllers" }
            );
        }
    }
}