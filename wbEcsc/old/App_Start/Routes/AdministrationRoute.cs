﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace wbEcsc.App_Start.Routes
{
    public class AdministrationRoute:IRoute
    {
        private RouteCollection _routes { get; set; }
        public AdministrationRoute(RouteCollection routes)
        {
            this._routes = routes;
        }
        public void MapRoute()
        {
            _routes.MapRoute(
                name: "AdministrationRouter",
                url: "Administration/{controller}/{action}/{id}",
                defaults: new { controller = "AdminHome", action = "HomeUI", id = UrlParameter.Optional }
                ,namespaces:new[] { "wbEcsc.Controllers.Administration" }
            );
        }
    }
}