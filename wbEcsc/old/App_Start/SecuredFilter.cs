﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models;
using wbEcsc.Models.AppResource;

namespace wbEcsc.App_Start
{
    public class SecuredFilter : FilterAttribute, IAuthenticationFilter, IAuthorizationFilter
    {
        //1
        string getOptionalFormattedParam(RouteValueDictionary routValues)
        {
            string formattedParamValue = "";
            foreach (var item in routValues)
            {
                if(string.Equals(item.Key, "Controller",StringComparison.OrdinalIgnoreCase) || string.Equals(item.Key, "action", StringComparison.OrdinalIgnoreCase))
                {
                    //skip
                }
                else
                {
                    formattedParamValue += string.Format("{0}={1}&", item.Key.Trim(), item.Value.ToString().Trim());
                }
            }
            if(formattedParamValue.Length>0)
            {
                formattedParamValue = formattedParamValue.Remove(formattedParamValue.Length - 1);
            }
            return formattedParamValue.Trim();
        }
        bool validateWithParams(string pattern,string value)
        {
         if(string.IsNullOrEmpty(pattern))
            {
                return true;
            }   
            return Regex.IsMatch(value, pattern);
        }
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            try
            {

                var controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
                var actionName = filterContext.ActionDescriptor.ActionName;
                string[] controllerPackages = filterContext.RouteData.DataTokens["Namespaces"] as string[];

                var routeValues = filterContext.RouteData.Values;

                string controllerPackage = null;//  filterContext.RouteData.DataTokens["Namespaces"][0] as string ;
                if (controllerPackages.Length > 0)
                {
                    controllerPackage = controllerPackages[0];
                }
                else
                {
                    throw new InvalidOperationException("404$Resource Not Found");
                }
                UrlBLL urlHandler = new UrlBLL();
                //get url model
                AppUrl filterModel = urlHandler.GetUrl(actionName, controllerName, controllerPackage);//, getOptionalFormattedParam(routeValues));
                

                if (filterModel == null)
                {
                    throw new InvalidOperationException("404$Resource Not Found");
                }
                if(!validateWithParams(filterModel.ParamExpression,getOptionalFormattedParam(routeValues)))
                {
                    throw new InvalidOperationException("404$Resource Not Found");
                }
                //if anounymous take no action.
                if (!filterModel.IsAnonymousUrl)
                {
                    //must be authenticated. else throw 401
                    if (SessionContext.IsAuthenticated)
                    {
                        SessionContext.InitSessionData();
                        if (filterModel.IsSharedUrl)
                        {
                            //ok
                        }
                        else if (urlHandler.hasUrlInCurRole(filterModel, SessionContext.CurrentUser))
                        {
                            //ok
                        }
                        else
                        {
                            throw new InvalidOperationException("403$Forbidden");
                        }
                    }
                    else
                    {
                        throw new InvalidOperationException("401$Authentication Required");
                    }
                }
            }
            catch (Exception ex)
            {
                Action<Exception> handeException = (exception) =>
                {
                    int statusCode = 500;
                    string message = "Unknown Application Error. Error source-'UrlFilter'";
                    if (exception.GetType() == typeof(InvalidOperationException))
                    {
                        statusCode = Convert.ToInt32(ex.Message.Split('$')[0]);
                        message = ex.Message.Split('$')[1];
                    }
                    if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                    {
                        ResponseStatus responseStatus = (ResponseStatus)statusCode;
                        filterContext.Result = new JsonResult()
                        {
                            Data = new ClientJsonResult()
                            {
                                Data = null,
                                Message = message,
                                Status = responseStatus
                            }
                        };
                    }
                    else
                    {
                        filterContext.Result = new RedirectToRouteResult("ErrorRouter",
                               new System.Web.Routing.RouteValueDictionary{
                                {"controller", string.Format("Error{0}",statusCode)},
                                {"action", "Index"},
                                {"errorCode", statusCode},
                                {"Message", message},
                               });
                    }
                };

                handeException(ex);
            }

        }

        //3
        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {

        }

        //2
        public void OnAuthorization(AuthorizationContext filterContext)
        {

        }

    }
}