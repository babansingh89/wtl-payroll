﻿using System.Web.Mvc;
using System.Web.Routing;
using wbEcsc.App_Start;
using wbEcsc.App_Start.Routes;

namespace wbEcsc
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            CustomRouter.AddRoute(new ReportRoute(routes));
            CustomRouter.AddRoute(new MasterRoute(routes));
            CustomRouter.AddRoute(new AdministrationRoute(routes));
            CustomRouter.AddRoute(new SharedRoute(routes));
            CustomRouter.AddRoute(new ErrorRoute(routes));
            CustomRouter.AddRoute(new DefaultRoute(routes));
        }
    }
}
