﻿<%@ WebHandler Language="C#" Class="Get_LeavePurpose" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;

public class Get_LeavePurpose : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) {
        string JSONVal = "";
        string RType = "";
        ApiResponceData GER = new ApiResponceData();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        try
        {
            if (context.Request.QueryString.Count > 0)
            {
                RType = context.Request.QueryString["RType"].ToString();
                //ApplicationID = Convert.ToInt64(context.Request.QueryString["AID"]);

            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                RType = context.Request.Params["RType"].ToString();
                //ApplicationID = Convert.ToInt64(context.Request.Params["AID"]);
            }

            if (RType!="" )
            {

                 DataTable dt = new DataTable();
                using (var command = new SqlCommand("payroll.Get_Reason", con)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    con.Open();
                    command.Parameters.AddWithValue("@RType", RType);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    sda.Fill(dt);
                    con.Close();
                }
                if (dt.Rows.Count > 0)
                {
                    List<Dictionary<string, object>> rowss = new List<Dictionary<string, object>>();
                    if (dt.Rows.Count > 0)
                    {

                        Dictionary<string, object> row1;

                        foreach (DataRow r in dt.Rows)
                        {
                            row1 = new Dictionary<string, object>();
                            foreach (DataColumn col in dt.Columns)
                            {
                                row1.Add(col.ColumnName, r[col]);
                            }
                            rowss.Add(row1);
                        }
                    }


                    GER.Data = rowss;
                    GER.Status = "Y";
                    GER.Message = "SUCCESS";
                    JSONVal = GER.ToJSON();
                }
                else
                {
                    GER.Status = "N";
                    GER.Message = "No records found ";
                    JSONVal = GER.ToJSON();
                }
            }
            else
            {
                GER.Status = "N";
                GER.Message = "Provide all the parameters ";
                JSONVal = GER.ToJSON();
            }
        }
        catch (Exception ex)
        {
            GER.Status = "N";
            GER.Message = ex.Message;
            JSONVal = GER.ToJSON();
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(JSONVal);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}