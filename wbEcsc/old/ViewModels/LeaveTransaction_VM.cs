﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.ViewModels
{
    public class LeaveTransaction_VM
    {

            public int FieldID { get; set; }
            public string FieldName { get; set; }
            public int IsMand { get; set; }

            public int EmployeeID { get; set; }
        public string EmpName { get; set; }

        public string  LeaveAbbv { get; set; }
        public int LvId { get; set; }
        public string LtNo { get; set; }
        public string LtDate { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public decimal? LBal { get; set; }
        public int IsLeave { get; set; }
        public int IsRound { get; set; }
        public decimal? NoOfDays { get; set; }
        public string Remarks { get; set; }
        public string DisEmpName { get; set; }

        public string RDesc { get; set; }
        public int? RCode { get; set; }
    }
}