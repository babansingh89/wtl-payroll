﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.ViewModels
{
    public class PayScaleViewModel
    {
        public List<Field>  ListField { get; set; }
        public PayScale_MD SelectedPayScale { get; set; }
        public List<PayScale_MD> ListPayScale { get; set; }
    }
}