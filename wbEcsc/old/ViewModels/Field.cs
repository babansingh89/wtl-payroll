﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.ViewModels
{
    public class Field
    {
        public int? FieldID { get; set; }
        public string FieldName  { get; set; }
        public  int IsMand { get; set; }
    }
}