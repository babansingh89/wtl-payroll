﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.ViewModels
{
    public class BankViewModel
    {
        public Bank_MD SelectedBank { get; set; }
        public List<Field> FieldList { get; set; }
        public List<Bank_MD> BankList { get; set; } 
    }

    //public class Field
    //{
    //    public int? FieldID { get; set; }
    //    public string FieldName { get; set; }
    //    public int IsMand { get; set; }
    //}
}