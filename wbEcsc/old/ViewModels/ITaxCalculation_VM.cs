﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace wbEcsc.ViewModels
{
    public class ITaxCalculation_vm
    {
        public string FinYear { get; set; }
        public string AssessmentYear { get; set; }
        public string EmpID { get; set; }
        public string hdnEmpGender { get; set; }
        public decimal hdnTotalGrossPay { get; set; }
        public decimal hdnTotalPtax { get; set; }
        public decimal hdnTotalITax { get; set; }
        public decimal hdnTotalGIS { get; set; }
        public decimal hdnTotalHRA { get; set; }
        public decimal hdnTotalGPF { get; set; }
        public decimal hdnTotalGPFRec { get; set; }
        public decimal hdnTotalIHBL { get; set; }
        public decimal hdnTotalHBL { get; set; }
        public decimal hdnTotalOtherDe { get; set; }
        public decimal hdnTotalNetPay { get; set; }
        public decimal hdnTotalArrearOT { get; set; }
        public decimal IncomeFromSalary { get; set; }
        public decimal InuterestFromSaving { get; set; }
        public decimal InterestFromFD { get; set; }
        public decimal InterestFromOther { get; set; }
        public decimal TotDeduChapVIA { get; set; }
        public decimal TotTaxableIncome { get; set; }
        public decimal TaxOnNetIncome { get; set; }
        public decimal RabateSec87A { get; set; }
        public decimal Surcharge { get; set; }
        public decimal EducationCess { get; set; }
        public decimal HSEducationCess { get; set; }
        public decimal TaxLiabilities { get; set; }
        public decimal ReliefUS89 { get; set; }
        public decimal TaxPayabl { get; set; }
        public decimal OtherITAX { get; set; }
        public decimal BalanceTax { get; set; }

        public List<TableChild_vm> TableChild { get; set; }
        public List<OtherIncome_vm> TableOtherIncome { get; set; }
        public List<TableParent_vm> TableParents { get; set; }
        public List<TableChallan_vm> TableChallan { get; set; }

        public ITaxCalculation_vm()
        {
            TableChild = new List<TableChild_vm>();
            TableOtherIncome = new List<OtherIncome_vm>();
            TableParents = new List<TableParent_vm>();
            TableChallan = new List<TableChallan_vm>();
        }
        #region other dependent classes
        public class OtherIncome_vm
        {
            public decimal Amount { get; set; }
            public string Description { get; set; }
        }
        public class TableParent_vm
        {
            public decimal ParentAmt { get; set; }
            public int ParentID { get; set; }
        }
        public class TableChild_vm
        {
            public decimal ChildAmt { get; set; }
            public int ChildID { get; set; }
        }
        public class TableChallan_vm
        {
            public string ChallanNo { get; set; }
            public string BSRNo { get; set; }
            public string ChallanDate { get; set; }
            public decimal Amount { get; set; }
        }
        #endregion

    }

}