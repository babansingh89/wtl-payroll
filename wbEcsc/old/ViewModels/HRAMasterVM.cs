﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.ViewModels
{
    public class HRAMasterVM
    {
        public int? HRAID { get; set; }
        public int HRACode { get; set; }
        public string EmpType { get; set; }
        public string EmpTypeDesc { get; set; }
        public string HRA { get; set; }
        public string MaxAmount { get; set; }
        public string EffectiveFrom { get; set; }
        public string EffectiveTo { get; set; }
        public string Edit { get; set; }
        public string Del { get; set; }
    }//ALL END
}