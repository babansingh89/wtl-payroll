﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.ViewModels
{
    public class SalaryGeneration_MD
    {
        public int MaxSalMonthID { get; set; }
        public string MaxSalMonth { get; set; }
        public string EncodeValue { get; set; }
    }
}