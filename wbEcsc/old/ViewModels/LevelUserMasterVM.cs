﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.ViewModels
{
    public class LevelUserMasterVM
    {
        public string FormName { get; set; }
        public int? ConfID { get; set; }
        public string UserName { get; set; }
        public long? UserID { get; set; }
    }
}