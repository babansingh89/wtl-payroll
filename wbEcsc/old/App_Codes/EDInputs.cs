﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes
{
    public class EDInputs : BllBase
    {
        public EDDetail_MD EDDetails(int EDID)
        {
            SqlCommand cmd = new SqlCommand("Get_EarnDeductionByEDID", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EDID", EDID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_EDDetail(dt)[0];
            }
            catch (SqlException e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                cn.Close();
            }
        }
        public List<EDInputs_MD> Get_EmpDetails(string SectorID, string LocationID, string EDID, string SalFinYear, string SalMonthID, string MenuID, long UserID)
        {
            SqlCommand cmd = new SqlCommand("Get_SalMonthandCooperativeDetails", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SecID", SectorID);
            cmd.Parameters.AddWithValue("@CenterID", LocationID.Equals("") ? 0 : (object)LocationID);
            cmd.Parameters.AddWithValue("@Edid", EDID);
            cmd.Parameters.AddWithValue("@MenuId", MenuID);
            cmd.Parameters.AddWithValue("@SalMonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@SalaryFinancialYear", SalFinYear);
            cmd.Parameters.AddWithValue("@insertedBy", UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException ex)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public string Update_Details_RowWise(string EmpNo, string EDID, string Amount, string SectorID, string CurFinYear, long UserID)
        {
            string result = "";
            SqlCommand cmd = new SqlCommand("Update_CoOperativeCal", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpNo", EmpNo);
            cmd.Parameters.AddWithValue("@EDID", EDID);
            cmd.Parameters.AddWithValue("@EDAmount", Amount);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@CurrFinYear", CurFinYear);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt.Rows[0]["Result"].ToString();
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        #region Converter
        List<EDInputs_MD> Convert(DataTable dt)
        {
            List<EDInputs_MD> lst = new List<EDInputs_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new EDInputs_MD();
                obj.EmpNo = t.Field<string>("EmpNo");
                obj.EmpName = t.Field<string>("EmpName");
                obj.EDAmount = t.Field<decimal>("EDAmount");
                obj.EDName = t.Field<string>("EDName");
                obj.ItaxForThisMonth = t.Field<decimal?>("ItaxForThisMonth");
                obj.BalanceItax = t.Field<decimal?>("BalanceItax");
                return obj;
            }).ToList();
            return lst;
        }

        List<EDDetail_MD> Convert_EDDetail(DataTable dt)
        {
            List<EDDetail_MD> lst = new List<EDDetail_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new EDDetail_MD();
                obj.EDID = t.Field<int>("EDID");
                obj.EDName = t.Field<string>("EDName");
                obj.MenuID = t.Field<int>("MenuID");
                return obj;
            }).ToList();
            return lst;
        }
        #endregion
    }
}