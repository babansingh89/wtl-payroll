﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using wbEcsc.Models.Application;
using wbEcsc.Models.Account.Logins;


namespace wbEcsc.App_Codes.BLL
{
    public class Logo:BllBase
    {
        public List<Logo_MD> GetLogo()
        {
            SqlCommand cmd = new SqlCommand("Get_Logo", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public string Get_EmpNoLenth()
        {
            SqlCommand cmd = new SqlCommand("Get_Logo", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt.Rows[0]["EmpNoLength"].ToString();
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }
        public Logo_MD GetAppsInfo()
        {
            SqlCommand cmd = new SqlCommand("Get_Logo", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert(dt)[0];
                }
                return null;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public SalMonth_FinYear_AccFinYear Get_SalMonth_FinYear_AccFinYear(int? SectorID)
        {
            SqlCommand cmd = new SqlCommand("Get_SalMonth_FinYear_AccFinYear", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SectorID", SectorID.Equals(null) ? DBNull.Value : (object)SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if(dt.Rows.Count>0)
                {
                    return Convert_SalMonth_FinYear_AccFinYear(dt)[0];
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }



        #region Converter
        List<Logo_MD> Convert(DataTable dt)
        {
            List<Logo_MD> lst = new List<Logo_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Logo_MD();
                obj.LogoID = t.Field<int?>("ID");
                obj.OfficeName = t.Field<string>("OfficeName");
                obj.Address = t.Field<string>("Address");
                obj.WebSiteHeader_1 = t.Field<string>("WebSiteHeader_1");
                obj.WebSiteHeader_2 = t.Field<string>("WebSiteHeader_2");
                obj.LogoPath = t.Field<string>("Logo");
                obj.EmpNoLength = t.Field<int?>("EmpNoLength");
                return obj;
            }).ToList();
            return lst;
        }

        List<SalMonth_FinYear_AccFinYear> Convert_SalMonth_FinYear_AccFinYear(DataTable dt)
        {
            List<SalMonth_FinYear_AccFinYear> lst = new List<SalMonth_FinYear_AccFinYear>();
            lst = dt.AsEnumerable().Select(t =>
              {
                  var obj = new SalMonth_FinYear_AccFinYear();
                  obj.SalMonthID = t.Field<int?>("SalMonthID"); 
                  obj.SalMonth = t.Field<string>("SalMonth");
                  obj.SalFinYear = t.Field<string>("SalFinYear");
                  obj.AccFinYear = t.Field<string>("AccFinYear");
                  return obj;

              }).ToList();
            return lst;
        }
        #endregion
    }
}