﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using System.IO;

namespace wbEcsc.App_Codes.BLL
{
    public class SalaryProjection:BllBase
    {
        public DataTable Get_EmployeeDetails(string EmployeeNo, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("Get_EmpDetails", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpNo", EmployeeNo);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Get_EmployeeSalProjection(string EmployeeNo, int SectorID, string FinYear, long UserID)
        {
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();

            DataTable dt = new DataTable();
            try
            {
                try
                {
                    cmd = new SqlCommand("Rpt_EmpWiseSalProjection", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@SalFinYear", FinYear);
                    cmd.Parameters.AddWithValue("@SectorID", SectorID);
                    cmd.Parameters.AddWithValue("@EmpNo", EmployeeNo);
                    cmd.Parameters.AddWithValue("@insertedBy", UserID);
                    cmd.ExecuteNonQuery();


                    cmd = new SqlCommand("Get_ITaxEmployeeDetailsNew", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@FinYear", FinYear);
                    cmd.Parameters.AddWithValue("@SecID", SectorID);
                    cmd.Parameters.AddWithValue("@EmpNo", EmployeeNo);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                   
                    da.Fill(dt);
                    transaction.Commit();
                    if (dt.Rows.Count > 0)
                        return dt;
                    else
                        return null;
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException e)
            {
                throw new Exception("Database Error !!");
            }
            finally
            {
                dbConn.Close();
            }
           
        }

        public DataTable Get_Empwise_Hist_PaySlip(string Status, string SalMonth, string EmployeeID, string SecID, long UserID)
        {
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();

            DataTable dt = new DataTable();
            try
            {
                try
                {
                    cmd = new SqlCommand("SalaryProcessPresentHistoryAutomatic_PaySlip", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Status", Status);
                    cmd.Parameters.AddWithValue("@SalMonth", SalMonth);
                    cmd.Parameters.AddWithValue("@EmpID", EmployeeID);
                    cmd.Parameters.AddWithValue("@SectorID", SecID);
                    cmd.Parameters.AddWithValue("@insertedBy", UserID);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);

                    da.Fill(dt);

                    transaction.Commit();

                    if (dt.Rows.Count > 0)
                        return dt;
                    else
                        return null;

                   
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException e)
            {
                throw new Exception("Database Error !!");
            }
            finally
            {
                dbConn.Close();
            }

        }

        public string Original_Sal_Statement(string EmpNo, int SecID, string FinYear, string strReportType, long UserID)
        {
            string Formula = "";
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();

            DataTable dt = new DataTable();
            try
            {
                try
                {
                    if (strReportType == "2")
                    {
                        cmd = new SqlCommand("Rpt_EmpWiseSalOrginal", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@SalFinYear", FinYear);
                        cmd.Parameters.AddWithValue("@SectorID", SecID);
                        cmd.Parameters.AddWithValue("@EmpNo", EmpNo);
                        cmd.Parameters.AddWithValue("@insertedBy", UserID);
                        SqlDataAdapter daa = new SqlDataAdapter(cmd);

                        Formula = "{MST_Employee.EmpNo}='" + EmpNo + "' and {MST_Sector.SectorID}=" + SecID + " and {temp_itax_emp_wise.FinYear}='" + FinYear + "' and {temp_itax_emp_wise.Status} in['C','H','B'] and {temp_itax_emp_wise.InsertedBy}=" + UserID + "";

                        cmd = new SqlCommand("Get_Check_ReportValue", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@UserID", UserID);
                        cmd.Parameters.AddWithValue("@ReportID", 9);       //See Table DAT_ReportConfig
                        cmd.Parameters.AddWithValue("@ReportTypeID", 9);   //See Table DAT_ReportConfig
                        cmd.Parameters.AddWithValue("@Formula", Formula);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);

                        da.Fill(dt);
                    }
                    if (strReportType == "1")
                    {
                        Formula = "{ MST_Employee.EmpNo}= '" + EmpNo + "' and { MST_Sector.SectorID}= " + SecID + " and { temp_itax_emp_wise.FinYear}= '" + FinYear + "' and { temp_itax_emp_wise.InsertedBy}= " + UserID + "";
                        cmd = new SqlCommand("Get_Check_ReportValue", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@UserID", UserID);
                        cmd.Parameters.AddWithValue("@ReportID", 10);       //See Table DAT_ReportConfig
                        cmd.Parameters.AddWithValue("@ReportTypeID", 10);   //See Table DAT_ReportConfig
                        cmd.Parameters.AddWithValue("@Formula", Formula);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);

                        da.Fill(dt);
                    }
                        
                    
                   

                    transaction.Commit();
                    return "success";
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException e)
            {
                throw new Exception("Database Error !!");
            }
            finally
            {
                dbConn.Close();
            }

        }

    }
}