﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL
{
    public class Category:BllBase
    {
        public List<Category_MD> Get_Category()
        {
            SqlCommand cmd = new SqlCommand("Load_Category", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        #region Converter
        List<Category_MD> Convert(DataTable dt)
        {
            List<Category_MD> lst = new List<Category_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Category_MD();
                obj.CategoryID = t.Field<int?>("CategoryID");
                obj.Category = t.Field<string>("Category");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(Category v)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}