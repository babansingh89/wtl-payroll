﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL
{
    public class Cooperative:BllBase
    {
        public List<Cooperative_MD> Get_Cooperative()
        {
            SqlCommand cmd = new SqlCommand("Load_Cooperative", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        #region Converter
        List<Cooperative_MD> Convert(DataTable dt)
        {
            List<Cooperative_MD> lst = new List<Cooperative_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Cooperative_MD();
                obj.CooperativeID = t.Field<int?>("CooperativeID");
                obj.Cooperative = t.Field<string>("Cooperative");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(Cooperative v)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}