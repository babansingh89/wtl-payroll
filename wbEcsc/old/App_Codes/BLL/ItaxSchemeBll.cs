﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using wbEcsc.Models.Application;
using System.Configuration;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL
{
    public class ItaxSchemeBll:BllBase
    {
      //  readonly string constr;
     //   readonly SqlConnection con;
        SqlCommand cmd;
        SessionData sds;
        public ItaxSchemeBll()
        {
            sds = SessionContext.SessionData;
         //   constr = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
         //   con = new SqlConnection(constr);
        }
        public DataTable Save(ItaxScheme_MD ima)
        {
            try
            {
                cmd = new SqlCommand("check_itaxsavetype_mast", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ItaxSavingTypeID", ima.ID ?? 0);
                cmd.Parameters.AddWithValue("@FinYear", ima.FinYear ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Gender", ima.GenderID ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@TypeDesc", ima.Description ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@MaxType", ima.IsAmount ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@MaxAmount", ima.Amount ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@InsertedBy", sds.UserID);
                cn.Open();
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();               
                ads.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Delete(ItaxScheme_MD ima)
        {
            try
            {
                cmd = new SqlCommand("Delete_ItaxSavingType", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ItaxSavingTypeID", ima.ID);
                cmd.Parameters.AddWithValue("@CanBy", sds.UserID);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Field> GetField()
        {
            try
            {
                cmd = new SqlCommand("Check_Mand", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfID", 12);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return Converter_Field(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<ItaxScheme_MD> GetGender()
        {
            try
            {
                cmd = new SqlCommand("Load_Gender", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return Converter_Gender(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<ItaxScheme_MD> GetData(ItaxScheme_MD ima)
        {
            try
            {
                cmd = new SqlCommand("Load_MST_ItaxSavingType", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FinYear", ima.FinYear);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return Converter_Data(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Field> Converter_Field(DataTable dt)
        {
            try
            {
                List<Field> lst = new List<Field>();
                lst = dt.AsEnumerable().Select(t =>
                {
                    var obj = new Field()
                    {
                        FieldID = t.Field<int>("FieldID"),
                        FieldName = t.Field<string>("FieldName"),
                        IsMand = t.Field<int>("IsMand")
                    };
                    return obj;
                }
                ).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ItaxScheme_MD> Converter_Gender(DataTable dt)
        {
            try
            {
                List<ItaxScheme_MD> lst = new List<ItaxScheme_MD>();
                lst = dt.AsEnumerable().Select(t => {
                    var obj = new ItaxScheme_MD()
                    {                       
                        GenderID = t.Field<string>("GENDER"),
                        Gender = t.Field<string>("GENDER_DESC"),                       
                    };
                    return obj;
                }).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ItaxScheme_MD> Converter_Data(DataTable dt)
        {
            try
            {
                List<ItaxScheme_MD> lst = new List<ItaxScheme_MD>();
                lst = dt.AsEnumerable().Select(t => {
                    var obj = new ItaxScheme_MD()
                    { 
                        ID = t.Field<int?>("ItaxSavingTypeID"),
                        FinYear = t.Field<string>("FinYear"),
                        Description=t.Field<string>("TypeDesc"),
                        GenderID = t.Field<string>("Gender"),
                        Gender = t.Field<string>("Gender_desc"),
                        IsAmount=t.Field<string>("MaxType"),
                        Amount = t.Field<decimal?>("MaxAmount"),
                    };
                    return obj;
                }).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}