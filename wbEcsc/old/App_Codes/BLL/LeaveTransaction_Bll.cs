﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;
using System.Data.SqlClient;
using System.IO;
using System.Data;


namespace wbEcsc.App_Codes.BLL
{
    public class LeaveTransaction_Bll:BllBase
    {
        public List<LeaveTransaction_VM> Get_FieldName(int FormID)
        {
            SqlCommand cmd = new SqlCommand("Check_Mand", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ConfID", FormID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Get_FormDATA(dt);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeaveTransaction_VM> Get_EmpNameAuto_Complete(string EmpName, string SectorID, string EmpNoName, int UserType, string SalMonthId)
        {
            SqlCommand cmd = new SqlCommand("GetEmployee", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SecID", SectorID);
            cmd.Parameters.AddWithValue("@UserType", UserType);
            cmd.Parameters.AddWithValue("@EmpName", EmpName);
            cmd.Parameters.AddWithValue("@EmpId", EmpNoName);
            cmd.Parameters.AddWithValue("@SalMonthId", SalMonthId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_EmpDetails(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LeaveTransaction_VM> Get_EmpNameAuto_Complete_Search(string EmpName, string SectorID, int UserType, string EmpId, string SalMonthId)
        {
            SqlCommand cmd = new SqlCommand("GetLvEmpSrch", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SecID", SectorID);
            cmd.Parameters.AddWithValue("@UserType", UserType);
            cmd.Parameters.AddWithValue("@EmpName", EmpName);
            cmd.Parameters.AddWithValue("@EmpId", EmpId);
            cmd.Parameters.AddWithValue("@SalMonthId", SalMonthId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_EmpDetails_Search(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LeaveTransaction_VM> Get_TourLeave(int isTourLeave)
        {
            SqlCommand cmd = new SqlCommand("Get_LeaveType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LeaveAbbv", "");
            cmd.Parameters.AddWithValue("@IsLeave", isTourLeave);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Get_TourLeave(dt);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DataTable Get_LeaveBalance(string ApplicationNo,  string ApplicationDate, string EmpID, string LeaveID, int isTourLeave)
        {
            SqlCommand cmd = new SqlCommand("Get_leaveBal", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LtNo", ApplicationNo);
            cmd.Parameters.AddWithValue("@LtDate", ApplicationDate);
            cmd.Parameters.AddWithValue("@EmployeeID", EmpID);
            cmd.Parameters.AddWithValue("@LvId", LeaveID);
            cmd.Parameters.AddWithValue("@IsLeave", isTourLeave);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DataTable Save_LeaveTransaction(string LVAppNo, string LVAppDate, string EmployeeID, string LeaveID, string FromDate, string ToDate, string LeaveBal, string isRound,
                                                string isLeaveTour, string NoofDays, string Remarks, string SalMonthID, string ReasonID, long UserID, string LvFlag)
        {
            SqlCommand cmd = new SqlCommand("check_leaveTransaction", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LtNo", LVAppNo);
            cmd.Parameters.AddWithValue("@LtDate", LVAppDate.Equals("") ? DBNull.Value : (object)LVAppDate);
            cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            cmd.Parameters.AddWithValue("@LvId", LeaveID);
            cmd.Parameters.AddWithValue("@FromDate", FromDate.Equals("") ? DBNull.Value : (object)FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate.Equals("") ? DBNull.Value : (object)ToDate);
            cmd.Parameters.AddWithValue("@Bal", LeaveBal.Equals("") ? DBNull.Value : (object)LeaveBal);
            cmd.Parameters.AddWithValue("@IsRound", isRound);
            cmd.Parameters.AddWithValue("@IsLeave", isLeaveTour);
            cmd.Parameters.AddWithValue("@Sanctioned", 1);
            cmd.Parameters.AddWithValue("@NoOfDays", NoofDays.Equals("") ? DBNull.Value : (object)NoofDays);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            cmd.Parameters.AddWithValue("@Remarks", Remarks);
            cmd.Parameters.AddWithValue("@SalMonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@RCode", ReasonID);
            cmd.Parameters.AddWithValue("@LvFlag", LvFlag);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DataTable Check_From_Date(string ApplicationNo, string ApplicationDate, string EmpID, string FromDate, string ToDate)
        {
            SqlCommand cmd = new SqlCommand("Get_LvFromDt", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LtNo", ApplicationNo);
            cmd.Parameters.AddWithValue("@LtDate", ApplicationDate);
            cmd.Parameters.AddWithValue("@EmployeeID", EmpID);
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DataTable Check_To_Date(string ApplicationNo, string ApplicationDate, string EmpID, string FromDate, string ToDate, string LeaveBal, string isRound, string isLeave, string LeaveID)
        {
            SqlCommand cmd = new SqlCommand("Get_LvToDt", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LtNo", ApplicationNo);
            cmd.Parameters.AddWithValue("@LtDate", ApplicationDate);
            cmd.Parameters.AddWithValue("@EmployeeID", EmpID);
            cmd.Parameters.AddWithValue("@LvId", LeaveID);
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            cmd.Parameters.AddWithValue("@Bal", LeaveBal.Equals("") ? DBNull.Value : (object)LeaveBal);
            cmd.Parameters.AddWithValue("@IsLeave", isLeave);
            cmd.Parameters.AddWithValue("@IsRound", isRound);

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DataTable Get_Date_MinMax_Date()
        {
            SqlCommand cmd = new SqlCommand("Get_MaxSalDt", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Delete_LeaveDetails(string ApplicationNo, string EmployeeID, string FromDate, string ToDate, long UserID)
        {
            SqlCommand cmd = new SqlCommand("Delete_LeaveTransaction", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LtNo", ApplicationNo);
            cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open(); 
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<LeaveTransaction_VM> Get_Reasons()
        {
            SqlCommand cmd = new SqlCommand("Get_Reason", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RType", "L");
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Convert_Reasons(dt);
            }
            catch (Exception)
            {
                throw;
            }
        }





        #region Converter
        List<LeaveTransaction_VM> Get_FormDATA(DataTable dt)
        {
            List<LeaveTransaction_VM> lst = new List<LeaveTransaction_VM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new LeaveTransaction_VM();
                obj.FieldID = s.Field<int>("FieldID");
                obj.FieldName = s.Field<string>("FieldName");
                obj.IsMand = s.Field<int>("IsMand");
                return obj;
            }).ToList();
            return lst;
        }

        public static List<LeaveTransaction_VM> Convert_EmpDetails(DataTable dt)
        {
            List<LeaveTransaction_VM> lst = new List<LeaveTransaction_VM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new LeaveTransaction_VM();
                obj.EmployeeID = t.Field<int>("EmployeeID");
                obj.EmpName = t.Field<string>("EmpName");
                return obj;
            }).ToList();
            return lst;
        }

        public static List<LeaveTransaction_VM> Get_TourLeave(DataTable dt)
        {
            List<LeaveTransaction_VM> lst = new List<LeaveTransaction_VM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new LeaveTransaction_VM();
                obj.LeaveAbbv = t.Field<string>("LeaveAbbv");
                obj.LvId = t.Field<int>("LvId");
                return obj;
            }).ToList();
            return lst;
        }

        public static List<LeaveTransaction_VM> Convert_EmpDetails_Search(DataTable dt)
        {
            List<LeaveTransaction_VM> lst = new List<LeaveTransaction_VM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new LeaveTransaction_VM();
                obj.EmployeeID = t.Field<int>("EmployeeID");
                obj.EmpName = t.Field<string>("EmpName");
                obj.DisEmpName = t.Field<string>("DisEmpName");
                obj.LtNo = t.Field<string>("LtNo");
                obj.LtDate = t.Field<string>("LtDate");
                obj.FromDate = t.Field<string>("FromDate");
                obj.ToDate = t.Field<string>("ToDate");
                obj.LBal = t.Field<decimal?>("LBal");
                obj.IsLeave = t.Field<int>("IsLeave");
                obj.IsRound = t.Field<int>("IsRound");
                obj.NoOfDays = t.Field<decimal?>("NoOfDays");
                obj.Remarks = t.Field<string>("Remarks");
                obj.LvId = t.Field<int>("LvId");
                obj.RCode = t.Field<int?>("RCode");
                return obj;
            }).ToList();
            return lst;
        }

        List<LeaveTransaction_VM> Convert_Reasons(DataTable dt)
        {
            List<LeaveTransaction_VM> lst = new List<LeaveTransaction_VM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new LeaveTransaction_VM();
                obj.RDesc = s.Field<string>("RDesc");
                obj.RCode = s.Field<int?>("RCode");
                return obj;
            }).ToList();
            return lst;
        }

        #endregion
    }
}