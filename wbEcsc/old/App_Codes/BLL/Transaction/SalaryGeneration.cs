﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wbEcsc.ViewModels;
using System.Data.SqlClient;
using System.Data;

namespace wbEcsc.App_Codes.BLL.Transaction
{
    public class SalaryGeneration:BllBase
    {
        public SalaryGeneration_MD Get_SalPayMonth(int? SectorID, string SalFinYear)
        {

            SqlCommand cmd = new SqlCommand("Get_SalPayMonth", cn);
            cmd.Parameters.AddWithValue("@SecID", SectorID);
            cmd.Parameters.AddWithValue("@SalFinYear", SalFinYear.Equals("") ? DBNull.Value : (object)SalFinYear);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if(dt.Rows.Count>0)
                {
                   return Convert(dt)[0];
                }
                return null;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public string Salary_Process(int SalMonthID, int SectorID, string SalFinYear, long UserID)
        {
            string result = ""; string EncodeValue = "";
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
           
            SqlDataReader dr;
            try
            {
                try
                {
                    cmd = new SqlCommand("Get_SalPayMonth", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@SecID", SectorID);
                    cmd.Parameters.AddWithValue("@SalFinYear", SalFinYear.Equals("") ? DBNull.Value : (object)SalFinYear);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        EncodeValue = dr["EncodeValue"].ToString();
                    }
                    dr.Close();

                    if (EncodeValue == "" || EncodeValue == null)
                    {
                        cmd = new SqlCommand("SalaryProcess", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@sal_mon", SalMonthID);
                        cmd.Parameters.AddWithValue("@secid", SectorID);
                        cmd.Parameters.AddWithValue("@insertedBy", UserID);

                        cmd.CommandTimeout = 0;
                        cmd.ExecuteNonQuery();

                        transaction.Commit();
                        result = "success";
                    }
                    else
                    {
                        result = "encode";
                    }
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    result = "fail";
                    throw new Exception(sqlError.Message);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                dbConn.Close();
            }
            return result;
        }

        #region Converter
        List<SalaryGeneration_MD> Convert(DataTable dt)
        {
            List<SalaryGeneration_MD> lst = new List<SalaryGeneration_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new SalaryGeneration_MD();
                obj.MaxSalMonthID = t.Field<int>("MaxSalMonthID");
                obj.MaxSalMonth = t.Field<string>("MaxSalMonth");
                obj.EncodeValue = t.Field<string>("EncodeValue");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(SalaryGeneration v)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}