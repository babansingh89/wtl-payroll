﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace wbEcsc.App_Codes.BLL.Master
{
    public class PTaxMasterBLL : BllBase
    {
        public DataSet Populate_GridBLL(string PTaxID)
        {
            SqlCommand cmd = new SqlCommand("Get_PTaxByID", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PTaxID", PTaxID);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sd.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally { cn.Close(); }
        }

        public DataSet Delete_PTaxBLL(string PTaxID)
        {
            SqlCommand cmd = new SqlCommand("Delete_PTaxByID", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PTaxID", PTaxID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public DataSet SaveBLL(string SecID, string PTaxID, string SalFrom, string SalTo, string PTax, int UserID)
        {
            SqlCommand cmd = new SqlCommand("check_ptax_mast", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SecID", SecID);
            cmd.Parameters.AddWithValue("@SalFrom", SalFrom);
            cmd.Parameters.AddWithValue("@SalTo", SalTo);
            cmd.Parameters.AddWithValue("@PTax", PTax);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            cmd.Parameters.AddWithValue("@PTaxID", PTaxID);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sd.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally { cn.Close(); }
        }
        
    }//ALL END
}