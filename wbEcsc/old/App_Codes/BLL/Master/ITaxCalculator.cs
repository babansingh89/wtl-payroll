﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace wbEcsc.App_Codes.BLL.Master
{
    public class ITaxCalculator : BllBase
    {
        public DataSet Get_ITaxData_onPageLoadBLL(string TransType, string SalFinYear, string EmpNo, string SectorID, string UserID)
        {
            SqlCommand cmd = new SqlCommand("ecsc.Itax_allPurpose", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@salFinYear", SalFinYear);
            cmd.Parameters.AddWithValue("@EmpNo", EmpNo);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception Ex) { throw Ex; }
            finally { cn.Close(); }
        }

        public DataSet Get_ITaxDataCalculationBLL(string TransType, string SalFinYear, string EmpNo, string SectorID, string UserID, string AssesmentYear, string TaxableIncome)
        {
            SqlCommand cmd = new SqlCommand("ecsc.Itax_allPurpose", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@salFinYear", SalFinYear);
            cmd.Parameters.AddWithValue("@EmpNo", EmpNo);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@AssesmentYear", AssesmentYear);
            cmd.Parameters.AddWithValue("@TaxableIncome", TaxableIncome);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception Ex) { throw Ex; }
            finally { cn.Close(); }
        }

        public DataSet Get_OtherSourceIncomeBLL(string OtherSourceName)
        {
            SqlCommand cmd = new SqlCommand("ecsc.Get_OtherSourceIncome", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OtherSourceName", OtherSourceName);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception Ex) { throw Ex; }
            finally { cn.Close(); }
        }

        public DataSet Save_OtherSourceIncomeBLL(string OtherSourceId, string OtherSourceName, string UserID)
        {
            SqlCommand cmd = new SqlCommand("ecsc.Insert_ITaxOtherSourceIncome", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OtherSourceId", OtherSourceId);
            cmd.Parameters.AddWithValue("@OtherSourceName", OtherSourceName);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception Ex) { throw Ex; }
            finally { cn.Close(); }
        }

        public DataSet SaveNewSubSavingTypeBLL(string ItaxSaveID, string Description, string TaxTypeId, string Abbre, string MaxAmount, string type, string Percentage, string UserID)
        {
            SqlCommand cmd = new SqlCommand("ecsc.InstUpdtItaxSavingType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ItaxSaveID", ItaxSaveID);
            cmd.Parameters.AddWithValue("@SavingTypeAbbr", Abbre);
            cmd.Parameters.AddWithValue("@SavingTypeDesc", Description);
            cmd.Parameters.AddWithValue("@MaxType", type);
            cmd.Parameters.AddWithValue("@MaxAmount", MaxAmount);
            cmd.Parameters.AddWithValue("@Percentage", Percentage.Equals("") ? DBNull.Value : (object)Percentage);
            cmd.Parameters.AddWithValue("@ItaxTypeID", TaxTypeId);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception Ex) { throw Ex; }
            finally { cn.Close(); }
        }





    }//ALL END
}