﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL.Master
{
    public class DAMaster : BllBase
    {
        public List<DAMasterVM> Get_DAData(string EmpType)
        {
            SqlCommand cmd = new SqlCommand("Get_DAbyEmpType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpType", EmpType.Equals("") ? DBNull.Value : (object)EmpType);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Get_GridDATA(dt);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<DAMasterVM> Delete_DA(string DAID, int? UserID)
        {
            SqlCommand cmd = new SqlCommand("Delete_DAByID", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DAID", DAID.Equals("") ? 0 : (object)DAID);
            cmd.Parameters.AddWithValue("@CanBy", UserID.Equals("") ? 0 : (object)UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return ThrowMessege(dt);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<DAMasterVM> SaveUpdate_DA(string SecID, string DAID, string EmpType, string OrderNo, string OrderDate, string DARate, string MaxAmount, string EffectiveFrom, string EffectiveTo, int? UserID)
        {
            SqlCommand cmd = new SqlCommand("check_da_mast", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SecID", SecID.Equals("") ? DBNull.Value : (object)SecID);
            cmd.Parameters.AddWithValue("@DAID", DAID.Equals("") ? 0 : (object)DAID);
            cmd.Parameters.AddWithValue("@EmpType", EmpType.Equals("") ? DBNull.Value : (object)EmpType);
            cmd.Parameters.AddWithValue("@OrderNo", OrderNo.Equals("") ? DBNull.Value : (object)OrderNo);
            cmd.Parameters.AddWithValue("@OrderDate", OrderDate.Equals("") ? DBNull.Value : (object)OrderDate);
            cmd.Parameters.AddWithValue("@DARate", DARate.Equals("") ? DBNull.Value : (object)DARate);
            cmd.Parameters.AddWithValue("@MaxAmount", MaxAmount.Equals("") ? DBNull.Value : (object)MaxAmount);
            cmd.Parameters.AddWithValue("@EffectiveFrom", EffectiveFrom.Equals("") ? DBNull.Value : (object)EffectiveFrom);
            cmd.Parameters.AddWithValue("@EffectiveTo", EffectiveTo.Equals("") ? DBNull.Value : (object)EffectiveTo);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID.Equals("") ? 0 : (object)UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return ThrowMessege(dt);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<DAMasterVM> Get_EffectFromDT(string SecID, string EffectiveFrom)
        {
            SqlCommand cmd = new SqlCommand("Get_DAEffFrom", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SecID", SecID.Equals("") ? DBNull.Value : (object)SecID);
            cmd.Parameters.AddWithValue("@EffectiveFrom", EffectiveFrom.Equals("") ? DBNull.Value : (object)EffectiveFrom);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Get_EffectiveFromDate(dt);
            }
            catch (Exception)
            {

                throw;
            }
        }









        List<DAMasterVM> Get_GridDATA(DataTable dt)
        {
            List<DAMasterVM> lst = new List<DAMasterVM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new DAMasterVM();
                obj.DAID = s.Field<int?>("DAID");
                obj.EmpType = s.Field<string>("EmpType");
                obj.EmpTypeDesc = s.Field<string>("EmpTypeDesc");
                obj.DARate = s.Field<decimal?>("DARate").ToString();
                obj.MaxAmount = s.Field<decimal?>("MaxAmount").ToString();
                obj.OrderNo = s.Field<string>("OrderNo");
                obj.OrderDate = s.Field<string>("OrderDate");
                obj.EffectiveFrom = s.Field<string>("EffectiveFrom");
                obj.EffectiveTo = s.Field<string>("EffectiveTo");
                obj.Edit = s.Field<string>("Edit");
                obj.Del = s.Field<string>("Del");
                return obj;
            }).ToList();
            return lst;
        }

        List<DAMasterVM> ThrowMessege(DataTable dt)
        {
            List<DAMasterVM> lst = new List<DAMasterVM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new DAMasterVM();
                obj.ErrorCode = s.Field<int?>("ErrorCode");
                obj.Messege = s.Field<string>("Messege");
                return obj;
            }).ToList();
            return lst;
        }

        List<DAMasterVM> Get_EffectiveFromDate(DataTable dt)
        {
            List<DAMasterVM> lst = new List<DAMasterVM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new DAMasterVM();
                obj.ErrorCode = s.Field<int?>("ErrorCode");
                obj.Messege = s.Field<string>("Messege");
                obj.EffectiveFrom = s.Field<string>("EffectiveDate");
                return obj;
            }).ToList();
            return lst;
        }
    }
}