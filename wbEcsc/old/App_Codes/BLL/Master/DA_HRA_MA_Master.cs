﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL.Master
{
    public class DA_HRA_MA_Master:BllBase
    {
        public List<DA_HRA_MA_MaterVM> Get_EarningTypeBLL()
        {
            SqlCommand cmd = new SqlCommand("Get_EarningType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Get_EarningType(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<DA_HRA_MA_MaterVM> Get_EmpTypeBLL(string EmpType)
        {
            SqlCommand cmd = new SqlCommand("Get_DAbyEmpType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpType", EmpType.Equals("") ? DBNull.Value : (object)EmpType);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Get_EmpType(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<DA_HRA_MA_MaterVM> Get_DA_HRA_MA_RateBLL(string SecID, string EmpType, string EarningType, string PayMonthID)
        {
            SqlCommand cmd = new SqlCommand("Get_DA_HRA_MA_Rate", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SecID", SecID.Equals("") ? DBNull.Value : (object)SecID);
            cmd.Parameters.AddWithValue("@EmpType", EmpType.Equals("") ? DBNull.Value : (object)EmpType);
            cmd.Parameters.AddWithValue("@EarningType", EarningType.Equals("") ? DBNull.Value : (object)EarningType);
            cmd.Parameters.AddWithValue("@PayMonthID", PayMonthID.Equals("") ? DBNull.Value : (object)PayMonthID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Get_DA_HRA_MA_Rate(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<DA_HRA_MA_MaterVM> Get_GridDataBLL(string SecID, string EarningCode, string EmpNo, string Emptype, string DaHraMa, string PayMonthID, int UserID)
        {
            SqlCommand cmd = new SqlCommand("Get_EmpDaHraMaDetail", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SecID", SecID.Equals("") ? DBNull.Value : (object)SecID);
            cmd.Parameters.AddWithValue("@EarningCode", EarningCode.Equals("") ? DBNull.Value : (object)EarningCode);
            cmd.Parameters.AddWithValue("@EmpNo", EmpNo.Equals("") ? 0: (object)EmpNo);
            cmd.Parameters.AddWithValue("@Emptype", Emptype.Equals("") ? DBNull.Value : (object)Emptype);
            cmd.Parameters.AddWithValue("@DaHraMa", DaHraMa.Equals("") ? DBNull.Value : (object)DaHraMa);
            cmd.Parameters.AddWithValue("@PayMonthID", PayMonthID.Equals("") ? DBNull.Value : (object)PayMonthID);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Get_Grid(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<DA_HRA_MA_MaterVM> Get_UpdateBLL(string EmpNo, string Decline, string Reasons, int UserID)
        {
            SqlCommand cmd = new SqlCommand("Update_EmpDaHraMa", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpNo", EmpNo.Equals("") ? DBNull.Value : (object)EmpNo);
            cmd.Parameters.AddWithValue("@Decline", Decline.Equals("") ? "N" : (object)Decline);
            cmd.Parameters.AddWithValue("@Reasons", Reasons.Equals("") ? DBNull.Value : (object)Reasons);
            cmd.Parameters.AddWithValue("@ModifiedBy", UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return ThrowMessege(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<DA_HRA_MA_MaterVM> Get_GenerateBLL(string SecID, string Daid, string EarningCode, int UserID)
        {
            SqlCommand cmd = new SqlCommand("Generate_EmpDAHRAMA", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SecID", SecID.Equals("") ? DBNull.Value : (object)SecID);
            cmd.Parameters.AddWithValue("@Daid", Daid.Equals("") ? DBNull.Value : (object)Daid);
            cmd.Parameters.AddWithValue("@EarningCode", EarningCode.Equals("") ? DBNull.Value : (object)EarningCode);
            cmd.Parameters.AddWithValue("@ModifiedBy", UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return ThrowMessege(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }





        List<DA_HRA_MA_MaterVM> Get_EarningType(DataTable dt)
        {
            List<DA_HRA_MA_MaterVM> lst = new List<DA_HRA_MA_MaterVM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new DA_HRA_MA_MaterVM();
                obj.EarningCode = s.Field<int?>("EarningCode");
                obj.EarningType = s.Field<string>("EarningType");
                return obj;
            }).ToList();
            return lst;
        }

        List<DA_HRA_MA_MaterVM> Get_EmpType(DataTable dt)
        {
            List<DA_HRA_MA_MaterVM> lst = new List<DA_HRA_MA_MaterVM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new DA_HRA_MA_MaterVM();
                obj.EmpType = s.Field<string>("EmpType");
                obj.EmpTypeDesc = s.Field<string>("EmpTypeDesc");
                return obj;
            }).ToList();
            return lst;
        }

        List<DA_HRA_MA_MaterVM> Get_DA_HRA_MA_Rate(DataTable dt)
        {
            List<DA_HRA_MA_MaterVM> lst = new List<DA_HRA_MA_MaterVM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new DA_HRA_MA_MaterVM();
                obj.IDS = s.Field<int?>("IDS");
                obj.VALS = s["VALS"].ToString();
                obj.DA_HRA_MA_ID = s.Field<int?>("DA_HRA_MA_ID");
                obj.ErrorCode = s.Field<int?>("ErrorCode");
                obj.Messege = s.Field<string>("Messege");
                return obj;
            }).ToList();
            return lst;
        }

        List<DA_HRA_MA_MaterVM> Get_Grid(DataTable dt)
        {
            List<DA_HRA_MA_MaterVM> lst = new List<DA_HRA_MA_MaterVM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new DA_HRA_MA_MaterVM();
                obj.EmpNo = s.Field<string>("EmpNo");
                obj.EmpName = s.Field<string>("EmpName");
                obj.CenterName = s.Field<string>("CenterName");
                obj.OldDa = s["OldDa"].ToString();
                obj.NewDa = s["NewDa"].ToString();
                obj.Declined = s.Field<string>("Declined");
                return obj;
            }).ToList();
            return lst;
        }

        List<DA_HRA_MA_MaterVM> ThrowMessege(DataTable dt)
        {
            List<DA_HRA_MA_MaterVM> lst = new List<DA_HRA_MA_MaterVM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new DA_HRA_MA_MaterVM();
                obj.ErrorCode = s.Field<int?>("ErrorCode");
                obj.Messege = s.Field<string>("Messege");
                return obj;
            }).ToList();
            return lst;
        }




    }//ALL END
}