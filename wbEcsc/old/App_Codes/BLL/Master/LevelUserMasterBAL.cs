﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL.Master
{
    public class LevelUserMasterBAL : BllBase
    {
        public List<LevelUserMasterVM> Get_LevelName(string TransactionType, string FormName, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("Get_LevelName", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@FormName", FormName);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return LevelName_Bind(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        List<LevelUserMasterVM> LevelName_Bind(DataTable dt)
        {
            List<LevelUserMasterVM> lst = new List<LevelUserMasterVM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new LevelUserMasterVM();
                obj.ConfID = s.Field<int?>("ConfID");
                obj.FormName = s.Field<string>("FormName");
                return obj;
            }).ToList();
            return lst;
        }

        public List<LevelUserMasterVM> Get_UserName(string LevelID, string UserName, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("Get_LevelUserName", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LevelID", LevelID);
            cmd.Parameters.AddWithValue("@UserName", UserName);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return UserName_Bind(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        List<LevelUserMasterVM> UserName_Bind(DataTable dt)
        {
            List<LevelUserMasterVM> lst = new List<LevelUserMasterVM>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new LevelUserMasterVM();
                obj.UserID = s.Field<long?>("UserID");
                obj.UserName = s.Field<string>("UserName");
                return obj;
            }).ToList();
            return lst;
        }

        public DataSet GridLevel_DAL(string ConfID, string LevelSubID, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("Load_LevelUser", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ConfID", ConfID);
            cmd.Parameters.AddWithValue("@LevelSubID", LevelSubID);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw;
            }
            finally { cn.Close(); }
        }

        public DataSet SaveLevel_DAL(string LevelID, string LevelSubID, string LevelUser, string LevelRank, string IsActive, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("Check_LevelUser", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LevelID", LevelID);
            cmd.Parameters.AddWithValue("@LevelSubID", LevelSubID);
            cmd.Parameters.AddWithValue("@LevelUser", LevelUser);
            cmd.Parameters.AddWithValue("@LevelRank", LevelRank);
            cmd.Parameters.AddWithValue("@IsActive", IsActive);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw;
            }
            finally { cn.Close(); }
        }

    }
}