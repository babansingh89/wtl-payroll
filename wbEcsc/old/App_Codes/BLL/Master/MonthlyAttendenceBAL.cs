﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL.Master
{
    public class MonthlyAttendenceBAL : BllBase
    {
        public List<MonthlyAttendenceVM> Get_Year(string TransactionType)
        {
            SqlCommand cmd = new SqlCommand("DAT_EmpAttendance_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);

                List<MonthlyAttendenceVM> lst = new List<MonthlyAttendenceVM>();
                lst = dt.AsEnumerable().Select(s =>
                {
                    var obj = new MonthlyAttendenceVM();
                    obj.CurrYr = s.Field<int?>("CurrYr");
                    return obj;
                }).ToList();
                return lst;

            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally { cn.Close(); }
        }

        public DataSet SelectMonthYearData_DAL(string TransactionType, string Year, string SectorID, string UserID)
        {
            SqlCommand cmd = new SqlCommand("DAT_EmpAttendance_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@YEAR", Year);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally { cn.Close(); }
        }

        public DataSet SelectDetailData_DAL(string TransactionType, string Year, string SectorID, string SalMonthID, string UserID, string FromDate, string ToDate)
        {
            SqlCommand cmd = new SqlCommand("DAT_EmpAttendance_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@YEAR", Year);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@SalMonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            cmd.Parameters.AddWithValue("@ProccFrmDt1", FromDate);
            cmd.Parameters.AddWithValue("@ProccToDt1", ToDate);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally { cn.Close(); }
        }

        public DataSet UpdateDetailData_DAL(string TransactionType, string Year, string SectorID, string SalMonthID, string UserID
            , string EmployeeID, string AttDays, string TourDays, string ClDays1, string MlDays1, string ElDays1, string OthLvDays1
            , string ActOffDays, string ActHoliday, string LwpDays, string AdjustTiffinDays, string TiffinAllowance, string LateAtt)
        {
            SqlCommand cmd = new SqlCommand("DAT_EmpAttendance_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@YEAR", Year);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@SalMonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            cmd.Parameters.AddWithValue("@AttDays1", AttDays);
            cmd.Parameters.AddWithValue("@TourDays1", TourDays);
            cmd.Parameters.AddWithValue("@ClDays1", ClDays1);
            cmd.Parameters.AddWithValue("@MlDays1", MlDays1);
            cmd.Parameters.AddWithValue("@ElDays1", ElDays1);
            cmd.Parameters.AddWithValue("@OthLvDays1", OthLvDays1);
            cmd.Parameters.AddWithValue("@ActOffDays1", ActOffDays);
            cmd.Parameters.AddWithValue("@ActHoliday1", ActHoliday);
            cmd.Parameters.AddWithValue("@LwpDays1", LwpDays);
            cmd.Parameters.AddWithValue("@AdjustTiffinDays1", AdjustTiffinDays);
            cmd.Parameters.AddWithValue("@TiffinAllowance1", TiffinAllowance);
            cmd.Parameters.AddWithValue("@LateAtt1", LateAtt);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally { cn.Close(); }
        }

        public DataSet LeaveEdit_DAL(string TransactionType, string SectorID, string SalMonthID, string EmployeeID, string Year, string UserID)
        {
            SqlCommand cmd = new SqlCommand("DAT_EmpAttendance_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@YEAR", Year);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@SalMonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally { cn.Close(); }
        }

        public DataSet LeaveBalance_DAL(string TransactionType, string Year, string SectorID, string SalMonthID, string UserID, string EmployeeID)
        {
            SqlCommand cmd = new SqlCommand("DAT_EmpAttendance_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@YEAR", Year);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@SalMonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally { cn.Close(); }
        }

        public DataSet LeaveDetails_DAL(string TransactionType, string Year, string SectorID, string SalMonthID, string UserID, string EmployeeID, string LvId)
        {
            SqlCommand cmd = new SqlCommand("DAT_EmpAttendance_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@YEAR", Year);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@SalMonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            cmd.Parameters.AddWithValue("@LvId", LvId);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally { cn.Close(); }
        }

        public DataSet CompanyDetails_DAL()
        {
            SqlCommand cmd = new SqlCommand("Get_Logo", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally { cn.Close(); }
        }

        public DataSet ChangeHoliday_DAL(string TransactionType, string Year, string SectorID, string SalMonthID, string UserID, string EmployeeID, string AttDays, string ActHoliday, string ChangeHoliday, string FromDate, string ToDate)
        {
            SqlCommand cmd = new SqlCommand("DAT_EmpAttendance_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@YEAR", Year);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@SalMonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            cmd.Parameters.AddWithValue("@AttDays1", AttDays);
            cmd.Parameters.AddWithValue("@ActHoliday1", ActHoliday == "" ? DBNull.Value : (object)ActHoliday);
            cmd.Parameters.AddWithValue("@ChHoliday1", ChangeHoliday == "" ? DBNull.Value : (object)ChangeHoliday);
            cmd.Parameters.AddWithValue("@ProccFrmDt1", FromDate);
            cmd.Parameters.AddWithValue("@ProccToDt1", ToDate);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally { cn.Close(); }
        }

        public DataSet ChangeOffDays_DAL(string TransactionType, string Year, string SectorID, string SalMonthID, string UserID, string EmployeeID, string AttDays, string ActOffDays, string ChOffDays, string FromDate, string ToDate)
        {
            SqlCommand cmd = new SqlCommand("DAT_EmpAttendance_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@YEAR", Year);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@SalMonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            cmd.Parameters.AddWithValue("@AttDays1", AttDays);
            cmd.Parameters.AddWithValue("@ActOffDays1", ActOffDays == "" ? DBNull.Value : (object)ActOffDays);
            cmd.Parameters.AddWithValue("@ChOffDays1", ChOffDays == "" ? DBNull.Value : (object)ChOffDays);
            cmd.Parameters.AddWithValue("@ProccFrmDt1", FromDate);
            cmd.Parameters.AddWithValue("@ProccToDt1", ToDate);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally { cn.Close(); }
        }

        public DataSet UpdateSanction_DAL(string TransactionType, string Year, string SectorID, string SalMonthID, string UserID, string LevelRemarks)
        {
            SqlCommand cmd = new SqlCommand("DAT_EmpAttendance_SP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@YEAR", Year);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@SalMonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID);
            cmd.Parameters.AddWithValue("@LevelRemarks", LevelRemarks);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally { cn.Close(); }
        }



    }//ALL END
}