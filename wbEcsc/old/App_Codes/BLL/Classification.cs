﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL
{
    public class Classification:BllBase
    {
        public List<Classification_MD> Get_Classification()
        {

            SqlCommand cmd = new SqlCommand("Load_Classification", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }



        #region Converter
        List<Classification_MD> Convert(DataTable dt)
        {
            List<Classification_MD> lst = new List<Classification_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Classification_MD();
                obj.ClassificationID = t.Field<int?>("ClassificationID");
                obj.ClassificationCode = t.Field<string>("ClassificationCode");
                obj.Classification = t.Field<string>("Classification");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(Classification v)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}