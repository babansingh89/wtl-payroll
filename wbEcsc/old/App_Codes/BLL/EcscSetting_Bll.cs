﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.AppResource;

namespace wbEcsc.App_Codes.BLL
{
    public class EcscSetting_Bll : BllBase
    {
        
        public EcscSetting GetSettings()
        {
            SqlCommand cmd = new SqlCommand("Get_EcscSettings", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return Convert(dt)[0];
                }
                return null;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        List<EcscSetting> Convert(DataTable dt)
        {
            List<EcscSetting> lst = new List<EcscSetting>();
            foreach (DataRow dr in dt.Rows)
            {
                EcscSetting obj = new EcscSetting();
                obj.Id = dr.Field<int>("Id");
                obj.ApprovalServerAddress = dr.Field<string>("ApprovalServerAddress");
                obj.ChiperKey = dr.Field<string>("ChiperKey");
                lst.Add(obj);
            }
            return lst;
        }
    }
}