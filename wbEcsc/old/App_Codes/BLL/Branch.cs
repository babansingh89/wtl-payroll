﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using wbEcsc.App_Start;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL
{
    public class Branch:BllBase
    {
        SessionData sd = new SessionData();
        public Branch() {
            sd = SessionContext.SessionData;
        }
        public List<Field> GetField()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Check_Mand", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfID", 3);
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return Convert_Field(dt);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Field> Convert_Field(DataTable dt)
        {
            try
            {
                List<Field> lstField = new List<Field>();

                lstField = dt.AsEnumerable().Select(t =>
                {
                    var lstf = new Field();
                    lstf.FieldID = t.Field<int?>("FieldID");
                    lstf.FieldName = t.Field<string>("FieldName");
                    lstf.IsMand = t.Field<int>("IsMand");
                    return lstf;
                }).ToList();

                return lstField;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable Save(Branch_MD obj)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("check_branch_mast", cn);
                cmd.Parameters.AddWithValue("@BankID", obj.BankID??0);
                cmd.Parameters.AddWithValue("@BranchName", obj.Branch ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@BranchID", obj.BranchID??0);
                cmd.Parameters.AddWithValue("@IFSC", obj.IFSC ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@MICR", obj.MICR ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@InsertedBy", sd.UserID);       

                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();
                SqlDataAdapter asd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                    asd.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }     
        public List<Branch_MD> Get_Branch()
        {
            SqlCommand cmd = new SqlCommand("Get_Branch", cn);           
            cmd.CommandType = CommandType.StoredProcedure;          
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Branch_MD> Get_Branch(int BankID)
        {
            SqlCommand cmd = new SqlCommand("Get_BranchbyBankID", cn);
            cmd.Parameters.AddWithValue("@BankID", BankID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Branch_MD> Get_Auto_IFSC(string IFSC)
        {
            SqlCommand cmd = new SqlCommand("Search_IFSC", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IFSCCode", IFSC);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public Branch_MD Get_BankandBranch(int BranchID)
        {
            SqlCommand cmd = new SqlCommand("Get_BankandBranch", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BranchID", BranchID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
            //    B.BranchID,B.Branch, B.IFSC,B.MICR, B.BankID, bb.BankName
                Branch_MD bra = new Branch_MD() {
                    BranchID = (int)dt.Rows[0]["BranchID"],
                    Branch=dt.Rows[0]["Branch"].ToString(),
                    BankID=(int)dt.Rows[0]["BankID"],
                    BankName=dt.Rows[0]["BankName"].ToString(),
                    IFSC=dt.Rows[0]["IFSC"].ToString(),
                    MICR=dt.Rows[0]["MICR"].ToString()
                };
                return bra;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Branch_MD>  Get_BankandBranch(string BranchID)
        {
            SqlCommand cmd = new SqlCommand("Get_BankandBranch", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BranchID", BranchID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);               
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        #region Converter
        List<Branch_MD> Convert(DataTable dt)
        {
            List<Branch_MD> lst = new List<Branch_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Branch_MD();
                obj.BranchID = t.Field<int?>("BranchID");
                obj.Branch = t.Field<string>("Branch");
                obj.BankID = t.Field<int?>("BankID");
                obj.BankName = t.Field<string>("BankName");
                obj.IFSC = t.Field<string>("IFSC");
                obj.MICR = t.Field<string>("MICR");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(Branch v)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}