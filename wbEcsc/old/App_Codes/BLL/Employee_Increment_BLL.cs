﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using wbEcsc.App_Start;


namespace wbEcsc.App_Codes.BLL
{
    public class Employee_Increment_BLL:BllBase
    {
        public DataTable EmployeeDetail(string SectorID , string SalMonth)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "Get_DetailforEmpIncrement";
            cmd.Parameters.AddWithValue("@SecId", SectorID);
            cmd.Parameters.AddWithValue("@PayMonth", SalMonth);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public void Empwise_UpdateIncrement(string EmpID, string OldBasic, string NewPayscaleID, string NewBasic, string IncrPer, string IncrOn, string Decline, string Remarks, string SectorID)
        {
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();

            try
            {
                try
                {
                    cmd = new SqlCommand("Update_EmployeeIncrement", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@EmployeeID", EmpID);
                    cmd.Parameters.AddWithValue("@OldBasic", OldBasic == null ? DBNull.Value : (object)OldBasic);
                    cmd.Parameters.AddWithValue("@NewPayscaleID", NewPayscaleID == null ? DBNull.Value : (object)NewPayscaleID);
                    cmd.Parameters.AddWithValue("@NewBasic", NewBasic == null ? DBNull.Value : (object)NewBasic);
                    cmd.Parameters.AddWithValue("@IncrPer", IncrPer == null ? DBNull.Value : (object)IncrPer);
                    cmd.Parameters.AddWithValue("@IncrOn", IncrOn == null ? DBNull.Value : (object)IncrOn);
                    cmd.Parameters.AddWithValue("@Decline", Decline == null ? DBNull.Value : (object)Decline);
                    cmd.Parameters.AddWithValue("@Remarks", Remarks == null ? DBNull.Value : (object)Remarks);
                    cmd.Parameters.AddWithValue("@All", "B");
                    cmd.Parameters.AddWithValue("@secid", SectorID);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();

                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
        }

        public string Apply(string EmpID, string OldBasic, string NewPayscaleID, string NewBasic, string IncrPer, string IncrOn, string Decline, string Remarks, string SectorID)
        {
            string result = ""; string countValue = "";
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            try
            {
                try
                {
                    cmd = new SqlCommand("Check_EmployeeAvailable", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SectorID", SectorID);
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        countValue = dr["Count1"].ToString();
                    }
                    dr.Close();
                    if (countValue == "" || countValue == "0")
                    {
                        result = "success";
                        cmd = new SqlCommand("Update_EmployeeIncrement", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@EmployeeID", EmpID);
                        cmd.Parameters.AddWithValue("@OldBasic", OldBasic == "" ? DBNull.Value : (object)OldBasic);
                        cmd.Parameters.AddWithValue("@NewPayscaleID", NewPayscaleID == null ? DBNull.Value : (object)NewPayscaleID);
                        cmd.Parameters.AddWithValue("@NewBasic", NewBasic == "" ? DBNull.Value : (object)NewBasic);
                        cmd.Parameters.AddWithValue("@IncrPer", IncrPer == "" ? DBNull.Value : (object)IncrPer);
                        cmd.Parameters.AddWithValue("@IncrOn", IncrOn == "" ? DBNull.Value : (object)IncrOn);
                        cmd.Parameters.AddWithValue("@Decline", Decline == "" ? DBNull.Value : (object)Decline);
                        cmd.Parameters.AddWithValue("@Remarks", Remarks == "" ? DBNull.Value : (object)Remarks);
                        cmd.Parameters.AddWithValue("@All", "A");
                        cmd.Parameters.AddWithValue("@secid", SectorID);
                        cmd.ExecuteNonQuery();

                        cmd = new SqlCommand("updt_emp_details", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Secid", SectorID);
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                         result = "Exist";
                    }

                    transaction.Commit();

                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return result;
        }

        public string Empwise_UpdateIncrementPerct(string EmpNo, string EmpID, string Perct, long UserID)
        {
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            string result = "";
            try
            {
                try
                {
                    cmd = new SqlCommand("Update_EmployeeIncrementPerct", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@EmployeeID", EmpID);
                    cmd.Parameters.AddWithValue("@EmpNo", EmpNo);
                    cmd.Parameters.AddWithValue("@Perct", Perct);
                    cmd.Parameters.AddWithValue("@UserID", UserID);

                    cmd.ExecuteNonQuery();
                    transaction.Commit();
                    result = "success";

                }
                catch (SqlException sqlError)
                {
                    result = "fail";
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return result;
        }
    }
    
}