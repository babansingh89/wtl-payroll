﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL
{
    public class Designation:BllBase
    {
        SessionData seed = new SessionData();
        public Designation() {
            seed = SessionContext.SessionData;
        }

        public List<Field> GetField()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Check_Mand", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfID", 4);
                cn.Open();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                return Convert_Field(dt);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        } 
        public DataTable Save(Designation_MD desig)
        {
            SqlCommand cmd = new SqlCommand("UPDATE_Designation", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                cmd.Parameters.AddWithValue("@DesignationID", desig.DesignationID??0);
                cmd.Parameters.AddWithValue("@Designation", desig.Designation??(object)DBNull.Value);
                cmd.Parameters.AddWithValue("@InsertedBy", seed.UserID);
                cn.Open();
                SqlDataAdapter asd = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                asd.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cn.Close();
            }
        }        

        public List<Designation_MD> Get_Designation()
        {
            SqlCommand cmd = new SqlCommand("Load_Designation", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Field> Convert_Field(DataTable dt)
        {
            try
            {
                List<Field> lstField = new List<Field>();

                lstField = dt.AsEnumerable().Select(t =>
                {
                    var objfield = new Field();
                    objfield.FieldID = t.Field<int?>("FieldID");
                    objfield.FieldName = t.Field<string>("FieldName");
                    objfield.IsMand = t.Field<int>("IsMand");
                    return objfield;
                    ;
                }).ToList();

                return lstField;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        #region Converter
        List<Designation_MD> Convert(DataTable dt)
        {
            List<Designation_MD> lst = new List<Designation_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Designation_MD();
                obj.DesignationID = t.Field<int?>("DesignationID");
                obj.Designation = t.Field<string>("Designation");                
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(Designation v)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}