﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;


namespace wbEcsc.App_Codes.BLL
{
    public class LeaveMaster : BllBase
    {
        public List<LeaveMaster_MD> SaveUpdate_LeaveMaster(string hdnLeaveTypeID, string txtLeaveDesc, string txtAbbv, string ddlGender, string txtMaxInYear, string txtMaxSave, string txtMaxInServicePeriod, int? chkIsActive, int? chkIsRound, int? chkIsLeave, int? UserID)
        {
            SqlCommand cmd = new SqlCommand("check_leavetype_mast", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LvId", hdnLeaveTypeID.ToString().Equals("") ? 0 : (object)hdnLeaveTypeID);
            cmd.Parameters.AddWithValue("@LeaveAbbv", txtAbbv.Equals("") ? DBNull.Value : (object)txtAbbv);
            cmd.Parameters.AddWithValue("@LeaveTypeDesc", txtLeaveDesc.Equals("") ? DBNull.Value : (object)txtLeaveDesc);
            cmd.Parameters.AddWithValue("@LvYearlyUpto", txtMaxInYear.Equals("") ? DBNull.Value : (object)txtMaxInYear);
            cmd.Parameters.AddWithValue("@MaxCfUpto", txtMaxSave.Equals("") ? DBNull.Value : (object)txtMaxSave);
            cmd.Parameters.AddWithValue("@MaxServUpto", txtMaxInServicePeriod.Equals("") ? DBNull.Value : (object)txtMaxInServicePeriod);
            cmd.Parameters.AddWithValue("@Gender", ddlGender.Equals("") ? DBNull.Value : (object)ddlGender);
            cmd.Parameters.AddWithValue("@ActiveFlag", chkIsActive.Equals("") ? 0 : (object) chkIsActive);
            cmd.Parameters.AddWithValue("@isRound", chkIsRound.Equals("") ? 0 : (object)chkIsRound);
            cmd.Parameters.AddWithValue("@LeaveTour", chkIsLeave.Equals("") ? 0 : (object)chkIsLeave);
            cmd.Parameters.AddWithValue("@InsertedBy", UserID.Equals("") ? 0 : (object)UserID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return ThrowMessege(dt);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        List<LeaveMaster_MD> ThrowMessege(DataTable dt)
        {
            List<LeaveMaster_MD> lst = new List<LeaveMaster_MD>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new LeaveMaster_MD();
                obj.ErrorCode = s.Field<int?>("ErrorCode");
                obj.Messege = s.Field<string>("Messege");
                return obj;
            }).ToList();
            return lst;
        }

        public List<LeaveMaster_MD> Delete_LeaveMaster(string hdnLeaveTypeID, int? UserId)
        {
            SqlCommand cmd = new SqlCommand("Delete_leavetype_mast", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LvId", hdnLeaveTypeID.Equals("") ? 0 : (object)hdnLeaveTypeID);
            cmd.Parameters.AddWithValue("@CanBy", UserId.Equals("") ? 0 : (object)UserId);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return ThrowMessege(dt);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DataTable Get_GridDATA(string LVID)
        {
            SqlCommand cmd = new SqlCommand("Load_MST_LeaveType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LvId", LVID);
            SqlDataAdapter sda=new SqlDataAdapter(cmd);
            DataTable ds=new DataTable();
            try
            {
                cn.Open();
                sda.Fill(ds);
                return ds;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                cn.Close();
            }
        }


        

        public List<LeaveMaster_MD> Get_FieldName(int FormID)
        {
            SqlCommand cmd = new SqlCommand("Check_Mand", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ConfID", FormID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Get_FormDATA(dt);
            }
            catch (Exception)
            {

                throw;
            }
        }

        List<LeaveMaster_MD> Get_FormDATA(DataTable dt)
        {
            List<LeaveMaster_MD> lst = new List<LeaveMaster_MD>();
            lst = dt.AsEnumerable().Select(s =>
            {
                var obj = new LeaveMaster_MD();
                obj.FieldId = s.Field<int>("FieldID");
                obj.FieldName = s.Field<string>("FieldName");
                obj.IsMand = s.Field<int>("IsMand");
                return obj;
            }).ToList();
            return lst;
        }


    }
}