﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using wbEcsc.Models.Application;
using System.Configuration;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL
{
    public class LoanTypeBll:BllBase
    {
        SqlCommand cmd;
        SessionData sds;
        public LoanTypeBll()
        {
            sds = SessionContext.SessionData;           
        }
        public DataTable Save(LoanType_MD ima)
        {
            try
            {
                cmd = new SqlCommand("check_loantype_mast", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@LoanTypeID", ima.LoanTypeID ?? 0);
                cmd.Parameters.AddWithValue("@LoanDescTypeID", ima.TypeID ?? 0);
                cmd.Parameters.AddWithValue("@LoanDesc", ima.Description ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@LoanAbbv", ima.Abbreviation ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Rank", ima.Rank ?? 0);
                cmd.Parameters.AddWithValue("@LoanActive", ima.IsActive);
                cmd.Parameters.AddWithValue("@IsRound", ima.IsRound);
                cmd.Parameters.AddWithValue("@InsertedBy", sds.UserID);
                cn.Open();
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                ads.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public DataTable Delete(string LoanTypeID)
        {
            try
            {
                cmd = new SqlCommand("Delete_LoanTypeMaster", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@LoanTypeID", LoanTypeID);
                cmd.Parameters.AddWithValue("@CanBy", sds.UserID);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Field> GetField()
        {
            try
            {
                cmd = new SqlCommand("Check_Mand", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfID", 15);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return Converter_Field(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }    
        public List<LoanType_MD> GetData()
        {
            try
            {
                cmd = new SqlCommand("Load_MST_LoanType", cn);
                cmd.CommandType = CommandType.StoredProcedure;               
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                cn.Open();
                ads.Fill(dt);
                return Converter_Data(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
        public List<Field> Converter_Field(DataTable dt)
        {
            try
            {
                List<Field> lst = new List<Field>();
                lst = dt.AsEnumerable().Select(t =>
                {
                    var obj = new Field()
                    {
                        FieldID = t.Field<int>("FieldID"),
                        FieldName = t.Field<string>("FieldName"),
                        IsMand = t.Field<int>("IsMand")
                    };
                    return obj;
                }
                ).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }        
        public List<LoanType_MD> Converter_Data(DataTable dt)
        {
            try
            {
                List<LoanType_MD> lst = new List<LoanType_MD>();
                lst = dt.AsEnumerable().Select(t => {
                    var obj = new LoanType_MD()
                    {
                        LoanTypeID = t.Field<int?>("LoanTypeID"),
                        TypeID = t.Field<int?>("LoanDescTypeID"),
                        TypeName = t.Field<string>("LoanDescType"),
                        Description = t.Field<string>("LoanDesc"),
                        Abbreviation = t.Field<string>("LoanAbbv"),
                        Rank = t.Field<int?>("Rank"),
                        IsActive = t.Field<string>("LoanActive"),
                        IsActiveText = t.Field<string>("IsActive"),
                        IsRound = t.Field<int?>("IsRound"),
                        IsRoundText = t.Field<string>("IsRoundText")

                        //             SELECT l.LoanTypeID,l.LoanDescTypeID,t.LoanDesc,l.LoanDesc,l.LoanAbbv,l.Rank,l.LoanActive,
                        //	case l.LoanActive when 'Y' then 'Active' else 'InActive' end as IsActive,
                        //    l.IsRound,case l.IsRound when 1 then 'Integer' else 'Decimal' end as IsRoundText

                    };
                    return obj;
                }).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}