﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.Utils;
using wbEcsc.Utils.EncryptionDecrytion;

namespace wbEcsc.App_Codes.BLL
{
    public class Menu_BLL
    {
        
            string constring = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
            SqlConnection cn;
            public Menu_BLL()
            {
                cn = new SqlConnection(constring);
            }

            public List<Menu> getMenuDrtails()
            {
            throw new NotImplementedException();
                SqlCommand cmd = new SqlCommand("Get_Menu", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                List<Menu> lstMenu = new List<Menu>();
                try
                {
                    cn.Open();
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)
                
                    {
                        Menu menu = new Menu();
                        menu.MenuID = row.Field<int  >("MenuID");
                        menu.MenuName = row.Field < string >("MenuName");
                        menu.ParrentMenuID = row.Field < int ?  >("ParrentMenuID");
                        menu.ActionMethodID = row.Field < int ?  >("ActionMethodID");
                        menu.Rank =row.Field < int >("Rank");
                        menu.IsActive = (row.Field < byte >("IsActive")==1 ? true:false);
                        menu.Url = getQuery(row.Field<string>("Directory"), row.Field<string>("ControllerName"), row.Field<string>("ActionName"), row.Field<string>("QueryStringValue"));//(string.IsNullOrWhiteSpace(row.Field<string>("Directory")) ?"":string.Format("/{0}", row.Field<string>("Directory")) ) + string.Format("/{0}/{1}/{2}/",  row.Field < string >("ControllerName"), row.Field < string >("ActionName"), row.Field<string>("QueryStringValue"));
                        lstMenu.Add(menu);
                    }
                    var mainList = GetMenuTree(lstMenu, null);

                    return mainList;
                    //JavaScriptSerializer js = new JavaScriptSerializer();
                    //return js.Serialize(mainList);
                }
                catch
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }
        string getQuery(string directory,string controller,string action,string parameter)
        {
            var url = string.Format("/{0}/{1}/{2}", directory??"", controller, action);

            string optionalParams = "";
            if(!string.IsNullOrEmpty( parameter))
            {
                string[] splitAmp = parameter.Split('&');
                foreach (var item in splitAmp)
                {
                    string[] singleItem = item.Split('=');
                    if(singleItem.Length>0)
                    {
                        optionalParams += string.Format("/{0}", singleItem[0]);
                    }
                }
            }
            return string.Format("{0}{1}",url,optionalParams);
        }
        public List<Menu> GetMenus(long userID)
        {

            SqlCommand cmd = new SqlCommand("Get_Menu", cn);
            cmd.Parameters.AddWithValue("@UserID", userID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            List<Menu> lstMenu = new List<Menu>();
            try
            {
                cn.Open();
                da.Fill(dt);

                foreach (DataRow row in dt.Rows)

                {
                    Menu menu = new Menu();
                    menu.MenuID = row.Field<int>("MenuID");
                    menu.MenuName = row.Field<string>("MenuName");
                    menu.ParrentMenuID = row.Field<int?>("ParrentMenuID");
                    menu.ActionMethodID = row.Field<int?>("ActionMethodID");
                    menu.Rank = row.Field<int>("Rank");
                    menu.IsActive = (row.Field<byte>("IsActive") == 1 ? true : false);
                    menu.Url = getQuery(row.Field<string>("Directory"), row.Field<string>("ControllerName"), row.Field<string>("ActionName"),   row.Field<string>("QueryStringValue"));
                    //menu.Url = getQuery(row.Field<string>("Directory"), row.Field<string>("ControllerName"), row.Field<string>("ActionName"), row.Field<string>("QueryStringValue"));
                    lstMenu.Add(menu);
                }
                var mainList = GetMenuTree(lstMenu, null);

                return mainList;
            }
            catch
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        private List<Menu> GetMenuTree(List<Menu> list, int? parent)
            {
                return list.Where(x => x.ParrentMenuID == parent).Select(x => new Menu
                {
                    MenuID = x.MenuID,
                    MenuName = x.MenuName,
                    ParrentMenuID = x.ParrentMenuID,
                    ActionMethodID = x.ActionMethodID,
                    Rank = x.Rank,
                    IsActive = x.IsActive,
                    Url = x.Url,
                    Menus = GetMenuTree(list, x.MenuID)
                }).ToList();
            }

        
        }
}