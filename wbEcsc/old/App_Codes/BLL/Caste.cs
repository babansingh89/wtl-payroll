﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL
{
    public class Caste:BllBase
    {
        public List<Caste_MD> Get_Caste()
        {
            SqlCommand cmd = new SqlCommand("Load_Caste", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }



        #region Converter
        List<Caste_MD> Convert(DataTable dt)
        {
            List<Caste_MD> lst = new List<Caste_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Caste_MD();
                obj.CasteID = t.Field<int?>("CasteID");
                obj.CasteCode = t.Field<string>("CasteCode");
                obj.Caste = t.Field<string>("Caste");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(Caste v)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}