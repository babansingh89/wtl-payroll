﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL
{
    public class Group:BllBase
    {
        public List<Group_MD> Get_Group()
        {
            SqlCommand cmd = new SqlCommand("Load_Group", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        #region Converter
        List<Group_MD> Convert(DataTable dt)
        {
            List<Group_MD> lst = new List<Group_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Group_MD();
                obj.GroupID = t.Field<int?>("GroupID");
                obj.GroupName = t.Field<string>("GroupName");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(Group v)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}