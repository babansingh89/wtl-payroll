﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using wbEcsc.Models.Application;
using System.Configuration;
using wbEcsc.ViewModels;

namespace wbEcsc.App_Codes.BLL
{
    public class ItaxMaxAmountSave
    {
        readonly string constr;
        readonly SqlConnection con;
        SqlCommand cmd;
        SessionData sds = new SessionData();
        public ItaxMaxAmountSave()
        {
            sds = SessionContext.SessionData;
            constr = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
            con = new SqlConnection(constr);          
        }
        public DataTable Save(ItaxMaxAmount ima)
        {
            try
            {
                cmd = new SqlCommand("check_itaxmax_mast", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaxSaveID", ima.ID ?? 0);
                cmd.Parameters.AddWithValue("@FinYear", ima.FinYear ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@Gender", ima.GenderID ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@MaxAmount", ima.Amount ?? (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@InsertedBy", sds.UserID);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);                
                DataTable dt = new DataTable();
                con.Open();
                ads.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();               
            }
        }

        public DataTable Delete(ItaxMaxAmount ima)
        {
            try
            {
                cmd = new SqlCommand("Delete_Itaxmax", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MaxSaveID", ima.ID);
                cmd.Parameters.AddWithValue("@CanBy", sds.UserID);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                con.Open();
                ads.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public List<Field> GetField()
        {
            try
            {
                cmd = new SqlCommand("Check_Mand", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfID", 11);               
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                con.Open();
                ads.Fill(dt);
                return Converter_Field(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public List<ItaxMaxAmount> GetGender()
        {
            try
            {
                cmd = new SqlCommand("Load_Gender", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                con.Open();
                ads.Fill(dt);
                return Converter_Gender(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public List<ItaxMaxAmount> GetData(ItaxMaxAmount ima)
        {
            try
            {
                cmd = new SqlCommand("Load_ItaxMax", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FinYear", ima.FinYear);
                SqlDataAdapter ads = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                con.Open();
                ads.Fill(dt);
                return Converter_Data(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public List<Field> Converter_Field(DataTable dt)
        {
            try
            {
                List<Field> lst = new List<Field>();
                lst=dt.AsEnumerable().Select(t=>
                {
                    var obj = new Field()
                    { 
                        FieldID = t.Field<int>("FieldID"),
                        FieldName = t.Field<string>("FieldName"),
                        IsMand = t.Field<int>("IsMand")
                    };
                   return obj;                    
                }
                ).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ItaxMaxAmount> Converter_Gender(DataTable dt)
        {
            try
            {
                List<ItaxMaxAmount> lst = new List<ItaxMaxAmount>();
                lst = dt.AsEnumerable().Select(t=> {
                    var obj = new ItaxMaxAmount()
                    {
                        ID=null,
                        FinYear=null,
                        GenderID=t.Field<string>("GENDER"),
                        Gender=t.Field<string>("GENDER_DESC"),
                        Amount=null
                    };
                    return obj;
                }).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ItaxMaxAmount> Converter_Data(DataTable dt)
        {
            try
            {
                List<ItaxMaxAmount> lst = new List<ItaxMaxAmount>();
                lst = dt.AsEnumerable().Select(t => {
                    var obj = new ItaxMaxAmount()
                    {                       
                        ID = t.Field<int?>("MaxSaveID"),
                        FinYear = t.Field<string>("FinYear"),
                        GenderID = t.Field<string>("Gender"),
                        Gender = t.Field<string>("Gender_desc"),
                        Amount = t.Field<decimal>("MaxAmount"),
                    };
                    return obj;
                }).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}