﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL
{
    public class Location : BllBase
    {
        public List<Location_MD> Get_Center(int? SectorID)
        {

            SqlCommand cmd = new SqlCommand("Get_CenterbySector", cn);
            cmd.Parameters.AddWithValue("@SecID", SectorID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }



        #region Converter
        List<Location_MD> Convert(DataTable dt)
        {
            List<Location_MD> lst = new List<Location_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Location_MD();
                obj.CenterID = t.Field<int?>("CenterID");
                obj.CenterCode = t.Field<string>("CenterCode");
                obj.CenterName = t.Field<string>("CenterName");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(Location v)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}