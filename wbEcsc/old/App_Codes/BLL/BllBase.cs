﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace wbEcsc.App_Codes.BLL
{
    public abstract class BllBase
    {
       string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
       protected SqlConnection cn;
        public BllBase()
        {
            cn = new SqlConnection(conString);
        }
        public BllBase(string constring)
        {
            cn = new SqlConnection(constring);
        }
    }
}