﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using System.IO;

namespace wbEcsc.App_Codes.BLL
{
    public class EmployeeMaster:BllBase
    {
        public List<Sector> Get_Sector(long UserID)
        {
            SqlCommand cmd = new SqlCommand("Get_Sector", cn);
           
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Converts(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Branch_MD> Get_IFSCMICR(int BranchID)
        {
            SqlCommand cmd = new SqlCommand("Get_IFSCandMICR", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BranchID", BranchID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_IFSCMICR(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<DA_MD> Get_DA(string EmpType)
        {
            SqlCommand cmd = new SqlCommand("Load_DA", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpType", EmpType);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_DA(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<HRA_MD> Get_HRA(string EmpType)
        {
            SqlCommand cmd = new SqlCommand("Load_HRA", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpType", EmpType);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_HRA(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<PayScaleType_MD> Get_PayScaleType(string EmpType)
        {
            SqlCommand cmd = new SqlCommand("get_PayScaleByEmpType", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpType", EmpType);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_PayScaleType(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<PayScale_MD> Get_PayScale(string PayScaleType)
        {
            SqlCommand cmd = new SqlCommand("Get_PayScalebyPayScaleType", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PayScaleType", PayScaleType);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_PayScale(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Get_Email_Eligible_Employee(int SalMonthID, string SectorID, long UserID)
        {
            SqlCommand cmd = new SqlCommand("Load_Email_Eligible_Employee", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SalMonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                    return dt;
               
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<EmployeeMasterDetails> Get_EmpNoAuto_Complete(string EmpNo)
        {
            SqlCommand cmd = new SqlCommand("Get_EmpNameDetail", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpNo", EmpNo);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_EmpDetails(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<EmployeeMasterDetails> Get_EmpNameAuto_Complete(string EmpName)
        {
            SqlCommand cmd = new SqlCommand("Get_EmpNameDetailbyEmpName", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpName", EmpName);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_EmpDetails(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public DataSet Get_Employee(string EmployeeNo, string SectorID, long UserID)
        {
            SqlCommand cmd = new SqlCommand("Get_EmployeeDetails", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpNo", EmployeeNo);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
                da.Fill(ds);
                return ds;
                //return Convert_EmpDetails(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public string Save(Employee_SaveData Master, string Detail, long UserID, string FinYear)
        {
            string EncodeValue = ""; string retunResult = ""; string EmpNo = "";

            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();

            SqlDataReader dr;
            try
            {
                try
                {
                    cmd = new SqlCommand("Insert_EmpMaster", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@SEmployeeID", Master.EmployeeID == null ? DBNull.Value : (object)Master.EmployeeID);
                    cmd.Parameters.AddWithValue("@SEmpNo", Master.EmpNo == null ? DBNull.Value : (object)Master.EmpNo);
                    cmd.Parameters.AddWithValue("@SectorID", Master.SectorID == null ? DBNull.Value : (object)Master.SectorID);
                    cmd.Parameters.AddWithValue("@CenterID", Master.LocationID == null ? DBNull.Value : (object)Master.LocationID);
                    cmd.Parameters.AddWithValue("@EmpType", Master.EmpType == null ? DBNull.Value : (object)Master.EmpType);
                    cmd.Parameters.AddWithValue("@ClassificationID", Master.ClassificationID == null ? DBNull.Value : (object)Master.ClassificationID);
                    cmd.Parameters.AddWithValue("@EmpName", Master.EmpName == null ? DBNull.Value : (object)Master.EmpName);
                    cmd.Parameters.AddWithValue("@FatherName", Master.FatherName == null ? DBNull.Value : (object)Master.FatherName);
                    cmd.Parameters.AddWithValue("@Religion", Master.Religion == null ? DBNull.Value : (object)Master.Religion);
                    cmd.Parameters.AddWithValue("@Qualification", Master.Qualification == null ? DBNull.Value : (object)Master.Qualification);
                    cmd.Parameters.AddWithValue("@MaritalStatus", Master.MaritalStatus == null ? DBNull.Value : (object)Master.MaritalStatus);
                    cmd.Parameters.AddWithValue("@Caste", Master.Caste == null ? DBNull.Value : (object)Master.Caste);
                    cmd.Parameters.AddWithValue("@DOB", Master.DOB == null ? DBNull.Value : (object)Master.DOB);
                    cmd.Parameters.AddWithValue("@Gender", Master.Gender == null ? DBNull.Value : (object)Master.Gender);
                    cmd.Parameters.AddWithValue("@AadharNo", Master.aadhaarNo == null ? DBNull.Value : (object)Master.aadhaarNo);
                    //Address Details
                    cmd.Parameters.AddWithValue("@EmpAddress", Master.EmpAddress == null ? DBNull.Value : (object)Master.EmpAddress);
                    cmd.Parameters.AddWithValue("@StateID", Master.StateID == null ? DBNull.Value : (object)Master.StateID);
                    cmd.Parameters.AddWithValue("@DistrictID", Master.DistrictID == null ? DBNull.Value : (object)Master.DistrictID);
                    cmd.Parameters.AddWithValue("@City", Master.City == null ? DBNull.Value : (object)Master.City);
                    cmd.Parameters.AddWithValue("@Pin", Master.Pin == null ? DBNull.Value : (object)Master.Pin);
                    cmd.Parameters.AddWithValue("@Phone", Master.Phone == null ? DBNull.Value : (object)Master.Phone);
                    cmd.Parameters.AddWithValue("@Mobile", Master.Mobile == null ? DBNull.Value : (object)Master.Mobile);
                    cmd.Parameters.AddWithValue("@Email", Master.Email == null ? DBNull.Value : (object)Master.Email);

                    cmd.Parameters.AddWithValue("@PermAddress", Master.PermAddress == null ? DBNull.Value : (object)Master.PermAddress);
                    cmd.Parameters.AddWithValue("@PermState", Master.PermState == null ? DBNull.Value : (object)Master.PermState);
                    cmd.Parameters.AddWithValue("@PermDistrict", Master.PermDistrict == null ? DBNull.Value : (object)Master.PermDistrict);
                    cmd.Parameters.AddWithValue("@PermCity", Master.PermCity == null ? DBNull.Value : (object)Master.PermCity);
                    cmd.Parameters.AddWithValue("@PermPin", Master.PermPin == null ? DBNull.Value : (object)Master.PermPin);
                    cmd.Parameters.AddWithValue("@PermPhone", Master.PermPhone == null ? DBNull.Value : (object)Master.PermPhone);
                    //Official Details
                    cmd.Parameters.AddWithValue("@DOJ", Master.DOJ == null ? DBNull.Value : (object)Master.DOJ);
                    cmd.Parameters.AddWithValue("@DOJPresentDept", Master.DOJPresentDept == null ? DBNull.Value : (object)Master.DOJPresentDept);
                    cmd.Parameters.AddWithValue("@DOR", Master.DOR == null ? DBNull.Value : (object)Master.DOR);
                    cmd.Parameters.AddWithValue("@PhysicalHandicapped", Master.PhysicalHandicapped == null ? DBNull.Value : (object)Master.PhysicalHandicapped);
                    cmd.Parameters.AddWithValue("@Designation", Master.Designation == null ? DBNull.Value : (object)Master.Designation);
                    cmd.Parameters.AddWithValue("@Group", Master.Group == null ? DBNull.Value : (object)Master.Group);
                    cmd.Parameters.AddWithValue("@Cooperative", Master.Cooperative == null ? DBNull.Value : (object)Master.Cooperative);
                    cmd.Parameters.AddWithValue("@CoopMembership", Master.CoopMembership == null ? DBNull.Value : (object)Master.CoopMembership);
                    cmd.Parameters.AddWithValue("@Category", Master.Category == null ? DBNull.Value : (object)Master.Category);
                    cmd.Parameters.AddWithValue("@DA", Master.DA == null ? DBNull.Value : (object)Master.DA);
                    cmd.Parameters.AddWithValue("@Status", Master.Status == null ? DBNull.Value : (object)Master.Status);
                    cmd.Parameters.AddWithValue("@PFCode", Master.PFCode == null ? DBNull.Value : (object)Master.PFCode);
                    cmd.Parameters.AddWithValue("@PayScaleType", Master.PayScaleType == null ? DBNull.Value : (object)Master.PayScaleType);
                    cmd.Parameters.AddWithValue("@PayScaleID", Master.PayScaleID == null ? DBNull.Value : (object)Master.PayScaleID);
                    cmd.Parameters.AddWithValue("@DNI", Master.DNI == null ? DBNull.Value : (object)Master.DNI);
                    cmd.Parameters.AddWithValue("@PAN", Master.PAN == null ? DBNull.Value : (object)Master.PAN);
                    cmd.Parameters.AddWithValue("@HRACat", Master.HRACat == null ? DBNull.Value : (object)Master.HRACat);
                    cmd.Parameters.AddWithValue("@HRAFixedAmount", Master.HRAFixedAmount == null ? DBNull.Value : (object)Master.HRAFixedAmount);
                    cmd.Parameters.AddWithValue("@HRAID", Master.HRAID == null ? DBNull.Value : (object)Master.HRAID);
                    cmd.Parameters.AddWithValue("@QuaterAllot", Master.QuaterAllot == null ? DBNull.Value : (object)Master.QuaterAllot);
                    cmd.Parameters.AddWithValue("@SpouseQuater", Master.SpouseQuater == null ? DBNull.Value : (object)Master.SpouseQuater);
                    cmd.Parameters.AddWithValue("@SpouseAmount", Master.SpouseAmount == null ? DBNull.Value : (object)Master.SpouseAmount);
                    cmd.Parameters.AddWithValue("@LicenceFees", Master.LicenceFees == null ? DBNull.Value : (object)Master.LicenceFees);
                    cmd.Parameters.AddWithValue("@HealthScheme", Master.HealthScheme == null ? DBNull.Value : (object)Master.HealthScheme);
                    cmd.Parameters.AddWithValue("@HealthSchemeDetail", Master.HealthSchemeDetail == null ? DBNull.Value : (object)Master.HealthSchemeDetail);
                    cmd.Parameters.AddWithValue("@EffectiveFrom", Master.EffectiveFrom == null ? DBNull.Value : (object)Master.EffectiveFrom);
                    cmd.Parameters.AddWithValue("@EffectiveTo", Master.EffectiveTo == null ? DBNull.Value : (object)Master.EffectiveTo);
                    cmd.Parameters.AddWithValue("@LWP", Master.LWP == null ? DBNull.Value : (object)Master.LWP);
                    cmd.Parameters.AddWithValue("@IR", Master.IRID == null ? DBNull.Value : (object)Master.IRID);
                    cmd.Parameters.AddWithValue("@UAN", Master.UAN == null ? DBNull.Value : (object)Master.UAN);
                    cmd.Parameters.AddWithValue("@PFPercent", Master.PFPercent == null ? DBNull.Value : (object)Master.PFPercent);
                    cmd.Parameters.AddWithValue("@PFValue", Master.PFValue == null ? DBNull.Value : (object)Master.PFValue);
                    //Bank Details
                    cmd.Parameters.AddWithValue("@BranchID", Master.BranchID == null ? DBNull.Value : (object)Master.BranchID);
                    cmd.Parameters.AddWithValue("@BankAccCode", Master.BankAccCode == null ? DBNull.Value : (object)Master.BankAccCode);
                    cmd.Parameters.AddWithValue("@FinYear_Form", FinYear);
                    cmd.Parameters.AddWithValue("@InsertedBy", UserID);
                    cmd.Parameters.AddWithValue("@Detailxml", Detail);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        retunResult = dr["Result"].ToString();
                        EncodeValue = dr["isEncode"].ToString();
                        EmpNo = dr["EmpNo"].ToString();
                    }
                    dr.Close();

                    if (EncodeValue == "NoEncod")
                    {
                        if (retunResult == "success")
                        {
                            if (EmpNo != "")
                            {
                                transaction.Commit();
                            }
                        }
                    }
                    else if (EncodeValue == "")
                    {
                        retunResult = "other";
                    }
                    else
                        retunResult = EncodeValue.ToString();
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    retunResult = "fail";
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException e)
            {
                throw new Exception("Database Error !!");
            }
            finally
            {
                dbConn.Close();
            }
            return retunResult + "#" + EmpNo;
        }

        public string Update(Employee_SaveData Master, string Detail, long UserID, string FinYear)
        {
            string EncodeValue = ""; string retunResult = ""; string EmpNo = "";

            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();

            SqlDataReader dr;
            try
            {
                try
                {
                    cmd = new SqlCommand("Update_EmpMaster", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@SEmployeeID", Master.EmployeeID == null ? DBNull.Value : (object)Master.EmployeeID);
                    cmd.Parameters.AddWithValue("@SEmpNo", Master.EmpNo == null ? DBNull.Value : (object)Master.EmpNo);
                    cmd.Parameters.AddWithValue("@SectorID", Master.SectorID == null ? DBNull.Value : (object)Master.SectorID);
                    cmd.Parameters.AddWithValue("@CenterID", Master.LocationID == null ? DBNull.Value : (object)Master.LocationID);
                    cmd.Parameters.AddWithValue("@EmpType", Master.EmpType == null ? DBNull.Value : (object)Master.EmpType);
                    cmd.Parameters.AddWithValue("@ClassificationID", Master.ClassificationID == null ? DBNull.Value : (object)Master.ClassificationID);
                    cmd.Parameters.AddWithValue("@EmpName", Master.EmpName == null ? DBNull.Value : (object)Master.EmpName);
                    cmd.Parameters.AddWithValue("@FatherName", Master.FatherName == null ? DBNull.Value : (object)Master.FatherName);
                    cmd.Parameters.AddWithValue("@Religion", Master.Religion == null ? DBNull.Value : (object)Master.Religion);
                    cmd.Parameters.AddWithValue("@Qualification", Master.Qualification == null ? DBNull.Value : (object)Master.Qualification);
                    cmd.Parameters.AddWithValue("@MaritalStatus", Master.MaritalStatus == null ? DBNull.Value : (object)Master.MaritalStatus);
                    cmd.Parameters.AddWithValue("@Caste", Master.Caste == null ? DBNull.Value : (object)Master.Caste);
                    cmd.Parameters.AddWithValue("@DOB", Master.DOB == null ? DBNull.Value : (object)Master.DOB);
                    cmd.Parameters.AddWithValue("@Gender", Master.Gender == null ? DBNull.Value : (object)Master.Gender);
                    cmd.Parameters.AddWithValue("@AadharNo", Master.aadhaarNo == null ? DBNull.Value : (object)Master.aadhaarNo);
                    //Address Details
                    cmd.Parameters.AddWithValue("@EmpAddress", Master.EmpAddress == null ? DBNull.Value : (object)Master.EmpAddress);
                    cmd.Parameters.AddWithValue("@StateID", Master.StateID == null ? DBNull.Value : (object)Master.StateID);
                    cmd.Parameters.AddWithValue("@DistrictID", Master.DistrictID == null ? DBNull.Value : (object)Master.DistrictID);
                    cmd.Parameters.AddWithValue("@City", Master.City == null ? DBNull.Value : (object)Master.City);
                    cmd.Parameters.AddWithValue("@Pin", Master.Pin == null ? DBNull.Value : (object)Master.Pin);
                    cmd.Parameters.AddWithValue("@Phone", Master.Phone == null ? DBNull.Value : (object)Master.Phone);
                    cmd.Parameters.AddWithValue("@Mobile", Master.Mobile == null ? DBNull.Value : (object)Master.Mobile);
                    cmd.Parameters.AddWithValue("@Email", Master.Email == null ? DBNull.Value : (object)Master.Email);

                    cmd.Parameters.AddWithValue("@PermAddress", Master.PermAddress == null ? DBNull.Value : (object)Master.PermAddress);
                    cmd.Parameters.AddWithValue("@PermState", Master.PermState == null ? DBNull.Value : (object)Master.PermState);
                    cmd.Parameters.AddWithValue("@PermDistrict", Master.PermDistrict == null ? DBNull.Value : (object)Master.PermDistrict);
                    cmd.Parameters.AddWithValue("@PermCity", Master.PermCity == null ? DBNull.Value : (object)Master.PermCity);
                    cmd.Parameters.AddWithValue("@PermPin", Master.PermPin == null ? DBNull.Value : (object)Master.PermPin);
                    cmd.Parameters.AddWithValue("@PermPhone", Master.PermPhone == null ? DBNull.Value : (object)Master.PermPhone);
                    //Official Details
                    cmd.Parameters.AddWithValue("@DOJ", Master.DOJ == null ? DBNull.Value : (object)Master.DOJ);
                    cmd.Parameters.AddWithValue("@DOJPresentDept", Master.DOJPresentDept == null ? DBNull.Value : (object)Master.DOJPresentDept);
                    cmd.Parameters.AddWithValue("@DOR", Master.DOR == null ? DBNull.Value : (object)Master.DOR);
                    cmd.Parameters.AddWithValue("@PhysicalHandicapped", Master.PhysicalHandicapped == null ? DBNull.Value : (object)Master.PhysicalHandicapped);
                    cmd.Parameters.AddWithValue("@Designation", Master.Designation == null ? DBNull.Value : (object)Master.Designation);
                    cmd.Parameters.AddWithValue("@Group", Master.Group == null ? DBNull.Value : (object)Master.Group);
                    cmd.Parameters.AddWithValue("@Cooperative", Master.Cooperative == null ? DBNull.Value : (object)Master.Cooperative);
                    cmd.Parameters.AddWithValue("@CoopMembership", Master.CoopMembership == null ? DBNull.Value : (object)Master.CoopMembership);
                    cmd.Parameters.AddWithValue("@Category", Master.Category == null ? DBNull.Value : (object)Master.Category);
                    cmd.Parameters.AddWithValue("@DA", Master.DA == null ? DBNull.Value : (object)Master.DA);
                    cmd.Parameters.AddWithValue("@Status", Master.Status == null ? DBNull.Value : (object)Master.Status);
                    cmd.Parameters.AddWithValue("@PFCode", Master.PFCode == null ? DBNull.Value : (object)Master.PFCode);
                    cmd.Parameters.AddWithValue("@PayScaleType", Master.PayScaleType == null ? DBNull.Value : (object)Master.PayScaleType);
                    cmd.Parameters.AddWithValue("@PayScaleID", Master.PayScaleID == null ? DBNull.Value : (object)Master.PayScaleID);
                    cmd.Parameters.AddWithValue("@DNI", Master.DNI == null ? DBNull.Value : (object)Master.DNI);
                    cmd.Parameters.AddWithValue("@PAN", Master.PAN == null ? DBNull.Value : (object)Master.PAN);
                    cmd.Parameters.AddWithValue("@HRACat", Master.HRACat == null ? DBNull.Value : (object)Master.HRACat);
                    cmd.Parameters.AddWithValue("@HRAFixedAmount", Master.HRAFixedAmount == null ? DBNull.Value : (object)Master.HRAFixedAmount);
                    cmd.Parameters.AddWithValue("@HRAID", Master.HRAID == null ? DBNull.Value : (object)Master.HRAID);
                    cmd.Parameters.AddWithValue("@QuaterAllot", Master.QuaterAllot == null ? DBNull.Value : (object)Master.QuaterAllot);
                    cmd.Parameters.AddWithValue("@SpouseQuater", Master.SpouseQuater == null ? DBNull.Value : (object)Master.SpouseQuater);
                    cmd.Parameters.AddWithValue("@SpouseAmount", Master.SpouseAmount == null ? DBNull.Value : (object)Master.SpouseAmount);
                    cmd.Parameters.AddWithValue("@LicenceFees", Master.LicenceFees == null ? DBNull.Value : (object)Master.LicenceFees);
                    cmd.Parameters.AddWithValue("@HealthScheme", Master.HealthScheme == null ? DBNull.Value : (object)Master.HealthScheme);
                    cmd.Parameters.AddWithValue("@HealthSchemeDetail", Master.HealthSchemeDetail == null ? DBNull.Value : (object)Master.HealthSchemeDetail);
                    cmd.Parameters.AddWithValue("@EffectiveFrom", Master.EffectiveFrom == null ? DBNull.Value : (object)Master.EffectiveFrom);
                    cmd.Parameters.AddWithValue("@EffectiveTo", Master.EffectiveTo == null ? DBNull.Value : (object)Master.EffectiveTo);
                    cmd.Parameters.AddWithValue("@LWP", Master.LWP == null ? DBNull.Value : (object)Master.LWP);
                    cmd.Parameters.AddWithValue("@IR", Master.IRID == null ? DBNull.Value : (object)Master.IRID);
                    cmd.Parameters.AddWithValue("@UAN", Master.UAN == null ? DBNull.Value : (object)Master.UAN);
                    cmd.Parameters.AddWithValue("@PFPercent", Master.PFPercent == null ? DBNull.Value : (object)Master.PFPercent);
                    cmd.Parameters.AddWithValue("@PFValue", Master.PFValue == null ? DBNull.Value : (object)Master.PFValue);
                    //Bank Details
                    cmd.Parameters.AddWithValue("@BranchID", Master.BranchID == null ? DBNull.Value : (object)Master.BranchID);
                    cmd.Parameters.AddWithValue("@BankAccCode", Master.BankAccCode == null ? DBNull.Value : (object)Master.BankAccCode);
                    cmd.Parameters.AddWithValue("@FinYear_Form", FinYear);
                    cmd.Parameters.AddWithValue("@InsertedBy", UserID);
                    cmd.Parameters.AddWithValue("@Detailxml", Detail);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        retunResult = dr["Result"].ToString();
                        EncodeValue = dr["isEncode"].ToString();
                        EmpNo = dr["EmpNo"].ToString();
                    }
                    dr.Close();

                    if (EncodeValue == "NoEncod")
                    {
                        if (retunResult == "updated")
                        {
                            if (EmpNo != "")
                            {
                                transaction.Commit();
                            }
                        }
                    }
                    else if(EncodeValue =="")
                    {
                        retunResult = "other";
                    }
                    else
                        retunResult = EncodeValue.ToString();
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    retunResult = "fail";
                    throw new Exception(sqlError.Message);
                }

            }
            catch (SqlException)
            {
                throw new Exception("Database Error !!");
            }
            finally
            {
                dbConn.Close();
            }
            return retunResult + "#" + EmpNo;
        }

        public string Upload_Photo_Signature(string EmpNo, Employee_SaveData Master)
        {
            string result = ""; string photo_Extension = ""; string sig_Extension = "";
            string p_fileName = ""; string s_fileName = "";
            string p_result = ""; string s_result = "";
            try
            {
                try
                {
                    string photo_filename = Master.Photo;
                    string sig_filename = Master.Signature;

                    string foldername = "EmpPhoto_Signature/" + EmpNo;
                    string path = HttpContext.Current.Server.MapPath("~/" + foldername);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    //PHOTO UPLOAD
                    if (photo_filename != null)
                    {
                        if (photo_filename.Contains("data:image/jpeg;base64,"))
                        {
                            photo_Extension = ".jpg";
                        }
                        if (photo_filename.Contains("data:image/png;base64,"))
                        {
                            photo_Extension = ".png";
                        }
                        string str = photo_filename.Replace("data:image/jpeg;base64,", " ");//jpg check
                        str = str.Replace("data:image/png;base64,", " ");//png check
                        byte[] data = Convert.FromBase64String(str);

                        MemoryStream ms = new MemoryStream(data, 0, data.Length);
                        ms.Write(data, 0, data.Length);
                        System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);

                        p_fileName = EmpNo + "_Photo" + photo_Extension;
                        string photo_fileName = Path.Combine(path, p_fileName);
                        System.IO.File.Delete(photo_fileName);
                        image.Save(photo_fileName);

                        p_result = foldername + "/" + p_fileName;
                    }
                    else
                    {
                        p_result = null;
                    }
                    //SIGNATURE UPLOAD
                   if (sig_filename != null)
                    {
                        if (sig_filename.Contains("data:image/jpeg;base64,"))
                        {
                            sig_Extension = ".jpg";
                        }
                        if (sig_filename.Contains("data:image/png;base64,"))
                        {
                            sig_Extension = ".png";
                        }
                        string str = sig_filename.Replace("data:image/jpeg;base64,", " ");//jpg check
                        str = str.Replace("data:image/png;base64,", " ");//png check
                        byte[] data = Convert.FromBase64String(str);

                        MemoryStream ms = new MemoryStream(data, 0, data.Length);
                        ms.Write(data, 0, data.Length);
                        System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);

                        s_fileName = EmpNo + "_Sig" + sig_Extension;
                        string sig_fileName = Path.Combine(path, s_fileName);
                        System.IO.File.Delete(sig_fileName);
                        image.Save(sig_fileName);

                        s_result = foldername + "/" + s_fileName;
                    }
                    else
                    {
                        s_result = null;
                    }

                    string msg = "";
                    if (photo_filename == null && sig_filename == null)
                        msg = "notfound";
                    else
                        msg = "success";

                    result = msg + "#" + p_result + "#" + s_result;
                    //else
                    //{
                    //    result = "notfound" + "#" + foldername + "/" + p_fileName + "#" + foldername + "/" + s_fileName;
                    //}
                }
                catch (SqlException sqlError)
                {
                    result = "fail";
                    throw new Exception(sqlError.Message);
                }
            }
            catch (Exception)
            {
                throw new Exception("Database Error !!");
            }
            return result;
        }

        public DataTable Check_PayScaleRange(string Amount, string PayScaleID)
        {
            SqlCommand cmd = new SqlCommand("Check_bandPay", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@bandPay", Amount);
            cmd.Parameters.AddWithValue("@payscaleID", PayScaleID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt;
                //return Convert_EmpDetails(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<Status_MD> Get_Status(string CategoryID)
        {
            SqlCommand cmd = new SqlCommand("Get_Status", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpCategory", CategoryID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_Status(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Add_PayDetails(string EmpNo, string EmpType, string EmpCategory, string PayScaleID, string Amount, string DAID,
                                         string hraCategory, string HRAID, string HRARent, string FixedHRAAmount, string SpouseAmount, string isHealth,
                                         string HealthFrom, string HealthTo, long UserID, string IRID, string PFID, string PFAmount, string PhysicalHandicapped)
        {
            SqlCommand cmd = new SqlCommand("CalculatePayXdetailsEmpWise", cn);

            cmd.CommandType = CommandType.StoredProcedure;
           
            cmd.Parameters.AddWithValue("@EmpNo", EmpNo);
            cmd.Parameters.AddWithValue("@empType", EmpType);
            cmd.Parameters.AddWithValue("@empCat", EmpCategory);
            cmd.Parameters.AddWithValue("@payscaleID", PayScaleID);
            cmd.Parameters.AddWithValue("@bandPay", Amount);
            cmd.Parameters.AddWithValue("@daid", DAID);
            cmd.Parameters.AddWithValue("@hraCategory", hraCategory);
            cmd.Parameters.AddWithValue("@hraID", HRAID);
            cmd.Parameters.AddWithValue("@hraRent", HRARent);
            cmd.Parameters.AddWithValue("@fixedhraAmount", FixedHRAAmount);
            cmd.Parameters.AddWithValue("@spouseAmount", SpouseAmount);
            cmd.Parameters.AddWithValue("@medicalApplicable", isHealth);
            cmd.Parameters.AddWithValue("@medicalApplicableFrom", HealthFrom);
            cmd.Parameters.AddWithValue("@medicalApplicableTo", HealthTo);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@irid", IRID);
            cmd.Parameters.AddWithValue("@pfid", PFID);
            cmd.Parameters.AddWithValue("@pfamount", PFAmount);
            cmd.Parameters.AddWithValue("@PhysicalHandicapped", PhysicalHandicapped);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt;
                //return Convert_EmpDetails(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        
        public List<Employee_PayDetails> Show_PayDetails(string EmpNo)
        {
            SqlCommand cmd = new SqlCommand("Get_EmployeePayDetails", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpNo", EmpNo);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_PayDetails(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public List<PF_Percentage_Value> Get_PFPercentage(string EmpType)
        {
            SqlCommand cmd = new SqlCommand("Get_PFPercentage", cn);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmpType", EmpType);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_PFPercentage(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Get_FieldName(int FormID)
        {
            SqlCommand cmd = new SqlCommand("Check_Mand", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ConfID", FormID);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DataTable Check_EmpMasterValidation(Employee_SaveData Master)
        {
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();

            try
            {
                cmd = new SqlCommand("check_EmpMaster", dbConn, transaction);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@SEmpNo", Master.EmpNo == null ? DBNull.Value : (object)Master.EmpNo);
                cmd.Parameters.AddWithValue("@CenterID", Master.LocationID == null ? 0 : (object)Master.LocationID);
                cmd.Parameters.AddWithValue("@EmpType", Master.EmpType == null ? 0 : (object)Master.EmpType);
                cmd.Parameters.AddWithValue("@ClassificationID", Master.ClassificationID == null ? 0 : (object)Master.ClassificationID);
                cmd.Parameters.AddWithValue("@EmpName", Master.EmpName == null ? DBNull.Value : (object)Master.EmpName);
                cmd.Parameters.AddWithValue("@Religion", Master.Religion == null ? 0 : (object)Master.Religion);
                cmd.Parameters.AddWithValue("@Qualification", Master.Qualification == null ? 0 : (object)Master.Qualification);
                cmd.Parameters.AddWithValue("@MaritalStatus", Master.MaritalStatus == null ? 0 : (object)Master.MaritalStatus);
                cmd.Parameters.AddWithValue("@Caste", Master.Caste == null ? 0 : (object)Master.Caste);
                cmd.Parameters.AddWithValue("@DOB", Master.DOB == null ? DBNull.Value : (object)Master.DOB);
                cmd.Parameters.AddWithValue("@Gender", Master.Gender == null ? 0 : (object)Master.Gender);
                cmd.Parameters.AddWithValue("@AadharNo", Master.aadhaarNo == null ? DBNull.Value : (object)Master.aadhaarNo);
                cmd.Parameters.AddWithValue("@FatherName", Master.FatherName == null ? DBNull.Value : (object)Master.FatherName);
                //Address Details
                cmd.Parameters.AddWithValue("@EmpAddress", Master.EmpAddress == null ? DBNull.Value : (object)Master.EmpAddress);
                cmd.Parameters.AddWithValue("@StateID", Master.StateID == null ? 0 : (object)Master.StateID);
                cmd.Parameters.AddWithValue("@DistrictID", Master.DistrictID == null ? 0 : (object)Master.DistrictID);
                cmd.Parameters.AddWithValue("@City", Master.City == null ? DBNull.Value : (object)Master.City);
                cmd.Parameters.AddWithValue("@Pin", Master.Pin == null ? DBNull.Value : (object)Master.Pin);
                cmd.Parameters.AddWithValue("@Phone", Master.Phone == null ? DBNull.Value : (object)Master.Phone);
                cmd.Parameters.AddWithValue("@Mobile", Master.Mobile == null ? DBNull.Value : (object)Master.Mobile);
                cmd.Parameters.AddWithValue("@Email", Master.Email == null ? DBNull.Value : (object)Master.Email);

                cmd.Parameters.AddWithValue("@PermAddress", Master.PermAddress == null ? DBNull.Value : (object)Master.PermAddress);
                cmd.Parameters.AddWithValue("@PermState", Master.PermState == null ? 0 : (object)Master.PermState);
                cmd.Parameters.AddWithValue("@PermDistrict", Master.PermDistrict == null ? 0 : (object)Master.PermDistrict);
                cmd.Parameters.AddWithValue("@PermCity", Master.PermCity == null ? DBNull.Value : (object)Master.PermCity);
                cmd.Parameters.AddWithValue("@PermPin", Master.PermPin == null ? DBNull.Value : (object)Master.PermPin);
                cmd.Parameters.AddWithValue("@PermPhone", Master.PermPhone == null ? DBNull.Value : (object)Master.PermPhone);
                //Official Details
                cmd.Parameters.AddWithValue("@DOJ", Master.DOJ == null ? DBNull.Value : (object)Master.DOJ);
                cmd.Parameters.AddWithValue("@DOJPresentDept", Master.DOJPresentDept == null ? DBNull.Value : (object)Master.DOJPresentDept);
                cmd.Parameters.AddWithValue("@DOR", Master.DOR == null ? DBNull.Value : (object)Master.DOR);
                cmd.Parameters.AddWithValue("@PhysicalHandicapped", Master.PhysicalHandicapped == null ? 0 : (object)Master.PhysicalHandicapped);
                cmd.Parameters.AddWithValue("@Designation", Master.Designation == null ? 0 : (object)Master.Designation);
                cmd.Parameters.AddWithValue("@Group", Master.Group == null ? 0 : (object)Master.Group);
                cmd.Parameters.AddWithValue("@Cooperative", Master.Cooperative == null ? 0 : (object)Master.Cooperative);
                cmd.Parameters.AddWithValue("@CoopMembership", Master.CoopMembership == null ? DBNull.Value : (object)Master.CoopMembership);
                cmd.Parameters.AddWithValue("@Category", Master.Category == null ? 0 : (object)Master.Category);
                cmd.Parameters.AddWithValue("@DA", Master.DA == null ? DBNull.Value : (object)Master.DA);
                cmd.Parameters.AddWithValue("@Status", Master.Status == null ? 0 : (object)Master.Status);
                cmd.Parameters.AddWithValue("@PFCode", Master.PFCode == null ? DBNull.Value : (object)Master.PFCode);
                cmd.Parameters.AddWithValue("@PayScaleType", Master.PayScaleType == null ? 0 : (object)Master.PayScaleType);
                cmd.Parameters.AddWithValue("@PayScaleID", Master.PayScaleID == null ? 0 : (object)Master.PayScaleID);
                cmd.Parameters.AddWithValue("@DNI", Master.DNI == null ? DBNull.Value : (object)Master.DNI);
                cmd.Parameters.AddWithValue("@PAN", Master.PAN == null ? DBNull.Value : (object)Master.PAN);
                cmd.Parameters.AddWithValue("@HRACat", Master.HRACat == null ? 0 : (object)Master.HRACat);
                cmd.Parameters.AddWithValue("@HRAFixedAmount", Master.HRAFixedAmount == null ? DBNull.Value : (object)Master.HRAFixedAmount);
                cmd.Parameters.AddWithValue("@HRAID", Master.HRAID == null ? DBNull.Value : (object)Master.HRAID);
                cmd.Parameters.AddWithValue("@QuaterAllot", Master.QuaterAllot == null ? 0 : (object)Master.QuaterAllot);
                cmd.Parameters.AddWithValue("@SpouseQuater", Master.SpouseQuater == null ? 0 : (object)Master.SpouseQuater);
                cmd.Parameters.AddWithValue("@SpouseAmount", Master.SpouseAmount == null ? DBNull.Value : (object)Master.SpouseAmount);
                cmd.Parameters.AddWithValue("@LicenceFees", Master.LicenceFees == null ? DBNull.Value : (object)Master.LicenceFees);
                cmd.Parameters.AddWithValue("@HealthScheme", Master.HealthScheme == null ? DBNull.Value : (object)Master.HealthScheme);
                cmd.Parameters.AddWithValue("@HealthSchemeDetail", Master.HealthSchemeDetail == null ? DBNull.Value : (object)Master.HealthSchemeDetail);
                cmd.Parameters.AddWithValue("@EffectiveFrom", Master.EffectiveFrom == null ? DBNull.Value : (object)Master.EffectiveFrom);
                cmd.Parameters.AddWithValue("@EffectiveTo", Master.EffectiveTo == null ? DBNull.Value : (object)Master.EffectiveTo);
                cmd.Parameters.AddWithValue("@LWP", Master.LWP == null ? DBNull.Value : (object)Master.LWP);
                cmd.Parameters.AddWithValue("@IR", Master.IRID == null ? DBNull.Value : (object)Master.IRID);
                cmd.Parameters.AddWithValue("@UAN", Master.UAN == null ? DBNull.Value : (object)Master.UAN);
                cmd.Parameters.AddWithValue("@PFPercent", Master.PFPercent == null ? DBNull.Value : (object)Master.PFPercent);
                cmd.Parameters.AddWithValue("@PFValue", Master.PFValue == null ? DBNull.Value : (object)Master.PFValue);
                //Bank Details
                cmd.Parameters.AddWithValue("@BranchID", Master.BranchID == null ? DBNull.Value : (object)Master.BranchID);
                cmd.Parameters.AddWithValue("@BankAccCode", Master.BankAccCode == null ? DBNull.Value : (object)Master.BankAccCode);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
               
                da.Fill(dt);
                return dt;
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
        }

        public List<Emp_Type_MD> Get_EmpType()
        {
            SqlCommand cmd = new SqlCommand("Load_EmpType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return Convert_EmpType(dt);
            }
            catch (Exception)
            {

                throw;
            }
        }



























        #region Converter
        List<Sector> Converts(DataTable dt)
        {
            List<Sector> lst = new List<Sector>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Sector();
                obj.SectorID = t.Field<int?>("SectorID");
                obj.SectorName = t.Field<string>("SectorName");
                return obj;
            }).ToList();
            return lst;
        }

        List<Branch_MD> Convert_IFSCMICR(DataTable dt)
        {
            List<Branch_MD> lst = new List<Branch_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Branch_MD();
                obj.IFSC = t.Field<string>("IFSC");
                obj.MICR = t.Field<string>("MICR");
                return obj;
            }).ToList();
            return lst;
        }

        List<DA_MD> Convert_DA(DataTable dt)
        {
            List<DA_MD> lst = new List<DA_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new DA_MD();
                obj.DAID = t.Field<int?>("DAID");
                obj.DARate = t.Field<decimal>("DARate");
                return obj;
            }).ToList();
            return lst;
        }

        List<HRA_MD> Convert_HRA(DataTable dt)
        {
            List<HRA_MD> lst = new List<HRA_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new HRA_MD();
                obj.HRAID = t.Field<int?>("HRAID");
                obj.HRA = t.Field<string>("HRA");
                return obj;
            }).ToList();
            return lst;
        }

        List<PayScaleType_MD> Convert_PayScaleType(DataTable dt)
        {
            List<PayScaleType_MD> lst = new List<PayScaleType_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new PayScaleType_MD();
                obj.PayScaleID = t.Field<string>("PayScaleID");
                obj.PayScale = t.Field<string>("PayScale");
                return obj;
            }).ToList();
            return lst;
        }

        List<PayScale_MD> Convert_PayScale(DataTable dt)
        {
            List<PayScale_MD> lst = new List<PayScale_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new PayScale_MD();
                obj.PayScaleID = t.Field<int?>("PayScaleID");
                obj.PayScale = t.Field<string>("PayScale");
                return obj;
            }).ToList();
            return lst;
        }
        public static List<EmployeeMasterDetails> Convert_EmpDetails(DataTable dt)
        {
            List<EmployeeMasterDetails> lst = new List<EmployeeMasterDetails>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new EmployeeMasterDetails();
                obj.EmployeeID = t.Field<string>("EmployeeID");
                obj.EmpNo = t.Field<string>("EmpNo");
                obj.EmpName = t.Field<string>("EmpName");
                return obj;
            }).ToList();
            return lst;
        }

        public static List<Status_MD> Convert_Status(DataTable dt)
        {
            List<Status_MD> lst = new List<Status_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Status_MD();
                obj.StatusID = t.Field<int?>("StatusID");
                obj.Status = t.Field<string>("Status");
                obj.Abbv = t.Field<string>("Abbv");
                return obj;
            }).ToList();
            return lst;
        }

        public static List<Employee_PayDetails> Convert_PayDetails(DataTable dt)
        {
            List<Employee_PayDetails> lst = new List<Employee_PayDetails>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Employee_PayDetails();
                obj.EmployeeID = t.Field<int>("EmployeeID");
                obj.EDID = t.Field<int>("EDID");
                obj.EDName = t.Field<string>("EDName");
                obj.EDAmount = t.Field<decimal>("EDAmount");
                obj.EDType = t.Field<string>("EDType");
                return obj;
            }).ToList();
            return lst;
        }

        public static List<PF_Percentage_Value> Convert_PFPercentage(DataTable dt)
        {
            List<PF_Percentage_Value> lst = new List<PF_Percentage_Value>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new PF_Percentage_Value();
                obj.ID = t.Field<int>("ID");
                obj.PFType = t.Field<string>("PFType");
                obj.PFRate = t.Field<decimal?>("PFRate");
                obj.Min = t.Field<int?>("Min");
                obj.Max = t.Field<int?>("Max");
                obj.MinPerc = t.Field<int?>("MinPerc");
                obj.MaxPerc = t.Field<int?>("MaxPerc");
                return obj;
            }).ToList();
            return lst;
        }

        public static List<Emp_Type_MD> Convert_EmpType(DataTable dt)
        {
            List<Emp_Type_MD> lst = new List<Emp_Type_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Emp_Type_MD();
                obj.EmpType = t.Field<string>("EmpType");
                obj.EmpTypeDesc = t.Field<string>("EmpTypeDesc");
                return obj;
            }).ToList();
            return lst;
        }
        #endregion
    }
}