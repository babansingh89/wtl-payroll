﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL
{
    public class House:BllBase
    {
        public List<House_MD> Get_House()
        {

            SqlCommand cmd = new SqlCommand("Load_House", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Get_HideShow(string HRACat)
        {

            SqlCommand cmd = new SqlCommand("Load_HouseByHouseAbbv", cn);
            cmd.Parameters.AddWithValue("@HRACat", HRACat);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt;
                //return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        #region Converter
        List<House_MD> Convert(DataTable dt)
        {
            List<House_MD> lst = new List<House_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new House_MD();
                obj.HouseID = t.Field<int>("HouseID");
                obj.HouseName = t.Field<string>("HouseName");
                return obj;
            }).ToList();
            return lst;
        }

        public static implicit operator List<object>(House v)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}