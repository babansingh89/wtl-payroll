﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL
{
    public class ReportBll:BllBase
    {
        public List<Report> GetReports(int ReportTypeID, string ReportID , long userID)
        {
            //return new List<Report>() { new Report() {ReportID=1,ReportName="Monthly Report" } , new Report() { ReportID = 2, ReportName = "Yearly Report" } };

            SqlCommand cmd = new SqlCommand("Get_Report", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ParentID", ReportTypeID);
            cmd.Parameters.AddWithValue("@ReportID", ReportID.Equals("") ? DBNull.Value : (object)ReportID);
            cmd.Parameters.AddWithValue("@UserID", userID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                string a = cn.ConnectionTimeout.ToString();
                da.Fill(dt);
                return Convert(dt);
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }
        public bool HasReportAuthorization(int reportID, long userID)
        {
            SqlCommand cmd = new SqlCommand("Check_AuthoriseReportforUser", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReportID", reportID);
            cmd.Parameters.AddWithValue("@UserID", userID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return dt.Rows[0]["Result"].ToString().Equals("1") ? true : false;
                }
                else
                    return false;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
            //if not not mapped with user then through illigal operation exception
            //throw new InvalidOperationException("Tergate report is not in authorization on current user");
        }

        public string Save_Report(int ReportID, long UserID, string SectorID, string SalFinYear, string SalMonthID, string SalMonth, int isPDFExcel)
        {
            string result = "";
            SqlCommand cmd = new SqlCommand("Load_reportDynamic", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SalmonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@salmonth", SalMonth);
            cmd.Parameters.AddWithValue("@salaryFinYear", SalFinYear);
            cmd.Parameters.AddWithValue("@Secid", SectorID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@ChildreportID", ReportID);

            cmd.Parameters.AddWithValue("@CenterID", DBNull.Value);
            cmd.Parameters.AddWithValue("@EmpType", DBNull.Value);
            cmd.Parameters.AddWithValue("@Status", DBNull.Value);
            cmd.Parameters.AddWithValue("@EDID", DBNull.Value);
            cmd.Parameters.AddWithValue("@SectorGroup", DBNull.Value);
            cmd.Parameters.AddWithValue("@CenterGroup", DBNull.Value);
            cmd.Parameters.AddWithValue("@EmpTypeGroup", DBNull.Value);
            cmd.Parameters.AddWithValue("@StatusGroup", DBNull.Value);
            cmd.Parameters.AddWithValue("@SchType", DBNull.Value);
            cmd.Parameters.AddWithValue("@isPDFExcel", isPDFExcel.Equals("") ? DBNull.Value : (object)isPDFExcel); 
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);
              
                result= "success";
            }
            catch (SqlException)
            {
                throw;
                result = "fail";
            }
            finally
            {
                cn.Close();
            }
            return result;
        }

        public List<Bank_Report_MD> Show_BankData(string SalMonthID, string SectorID, string CurFinYear, long UserID)
        {
            SqlCommand cmd = new SqlCommand("Gen_bankSlipCumList", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SalmonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@Secid", SectorID);
            cmd.Parameters.AddWithValue("@FinYear", CurFinYear);
            cmd.Parameters.AddWithValue("@UserID", UserID);
           
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            DataTable dtresult = new DataTable();
            DataSet ds = new DataSet();

            List<Bank_Report_MD> lstResult = new List<Bank_Report_MD>();
            try
            {
                cn.Open();
                da.Fill(ds);
                if (ds.Tables.Count > 0)
                {

                    dt = ds.Tables[0];
                    dtresult = ds.Tables[1];
                }
                return Convert_BankData(dt, dtresult);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
           
        }

        public string Update_BankSlip(string SalMonthID, string SectorID, long UserID, string ChequeNo, string ChequeDate)
        {
            string result = "";
            SqlCommand cmd = new SqlCommand("Update_bankSlipCumList", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SalmonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@Secid", SectorID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@chequeNo", ChequeNo);
            cmd.Parameters.AddWithValue("@chequeDate", ChequeDate);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                result = "success";
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return result;
        }





        #region Converter
        List<Report> Convert(DataTable dt)
        {
            List<Report> lst = new List<Report>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Report();
                obj.ReportID = t.Field<int>("ReportID");
                obj.ReportName = t.Field<string>("ReportName");
                obj.rptName = t.Field<string>("rptName");
                obj.isPDF = t.Field<int>("isPDF");
                obj.PDF_Procedure = t.Field<string>("PDF_Procedure");
                obj.isExcel = t.Field<int>("isExcel");
                obj.Excel_Procedure = t.Field<string>("Excel_Procedure");
                obj.ISSubReport = t.Field<int>("ISSubReport");
                obj.SubReportProcedure = t.Field<string>("SubReportProcedure");
                return obj;
            }).ToList();
            return lst;
        }

        List<Bank_Report_MD> Convert_BankData(DataTable dt, DataTable dtResult)
        {
            List<Bank_Report_MD> lst = new List<Bank_Report_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Bank_Report_MD();
                obj.SectorID = t.Field<int>("SectorID");
                obj.SectorName = t.Field<string>("SectorName");
                obj.SalMonthID = t.Field<int>("SalMonthID");
                obj.SalMonth = t.Field<string>("SalMonth");
                obj.EmpNo = t.Field<string>("EmpNo");
                obj.EmpName = t.Field<string>("EmpName");
                obj.Branch = t.Field<string>("Branch");
                obj.IFSC = t.Field<string>("IFSC");
                obj.MICR = t.Field<string>("MICR");
                obj.BankAccCode = t.Field<string>("BankAccCode");
                obj.NetPay = t.Field<decimal>("NetPay");
                obj.BranchID = t.Field<int>("BranchID");
                obj.BankName = t.Field<string>("BankName");
                obj.EncodeValue = t.Field<string>("EncodeValue");
                obj.Result = dtResult.Rows[0]["Result"].ToString();
                return obj;
            }).ToList();
            return lst;
        }
        #endregion
    }
}