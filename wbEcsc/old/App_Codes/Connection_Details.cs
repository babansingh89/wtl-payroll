﻿using System;
using System.Configuration;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions;
using CrystalDecisions.Shared;
using System.Web.Services;
using wbEcsc.App_Codes;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using System.Net;
using System.Net.Mail;
using System.Net.Security;

namespace wbEcsc.App_Codes
{
    public class Connection_Details
    {
       
        public static string ServerName()
        {
            string server = "";
            try
            {
                string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.ConnectionString = conString;
                server = builder.DataSource;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return server;
        }
        public static string DatabaseName()
        {
            string database = "";
            try
            {
                string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.ConnectionString = conString;
                database = builder.InitialCatalog;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return database;
        }
        public static string UserID()
        {
            string userid = "";
            try
            {
                string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.ConnectionString = conString;
                userid = builder.UserID;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return userid;
        }
        public static string Password()
        {
            string pwd = "";
            try
            {
                string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.ConnectionString = conString;
                pwd = builder.Password;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return pwd;
        }

        public static DataTable Get_EmailSettings()
        {
            try
            {
                string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
                SqlConnection con = new SqlConnection(conString);
                SqlCommand cmd = new SqlCommand("Get_EmailSettings", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    return dt;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void Update_EmailSetting(string EmployeeID, string Tag, string ErrMsg)
        {
            string conString = "";
            try
            {
                 conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
                SqlConnection con = new SqlConnection(conString);
                SqlCommand cmd = new SqlCommand("Update_Email_Eligible_Employee", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                cmd.Parameters.AddWithValue("@EmailSendingTag", Tag);
                cmd.Parameters.AddWithValue("@ErrorMessage", ErrMsg);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}