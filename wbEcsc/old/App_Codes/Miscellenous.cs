﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Security;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.Authenticator;
using wbEcsc.App_Codes.BLL;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Account.Logins;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes
{
    public class Miscellenous:BllBase
    {
        public string Save_UserColor(string Header, string Background, long UserID)
        {
            SqlCommand cmd = new SqlCommand("Save_UserColor" , cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Header", Header);
            cmd.Parameters.AddWithValue("@Background", Background);
            cmd.Parameters.AddWithValue("@UserID", UserID);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return "success";
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Get_UserColor(long UserID)
        {
            SqlCommand cmd = new SqlCommand("Get_UserColor", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", UserID);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        
    }
}