﻿using System.Collections.Generic;
using System.Web;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Account;
using wbEcsc.Models.Account.Logins;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes
{
    public static class SessionContext
    {
        static SessionContext()
        {
            
        }

        public static bool IsAuthenticated
        {
            get
            {
                return CurrentUser != null;
            }
        }
        public static object CurrentUser
        {
            get
            {
                var user = HttpContext.Current.Session[KeysLibrary.SessionKeys.CurrentUser_Key];
                return user;
            }
            private set
            {
                HttpContext.Current.Session[KeysLibrary.SessionKeys.CurrentUser_Key] = value;
            }
        }
        public static SessionData SessionData
        {
            get
            {
                var user = HttpContext.Current.Session[KeysLibrary.SessionKeys.SessionData_Key] as SessionData;
                return user;
            }
            set
            {
                HttpContext.Current.Session[KeysLibrary.SessionKeys.SessionData_Key] = value;
            }
        }
        public static void Login(IInternalUser internalUser)
        {
            CurrentUser = internalUser;
            var sData= new SessionData ();
            sData.Sectors = new EmployeeMaster().Get_Sector(internalUser.UserID);
            if(sData.Sectors.Count == 1)
            {
                foreach (var sector in sData.Sectors)
                {
                    sData.CurrSector = sector.SectorID;
                }
            }


            sData.UserID = internalUser.UserID;
            sData.UserType = internalUser.UserType;
            sData.EmpNoLength = new Logo().Get_EmpNoLenth();
            HttpContext.Current.Session["UserID"] = internalUser.UserID;
            

            SessionData = sData;
            GlobalContext_Operator.AddSession(HttpContext.Current.Session);
            
        }
        public static void Logout()
        {
            string ssid = System.Web.HttpContext.Current.Session.SessionID;
            System.Web.HttpContext.Current.Session.Clear();
            GlobalContext_Operator.RemoveSession(ssid);
        }
        public static void Logout(string sessionID)
        {
            var session = GlobalContext_Operator.GetSession(sessionID);
            if (session != null)
            {
                session.Clear();
            }
            GlobalContext_Operator.RemoveSession(sessionID);
        }
        public static void InitSessionData()
        {
            Logo lObj = new Logo();
            int sectorId = SessionData.CurrSector ?? 0;
            SalMonth_FinYear_AccFinYear obj_salfinacc = lObj.Get_SalMonth_FinYear_AccFinYear(sectorId);
            SessionData.CurFinYear = obj_salfinacc.AccFinYear;
            SessionData.CurSalFinYear = obj_salfinacc.SalFinYear;
            SessionData.SalMonth = obj_salfinacc.SalMonth;
            SessionData.SalMonthID = obj_salfinacc.SalMonthID??0;
        }
    }
}

