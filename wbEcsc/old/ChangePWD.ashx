﻿<%@ WebHandler Language="C#" Class="ChangePWD" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;

public class ChangePWD : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
{

    public void ProcessRequest (HttpContext context) {
        string JSONVal = "";
        string EmployeeID = "";
        string Pwd = "";
        string OPwd = "";
        ApiResponceData GER = new ApiResponceData();
        //string conString = DataAccess.DBHandler.GetConnectionString();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        try
        {
            if (context.Request.QueryString.Count > 0)
            {
                EmployeeID = (context.Request.QueryString["UserId"]);
                OPwd = (context.Request.QueryString["OldPassword"]);
                Pwd = (context.Request.QueryString["NewPassword"]);

            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                EmployeeID = (context.Request.Params["UserId"]);
                OPwd = (context.Request.Params["OldPassword"]);
                Pwd = (context.Request.Params["NewPassword"]);


            }

            if (EmployeeID !="" && Pwd!="")
            {

                DataTable dtSignInfo = new DataTable();
                List<Dictionary<string, object>> rowss = new List<Dictionary<string, object>>();
                using (var command = new SqlCommand("payroll.Change_UserPasswordApp", con)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    con.Open();
                    command.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                    command.Parameters.AddWithValue("@OldPassword", OPwd);
                    command.Parameters.AddWithValue("@NewPassword",Pwd);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    sda.Fill(dtSignInfo);
                    con.Close();
                }
                if (dtSignInfo.Rows.Count > 0)
                {
                    Dictionary<string, object> row1;
                    foreach (DataRow r in dtSignInfo.Rows)
                    {
                        row1 = new Dictionary<string, object>();
                        foreach (DataColumn col in dtSignInfo.Columns)
                        {
                            row1.Add(col.ColumnName, r[col]);
                        }
                        rowss.Add(row1);
                    }

                    //GER.Data = rowss;
                    GER.Status = dtSignInfo.Rows[0]["MESSEGE"].ToString() == "success" ? "Y" : "N";
                    GER.Message = dtSignInfo.Rows[0]["MESSEGE"].ToString();
                    JSONVal = GER.ToJSON();
                }
                else
                {
                    GER.Status = "N";
                    GER.Message = "No records found ";
                    JSONVal = GER.ToJSON();
                }
            }
            else
            {
                GER.Status = "N";
                GER.Message = "Provide all the parameters ";
                JSONVal = GER.ToJSON();
            }

        }
        catch (Exception ex)
        {
            GER.Status = "N";
            GER.Message = ex.Message;
            JSONVal = GER.ToJSON();
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(JSONVal);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}