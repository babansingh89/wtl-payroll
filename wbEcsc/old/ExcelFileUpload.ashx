﻿<%@ WebHandler Language="C#" Class="AjaxFileUpload" %>

using System;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Runtime.Serialization.Json;


public class AjaxFileUpload : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.Files.Count > 0)
        {
            string picsFolder = context.Request.QueryString["picsFolder"];
            string foldername = "";

            foldername = "Excel_Files";


            string path = context.Server.MapPath("~/" + foldername);

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            // Delete all files from the Directory
            foreach (string filename in Directory.GetFiles(path))
            {
                File.Delete(filename);
            }

            var file = context.Request.Files[0];
            string msg = "";


            string fileName = file.FileName;
            string name = Path.Combine(path, fileName);
            System.IO.File.Delete(name);
            context.Request.Files[0].SaveAs(name);



            msg = "{";
            msg += string.Format("error:'{0}',\n", "File Size is not within the specified range.");
            msg += string.Format("msg:'{0}'\n", foldername + "/" + fileName);
            msg += "}";


            context.Response.Write(msg);


        }
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}