﻿<%@ WebHandler Language="C#" Class="AjaxFileUpload" %>

using System;
using System.Web;
using System.IO;
using System.Web.SessionState;
using System.Runtime.Serialization.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;


public class AjaxFileUpload : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.Files.Count > 0)
        {
            string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            SqlConnection dbConn = new SqlConnection(conString);
            dbConn.Open();

            string photoFolder = context.Request.QueryString["photoFolder"];
            string sigFolder = context.Request.QueryString["sigFolder"];
            string EmpNo = context.Request.QueryString["EmpNo"];
            string foldername = "";
            string photo_ReturnPath = ""; string sig_ReturnPath = "";

            foldername = "EmpPhoto_Signature/" + EmpNo;
            string path = HttpContext.Current.Server.MapPath("~/" + foldername);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            var file = context.Request.Files[0];
            string filename1 = file.FileName;

            string msg = "";

            string filename = file.FileName;
            string fileEXT = Path.GetExtension(filename);
            if (photoFolder == "UploadPhoto")
            {
                string p_fileName = EmpNo + "_Photo" + fileEXT;
                string photo_fileName = Path.Combine(path, p_fileName);
                System.IO.File.Delete(photo_fileName);

                file.SaveAs(photo_fileName);

                photo_ReturnPath = foldername + "/" + p_fileName;

                cmd = new SqlCommand("Update_EmpPhoto_Signature", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@EmpNo", EmpNo.Equals("") ? DBNull.Value : (object)EmpNo);
                cmd.Parameters.AddWithValue("@Photo", photo_ReturnPath.Equals("") ? DBNull.Value : (object)photo_ReturnPath);
                cmd.Parameters.AddWithValue("@Signature", sig_ReturnPath.Equals("") ? DBNull.Value : (object)sig_ReturnPath);

                cmd.ExecuteNonQuery();
               
            }

            if (sigFolder == "UploadSign")
            {
                string s_fileName = EmpNo + "_Sig" + fileEXT;
                string sig_fileName = Path.Combine(path, s_fileName);
                System.IO.File.Delete(sig_fileName);

                file.SaveAs(sig_fileName);

                sig_ReturnPath = foldername + "/" + s_fileName;

               
                cmd = new SqlCommand("Update_EmpPhoto_Signature", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@EmpNo", EmpNo.Equals("") ? DBNull.Value : (object)EmpNo);
                cmd.Parameters.AddWithValue("@Photo", photo_ReturnPath.Equals("") ? DBNull.Value : (object)photo_ReturnPath);
                cmd.Parameters.AddWithValue("@Signature", sig_ReturnPath.Equals("") ? DBNull.Value : (object)sig_ReturnPath);

                cmd.ExecuteNonQuery();
               
            }
             dbConn.Close();
            //msg = "{";
            //msg += string.Format("error:'{0}',\n", string.Empty);
            //msg += string.Format("photopath:'{0}',\n", strFileName);
            //msg += string.Format("sigpath:'{0}'", ReturnPath);
            //msg += "}";




            context.Response.Write(msg);
        }
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}