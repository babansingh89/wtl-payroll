﻿<%@ WebHandler Language="C#" Class="LeaveMail" %>
using System;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions;
using CrystalDecisions.Shared;
using System.Web.Services;
using wbEcsc.App_Codes;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
public class LeaveMail : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
{

    public void ProcessRequest (HttpContext context) {
        string JSONVal = "";
        string EmailSUbject = "";
        string EmailBody = "";
        string CCMailId = "";
        string BccMailId = "";
        string ToMailID = "";
        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
        List<TYP_sendEmail> TYP_sendEmailList = new List<TYP_sendEmail>();
        ApiResponceData GER = new ApiResponceData();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        try
        {
            if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                EmailSUbject = context.Request.Params["EmailSUbject"].ToString();
                EmailBody = context.Request.Params["EmailBody"].ToString();
                TYP_sendEmailList = jsonSerializer.Deserialize<List<TYP_sendEmail>>(context.Request.Params["ToEmailData"]);
            }
            if(TYP_sendEmailList.Count>0 && EmailSUbject!="" && EmailBody != "")
            {
                // DataTable dtEmailSetting = Connection_Details.Get_EmailSettings();
                foreach (var TH in TYP_sendEmailList)
                {
                    //DataRow dr = TYP_ItemAtrbt.NewRow();
                    //dr["ItemAtrbtID"] = TH.ItemAtrbtID;
                    //TYP_ItemAtrbt.Rows.Add(dr);
                    if (TH.type == 1) { ToMailID = TH.mailID.ToString(); }
                    if (TH.type == 2) { CCMailId = TH.mailID.ToString(); }
                    if (TH.type == 3) { BccMailId = TH.mailID.ToString(); }
                }
                SmtpClient client = new SmtpClient();
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                client.Host = "smtp.gmail.com";//dtEmailSetting.Rows[0]["SMTP"].ToString(); 
                client.Port = 587;//Convert.ToInt32(dtEmailSetting.Rows[0]["Port"].ToString()); //

                //System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(dtEmailSetting.Rows[0]["UserName"].ToString(), dtEmailSetting.Rows[0]["Password"].ToString());
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("wtl_inhouse@wtl.co.in", "hr@12345");
                client.UseDefaultCredentials = false;
                client.Credentials = credentials;
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress("wtl_inhouse@wtl.co.in");
                if (ToMailID != "")
                {
                    msg.To.Add(new MailAddress(ToMailID));
                }
                if (CCMailId != "")
                {
                    msg.CC.Add(new MailAddress(CCMailId));        
                }
                if (BccMailId != "")
                {
                     msg.Bcc.Add(new MailAddress(BccMailId));
                }
                msg.Subject = EmailSUbject;
                msg.IsBodyHtml = true;
                msg.Body = EmailBody;
                client.Send(msg);
                client.Timeout = 1000;
                GER.Status = "Y";
                GER.Message = "Success";
                JSONVal = GER.ToJSON();
            }

        }
        catch(Exception ex)
        {
            GER.Status = "N";
            GER.Message = ex.Message;
            JSONVal = GER.ToJSON();
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(JSONVal);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}
public class TYP_sendEmail{
    public int type { get; set; }
    public string mailID { get; set; }
}