﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account.Logins
{
    public class SalMonth_FinYear_AccFinYear
    {
        public int? SalMonthID { get; set; }
        public string SalMonth { get; set; }
        public string SalFinYear { get; set; }
        public string AccFinYear { get; set; }
    }
}