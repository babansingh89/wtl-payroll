﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wbEcsc.Models.Account 
{
    public class InternalUser : wbEcsc.Models.Account.IInternalUser
    {

        public string Email
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public bool IsAccountLocked
        {
            get;
            set;
        }

        public bool IsMailVerified
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string PhoneNo
        {
            get;
            set;
        }

        public long UserID
        {
            get;
            set;
        }

        public int UserType
        {
            get;
             set;
        }
    }
}
