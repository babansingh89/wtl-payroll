﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class Designation_MD
    {
        public int? DesignationID { get; set; }
        public string Designation { get; set; }
    }
}