﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class District_MD
    {
        public int? DistID { get; set; }
        public string District { get; set; }
    }
}