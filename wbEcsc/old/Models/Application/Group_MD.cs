﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.App_Codes.BLL
{
    public class Group_MD
    {
        public int? GroupID { get; set; }
        public string GroupCode { get; set; }

        public string GroupName { get; set; }
    }
}