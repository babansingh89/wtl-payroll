﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class ItaxSlapPercentage_MD
    {
        public int? RateSlabID { get; set; }
        public string FinYear { get; set; }
        public string GenderID { get; set; }
        public string Gender { get; set; }
        public decimal? AddAmount { get; set; }
        public decimal? AmountFrom { get; set; }
        public decimal? AmountTo { get; set; }
        public decimal? Percentage { get; set; }       

    }
}