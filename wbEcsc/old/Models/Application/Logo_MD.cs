﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class Logo_MD
    {
            public int? LogoID { get; set; }
            public string OfficeName { get; set; }
            public string Address { get; set; }
            public string WebSiteHeader_1 { get; set; }
            public string WebSiteHeader_2 { get; set; }
            public string LogoPath { get; set; }
            public int? EmpNoLength { get; set; }

    }
}