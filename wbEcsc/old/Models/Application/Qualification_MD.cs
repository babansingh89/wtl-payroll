﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class Qualification_MD
    {
        public int? QualificationID { get; set; }
        public string Qualification { get; set; }
    }
}