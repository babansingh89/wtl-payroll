﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class House_MD
    {
        public int? HouseID { get; set; }
        public string HouseName { get; set; }
    }
}