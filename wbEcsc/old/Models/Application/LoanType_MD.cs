﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class LoanType_MD
    {
        public int? LoanTypeID { get; set; }
        public int? TypeID { get; set; }
        public string TypeName { get; set; }
        public string Description { get; set; }
        public string Abbreviation { get; set; }
        public int? Rank { get; set; }
        public string IsActive { get; set; }
        public string IsActiveText { get; set; }
        public int? IsRound { get; set; }
        public string IsRoundText { get; set; }

    }
}