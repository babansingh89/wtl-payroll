﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class LoanMaster_MD
    {
        public int? LoanTypeID { get; set; }
        public string LoanTypeCode { get; set; }
        public string LoanType { get; set; }
        public string LoanAbbv { get; set; }
        public string LoanDesc { get; set; }
        public string LoanActive { get; set; }
        public string LoanPaymentType { get; set; }
        public int? IsRound { get; set; }
    }
}