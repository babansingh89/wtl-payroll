﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class LeaveMaster_MD
    {
        public int? LeaveTypeID { get; set; }
        public string txtLeaveDesc { get; set; }
        public string txtAbbv { get; set; }
        public string ddlGender { get; set; }
        public string txtMaxInYear { get; set; }
        public string txtMaxSave { get; set; }
        public string txtMaxInServicePeriod { get; set; }
        public int? isActive { get; set; }
        public int? isRound { get; set; }
        public int? isLeave { get; set; }

        public string Edit { get; set; }
        public string Del { get; set; }
        public int? ErrorCode { get; set; }
        public string Messege { get; set; }
        public int? FieldId { get; set; }
        public string FieldName { get; set; }
        public int? IsMand { get; set; }
    }
}