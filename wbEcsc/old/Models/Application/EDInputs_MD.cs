﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wbEcsc.App_Codes;

namespace wbEcsc.Models.Application
{
    public class EDInputs_MD
    {
        public string EmpNo { get; set; }
        public string EmpName { get; set; }
        public decimal EDAmount { get; set; }
        public string EDName { get; set; }
        public decimal? ItaxForThisMonth { get; set; }
        public decimal? BalanceItax { get; set; }

    }

    public class EDDetail_MD
    {
        public int EDID { get; set; }
        public string EDName { get; set; }
        public int MenuID { get; set; }
    }
}