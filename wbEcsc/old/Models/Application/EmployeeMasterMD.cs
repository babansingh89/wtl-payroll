﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class EmployeeMasterMD
    {
            public int? LogoID { get; set; }
            public string OfficeName { get; set; }
            public string Address { get; set; }
            public string WebSiteHeader_1 { get; set; }
            public string WebSiteHeader_2 { get; set; }
            public string LogoPath { get; set; }
    }

    public class Sector
    {
        public int? SectorID { get; set; }
        public string SectorName { get; set; }

    }

    public class EmployeeMasterDetails
    {
        public string EmployeeID { get; set; }
        public string EmpNo { get; set; }
        public string EmpName { get; set; }
    }

    public class Employee_SaveData
    {
        //Personal Details
        public string EmployeeID { get; set; }
        public string EmpNo { get; set; }
        public int? SectorID { get; set; }
        public int? LocationID { get; set; }
        public string EmpType { get; set; }
        public int? ClassificationID { get; set; }
        public string EmpName { get; set; }
        public string FatherName {get; set;}
        public int? Religion { get; set; }
        public int? Qualification { get; set; }
        public string MaritalStatus { get; set; }
        public int? Caste { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }
        public string aadhaarNo { get; set; }

        //Address
        public string EmpAddress { get; set; }
        public string StateID { get; set; }
        public string DistrictID { get; set; }
        public string City { get; set; }
        public string Pin { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        public string PermAddress { get; set; }
        public string PermState { get; set; }
        public string PermDistrict { get; set; }
        public string PermCity { get; set; }
        public string PermPin { get; set; }
        public string PermPhone { get; set; }

        //Official Details
        public string DOJ { get; set; }
        public string DOJPresentDept { get; set; }
        public string DOR { get; set; }
        public string PhysicalHandicapped { get; set; }
        public string Designation { get; set; }
        public string Group { get; set; }
        public string Cooperative { get; set; }
        public string CoopMembership { get; set; }
        public string Category { get; set; }
        public string DA { get; set; }
        public string Status { get; set; }
        public string PFCode { get; set; }
        public string PayScaleType { get; set; }
        public string PayScaleID { get; set; }
        public string DNI { get; set; }
        public string PAN { get; set; }
        public int? HRACat { get; set; }
        public string HRAFixedAmount { get; set; }
        public string HRAID { get; set; }
        public string QuaterAllot { get; set; }
        public string SpouseQuater { get; set; }
        public string SpouseAmount { get; set; }
        public string LicenceFees { get; set; }
        public string HealthScheme { get; set; }
        public string HealthSchemeDetail { get; set; }
        public string EffectiveFrom { get; set; }
        public string EffectiveTo { get; set; }
        public string LWP { get; set; }
        public string IRID { get; set; }
        public string UAN { get; set; }
        public int? PFPercent { get; set; }
        public string PFValue { get; set; }
        public int? BranchID { get; set; }
        public string BankAccCode { get; set; }

        //Photo & Signature
        public string Photo { get; set; }
        public string Signature { get; set; }

        public string SalMonth { get; set; }
        public int? IsMultiple { get; set; }
    }

    public class Employee_SaveData_PayDetails
    {
        public string EDID { get; set; }
        public string EDAmount { get; set; }
    }

    public class Employee_PayDetails
    {
        public int EmployeeID { get; set; }
        public int EDID { get; set; }
        public string EDName { get; set; }
        public decimal EDAmount { get; set; }
        public string EDType { get; set; }
    }

    public class PF_Percentage_Value
    {
        public int ID { get; set; }
        public string PFType { get; set; }
        public decimal? PFRate { get; set; }
        public int? Min { get; set; }
        public int? Max { get; set; }
        public int? MinPerc { get; set; }
        public int? MaxPerc { get; set; }

    }

    public class Emp_Type_MD
    {
        public string EmpType { get; set; }
        public string EmpTypeDesc { get; set; }
    }
}