﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class Branch_MD
    {
        public int? BranchID { get; set; }

        public int? BankID { get; set; }

        public string Branch { get; set; }

        public string BankName { get; set; }

        public string IFSC { get; set; }

        public string MICR { get; set; }
    }
}