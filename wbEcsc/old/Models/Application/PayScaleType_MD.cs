﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class PayScaleType_MD
    {
        public string PayScaleID { get; set; }
        public string PayScale { get; set; }
    }
}