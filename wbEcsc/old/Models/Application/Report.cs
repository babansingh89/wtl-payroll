﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class Report
    {
        public int ReportID { get; set; }
        public string ReportName { get; set; }
        public string rptName { get; set; }
        public int isPDF { get; set; }
        public string PDF_Procedure { get; set; }
        public int isExcel { get; set; }
        public string Excel_Procedure { get; set; }
        public int ISSubReport { get; set; }
        public string SubReportProcedure { get; set; }

    }

    public class Bank_Report_MD
    {
        public int SectorID { get; set; }
        public string SectorName { get; set; }
        public int SalMonthID { get; set; }
        public string SalMonth { get; set; }
        public string EmpNo { get; set; }
        public string EmpName { get; set; }
        public string Branch { get; set; }
        public string MICR { get; set; }
        public string IFSC { get; set; }
        public string BankAccCode { get; set; }
        public decimal NetPay { get; set; }
        public int BranchID { get; set; }
        public string BankName { get; set; }
        public string EncodeValue { get; set; }
        public string Result { get; set; }
    }
}