﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class DA_MD
    {
        public int? DAID { get; set; }
        public string DACode { get; set; }
        public decimal DARate { get; set; }
        public string EmpType { get; set; }
    }
}