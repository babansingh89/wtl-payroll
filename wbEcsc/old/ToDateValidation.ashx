﻿<%@ WebHandler Language="C#" Class="ToDateValidation" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;

public class ToDateValidation : IHttpHandler {

    public void ProcessRequest (HttpContext context) {
        string JSONVal = "";
        string LtNo = "";
        string LtDate = "";
        int EmployeeID = 0;
        int LvId = 0;
        string FromDate = "";
        string ToDate = "";
        decimal Bal = 0;
        int IsLeave = 0;
        int IsRound = 0;
        ApiResponceData GER = new ApiResponceData();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        try
        {
            if (context.Request.QueryString.Count > 0)
            {
                LtNo = context.Request.QueryString["LtNo"].ToString();
                LtDate = context.Request.QueryString["LtDate"].ToString();
                EmployeeID = Convert.ToInt32(context.Request.QueryString["EmployeeID"]);
                LvId = Convert.ToInt32(context.Request.QueryString["LvId"]);
                FromDate = context.Request.QueryString["FromDate"].ToString();
                ToDate = context.Request.QueryString["ToDate"].ToString();
                Bal = Convert.ToDecimal(context.Request.QueryString["Bal"]);
                IsLeave = Convert.ToInt32(context.Request.QueryString["IsLeave"]);
                IsRound = Convert.ToInt32(context.Request.QueryString["IsRound"]);
            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                LtNo = context.Request.Params["LtNo"].ToString();
                LtDate = context.Request.Params["LtDate"].ToString();
                EmployeeID = Convert.ToInt32(context.Request.Params["EmployeeID"]);
                LvId = Convert.ToInt32(context.Request.Params["LvId"]);
                FromDate = context.Request.Params["FromDate"].ToString();
                ToDate = context.Request.Params["ToDate"].ToString();
                Bal = Convert.ToDecimal(context.Request.Params["Bal"]);
                IsLeave = Convert.ToInt32(context.Request.Params["IsLeave"]);
                IsRound = Convert.ToInt32(context.Request.Params["IsRound"]);
            }
            if (EmployeeID>0)
            {
                DataTable dt = new DataTable();
                using (var command = new SqlCommand("Get_LvToDtApp", con)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    con.Open();
                    command.Parameters.AddWithValue("@LtNo", LtNo==""?DBNull.Value:(Object)LtNo);
                    command.Parameters.AddWithValue("@LtDate", LtDate);
                    command.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                    command.Parameters.AddWithValue("@LvId", LvId);
                    command.Parameters.AddWithValue("@FromDate", FromDate);
                    command.Parameters.AddWithValue("@ToDate", ToDate);
                    command.Parameters.AddWithValue("@Bal", Bal);
                    command.Parameters.AddWithValue("@IsLeave", IsLeave);
                    command.Parameters.AddWithValue("@IsRound", IsRound);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    sda.Fill(dt);
                    con.Close();
                }
                if (dt.Rows.Count > 0)
                {
                    List<Dictionary<string, object>> rowss = new List<Dictionary<string, object>>();
                    if (dt.Rows.Count > 0)
                    {

                        Dictionary<string, object> row1;

                        foreach (DataRow r in dt.Rows)
                        {
                            row1 = new Dictionary<string, object>();
                            foreach (DataColumn col in dt.Columns)
                            {
                                row1.Add(col.ColumnName, r[col]);
                            }
                            rowss.Add(row1);
                        }
                    }


                    GER.Data = rowss;
                    GER.Status = "Y";
                    GER.Message = "SUCCESS";
                    JSONVal = GER.ToJSON();
                }
                else
                {
                    GER.Status = "N";
                    GER.Message = "No records found ";
                    JSONVal = GER.ToJSON();
                }
            }
            else
            {
                GER.Status = "N";
                GER.Message = "Provide all the parameters ";
                JSONVal = GER.ToJSON();
            }
        }
        catch (Exception ex)
        {
            GER.Status = "N";
            GER.Message = ex.Message;
            JSONVal = GER.ToJSON();
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(JSONVal);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}