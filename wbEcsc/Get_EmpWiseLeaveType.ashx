﻿<%@ WebHandler Language="C#" Class="Get_EmpWiseLeaveType" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;

public class Get_EmpWiseLeaveType : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
{

    public void ProcessRequest (HttpContext context) {
        string JSONVal = "";
        int EmployeeID = 0;
        string LtDate = "";
        ApiResponceData GER = new ApiResponceData();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        //LeaveTypeInfo LTI = new LeaveTypeInfo();
        try
        {
            if (context.Request.QueryString.Count > 0)
            {
                EmployeeID = Convert.ToInt32(context.Request.QueryString["EmpID"]);
                LtDate = context.Request.QueryString["LtDate"].ToString();
            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                EmployeeID = Convert.ToInt32(context.Request.Params["EmpID"]);
                LtDate = context.Request.Params["LtDate"].ToString();
            }

            if (EmployeeID > 0 && LtDate!="")
            {
                List<Dictionary<string, object>> rowss = new List<Dictionary<string, object>>();
                DataTable dt = new DataTable();
                using (var command = new SqlCommand("Get_EmpRankWiseLeaveTypes", con)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    con.Open();
                    command.Parameters.AddWithValue("@LtDate", LtDate);
                    command.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    sda.Fill(dt);
                    con.Close();
                }
                if (dt.Rows.Count > 0)
                {
                   Dictionary<string, object> row1;

                        foreach (DataRow r in dt.Rows)
                        {
                            row1 = new Dictionary<string, object>();
                            foreach (DataColumn col in dt.Columns)
                            {
                                row1.Add(col.ColumnName, r[col]);
                            }
                            rowss.Add(row1);
                        }
                    GER.Status = "Y";
                    GER.Message = "SUCCESS";
                    GER.Data =rowss;
                    JSONVal = GER.ToJSON();
                }
                else
                {
                    GER.Status = "N";
                    GER.Message = "No records found.";
                    JSONVal = GER.ToJSON();
                }
            }
            else
            {
                GER.Status = "N";
                GER.Message = "Provide all the parameters ";
                JSONVal = GER.ToJSON();
            }

        }
        catch (Exception ex)
        {
            //var lst = new { UserID = "", Success = "N", GuestName = "", Message = ex.Message };
            //JSONVal = lst.ToJSON();
            GER.Status = "N";
            GER.Message = ex.Message;
            JSONVal = GER.ToJSON();
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(JSONVal);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}