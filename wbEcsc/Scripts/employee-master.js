﻿var TotalDedus = 0;
var TotalEarnings = 0;
var TotalNets = 0;
var proc_result = '';

$(document).ready(function () {
    var OverLayContainer = $("#overlay_Common");
    FieldNameData();
    Calculate_DNI();

    $('#txtDOB, #txtDOJ, #txtDOJDept, #txtDOR, #txtDNI, #txtEffFrm, #txtEffTo').mask("99/99/9999", { placeholder: "_" });

    $("#txtValue").hide(); $("#ddlPercent").hide();

    $('#txtDOB, #txtDOJ, #txtDOJDept, #txtDOR, #txtDNI, #txtEffFrm, #txtEffTo').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });

    $("#txtEmpNo, #txtPhone, #txtMobile, #txtPin, #txtPin_P, #txtPhone_P, #txtBankCode, #txtAadhar, #txtmHRA, #txtrentAmount").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#ddlSector").change(function () {
        var s = $('#ddlSector').val();

        if (s != '') {
            var E = "{SectorID: " + $('#ddlSector').val() + "}";
            var options = {};
            options.url = "/Administration/Master/EmployeeMaster/Get_Center";
            options.type = "POST";
            options.data = E;
            options.dataType = "json";
            options.contentType = "application/json";
            options.success = function (listCenter) {
                var a = listCenter.Data.length;
                if (a > 0) {
                    $.each(listCenter.Data, function (index, value) {
                        var label = value.CenterName; 
                        var val = value.CenterID; 
                        $("#ddlCenter").append($("<option></option>").val(val).html(label));
                    });
                }
            };
            options.error = function () { alert("Error in retrieving Center!"); };
            $.ajax(options);
        }
        else {
            $("#ddlCenter").empty();
            $("#ddlCenter").append("<option value=''>-- Select --</option>")
        }
    });

    $("#ddlState").change(function () {
            Bind_District();
    });

    $("#ddlState_P").change(function () {
        Bind_PermDistrict();
    });

    $('#ChkDo').click(function () {
        if ($("#ChkDo").is(':checked')) {
            $("#txtAddress_P").val($("#txtAddress").val());
            $("#ddlState_P").val($("#ddlState").val()); 

            if ($("#ddlState").val() != "" && $("#ddlState").val() != null) {
                var options = {};
                options.url = "/Administration/Master/EmployeeMaster/Bind_District",
                options.type = "POST";
                options.data = "{StateID: " + $('#ddlState').val() + "}";
                options.dataType = "json";
                options.contentType = "application/json";
                options.success = function (listDistrict) {
                    var a = listDistrict.Data.length;
                    if (a > 0) {
                        $("#ddlDistrict_P").empty();
                        $("#ddlDistrict_P").append("<option value=''>-- Select --</option>")
                        $.each(listDistrict.Data, function (index, value) {
                            var label = value.District;
                            var val = value.DistID;
                            $("#ddlDistrict_P").append($("<option></option>").val(val).html(label));
                        });
                        $("#ddlDistrict_P").val($("#ddlDistrict").val());
                    }
                };
                options.error = function () { alert("Error retrieving Districts!"); };
                $.ajax(options);
            }
            $("#txtCity_P").val($("#txtCity").val());
            $("#txtPin_P").val($("#txtPin").val());
            $("#txtPhone_P").val($("#txtPhone").val());

        }
        else {
            $("#txtAddress_P").val('');
            $("#ddlState_P").val('');
            $("#ddlDistrict_P").empty();
            $("#ddlDistrict_P").append("<option value=''>-- Select --</option>")
            $("#txtCity_P").val('');
            $("#txtPin_P").val('');
            $("#txtPhone_P").val('');

        }
    });

    $("#ddlBank").change(function () {
        if ($("#ddlBank").val() != "") {
            $("#txtIFSC").val('');
            $("#txtMICR").val('');

                var options = {};
                options.url = "/Administration/Master/EmployeeMaster/Bind_BankBranch";
                options.type = "POST";
                options.data = "{BankID: " + $('#ddlBank').val() + "}";
                options.dataType = "json";
                options.contentType = "application/json";
                options.success = function (listbranch) {
                    var a = listbranch.Data.length;
                    if (a > 0) {
                        $("#ddlBranch").empty();
                        $("#ddlBranch").append("<option value=''>-- Select --</option>")
                        $.each(listbranch.Data, function (index, value) {
                            var label = value.Branch;
                            var val = value.BranchID;
                            $("#ddlBranch").append($("<option></option>").val(val).html(label));
                        });
                    }
                    
                };
                options.error = function () { alert("Error retrieving BankName!"); };
                $.ajax(options);

        }
        else {
            $("#ddlBranch").empty();
            $("#ddlBranch").append("<option value=''>-- Select --</option>")
            $("#txtIFSC").val('');
            $("#txtMICR").val('');
            $('#hdnIFSC').val('');
        }
    });

    $("#ddlBranch").change(function () {
        var C = "{BranchID: " + $('#ddlBranch').val() + "}";
        if ($('#ddlBranch').val() != '') {
            $.ajax({
                type: "POST",
                url: "/Administration/Master/EmployeeMaster/Bind_IFSCMICR",
                data: C,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (D) {
                    var t = D.Data;
                    var ifsc = t[0].IFSC;
                    var micr = t[0].MICR;
                    $("#txtIFSC").val(ifsc);
                    $("#txtMICR").val(micr);
                }
            });
        }
        else {
            $("#txtIFSC").val('');
            $("#txtMICR").val('');
            $('#hdnIFSC').val('');
        }
    });

    Bind_DA(); Bind_HRA(); Bind_PayScaleType(); Bind_IR(); Bind_PFPercent();

    //This is for Bind DA on the Basis of EmployeeType
    $("#ddlEmpType").change(function () {
        Bind_DA();
        Bind_HRA();
        Bind_PayScaleType();
        Bind_IR();
        Bind_PFPercent();
        Calculate_DNI();
    });

    $("#ddlEarnDeduction").change(function (evt) {
        $("#btnAddPayDetails").html('Add');
        Check_Validations();
    });

    $("#ddlCategory").change(function () {
        Bind_Status();
    });

    $("#ddlPayScaleType").change(function () {
        Bind_PayScale();
    });

    $('#chkPercent').click(function () {
        if ($("#chkPercent").is(':checked')) {
            $("#chkValue").prop("checked", false); $("#ddlPercent").show(); $("#txtValue").hide(); $("#txtValue").val(''); $("#ddlPercent").focus(); $("#ddlPercent").val('');
        }
        else
        {
            $("#ddlPercent").hide(); $("#ddlPercent").val('');
        }
            
    });
    $('#chkValue').click(function () {
        if ($("#chkValue").is(':checked')) {
            $("#chkPercent").prop("checked", false); $("#txtValue").show(); $("#ddlPercent").val(''); $("#ddlPercent").hide(); $("#txtValue").focus(); $("#txtValue").val('');
        }
        else
        {
            $("#txtValue").hide(); $("#txtValue").val('');
        }
            
    });

    $("#txtIFSC").autocomplete({
        source: function (request, response) {

            var E = "{IFSC: '" + $("#txtIFSC").val() + "'}"; 

            $.ajax({
                type: "POST",
                url: "/Administration/Master/EmployeeMaster/Search_IFSCAutoComplete",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if(t.length >0)
                    {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.IFSC,
                                BranchID: item.BranchID
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $("#hdnIFSC").val(i.item.BranchID);

            E = "{BranchID:'" + i.item.BranchID + "'}";
            $.ajax({
                type: "POST",
                url: "/Administration/Master/EmployeeMaster/Get_BankandBranch",
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    if (t.length > 0) {
                        
                        $('#ddlBank').val(t[0].BankID);

                        if ($('#ddlBank').val() != "")
                        Bind_Branch(t[0].BranchID);

                        //$("#txtIFSC").val(t[0].IFSC);
                        $("#txtMICR").val(t[0].MICR);
                    }
                }
            });

        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtIFSC').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtIFSC').val('');
            $('#hdnIFSC').val('');
            $('#ddlBank').val('');
            $('#ddlBranch').val('');
        }
        if (iKeyCode == 46) {
            $('#txtIFSC').val('');
            $('#hdnIFSC').val('');
            $('#ddlBank').val('');
            $('#ddlBranch').val('');
        }
    });

    $("#txtSearchEmpNo").autocomplete({
        source: function (request, response) {

            var E = "{EmpNo: '" + $.trim($("#txtSearchEmpNo").val()) + "'}";

            $.ajax({
                type: "POST",
                url: "/Administration/Master/EmployeeMaster/Search_EmpNoAutoComplete",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.EmpNo,
                                EmpNo: item.EmployeeID
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            var EmpNoID = i.item.EmpNo;
            var a = EmpNoID.split('#');
            var EmployeeID = a[0];
            var EmpNo = a[1];

            $("#hdnEmployeeID").val(EmployeeID);
            var SectorID = $("#ddlSector").val();
            if (SectorID == "") {
                alert('Please Select Sector !'); $('#txtSearchEmpNo').val(''); $("#hdnEmployeeNo").val(''); $("#ddlSector").focus(); return false;
            }
            Populate_Employee(EmpNo);
        },
        minLength: 0
    }).click(function () {
        $("#txtSearchEmpName").val(''); $("#hdnEmployeeID").val('');
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtSearchEmpNo').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtSearchEmpNo').val('');
            $('#hdnEmployeeID').val('');
        }
        if (iKeyCode == 46) {
            $('#txtSearchEmpNo').val('');
            $('#hdnEmployeeID').val('');
        }
    });

    $("#txtSearchEmpName").autocomplete({
        source: function (request, response) {

            var E = "{EmpName: '" + $.trim($("#txtSearchEmpName").val()) + "'}";

            $.ajax({
                type: "POST",
                url: "/Administration/Master/EmployeeMaster/Search_EmpNameAutoComplete",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.EmpName,
                                EmpNo: item.EmployeeID
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            var EmpNoID = i.item.EmpNo;
            var a = EmpNoID.split('#');
            var EmployeeID = a[0];
            var EmpNo = a[1];

            $("#hdnEmployeeID").val(EmployeeID);

            var SectorID = $("#ddlSector").val();
            if (SectorID == "") {
                alert('Please Select Sector !'); $('#txtSearchEmpNo').val(''); $("#hdnEmployeeNo").val(''); $("#ddlSector").focus(); return false;
            }
            Populate_Employee(EmpNo);
        },
        minLength: 0
    }).click(function () {
        $("#txtSearchEmpNo").val(''); $("#hdnEmployeeID").val('');
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtSearchEmpName').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtSearchEmpName').val('');
            $('#hdnEmpNo').val('');
        }
        if (iKeyCode == 46) {
            $('#txtSearchEmpName').val('');
            $('#hdnEmpNo').val('');
        }
    });

    $("#ddlSpouseQtr").change(function () {
        var B = $("#ddlSpouseQtr").val();
        if (B == 'Y') {
            document.getElementById("txtSpouseAmt").disabled = false;
            $("#txtSpouseAmt").focus();
        }
        else {
            $("#txtSpouseAmt").val('');
            document.getElementById("txtSpouseAmt").disabled = true;
        }
    });

    $("#ddlQuatrAlt").change(function () {

            var B = $("#ddlQuatrAlt").val();
            if (B == '0') {
                $("#ddlHRA").val(0); $("#ddlHRA").prop("disabled", true);

                $("#ddlHRACat").val(''); $("#ddlHRACat").prop("disabled", true);
                $("#txtrentAmount").val(''); $("#txtrentAmount").prop("disabled", true);
                $("#ddlSpouseQtr").val(''); $("#ddlSpouseQtr").prop("disabled", true);
            }
            else if(B=='N')
            {
               
                var MaxArrary = [];
                $('#ddlHRA option').filter(function () {
                    var textValueID = $.trim($(this).val());
                    if(textValueID !='')
                    MaxArrary.push(textValueID); 
                    
                })
                var MaxID = Math.max.apply(Math, MaxArrary);
                $("#ddlHRA").val(MaxID);

                $("#ddlHRA").focus(); $("#ddlHRA").prop("disabled", false);
                $("#ddlSpouseQtr").val('N'); $("#ddlSpouseQtr").prop("disabled", false);

                $("#ddlHRACat").val(''); $("#ddlHRACat").prop("disabled", true);
                $("#txtrentAmount").val(''); $("#txtrentAmount").prop("disabled", true);
            }
            else if (B == 'Y') {
                $("#ddlHRA").val(''); $("#ddlHRA").prop("disabled", true);
                $("#ddlSpouseQtr").val(''); $("#ddlSpouseQtr").prop("disabled", true);
                $("#txtSpouseAmt").val(''); $("#txtSpouseAmt").prop("disabled", true);

                $("#ddlHRACat").val(''); $("#ddlHRACat").prop("disabled", false);
                $("#txtrentAmount").val(''); $("#txtrentAmount").prop("disabled", false);
            }
            else if (B == '') {
                $("#ddlHRA").val(''); $("#ddlHRA").prop("disabled", true);
                $("#ddlSpouseQtr").val(''); $("#ddlSpouseQtr").prop("disabled", true);

                $("#ddlHRACat").val(''); $("#ddlHRACat").prop("disabled", true);
                $("#txtrentAmount").val(''); $("#txtrentAmount").prop("disabled", true);
            }
    });
    $("#ddlHRA").change(function () {

        var B = $("#ddlHRA").val();
        if (B == '0') {
            $("#ddlQuatrAlt").val(0); 
        }
        else if ( B > 0) {
            $("#ddlQuatrAlt").val('N');
        }
        else if (B == '') {
            $("#ddlQuatrAlt").val(''); 
        }
    });

    $('#chkHRA').click(function () {

        if ($("#chkHRA").is(':checked')) {
            $("#txtmHRA").prop("disabled", false); $("#txtmHRA").focus(); $("#txtmHRA").val('');
            $("#ddlQuatrAlt").prop("disabled", true); $("#ddlQuatrAlt").val('');
            $("#ddlHRA").prop("disabled", true); $("#ddlHRA").val('');
            $("#ddlSpouseQtr").prop("disabled", true); $("#ddlSpouseQtr").val('');
            $("#txtSpouseAmt").prop("disabled", true); $("#txtSpouseAmt").val('');
            $("#ddlHRACat").prop("disabled", true); $("#ddlHRACat").val('');
            $("#txtrentAmount").prop("disabled", true); $("#txtrentAmount").val('');
        }
        else {
            $("#txtmHRA").prop("disabled", true); $("#txtmHRA").val('');
            $("#ddlQuatrAlt").prop("disabled", false); $("#ddlQuatrAlt").val('');
            $("#ddlHRA").prop("disabled", false); $("#ddlHRA").val('');
            $("#ddlSpouseQtr").prop("disabled", false); $("#ddlSpouseQtr").val('');
            $("#txtSpouseAmt").prop("disabled", true); $("#txtSpouseAmt").val('');
            $("#ddlHRACat").prop("disabled", false); $("#ddlHRACat").val('');
            $("#txtrentAmount").prop("disabled", false); $("#txtrentAmount").val('');

            $("#ddlQuatrAlt").val('N');

            var MaxArrary = [];
            $('#ddlHRA option').filter(function () {
                var textValueID = $.trim($(this).val());
                if (textValueID != '')
                    MaxArrary.push(textValueID);

            })
            var MaxID = Math.max.apply(Math, MaxArrary);
            $("#ddlHRA").val(MaxID);

            $("#ddlSpouseQtr").val('N');
        }
    });

    $('#btnRefresh').click(function () {
        location.reload();
    });
    $('#btnSave').click(function () {
        var Master = {}; var Detail = [];
        

                var DOB = '';   //1500-01-01
                if ($.trim($('#txtDOB').val()) != "") {
                    var ddob = ($.trim($('#txtDOB').val())).split('/');
                    DOB = ddob[2] + "-" + ddob[1] + "-" + ddob[0];
                }
                var DOJ = '';
                if ($.trim($('#txtDOJ').val()) != "") {
                    var ddoj = ($.trim($('#txtDOJ').val())).split('/');
                    DOJ = ddoj[2] + "-" + ddoj[1] + "-" + ddoj[0];
                }
                var DOJDept = '';
                if ($.trim($('#txtDOJDept').val()) != "") {
                    var dDOJDept = ($.trim($('#txtDOJDept').val())).split('/');
                    DOJDept = dDOJDept[2] + "-" + dDOJDept[1] + "-" + dDOJDept[0];
                }
                var DOR = '';
                if ($.trim($('#txtDOR').val()) != "") {
                    var dDOR = ($.trim($('#txtDOR').val())).split('/');
                    DOR = dDOR[2] + "-" + dDOR[1] + "-" + dDOR[0];
                }
                var DNI = '';
                if ($.trim($('#txtDNI').val()) != "") {
                    var dDNI = ($.trim($('#txtDNI').val())).split('/');
                    DNI = dDNI[2] + "-" + dDNI[1] + "-" + dDNI[0];
                }
                var EffectiveFrom = '';
                if ($.trim($('#txtEffFrm').val()) != "") {
                    var dFrom = ($.trim($('#txtEffFrm').val())).split('/');
                    EffectiveFrom = dFrom[2] + "-" + dFrom[1] + "-" + dFrom[0];
                }
                var EffectiveTo = '';
                if ($.trim($('#txtEffTo').val()) != "") {
                    var dTo = ($.trim($('#txtEffTo').val())).split('/');
                    EffectiveTo = dTo[2] + "-" + dTo[1] + "-" + dTo[0];
                }

                var hraCategory = "";
                if ($.trim($("#ddlQuatrAlt").val()) == "Y" || ($.trim($("#ddlHRACat").val()) == 1 || $.trim($("#ddlHRACat").val()) == 2 || $.trim($("#ddlHRACat").val()) == 3))
                    hraCategory = $.trim($("#ddlHRACat").val());

                if ($('#chkHRA').prop("checked") == true) {
                    if ($.trim($("#txtmHRA").val()) == "") {
                        alert('Please Enter HRA Fixed Amount !'); return false;
                    }
                    hraCategory = 4;
                }
                if ($.trim($("#ddlQuatrAlt").val()) == "N" && $.trim($("#ddlHRA").val()) > 0) {
                    hraCategory = 5;
                }
                if ($.trim($("#ddlQuatrAlt").val()) == "N" && $.trim($("#ddlHRA").val()) > 0 && $.trim($("#ddlSpouseQtr").val()) == "Y" && $.trim($("#txtSpouseAmt").val()) > 0) {
                    hraCategory = 7;
                }
                if ($.trim($("#ddlQuatrAlt").val()) == "0" && $.trim($("#ddlHRA").val()) == "0") {
                    hraCategory = 6;
                }

                //PHOTO
                var photo_fileUpload = ""; var binimage = "";
                if ($("#fileToUpload").val() != "") {
                    photo_fileUpload = $("#fileToUpload").get(0);
                    if (photo_fileUpload != "") {
                        var photo_files = photo_fileUpload.files;
                        var Photo_filename = photo_files[0].name;

                        var photo_Stream = $("#txtPhotoStream").val();
                    }
                }

                //SIGNATURE
                var sig_fileUpload = "";
                if ($("#fileToUploadSign").val() != "") {
                    sig_fileUpload = $("#fileToUploadSign").get(0);
                    if (sig_fileUpload != "") {
                        var sig_files = sig_fileUpload.files;
                        var sig_filename = sig_files[0].name;

                        var sig_Stream = $("#txtsigStream").val();
                    }
                }
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

                Master = {
                    EmployeeID: $.trim($('#hdnEmployeeID').val()), EmpNo: $.trim($('#txtEmpNo').val()),  SectorID: $.trim($('#ddlSector').val()), LocationID: $.trim($('#ddlCenter').val()), EmpType: $.trim($('#ddlEmpType').val()), ClassificationID: $.trim($('#ddlClassifctn').val()),
                    EmpName: $.trim($('#txtEmpName').val()), FatherName: $.trim($('#txtFatherName').val()), Religion: $.trim($('#ddlReligion').val()), Qualification: $.trim($('#ddlQualftn').val()), MaritalStatus: $.trim($('#ddlMarital').val()),
                    Caste: $.trim($('#ddlCaste').val()), DOB: $.trim(DOB), Gender: $.trim($('#ddlSex').val()), aadhaarNo: $.trim($('#txtAadhar').val()),

                    EmpAddress: $.trim($('#txtAddress').val()), StateID: $.trim($('#ddlState').val()), DistrictID: $.trim($('#ddlDistrict').val()), City: $.trim($('#txtCity').val()),
                    Pin: $.trim($('#txtPin').val()), Phone: $.trim($('#txtPhone').val()), Mobile: $.trim($('#txtMobile').val()), Email: $.trim($('#txtEmail').val()),
                    PermAddress: $.trim($('#txtAddress_P').val()), PermState: $.trim($('#ddlState_P').val()), PermDistrict: $.trim($('#ddlDistrict_P').val()), PermCity: $.trim($('#txtCity_P').val()),
                    PermPin: $.trim($('#txtPin_P').val()), PermPhone: $.trim($('#txtPhone_P').val()),

                    DOJ: $.trim(DOJ), DOJPresentDept: $.trim(DOJDept), DOR: $.trim(DOR), PhysicalHandicapped: $.trim($('#ddlPhysical').val()), Designation: $.trim($('#ddlDesig').val()),
                    Group: $.trim($('#ddlGroup').val()), Cooperative: $.trim($('#ddlCoop').val()), CoopMembership: $.trim($('#txtCoMembership').val()), Category: $.trim($('#ddlCategory').val()),
                    DA: $.trim($('#ddlDA').val()), Status: $.trim($('#ddlStatus').val()), PFCode: $.trim($('#txtPFCode').val()), PayScaleType: $.trim($('#ddlPayScaleType').val()),
                    PayScaleID: $.trim($('#ddlPayScale').val()), DNI: $.trim(DNI), PAN: $.trim($('#txtPAN').val()), HRACat: $.trim(hraCategory), HRAFixedAmount: $.trim($('#txtmHRA').val()),
                    QuaterAllot: $.trim($('#ddlQuatrAlt').val()), HRAID: $.trim($('#ddlHRA').val()), SpouseQuater: $.trim($('#ddlSpouseQtr').val()), SpouseAmount: $.trim($('#txtSpouseAmt').val()),
                    LicenceFees: $.trim($('#txtrentAmount').val()), HealthScheme: $.trim($('#ddlHealth').val()), HealthSchemeDetail: $.trim($('#txtHealthScheme').val()),
                    EffectiveFrom: $.trim(EffectiveFrom), EffectiveTo: $.trim(EffectiveTo), LWP: $.trim($('#txtLWP').val()), IRID: $.trim($('#ddlIR').val()), UAN: $.trim($('#txtUAN').val()),
                    PFPercent: $.trim($('#ddlPercent').val()), PFValue: $.trim($('#txtValue').val()),

                    BranchID: $.trim($('#ddlBranch').val()), BankAccCode: $.trim($('#txtBankCode').val()),
                    Photo: $.trim(photo_Stream), Signature: $.trim(sig_Stream)
                };

                var Detailxml = "<DetailGrid>";
                $.each($('#tbl tr.allData'), function (index, value) {
                    Detailxml += "<GridDetails>";

                    Detailxml += "<EDID>";
                    Detailxml += $(this).find('.edids').text();
                    Detailxml += "</EDID>";

                    Detailxml += "<EDAmount>";
                    Detailxml += $(this).find('.edamounts').text();
                    Detailxml += "</EDAmount>";

                    Detailxml += "</GridDetails>";
                });
                Detailxml += "</DetailGrid>";

                var E1 = JSON.stringify(Master);

               
                    URL = "";
                    var TextType = $('#btnSave').text();
                    if (TextType == 'Save')
                        URL = "/Administration/Master/EmployeeMaster/Save";
                    else
                        URL = "/Administration/Master/EmployeeMaster/Update";

                    var proc_result = Validation_Procedure(E1);

                    if (proc_result == true) {
                        var result = Validation();
                        if (result == true) {
                            var E = "{Master: " + E1 + ", Detail: '" + Detailxml + "'}";
                            $.ajax({
                                type: "POST",
                                url: URL,
                                data: E,
                                async: false,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (D) {
                                    var a = D.Data; 
                                    var t = a.split('#');

                                    if (t[0] == 'success' && t[1] != '') {
                                        Upload_Photo(t[1]); Upload_Sig(t[1]);
                                        alert('Employee details are Saved Successfully !'); location.reload(); return false;
                                    }
                                    if (t[0] == 'updated' && t[1] != '') {
                                        Upload_Photo(t[1]); Upload_Sig(t[1]);
                                        alert('Employee details are Updated Successfully !'); location.reload(); return false;
                                    }
                                    if (t[0] == 'Encod') {
                                        alert('Sorry ! Your Salary has been already Processed. You Can not Update any details.'); return false;
                                    }
                                    if (t[0] == 'fail') {
                                        alert('Database Error !!.'); return false;
                                    }
                                    if (t[0] == 'other') {
                                        alert('There is some problem !!'); return false;
                                    }
                                    else {
                                        alert(D.Message); return false;
                                    }
                                }
                            });
                        }
                }
        
    });

    $("#fileToUpload").on("change", readFile_Photo);
    $("#fileToUploadSign").on("change", readFile_Signature);

    $("#txtAmount").keypress(function (event) {
        if ($("#ddlEarnDeduction").val() != '') {
            if ($("#txtAmount").hasClass('Decimal')) {
                return isNumber(event, this)
            }
            else {
                return notNumber(event, this)
            }
        }
        else
            event.preventDefault();
    });

    $('#btnAddPayDetails').click(function () {
        if ($("#ddlEarnDeduction").val() == '') {
            $("#ddlEarnDeduction").focus(); return false;
        }
        if ($("#txtAmount").val() == '') {
            $("#txtAmount").focus(); return false;
        }
        if ($("#txtAmount").val() < 0 || $("#txtAmount").val() == 0) {
            $("#txtAmount").focus(); return false;
        }

        var EarnDeductionIDVal = $("#ddlEarnDeduction").val();
        if (EarnDeductionIDVal == '') {
            $("#ddlEarnDeduction").focus(); $("#txtAmount").val(''); return false;
        }
        var EarnDeductionIDVal = $("#ddlEarnDeduction").val();
        var a = EarnDeductionIDVal.split('#');
        var EarnDeductionID = a[0];

       
        if ((EarnDeductionID == '1') && ($("#txtPAN").val() == "")) {
            alert('Sorry !! Please Enter PAN for ITax.');
            $("#ddlEarnDeduction").val(''); $("#txtAmount").val('');
            $("#txtPAN").prop("placeholder", "Enter PAN");
            $("#txtPAN").addClass("Red");
            return false;
        }

        if (EarnDeductionID == 2)
            Check_PayScaleRange();
        else
        {
            var TextType = $('#btnAddPayDetails').text();
            if (TextType == 'Add')
                Add_PayDetails();
            else
                Add_PayDetails();
        }
           
    });

    $('#ddlStatus').change(function () {
        Bind_EarnDeduction();
    });
    
    $("#txtDOB").change(function (event) {
        Calculate_DOR();
    });

    $("#ddlHRACat").change(function (event) {
        $("#txtrentAmount").val('');
    });

    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $(".allownumericwithoutdecimal").on("keypress keyup blur, onkeydown", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
});

function FieldNameData() {
    $.ajax({
        type: "POST",
        url: "/Administration/Master/EmployeeMaster/Get_FieldName",
        data: "",
        dataType: "json",
        async:false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data; 
            if (t.length > 0) {
                $.each(t, function (index, value) {
                    var MandatoryStyle = '*';
                    var Id = '#' + value.FieldID;
                    var isRound = value.IsRound; 
                    $(Id).text(value.FieldName);
                    $('#spn' + value.FieldID).text(value.IsMand == 1 ? '   ' + MandatoryStyle : '');


                    //isRound == 0 ? ($(".cls_" + value.FieldID).addClass('allownumericwithdecimal')) : ($(".cls_" + value.FieldID).addClass('allownumericwithoutdecimal'));
                    if (isRound == 0)
                    {
                        $(".cls_" + value.FieldID).addClass('allownumericwithdecimal');
                    }
                    else if (isRound == 1)  
                    {
                        $(".cls_" + value.FieldID).addClass('allownumericwithoutdecimal');
                    }
                    else
                    {
                        $(".cls_" + value.FieldID).removeClass('allownumericwithdecimal');
                        $(".cls_" + value.FieldID).removeClass('allownumericwithoutdecimal');
                    }
                        
                        
                });
            }
        }
    });
}
function Upload_Photo(EmpNo)
{
    if ($("#fileToUpload").val() != "") {
        OverLayContainer.css('display', 'block');
        var fileUpload = $("#fileToUpload").get(0);
        var files = fileUpload.files;

        var formData = new window.FormData();
        var filename = files[0].name;

        var C = "{\'name\':\'" + filename + "\'}";

        $.ajaxFileUpload(
    {
        url: '/AjaxFileUpload.ashx?photoFolder=UploadPhoto&EmpNo=' + EmpNo,
        async: false,
        secureuri: false,
        fileElementId: 'fileToUpload',
        dataType: 'json',
        data: C,
        success: function (data, status) {
            OverLayContainer.css('display', 'none')
           // alert(data.filepath);
        }
    }
   )
        return false;
    }

}
function Upload_Sig(EmpNo) {
    if ($("#fileToUploadSign").val() != "") {
        OverLayContainer.css('display', 'block');
        var fileUpload = $("#fileToUploadSign").get(0);
        var files = fileUpload.files;

        var formData = new window.FormData();
        var filename = files[0].name;

        var C = "{\'name\':\'" + filename + "\'}";

        $.ajaxFileUpload(
    {
        url: '/AjaxFileUpload.ashx?sigFolder=UploadSign&EmpNo=' + EmpNo,
        async: false,
        secureuri: false,
        fileElementId: 'fileToUploadSign',
        dataType: 'json',
        data: C,
        success: function (data, status) {
            OverLayContainer.css('display', 'none');
            //alert(data.filepath);
        }
    }
   )
        return false;
    }

}

var readFile_Photo = function () {
    var a = this.files[0].name;
    if (a != "")
    {
        var validExtensions = ['jpg', 'gif']; //array of valid extensions
        var fileName = a;
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
        if ($.inArray(fileNameExt, validExtensions) == -1) {
            alert("Invalid file type");
            $("#fileToUpload").val('');
            document.getElementById("canPhoto").src = '';
            return false;
        }
    }
    if (this.files && this.files[0]) {

        var FR = new FileReader();

        FR.addEventListener("load", function (e) {
            document.getElementById("canPhoto").src = e.target.result;
            $("#txtPhotoStream").val(FR.result);
        });

        FR.readAsDataURL(this.files[0]);
    }
    
}
var readFile_Signature = function () {
    var a = this.files[0].name;
    if (a != "") {
        var validExtensions = ['jpg', 'gif', 'jpeg']; //array of valid extensions
        var fileName = a;
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
        if ($.inArray(fileNameExt, validExtensions) == -1) {
            alert("Invalid file type");
            $("#fileToUploadSign").val('');
            document.getElementById("canSign").src = '';
            return false;
        }

    }
    if (this.files && this.files[0]) {

        var FR = new FileReader();

        FR.addEventListener("load", function (e) {
            document.getElementById("canSign").src = e.target.result;
            $("#txtsigStream").val(FR.result);
        });

        FR.readAsDataURL(this.files[0]);
       
    }

}

function Bind_DA()
{
    var s = $("#ddlEmpType").val();

    if (s != "") {
        var E = "{EmpType: '" + $('#ddlEmpType').val() + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/EmployeeMaster/Bind_DA",
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0) {
                    $("#ddlDA").empty();
                    $("#ddlDA").append("<option value=''>-- Select --</option>")
                    $("#ddlDA").append("<option value='0'>Not Applicable</option>")
                    $.each(t, function (index, value) {
                        var label = value.DARate;
                        var val = value.DAID;
                        $("#ddlDA").append($("<option selected></option>").val(val).html(label));
                    });
                }
            }
        });
    }
    else {
        $("#ddlDA").empty();
        $("#ddlDA").append("<option value=''>-- Select --</option>")
        $("#ddlDA").append("<option value='0'>Not Applicable</option>")
    }
}
function Bind_HRA() {
    var s = $("#ddlEmpType").val();

    if (s != "") {
        var E = "{EmpType: '" + $('#ddlEmpType').val() + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/EmployeeMaster/Bind_HRA",
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0) {
                    $("#ddlHRA").empty();
                    $("#ddlHRA").append("<option value=''>-- Select --</option>")
                    $("#ddlHRA").append("<option value='0'>Not Applicable</option>")
                    $.each(t, function (index, value) {
                        var label = value.HRA;
                        var val = value.HRAID;
                        $("#ddlHRA").append($("<option selected></option>").val(val).html(label));
                    });
                }
            }
        });
    }
    else {
        $("#ddlHRA").empty();
        $("#ddlHRA").append("<option value=''>-- Select --</option>")
        $("#ddlHRA").append("<option value='0'>Not Applicable</option>")
    }
}
function Bind_PayScaleType() {
    var s = $("#ddlEmpType").val();

    if (s != "") {
        var E = "{EmpType: '" + $('#ddlEmpType').val() + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/EmployeeMaster/Bind_PayScaleType",
            data: E,
            async:false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0) {
                    $("#ddlPayScaleType").empty();
                    $("#ddlPayScaleType").append("<option value=''>-- Select --</option>")
                    $.each(t, function (index, value) {
                        var label = value.PayScale;
                        var val = value.PayScaleID;
                        $("#ddlPayScaleType").append($("<option></option>").val(val).html(label));
                    });
                }
            }
        });
    }
    else {
        $("#ddlPayScaleType").empty();
        $("#ddlPayScaleType").append("<option value=''>-- Select --</option>")
    }
}
function Bind_PayScale() {
    var s = $("#ddlPayScaleType").val();
   
    if (s != "") {
        var E = "{PayScaleType: '" + $('#ddlPayScaleType').val() + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/EmployeeMaster/Bind_PayScale",
            data: E,
            async:false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0) {
                    $("#ddlPayScale").empty();
                    $("#ddlPayScale").append("<option value=''>-- Select --</option>")
                    $.each(t, function (index, value) {
                        var label = value.PayScale;
                        var val = value.PayScaleID;
                        $("#ddlPayScale").append($("<option></option>").val(val).html(label));
                    });
                }
            }
        });
    }
    else {
        $("#ddlPayScale").empty();
        $("#ddlPayScale").append("<option value=''>-- Select --</option>")
    }
}
function Bind_IR()
{
    var s = $("#ddlEmpType").val();

    if (s != "") {
        var E = "{EmpType: '" + $('#ddlEmpType').val() + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/EmployeeMaster/Bind_IR",
            data: E,
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0) {
                    $("#ddlIR").empty();
                    $("#ddlIR").append("<option value=''>-- Select --</option>");
                    $("#ddlIR").append("<option value='0'>-- Not Applicable --</option>");
                    $.each(t, function (index, value) {
                        var label = value.IRRate;
                        var val = value.IRID;
                        $("#ddlIR").append($("<option></option>").val(val).html(label));
                    });
                }
            }
        });
    }
    else {
        $("#ddlIR").empty();
        $("#ddlIR").append("<option value=''>-- Select --</option>");
        $("#ddlIR").append("<option value='0'>-- Not Applicable --</option>");
    }
}
function Bind_EarnDeduction()
{
    if ($("#ddlStatus").val() != '') {
        var EmpCategory = $("#ddlCategory").val();
        var StatusIDVal = $("#ddlStatus").val();
        var a = StatusIDVal.split('#');
        var StatusID = a[0];
        if (EmpCategory == '') {
            alert('Please Select Employee Category !'); return false;
        }
        if (StatusIDVal == '') {
            alert('Please Select Employee Status !'); return false;
        }

        var E = "{EmpCategory: '" + $.trim(EmpCategory) + "', StatusID: '" + $.trim(StatusID) + "'}";

        $.ajax({
            type: "POST",
            url: "/Administration/Master/EmployeeMaster/Bind_EarnDeduction", 
            data: E,
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (lstEarnDeduction) {
                var a = lstEarnDeduction.Data.length;
                if (a > 0) {
                    $("#ddlEarnDeduction").empty();
                    $("#ddlEarnDeduction").append("<option value=''>-- Select --</option>")
                    $.each(lstEarnDeduction.Data, function (index, value) {
                        var label = value.EarnDeduction;
                        var val = value.EDID;
                        $("#ddlEarnDeduction").append($("<option></option>").val(val).html(label));
                    });
                }
            }
        });
    }
}
function Bind_PFPercent()
{
    var s = $("#ddlEmpType").val();

    if (s != "") {
        var E = "{EmpType: '" + $('#ddlEmpType').val() + "'}"; 
        $.ajax({
            type: "POST",
            url: "/Administration/Master/EmployeeMaster/Bind_PFPercentage",
            data: E,
            async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                var t = D.Data;

                if (t.length > 0) {
                    $("#ddlPercent").empty();
                    $("#ddlPercent").append("<option value=''>-- Select --</option>")
                    $("#ddlPercent").append("<option value='0'>Not Applicable</option>");
                    var lstPFType = [];
                    $.each(t, function (index, value) {
                        var label = value.PFRate;
                        var val = value.ID;
                        var pfType = value.PFType;
                        lstPFType.push(pfType);
                        if (pfType == 'P' && label !=null)
                            $("#ddlPercent").append($("<option selected></option>").val(val).html(label));
                    });
                  
                    var pResult = checkValue('P', lstPFType); //alert(pResult);
                    var vResult = checkValue('V', lstPFType); //alert(vResult);

                    $("#chkPercent").prop("checked", false); $("#ddlPercent").val('');
                    $("#chkValue").prop("checked", false); $("#txtValue").val('');
                    if (pResult == '')
                    {
                        $("#chkPercent").prop("disabled", false); 
                    }
                    else
                    {
                        $("#chkPercent").prop("disabled", true);
                        $("#chkPercent").prop("checked", false);
                        $("#ddlPercent").hide(); $("#ddlPercent").val('');
                    }
                        

                    if (vResult == '')
                    {
                        $("#chkValue").prop("disabled", false);
                    }
                    else
                    {
                        $("#chkValue").prop("disabled", true);
                        $("#chkValue").prop("checked", false);
                        $("#txtValue").hide(); $("#txtValue").val('');
                    }
                }
            }
        });
    }
    else {
        $("#ddlPercent").empty();
        $("#ddlPercent").append("<option value=''>-- Select --</option>")
        $("#ddlPercent").append("<option value='0'>Not Applicable</option>")
    }
}

function CheckPAN() {
    if ($.trim($("#txtPAN").val()).length == "9") {
        var ObjVal = $("#txtPAN").val().toUpperCase();
        var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
        var code = /([C,P,H,F,A,T,B,L,J,G])/;
        var code_chk = ObjVal.substring(3, 4).toUpperCase();
        if (ObjVal.search(panPat) == -1) {
            var msg = "Invalid Pan No";
            alert(msg);
            $("#txtPAN").focus();
            return false;
        }
        else if (code.test(code_chk) == false) {
            var msg = "Invalid Pan Card No";
            alert(msg);
            return false;
        }
        else {
            return true;
        }
    }
}
function Bind_Branch(BranchID)
{
        var options = {};
        options.url = "/Administration/Master/EmployeeMaster/Bind_BankBranch";
        options.type = "POST";
        options.data = "{BankID: " + $('#ddlBank').val() + "}";
        options.dataType = "json";
        options.contentType = "application/json";
        options.success = function (listbranch) {
            var a = listbranch.Data.length;
            if (a > 0) {
                $("#ddlBranch").empty();
                $("#ddlBranch").append("<option value=''>-- Select --</option>")
                $.each(listbranch.Data, function (index, value) {
                    var label = value.Branch;
                    var val = value.BranchID;
                    $("#ddlBranch").append($("<option></option>").val(val).html(label));
                });
  
                    $("#ddlBranch").val(BranchID);
            }

        };
        options.error = function () { alert("Error retrieving BankName!"); };
        $.ajax(options);


}

function Bind_District()
{
    var s = $('#ddlState').val();
    if (s != '') {
        var E = "{StateID: " + $('#ddlState').val() + "}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/EmployeeMaster/Bind_District",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (listDistrict) {
                var a = listDistrict.Data.length;
                if (a > 0) {
                    $("#ddlDistrict").empty();
                    $("#ddlDistrict").append("<option value=''>-- Select --</option>")
                    $.each(listDistrict.Data, function (index, value) {
                        var label = value.District;
                        var val = value.DistID;
                        $("#ddlDistrict").append($("<option></option>").val(val).html(label));
                    });
                }
            }
        });
    }
}
function Bind_PermDistrict() {
    var s = $('#ddlState_P').val();
    if (s != '') {
        var E = "{StateID: " + $('#ddlState_P').val() + "}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/EmployeeMaster/Bind_District",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (listDistrict) {
                var a = listDistrict.Data.length;
                if (a > 0) {
                    $("#ddlDistrict_P").empty();
                    $("#ddlDistrict_P").append("<option value=''>-- Select --</option>")
                    $.each(listDistrict.Data, function (index, value) {
                        var label = value.District;
                        var val = value.DistID;
                        $("#ddlDistrict_P").append($("<option></option>").val(val).html(label));
                    });
                }
            }
        });
    }
}

function Populate_Employee(EmpNo)
{
    var EmployeeNo = $("#hdnEmployeeNo").val();
    var SectorID = $("#ddlSector").val();


    if (EmpNo != '')
    {
        var E = "{EmployeeNo: '" + EmpNo + "', SectorID: '" + SectorID + "'}"; 
        $.ajax({
            type: "POST",
            url: "/Administration/Master/EmployeeMaster/Get_Employee",
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                var t = D.Data; 
                var table =t.Table;
                var table1=t.Table1;
                var table2=t.Table2;

                if (table.length > 0)
                {
                    Bind_Table(table);
                }
            }
        });
    }
}
function Populate_BankandBranch(BranchID)
{
    E = "{BranchID:'" + BranchID + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/EmployeeMaster/Get_BankandBranch",
        data: E,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {

                $('#ddlBank').val(t[0].BankID);

                if ($('#ddlBank').val() != "")
                    Bind_Branch(t[0].BranchID);

                $("#txtIFSC").val(t[0].IFSC);
                $("#txtMICR").val(t[0].MICR);
            }
        }
    });
}
function Bind_Table(table)
{
    if (table.length > 0) {
        //Personal
        $('#txtEmpNo').val(table[0].EmpNo); $('#ddlCenter').val(table[0].CenterID); $('#ddlEmpType').val(table[0].EmpType);
        $('#ddlClassifctn').val(table[0].ClassificationID); $('#txtEmpName').val(table[0].EmpName); $('#txtFatherName').val(table[0].FatherName); $('#ddlReligion').val(table[0].Religion);
        $('#ddlQualftn').val(table[0].Qualification); $('#ddlMarital').val(table[0].MaritalStatus); $('#ddlMarital').val(table[0].MaritalStatus);
        $('#ddlCaste').val(table[0].Caste); $('#txtDOB').val(table[0].EmpDob); Calculate_DOR(); $('#ddlSex').val(table[0].Gender); $('#txtAadhar').val(table[0].aadhaarNo);
        //Address
        $('#txtAddress').val(table[0].EmpAddress); $('#ddlState').val(table[0].StateID); Bind_District(); $('#ddlDistrict').val(table[0].DistrictID);
        $('#txtCity').val(table[0].City); $('#txtPin').val(table[0].Pin); $('#txtPhone').val(table[0].Phone); $('#txtMobile').val(table[0].Mobile); $('#txtEmail').val(table[0].Email);
        $('#txtAddress_P').val(table[0].PermAddress); $('#ddlState_P').val(table[0].PermState); Bind_PermDistrict(); $('#ddlDistrict_P').val(table[0].Perm_District);
        $('#txtCity_P').val(table[0].Perm_City); $('#txtPin_P').val(table[0].Perm_Pin); $('#txtPhone_P').val(table[0].Perm_Phone);
        //Official
        $('#txtDOJ').val(table[0].EmpDOJ); $('#txtDOJDept').val(table[0].EmpDOJPresentDept); $('#txtDOR').val(table[0].EmpDOR); $('#ddlPhysical').val(table[0].PhysicalHandicapped);
        $('#ddlDesig').val(table[0].Designation); $('#ddlGroup').val(table[0].Group); $('#ddlCoop').val(table[0].Cooperative); $('#txtCoMembership').val(table[0].CoopMembership);
        $('#ddlCategory').val(table[0].Category); Bind_Status(); $('#ddlDA').val(table[0].DA);  $('#txtPFCode').val(table[0].PFCode);
        Bind_PayScaleType();
        $('#ddlPayScaleType').val(table[0].PayScaleType); Bind_PayScale(); $('#ddlPayScale').val(table[0].PayScaleID); $('#txtDNI').val(table[0].EmpDNI); $('#txtPAN').val(table[0].PAN);
       
      
        $('#ddlStatus option').filter(function () {
            var textValueID = $.trim($(this).val());
            if (textValueID != '')
            {
                var a = textValueID.split('#'); 
                var Status = a[0]; 
                if (Status == table[0].Status) {
                    $('#ddlStatus').val(textValueID); Bind_EarnDeduction(); return false;
                }
            }
        })

        Hide_Show(table[0].HRACat);

        if (table[0].HRAFixedAmount > 0) {
            $("#chkHRA").prop("checked", true);
            $("#txtmHRA").val(table[0].HRAFixedAmount);
        }
        else {
            $("#chkHRA").prop("checked", false);
            $("#txtmHRA").val('');
        }

        
        $("#ddlQuatrAlt").val(table[0].QuaterAllot);
        $("#ddlHRA").val(table[0].HRAID);
        $("#ddlSpouseQtr").val(table[0].SpouseQuater);
        $("#txtSpouseAmt").val(table[0].SpouseAmount);

        $("#txtrentAmount").val(table[0].LicenceFees);  //Quarter Rent Amount
        $("#ddlHealth").val(table[0].HealthScheme);$("#txtHealthScheme").val(table[0].HealthSchemeDetail);
        $("#txtEffFrm").val(table[0].EffectiveFrom); $("#txtEffTo").val(table[0].EffectiveTo);
        $("#txtLWP").val(table[0].LWP); 
        Bind_IR(); $("#ddlIR").val(table[0].IRID); $("#txtUAN").val(table[0].UAN);


        //PFDetails
        
        if (table[0].PFPercent != '' && table[0].PFPercent !=null)
        {
            $("#chkPercent").prop("checked", true); $("#ddlPercent").show(); $("#ddlPercent").val(table[0].PFPercent);
        }
        if (table[0].PFValue != '' && table[0].PFValue != null) {
            $("#chkValue").prop("checked", true); $("#txtValue").show(); $("#txtValue").val(table[0].PFValue);
        }

        //BANK DETAILS
        Populate_BankandBranch(table[0].BranchID);
        $("#txtBankCode").val(table[0].BankAccCode);

        
        //PHOTO & SIGNATURE
        //$("#fileToUpload").val(table[0].Photo); 
        $('#canPhoto').attr('src', '/' + table[0].Photo); $('#canSign').attr('src', '/' + table[0].Signature);
       

        Show_PayDetails(table[0].EmpNo);

        $("#btnSave").html('Update');
    }
}

function Show_PayDetails(EmpNo)
{
    var StatusIDVal = $("#ddlStatus").val();
    var b = StatusIDVal.split('#');
    var StatusID = b[0];
    var MandatoryID = b[2];
    var EDName = b[3];
    var IDforMandatory = []; IDforMandatory = MandatoryID.split(',');
    var IDforEdit = []; IDforEdit = b[1].split(',');
    //*********************************************************************************************************************************************
   
    var E = "{EmpNo: '" + EmpNo + "'}"; 
    $.ajax({
        type: "POST",
        url: "/Administration/Master/EmployeeMaster/Show_PayDetails",
        data: E,
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            var t = D.Data; 
            if (t.length > 0) {
                $("#tbl tbody tr.allData").remove();
               
                for (var i = 0; i < t.length; i++) {
                    var EDID = t[i].EDID;
                    var hide = checkValue(EDID, IDforEdit);

                    if (EDID == 2 || EDID == 9 || EDID == 3 || EDID == 4 || EDID == 7 || EDID == 5 || EDID == 39 || EDID == 14) {
                        if (t[i].EDAmount > 0) {
                            $("#tbl").append("<tr class='trdata_Basic allData'><td style='display:none;' class='BEDID edids allEDID'>" +
                                            t[i].EDID + "</td><td class='BEDName ednames'>" +
                                            t[i].EDName + "</td><td class='BEDAmount edamounts' style='text-align:right'>" +
                                            t[i].EDAmount + "</td><td class='edtype' style='display:none;'>" +
                                            t[i].EDType + "</td><td class='Label'>" +
                                            "<img style='display:" + hide + " ; cursor:pointer;' src='/Content/Images/Edit.png' alt='cross' height=20 width=20 onClick='Update_Data(this)' /></td></tr>");
                        }
                    }
                    else {
                      
                            var EDID = t[i].EDID;
                            var hide = checkValue(EDID, IDforEdit);
                            if (t[i].EDAmount > 0) {
                                $("#tbl").append("<tr class='trdata allData'><td style='display:none;' class='EDID edids allEDID'>" +
                                                t[i].EDID + "</td><td class='EDName ednames'>" +
                                                t[i].EDName + "</td><td class='EDAmount edamounts' style='text-align:right'>" +
                                                t[i].EDAmount + "</td><td class='edtype' style='display:none;'>" +
                                                t[i].EDType + "</td><td class='Label'>" +
                                                "<img style='display:" + hide + " ; cursor:pointer;' src='/Content/Images/Edit.png' alt='cross' height=20 width=20 onClick='Update_Data(this)' /></td></tr>");
                            }
                    }
                }
            }
            Calculate_Total();
        }
    });
}

function Hide_Show(HRACat)
{
    var E = "{HRACat: '" + HRACat + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/EmployeeMaster/HideShow",
        data: E,
        async:false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            var t = D.Data;
            if(t.length>0)
            {
                //FIXED HRA
                var fixedHRA = t[0].V_FixedHra;
                var fixedHRAAmount = t[0].V_FixedHraAmount;
                if (fixedHRA == 1 && fixedHRAAmount ==1)
                {
                    $("#chkHRA").prop("checked", true); $("#txtmHRA").prop("disabled", false); 
                }
                else {
                    $("#chkHRA").prop("checked", false); $("#txtmHRA").prop("disabled", true);
                }
                //QuaraterAllot
                var quarterAllot = t[0].V_QuarterAllot;
                if (quarterAllot == 1) { $("#ddlQuatrAlt").prop("disabled", false); } else { $("#ddlQuatrAlt").prop("disabled", true); }
                //HRA
                var hraID = t[0].V_hraid;
                if (hraID == 1) { $("#ddlHRA").prop("disabled", false); } else { $("#ddlHRA").prop("disabled", true); }
                //SPOUSE HRA
                var spouseHRA = t[0].V_spouseHra;
                if (spouseHRA == 1) { $("#ddlSpouseQtr").prop("disabled", false); } else { $("#ddlSpouseQtr").prop("disabled", true); }
                //SPOUSE HRA AMOUNT
                var spouseHRAAmt = t[0].V_SpouseHraAmount;
                if (spouseHRAAmt == 1) { $("#txtSpouseAmt").prop("disabled", false); } else { $("#txtSpouseAmt").prop("disabled", true); }
                //HRA CATEGORY
                var hraCat = t[0].V_HraCat;
                if (hraCat == 1) { $("#ddlHRACat").prop("disabled", false); } else { $("#ddlHRACat").prop("disabled", true); }
                //HRA RENT AMOUNT
                var rentAmt = t[0].V_rentAmount; //alert(t[0].V_rentAmount);
                if (rentAmt == 1) { $("#txtrentAmount").prop("disabled", false); } else { $("#txtrentAmount").prop("disabled", true); }


                var visibility = t[0].Visibility;
                if(visibility == 0)
                    $("#ddlHRACat").val('');
                else
                    $("#ddlHRACat").val(HRACat);

            }
        }
    });
}
function Calculate_DOR()
{
    var DOB = $('#txtDOB').val(); var DOR = ""; var DORYear = ""; var date = ""; var month = ""; var year = "";
    dorDate = ""; var dorMonth = ""; var dorYear = "";
    //alert(DOB);
    var a = DOB.split('/');
    for (var i = 0; i < a.length; i++) {
        date = a[0]; month = a[1]; year = a[a.length - 1];
    }
    DORYear = (parseInt(year) + parseInt(60)); //alert(DORYear);
    if (date == 1) {

        //alert(date); alert(month); alert(year);
        //var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        var newdate = new Date(year, month - 1, date); //alert(newdate);
        var lastDay = new Date(newdate.getFullYear(), newdate.getMonth(), 0); //alert(lastDay);
        dorDate = lastDay.getDate() < 10 ? '0' + lastDay.getDate() : lastDay.getDate(); //alert(dorDate);



        if (lastDay.getMonth() == 9)
            dorMonth = parseInt(lastDay.getMonth()) + 1;
        else
            dorMonth = lastDay.getMonth() < 10 ? '0' + (parseInt(lastDay.getMonth()) + 1) : (parseInt(lastDay.getMonth()) + 1); //alert(dorMonth);


        dorYear = lastDay.getFullYear();
        //DOR = dorDate + '/' + dorMonth + '/' + DORYear; 
        DOR = dorDate + '/' + dorMonth + '/' + (parseInt(dorYear) + parseInt(60));
        $("#txtDOR").val(DOR);

    } else {

        var newdate = new Date(year, month - 1, date); //alert(newdate);
        var lastDay = new Date(newdate.getFullYear(), newdate.getMonth() + 1, 0); //alert(lastDay);
        dorDate = lastDay.getDate() < 10 ? '0' + lastDay.getDate() : lastDay.getDate(); //alert(dorDate);

        var months = (parseInt(lastDay.getMonth()) + 1); //alert(months);
        if (months == 9)
            dorMonth = months < 10 ? '0' + months : months;
        else
            dorMonth = months < 10 ? '0' + months : months; //alert(dorMonth);


        dorYear = lastDay.getFullYear();
        //DOR = dorDate + '/' + dorMonth + '/' + DORYear; //alert(DOR);
        DOR = dorDate + '/' + dorMonth + '/' + (parseInt(dorYear) + parseInt(60));;
        $("#txtDOR").val(DOR);
    }


    if (DOB != "") {
        //var today = new Date(now.getYear(), now.getMonth(), now.getDate());
        var today = new Date();
        var yearNow = today.getFullYear(); //alert(yearNow);
        var monthNow = today.getMonth();
        var dateNow = today.getDate();

        var dob = new Date(DOB.substring(6, 10),
                           DOB.substring(3, 5) - 1,
                           DOB.substring(0, 2)
                           ); //alert(dob);

        var yearDob = dob.getFullYear(); //alert(yearDob);
        var monthDob = dob.getMonth();
        var dateDob = dob.getDate();
        //var birthyear = DOB.substring(6, 10);
        //var birthmonth = DOB.substring(3, 5) - 1;
        //var birthday = DOB.substring(0, 2); alert(birthday);

        var age = {};
        var ageString = "";
        var yearString = "";
        var monthString = "";
        var dayString = "";


        yearAge = yearNow - yearDob; //alert(yearAge);

        if (monthNow >= monthDob)
            var monthAge = monthNow - monthDob;
        else {
            yearAge--;
            var monthAge = 12 + monthNow - monthDob;
        }

        if (dateNow >= dateDob)
            var dateAge = dateNow - dateDob;
        else {
            monthAge--;
            var dateAge = 31 + dateNow - dateDob;

            if (monthAge < 0) {
                monthAge = 11;
                yearAge--;
            }
        }

        age = {
            years: yearAge,
            months: monthAge,
            days: dateAge
        };
        //alert(JSON.stringify(yearAge));

        $('#lblYears').show();
        $('#lblMonths').show();
        $('#lblDa').show();

        $('#lblYear').show();
        $('#lblMonth').show();
        $('#lblDay').show();

        $('#lblYear').text(age.years);
        $('#lblMonth').text(age.months);
        $('#lblDay').text(age.days);
    }
    else {
        $("#lblYears").hide();
        $("#lblMonths").hide();
        $("#lblDa").hide();

        $('#lblYear').hide();
        $('#lblMonth').hide();
        $('#lblDay').hide();
    }
}

function Bind_Status()
{
    var s = $('#ddlCategory').val();
    if (s != '') {
        var E = "{CategoryID: '" + $('#ddlCategory').val() + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/EmployeeMaster/Bind_Status",
            data: E,
            dataType: "json",
            async:false,
            contentType: "application/json; charset=utf-8",
            success: function (listDistrict) {
                var a = listDistrict.Data.length;
                if (a > 0) {
                    $("#ddlStatus").empty();
                    $("#ddlStatus").append("<option value=''>-- Select --</option>")
                    $.each(listDistrict.Data, function (index, value) {
                        var label = value.Status;
                        var val = value.Abbv;
                        $("#ddlStatus").append($("<option></option>").val(val).html(label));
                    });
                }
            }
        });
    }
}

function Validation()
{


 if ($("#ddlStatus").val() != '') {
        var StatusIDVal = $("#ddlStatus").val(); 
        var b = StatusIDVal.split('#');
        var StatusID = b[0];
        var MandatoryID = b[2];
        var EDName = b[3];
        var IDforMandatory = []; IDforMandatory = MandatoryID.split(',');
        var IDforEdit = []; IDforEdit = b[1].split(',');
    }

 //if ($("#ddlCenter").val() == "") {
 //   //alert("Please Select Location."); //$("#address").addClass("active"); $("#pay").removeClass("active");
 //   //$("#ddlCenter").attr("placeholder", "required");
 //   //$("#ddlCenter").focus();
 //   //return false;
 //}
//else if ($("#ddlEmpType").val() == "") {
//     alert("Please Enter Employee Type !"); return false;
// }
 //else   if ($("#txtEmpName").val() == "") {
 //       alert("Please Enter Employee Name !"); return false;
 //}
 //else if ($("#txtDOB").val() == "") {
 //    alert("Please Enter DOB !"); return false;
 //}
 //else if ($("#txtAadhar").val() == "") {
 //    alert("Please Enter Aadhar No. !"); return false;
 //}
 //else if ($("#txtMobile").val() == "") {
 //    alert("Please Enter Mobile No. !"); return false;
 //}
 //else if ($("#txtEmail").val() == "") {
 //    alert("Please Enter Email-ID !"); return false;
 //}
 //else if ($("#txtDOJ").val() == "") {
 //    alert("Please Enter DOJ !"); return false;
 //}
 //else if ($("#txtDOR").val() == "") {
 //    alert("Please Enter DOR !"); return false;
 //}
 //else if ($("#ddlDesig").val() == "") {
 //    alert("Please Select Designation !"); return false;
 //}
 // else  if ($("#ddlCategory").val() == "") {
 //       alert('Please Select Employee Category !'); return false;
 //   }
 // else  if ($("#ddlStatus").val() == "") {
 //   alert('Please Select Employee Status !'); return false;
 // }

  //else if ($("#ddlPayScaleType").val() == "" && $("#ddlCategory").val() != "6") {
  //    alert('Please Select PayScaleType !'); return false;
  //}
  //else if ($("#ddlPayScale").val() == "" && $("#ddlCategory").val() != "6") {
  //    alert('Please Select PayScale !'); return false;
  //}
  //else if ($("#txtPAN").val() == "" && $("#ddlCategory").val() != "6") {
  //    alert('Please Enter PAN !'); return false;
  //}

  if ($('#chkHRA').prop("checked") == true) {
        if ($.trim($("#txtmHRA").val()) == "") {
            alert('Please Enter HRA Fixed Amount !'); return false;
        }
        return true;
    }
  else if ($("#ddlQuatrAlt").val() == "" && $('#chkHRA').prop("checked") == false) {
            alert('Please Select Quarter Allotment for HRA !'); return false;
    }
  else if ($("#ddlQuatrAlt").val() == "Y" && $('#chkHRA').prop("checked") == false && $("#ddlHRACat").val() == "") {
        alert('Please Select HRA Quarter Category !'); return false;
    }
  //else if ($("#ddlQuatrAlt").val() == "Y" && $('#chkHRA').prop("checked") == false && $("#ddlHRACat").val() != "" && $("#txtrentAmount").val() == "") {
  //      alert('Please Enter HRA Quarter Rent Amount !'); return false;
  //  }
  else if ($("#ddlSpouseQtr").val() == "Y" && ($("#txtSpouseAmt").val() == "" || $("#txtSpouseAmt").val() == 0 || $("#txtSpouseAmt").val() < 0)) {
        alert('Please Enter Spouse HRA Amount !'); return false;
  }

  else if (($('#chkPercent').prop("checked") == true || $('#chkValue').prop("checked") == true) && $('#txtPFCode').val() == "") {
      alert('Sorry ! your PFCode is necessary. Please enter PFCode.\nBecause you have selected either PFPercent or PFValue.'); return false;
  }
  else if ($('#chkPercent').prop("checked") == true && $("#ddlPercent").val() == "") {
        alert('Please Select PF Percentage Value !'); return false;
    }
  else if ($('#chkValue').prop("checked") == true && $("#txtValue").val() == "") {
        alert('Please Enter PF Percentage Value !'); return false;
    }
  else if ($("#tbl tbody tr.allData").length == 0 ) {
        alert('Please Enter Employee Pay Details !'); return false;
  }
  else if (IDforMandatory.length>0)
  {
      var boolResult = true;
      if (IDforMandatory != 0) {
          $("#tbl tbody tr.allData").each(function (index, value) {
              var EDID = $(this).find('.allEDID').text();
              if (EDID == IDforMandatory) {
                  boolResult = true; return false;
              }
              else
                  boolResult = false;

          });
          if (boolResult == false) {
              if (EDName != '')
                  alert('Please Add ' + EDName + " in Pay Details."); return false;

          }
      }
      return boolResult;
  }

  else
    return true;



}
function Validation_Procedure(master) {
    var res = "";
    var E = "{Master: " + master + "}"; 
    $.ajax({
        type: "POST",
        url: "/Administration/Master/EmployeeMaster/Check_EmpMasterValidation",
        data: E,
        dataType: "json",
        async:false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
              
                var errCode = t[0].ErrorCode;
                if(errCode == 0)
                {
                    alert(t[0].Messege);
                    var Id = '.cls_' + t[0].FId;
                    $(Id).focus();
                    $(Id).prop("placeholder", t[0].Messege);
                    $(Id).addClass("Red");
                    res = false;
                    return false;
                }
            }
            else
            {
                res = true;
            }
        }
    });
    return res;
}

function ChangePlaceHolderColor() {
    var textBoxes = document.getElementsByTagName("input");
    for (var i = 0; i < textBoxes.length; i++) {
        if (textBoxes[i].type == "text") {
            if (!textBoxes[i].value) {
                textBoxes[i].className += " Red";
            }
        }
    }
}

function Apply_IsRound(evt)
{
    var EarnDeductionIDVal = $("#ddlEarnDeduction").val(); 
    if (EarnDeductionIDVal == '')
    {
        $("#ddlEarnDeduction").focus(); $("#txtAmount").val(''); return false;
    }
    var EarnDeductionIDVal = $("#ddlEarnDeduction").val();
    var a = EarnDeductionIDVal.split('#');
    var EarnDeductionID = a[0];
    var IsRound = a[1];
    var IsEdit = a[2];
    var IsDelete = a[3];
   
    if (IsRound == 1)
        $("#txtAmount").addClass('WithOutDecimal');
    else
        $("#txtAmount").removeClass('WithDecimal');
}
function Check_Validations()
{
    var EarnDeductionIDVal = $("#ddlEarnDeduction").val();
    if (EarnDeductionIDVal == '') {
        $("#ddlEarnDeduction").focus(); $("#txtAmount").val(''); return false;
    }
    var EarnDeductionIDVal = $("#ddlEarnDeduction").val();
    var a = EarnDeductionIDVal.split('#');
    var EarnDeductionID = a[0];
    var IsRound = a[1];
    var IsEdit = a[2];
    var IsDelete = a[3];
   
    if (IsRound == 1)
        $("#txtAmount").addClass('Decimal');
    else
        $("#txtAmount").removeClass('Decimal');

    //****************************************************************************************************************************************** 

    if ($("#ddlPayScale").val() == '') {
        alert('Please Select PayScale.'); $("#ddlEarnDeduction").val(''); return false;
    }
}

function isNumber(evt, element) {

    var charCode = (evt.which) ? evt.which : event.keyCode

    if (
        (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
        (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57))
        return false;

    return true;
}
function notNumber(evt, element) {

    var charCode = (evt.which) ? evt.which : event.keyCode

    if (
        (charCode != 45 || $(element).val().indexOf('-') == -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
        (charCode != 46 || $(element).val().indexOf('.') == -1) &&      // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function Check_PayScaleRange()
{
    var E = "{Amount: '" + $.trim($("#txtAmount").val()) + "', PayScaleID: '" + $.trim($("#ddlPayScale").val()) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/EmployeeMaster/Check_PayScaleRange",
        data: E,
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                var result = t[0].Result;
                if(result == 1)  //1-Amount is within PayScale, 0-not within PayScale
                {
                    Add_PayDetails();
                }
                else
                {
                    alert('Sorry ! Enter Amount is not within PayScale !');  $("#txtAmount").focus(); return false;
                }
            }
            else
            {
                alert('Data related Error, Check the data from database !'); return false;
            }
        }
    });
}
function Add_PayDetails()
{
    //******************************************************* EarnDeduction ***********************************************************************
    var EarnDeductionIDVal = $("#ddlEarnDeduction").val();
    if (EarnDeductionIDVal == '') {
        $("#ddlEarnDeduction").focus(); $("#txtAmount").val(''); return false;
    }
    var EarnDeductionIDVal = $("#ddlEarnDeduction").val();
    var a = EarnDeductionIDVal.split('#');
    var EarnDeductionID = a[0]; 
    var isRound = a[1];
    var FunctionRank = a[2];
    var EDType = a[3];
    //*********************************************************************************************************************************************
    var StatusIDVal = $("#ddlStatus").val();
    var b = StatusIDVal.split('#');
    var StatusID = b[0];
    var MandatoryID = b[2];
    var EDName = b[3];
    var IDforMandatory = []; IDforMandatory = MandatoryID.split(',');
    var IDforEdit = []; IDforEdit = b[1].split(',');
    //*********************************************************************************************************************************************

            if ($.trim($("#ddlEmpType").val()) == '') {
                alert('Please Select EmpType !'); return false;
            }
            if ($.trim($("#ddlCategory").val()) == '') {
                alert('Please Select Employee Category !'); return false;
            }
            if ($.trim($("#ddlStatus").val()) == '') {
                alert('Please Select Status !'); return false;
            }
            if ($.trim($("#ddlPayScale").val()) == '') {
                alert('Please Select PayScale !'); return false;
            }
            if ($.trim($("#txtAmount").val()) == '') {
                alert('Please Enter Amount !'); return false;
            }
            if ($.trim($("#ddlDA").val()) == '') {
                alert('Please Select DA Percentage !'); return false;
            }

            if (EarnDeductionID == 2) {
                if ($.trim($("#ddlPhysical").val()) == '') {
                    alert('Please Select Physical Handicapped !'); return false;
                }
                if ($.trim($("#ddlIR").val()) == '') {
                    alert('Please Select IR Percentage !'); return false;
                }

                var hraCategory = "";
                if ($.trim($("#ddlQuatrAlt").val()) == "Y" || ($.trim($("#ddlHRACat").val()) == 1 || $.trim($("#ddlHRACat").val()) == 2 || $.trim($("#ddlHRACat").val()) == 3))
                    hraCategory = $.trim($("#ddlHRACat").val());

                if ($('#chkHRA').prop("checked") == true) {
                    if ($.trim($("#txtmHRA").val()) == "") {
                        alert('Please Enter HRA Fixed Amount !'); return false;
                    }

                    hraCategory = 4;
                }
                if ($.trim($("#ddlQuatrAlt").val()) == "N" && $.trim($("#ddlHRA").val()) > 0) {
                    hraCategory = 5;
                }
                if ($.trim($("#ddlQuatrAlt").val()) == "0" && $.trim($("#ddlHRA").val()) == "0") {
                    hraCategory = 6;
                }
            }

            var pfid = ''; var pfvalue = '';
            if ($("#chkPercent").is(':checked') && $("#ddlPercent").val() != '') {
                pfid = $("#ddlPercent").val(); pfvalue = 0;
            }
            if ($("#chkValue").is(':checked') && $("#txtValue").val() != '') {
                pfid = 0; pfvalue = $("#txtValue").val();
            }
            if ($("#chkPercent").is(':checked') && $("#ddlPercent").val() == '0') {
                pfid = 0; pfvalue = 0;
            }

            if (FunctionRank != 0 && EarnDeductionID ==2) {
                var E = "{EmpNo: '" + $.trim($("#txtEmpNo").val()) + "', EmpType: '" + $.trim($("#ddlEmpType").val()) + "',  EmpCategory: '" + $.trim($("#ddlCategory").val()) + "', " +
                        " PayScaleID: '" + $.trim($("#ddlPayScale").val()) + "', Amount: '" + $.trim($("#txtAmount").val()) + "', DAID: '" + $.trim($("#ddlDA").val()) + "', " +
                        " hraCategory: '" + hraCategory + "', HRAID: '" + $.trim($("#ddlHRA").val()) + "', HRARent: '" + $.trim($("#txtrentAmount").val()) + "', " +
                        " FixedHRAAmount: '" + $.trim($("#txtmHRA").val()) + "', SpouseAmount: '" + $.trim($("#txtSpouseAmt").val()) + "', isHealth: '" + $.trim($("#ddlHealth").val()) + "', " +
                        " HealthFrom: '" + $.trim($("#txtEffFrm").val()) + "', HealthTo: '" + $.trim($("#txtEffTo").val()) + "', IRID: '" + $.trim($("#ddlIR").val()) + "', " +
                        " PFID: '" + pfid + "', PFAmount: '" + pfvalue + "', PhysicalHandicapped: '" + $.trim($("#ddlPhysical").val()) + "'}";
              // alert(E);
                $.ajax({
                    type: "POST",
                    url: "/Administration/Master/EmployeeMaster/Add_PayDetails",
                    data: E,
                    async: false,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (D) {
                        if (D.Status == 200) {
                            Populate_PayDetail(D, IDforEdit);
                        }
                        else {
                            alert(D.Message);
                        }
                    }
                });
            }
            else {
                var TextType = $('#btnAddPayDetails').text();
                if (TextType == 'Update') {
                   
                    if ($("#tbl tbody tr.trdata").length > 0) {
                       
                        $("#tbl tbody tr.trdata").each(function (index, value) {
                            var EDID = $(this).find('.EDID').text(); 
                            if (EDID == EarnDeductionID) {
                               
                                $(this).find('.EDAmount').text($.trim($("#txtAmount").val()));  return false;
                            }
                        });
                    }
                }
                else
                {
                    var hide = checkValue(EarnDeductionID, IDforEdit);
                    $("#tbl").append("<tr class='trdata allData'><td style='display:none;' class='EDID edids allEDID'>" +
                                    EarnDeductionID + "</td><td class='EDName ednames'>" +
                                    $("#ddlEarnDeduction option:selected").text() + "</td><td class='EDAmount edamounts' style='text-align:right'>" +
                                    $.trim($("#txtAmount").val()) + "</td><td class='edtype' style='display:none;'>" +
                                    $.trim(EDType) + "</td><td class='Label'>" +
                                    "<img style='display:" + hide + " ; cursor:pointer;' src='/Content/Images/Edit.png' alt='cross' height=20 width=20 onClick='Update_Data(this)' /></td></tr>");
                                    // "<img style='display:" + hide + " ; cursor:pointer;' src='/Content/Images/Edit.png' alt='cross' height=20 width=20 onClick='Update_Data(\"" + EarnDeductionID + "\", \"" + $.trim($("#txtAmount").val()) + "\", \"" + $("#ddlEarnDeduction option:selected").text() + "\")' /></td></tr>");
                   
                }

                
             
                Calculate_Total();
            }

}
function Populate_PayDetail(D, IDforEdit)
{
   
    var t = D.Data; 

    var TextType = $('#btnAddPayDetails').text();
    if (TextType == 'Update') {
        if ($("#tbl tbody tr.trdata_Basic").length > 0) {
            $("#tbl tbody tr.trdata_Basic").remove();
            
            for (var i = 0; i < t.length; i++) {
                var EDID = t[i].EDID; 
                var hide = checkValue(EDID, IDforEdit);

                if (t[i].EDAmount > 0) {
                    $("#tbl").append("<tr class='trdata_Basic allData'><td style='display:none;' class='BEDID edids allEDID'>" +
                                    t[i].EDID + "</td><td class='BEDName ednames'>" +
                                    t[i].EDName + "</td><td class='BEDAmount edamounts' style='text-align:right'>" +
                                    t[i].EDAmount + "</td><td class='edtype' style='display:none;'>" +
                                    t[i].EDType + "</td><td class='Label'>" +
                                    "<img style='display:" + hide + " ; cursor:pointer;' src='/Content/Images/Edit.png' alt='cross' height=20 width=20 onClick='Update_Data(this)' /></td></tr>");
                }
            }
        }
        else
        {
          
            for (var i = 0; i < t.length; i++) {
                var EDID = t[i].EDID;
                var hide = checkValue(EDID, IDforEdit);
                if (t[i].EDAmount > 0) {
                    $("#tbl").append("<tr class='trdata_Basic allData'><td style='display:none;' class='BEDID edids allEDID'>" +
                                    t[i].EDID + "</td><td class='BEDName ednames'>" +
                                    t[i].EDName + "</td><td class='BEDAmount edamounts' style='text-align:right'>" +
                                    t[i].EDAmount + "</td><td class='edtype' style='display:none;'>" +
                                    t[i].EDType + "</td><td class='Label'>" +
                                    "<img style='display:" + hide + " ; cursor:pointer;' src='/Content/Images/Edit.png' alt='cross' height=20 width=20 onClick='Update_Data(this)' /></td></tr>");

                }
            }
        }
    }
    else
    {
        for (var i = 0; i < t.length; i++) {
            var EDID = t[i].EDID;
            var hide = checkValue(EDID, IDforEdit);
            if (t[i].EDAmount > 0) {
                $("#tbl").append("<tr class='trdata_Basic allData'><td style='display:none;' class='BEDID edids allEDID'>" +
                                t[i].EDID + "</td><td class='BEDName ednames'>" +
                                t[i].EDName + "</td><td class='BEDAmount edamounts' style='text-align:right'>" +
                                t[i].EDAmount + "</td><td class='edtype' style='display:none;'>" +
                                t[i].EDType + "</td><td class='Label'>" +
                                "<img style='display:" + hide + " ; cursor:pointer;' src='/Content/Images/Edit.png' alt='cross' height=20 width=20 onClick='Update_Data(this)' /></td></tr>");

                //(t[i].isEdit == 1 ? "<img src='/Content/Images/Edit.png' alt='cross' height=20 width=20 onClick='Update_Data(\"" + t[i].EDID + "\", \"" + t[i].EDAmount + "\")' />" : "") + "</td><td style='text-align:center; cursor:pointer;'>" +
                //(t[i].isDelete == 1 ? "<img src='/Content/Images/Delete.png' alt='cross' height=20 width=20 onClick='Update_Data(\"" + t[i].EDID + "\", \"" + t[i].EDAmount + "\")' />" : "") + "</td></tr>");
            }
            }
    }
    Calculate_Total();
}
function checkValue(value, arr) {
    var status = 'none';

    for (var i = 0; i < arr.length; i++) {
        var name = arr[i];
        if (name == value) {
            status = '';
            break;
        }
    }

    return status;
}

function Update_Data(ID) {
    var Amount = $(ID).closest('tr').find('td:eq(2)').text();
    var EDName = $(ID).closest('tr').find('td:eq(1)').text();
    $("#btnAddPayDetails").html('Update');

    $('#ddlEarnDeduction option').filter(function () {
        var textValue = $.trim($(this).text());
        var textSearch = $.trim(EDName);

        if (textValue == textSearch)
            return $.trim(textValue) == textSearch;
    }).prop('selected', 'selected');

    $("#txtAmount").val(Amount);
}


function Calculate_Total()
{
    var TotalDedu = 0;
    var TotalEarning = 0;
    var TotalNet = 0;

    $("#tbl tbody tr.trfooter").empty();
    if ($("#tbl tbody tr.allData").length > 0) {
      
        $("#tbl tbody tr.allData").each(function (index, value) {
            var EDAmount = $(this).find('.edamounts').text();
            var EDType = $(this).find('.edtype').text();
            if(EDType=='E')
                TotalEarning = parseFloat(TotalEarning) + parseFloat(EDAmount);
            else
                TotalDedu = parseFloat(TotalDedu) + parseFloat(EDAmount);
        });
        TotalNet = parseFloat(TotalEarning) - parseFloat(TotalDedu); 
    }
    
    TotalNet = parseFloat(TotalEarning) - parseFloat(TotalDedu);
     if (TotalNet > 0) {
        $("#tbl").append("<tr class='trfooter' style='background-color:#d9eaf3'><td style='display:none;'>" +
            '' + "</td><td style='text-align:right; font-style:italic'>" +
            'Earning :' + TotalEarning + '&nbsp;&nbsp;&nbsp; Deduction :' + TotalDedu + '&nbsp;&nbsp;&nbsp;Net Amount :' + "</td><td style='text-align:right'>" +
            TotalNet + "</td><td class='edtype' style='display:none;'>" +
            '' + "</td><td>" +
            '' + "</td></tr>");
    }

        $("#ddlEarnDeduction").val(''); $("#txtAmount").val(''); $("#btnAddPayDetails").html('Add');
    
}
function Calculate_DNI() {
    var EmpType = $("#ddlEmpType").val();
    var today = new Date();
    var yearNow = today.getFullYear();
    var monthNow = today.getMonth();
    var dateNow = today.getDate();
    if (EmpType == "K") {
        $("#txtDNI").val("01/07/" + (parseInt(yearNow) + 1));
    }
    else {
        if (monthNow < 7) {
            $("#txtDNI").val("01/07/" + yearNow);
        }
        else {
            $("#txtDNI").val("01/07/" + (parseInt(yearNow) + 1));
        }
    }

}


