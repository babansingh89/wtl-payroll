﻿
var dniDate = "";
var main = {};

$(document).ready(function () {

    main.Populate();
    main.DNI();

    //Print
    $('#btnPrint').click(function (evt) { Print(); });

    //Decline
    $('#tbl').on('click', 'tr.allData', function () {
        var EmpNo = $(this).find(".empNo").text();
        var EmpName = $(this).find(".empName").text();
        var PayScaleType = $(this).find(".payScaleType").text();
        var PayScaleID = $(this).find(".payScaleID").text();
        var IncPert = $(this).find(".InctPert").text();
        var OldBasic = $(this).find(".oldBasic").text();
      
        var DNI = dniDate = $(this).find(".dni").text();
        var PayBand = $(this).find(".payBand").text();
        var Declined = $(this).find(".declined").text();
        var EmployeeID = $(this).find(".employeeID").text();
        var Remarks = $(this).find(".remarks").text();

        $('#txtEmpNo').val(EmpNo);
        $('#txtEmpName').val(EmpName);
        main.Populate_PayScale(PayScaleType, PayScaleID);
        $('#txtIncrPer').val(IncPert);
        $('#txtCurrBasic').val(OldBasic);

        var NewBasic = parseInt(((parseInt(IncPert) * parseInt(OldBasic)) / 100) + parseInt(OldBasic)); 
      
        if((NewBasic % 10) >= 1)
        {
            var a = parseInt(NewBasic % 10);
            $('#txtNewBasic').val((NewBasic - a) +10);
        }
        else
        $('#txtNewBasic').val((parseInt(NewBasic)));



        $('#txtIncrOn').val(DNI);
        $('#hdnPayBand').val(PayBand);
        $('#ddlDecline').val(Declined);
        $('#hdnEmployeeID').val(EmployeeID);
        $('#txtRemarks').val(Remarks);

        $("#txtIncrOn").datepicker("option", "minDate", DNI);
        $('#Div1').modal('show');
    });

    //Decline on Yes
    $("#ddlDecline").change(function () { main.Set_DNI(); });

    //New PayScale Change
    $("#ddlNewPayScale").change(function () { main.NewPayScaleChange(); });

    //Save Emp Wise decline or non-decline
    $("#btnEmpWiseSave").click(function () { main.EmpWiseSave(); });

    //Final Apply
    $("#btnApply").click(function () { main.Apply(); });
});

main = {

    Populate: function()
    {
        var V = "{SectorID: '" + $("#ddlSector").val() + "', SalMonth: '" + $("#txtSalMonth").val() + "'}";
        $.ajax({
            url: '/Administration/Master/Employee_Increment/EmployeeDetail',
            type: 'POST',
            data: V,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (D) {
                var t = D.Data;
                if (t.length > 0 && D.Status == 200) {
                    var table = $('#tbl');
                  
                    table.find('tr.allData').empty();

                    var totalEmp = 0; var totalNotEmp = 0; var color = "";

                    $.each(t, function (index, item) {
                        if (index % 2 == 0) color = "#DEFAF5"; else color = "white";

                        var declinedValue = item.declined;
                        if (declinedValue == 'Y') {
                            color = "#FADBD8";
                            totalNotEmp++;
                        }
                        else
                            totalEmp++;

                        table.append("<tr class='allData' style=' cursor:pointer ;background-color:" + color + "'>"
                            + "<td style='display:none' class='employeeID'>" + item.EmployeeID + "</td>"
                            + "<td class='empNo' style='width:100px; border-bottom:1px solid black;'>" + item.EmpNo + "</td>"
                            + "<td class='empName' style='width:200px; border-bottom:1px solid black;'>" + item.EmpName + "</td>"
                            + "<td style='width:200px; border-bottom:1px solid black;'>" + item.Designation + "</td>"
                            + "<td style='display:none;' class='payScaleType'>" + item.PayScaleType + "</td>"
                            + "<td style='display:none' class='payScaleID'>" + item.PayScaleID + "</td>"
                            + "<td style='width:100px; border-bottom:1px solid black;' class='payBand'>" + item.PayBand + "</td>"
                            + "<td style='width:250px; border-bottom:1px solid black;'>" + item.PayScale + "</td>"
                            + "<td style='width:150px; border-bottom:1px solid black;' class='oldBasic'>" + item.OldBasic + "</td>"
                            + "<td style='width:95px; border-bottom:1px solid black;' class='InctPert'>" + item.IncrementPercentage + "</td>"
                            + "<td style='display:none'>" + item.SectorID + "</td>"
                            + "<td style='display:none' class='dni'>" + item.DNI + "</td>"
                            + "<td style='display:none' class='declined'>" + item.declined + "</td>"
                            + "<td style='display:none' class='remarks'>" + item.remarks + "</td>"
                            + "</tr>");
                      
                    });
                    $("#lblTotalEmp").text(totalEmp);
                    $("#lblTotalNotEmp").text(totalNotEmp);
                }

            }
        });
    },
    Populate_PayScale: function (PayScaleType, PayScaleID)
    {
        var V = "{PayScaleType: '" + PayScaleType + "'}";
        $.ajax({
            url: '/Administration/Master/EmployeeMaster/Bind_PayScale',
            type: 'POST',
            data: V,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (D) {
                var t = D.Data;
                if (t.length > 0 && D.Status == 200) {
                    $("#ddlNewPayScale").empty();
                    $("#ddlNewPayScale").append("<option value=''>--Select--</option>")
                    $.each(t, function (index, value) {
                        var label = value.PayScale;
                        var val = value.PayScaleID;
                        $("#ddlNewPayScale").append($("<option></option>").val(val).html(label));
                    });
                    $('#ddlNewPayScale').val(PayScaleID);
                }
            }
        });
    },
    DNI:function()
    {
        $('#txtIncrOn').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow',
            dateFormat: 'dd/mm/yy',
            onSelect: function (dates) {
                var aa = $('#txtIncrOn').val();
                var a = aa.split('/');
                for (var i = 0; i < a.length; i++) {
                    date = a[0]; month = a[1]; year = a[a.length - 1];
                }
                $('#txtIncrOn').val("01" + "/" + month + "/" + year);

                var dat = $('#txtIncrOn').val();
                if (dat == ("01/07" + "/" + year) && $('#ddlDecline').val() == "Y") {
                    alert('Sorry !! Wrong Increment Date.'); $('#txtIncrOn').val(''); return false;
                }
            }
        });
    },
    Set_DNI:function()
    {
        var dni = $("#ddlDecline").val();
        if(dni == "Y")
        {
            var dniDate = $("#txtIncrOn").val();
            if (dniDate == '01/07/2017')
            {
                alert('Sorry !! Wrong Increment Date.'); $('#txtIncrOn').val(''); return false;
            }

            $("#txtIncrOn").val('');
            $("#txtIncrOn").focus();
        }
        else
        {
            $("#txtIncrOn").val(dniDate);
        }
    },
    NewPayScaleChange:function()
    {
        var gp = 0; var incrPer = $('#txtIncrPer').val();
        var P = $('#ddlNewPayScale').find('option:selected').text();
        if (P != "") {
            var a = P.split('-');
            for (var i = 0; i < a.length; i++) {
                var min = a[1]; var V = a[2];
                var max = V.substring(0, V.indexOf('('));
                gp = V.substring(V.indexOf('(') + 1, V.indexOf(')'));
            }
            var PayBand = $('#hdnPayBand').val();
            var gpANDpayBand = parseInt(gp) + parseInt(PayBand);
            var NetgpANDpayBand = ((parseInt(gpANDpayBand) * 3) / 100) + gpANDpayBand;

            $("#txtNewBasic").val(Math.round(NetgpANDpayBand));
        }
    },
    EmpWiseSave:function()
    {
        var res = confirm('Are You Sure want to Update Employee Details ??');
        if(res== true)
        {
            var EmpID = $('#hdnEmployeeID').val();
            var OldBasic = $('#txtCurrBasic').val();
            var NewPayscaleID = $('#ddlNewPayScale').val();
            var NewBasic = $('#txtNewBasic').val();
            var IncrPer = $('#txtIncrPer').val();
            var IncrOn = $('#txtIncrOn').val();
            var Decline = $('#ddlDecline').val();
            var Remarks = $('#txtRemarks').val();

            var E = "{EmpID: '" + EmpID + "', OldBasic: '" + OldBasic + "', OldBasic: '" + OldBasic + "', NewPayscaleID: '" + NewPayscaleID + "' , NewBasic: '" + NewBasic + "',  IncrPer: '" + IncrPer + "', IncrOn: '" + IncrOn + "', Decline: '" + Decline + "', Remarks: '" + Remarks + "', SectorID: '" + $('#ddlSector').val() + "'}";
           
                $.ajax({
                    type: "POST",
                    url: '/Administration/Master/Employee_Increment/Empwise_UpdateIncrement',
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                       
                        if (D.Status == 200) {
                            main.Populate();
                            alert("Employee details are Successfully Updated !!");
                            $('#txtEmpNo').val('');
                            $('#txtEmpName').val('');
                            $('#txtCurrBasic').val('');
                            $('#ddlNewPayScale').val('');
                            $('#txtNewBasic').val('');
                            $('#txtIncrPer').val('');
                            $('#txtIncrOn').val('');
                            $('#ddlDecline').val('');
                            $('#txtRemarks').val('');

                            $('#hdnPayBand').val('');
                            $('#hdnEmployeeID').val('');

                           
                            $('#Div1').modal('hide');
                        }
                    }
                });
        }
    },
    Apply:function()
    {
        var res = confirm('Are You Sure want to Apply all Employee Increment ??');
        if (res == true) {

            var table = $("#myTable");
            var tblCount = (table.find('tr.allData')).length; 
            if (tblCount == 0) {
                alert('Sorry !! You have not any Employee to apply Increment Changes ??'); return false;
            }

            var EmpID = $('#hdnEmployeeID').val();
            var OldBasic = $('#txtCurrBasic').val();
            var NewPayscaleID = $('#ddlNewPayScale').val();
            var NewBasic = $('#txtNewBasic').val();
            var IncrPer = $('#txtIncrPer').val();
            var IncrOn = $('#txtIncrOn').val();
            var Decline = $('#ddlDecline').val();
            var Remarks = $('#txtRemarks').val();
            var E = "{EmpID: '" + EmpID + "', OldBasic: '" + OldBasic + "', OldBasic: '" + OldBasic + "', NewPayscaleID: '" + NewPayscaleID + "' , NewBasic: '" + NewBasic + "',  IncrPer: '" + IncrPer + "', IncrOn: '" + IncrOn + "', Decline: '" + Decline + "', Remarks: '" + Remarks + "', SectorID: '" + $('#ddlSector').val() + "'}";
           
            $.ajax({
                type: "POST",
                url: '/Administration/Master/Employee_Increment/Apply',
                data: E,
                contentType: "application/json; charset=utf-8",
                success: function (D) {

                    if (D.Status == 200) {
                        alert("All Employee Increment details are Updated Successfully !!");

                        location.reload();
                       
                    }
                }
            });
        }
    }

}
function SearchTable() {
    var forSearchprefix = $("#txtSearch").val().trim().toUpperCase();
    var tablerow = $('#tbl').find('.allData');
    $.each(tablerow, function (index, value) {
        var empNo = $(this).find('.empNo').text().toUpperCase();
        var empName = $(this).find('.empName').text().toUpperCase();
        if (empName.indexOf(forSearchprefix) > -1 || empNo.indexOf(forSearchprefix) > -1) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}
function Print() {
    var table = $("#myTable");
    var tblCount = (table.find('tr.allData')).length;
    if (tblCount > 0) {
        var res = confirm('Are You Sure Want to Print Report ??');
        if (res == true) {
            var contents = $("#myDiv").html();
            var frame1 = $('<iframe />');
            frame1[0].name = "frame1";
            frame1.css({ "position": "absolute", "top": "-1000000px" });
            $("body").append(frame1);
            var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
            frameDoc.document.open();
            //Create a new HTML document.
            frameDoc.document.write("<html><head><title></title>");
            frameDoc.document.write('</head><body>');
            //Append the external CSS file.
            frameDoc.document.write('<link href="style.css" rel="stylesheet" type="text/css" />');
            //Append the DIV contents.
            frameDoc.document.write("<div style='width:100%'>" +
                "<div style='width:20%; float:left; text-align:right; padding-bottom:5px;'><img src='" + $("#hdnLogo").val() + "' alt='Delete Voucher' style='width:85px;height:80px;cursor:pointer;' /></div>");
            frameDoc.document.write("<div style='width:90%; height:80px; text-align:center; '>" +
                "<span>" + $("#hdnHeader").val() + "</span><br/>" +
                "<span>" + $("#hdnAddress").val() + "</span></div>");


            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');
            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                frame1.remove();
            }, 500);
        }
    }
    else {
        alert('Sorry !! You have not any Employee to apply Increment Changes ??'); return false;
    }
}
