﻿$(function () {
    var Edit = "";
    var Delete = "";
    var Update = "";

    var Selector = {
        txtAccFinYear: $("#txtAccFinYear"),
        hdnRateSlapID: $("#hdnRateSlapID"),
        txtFinYear: $("#txtFinYear"),
        ddlGender: $("#ddlGender"),
        txtAmountFrom: $("#txtAmountFrom"),
        txtAmountTo: $("#txtAmountTo"),
        txtAddAmount: $("#txtAddAmount"),
        txtPercentage: $("#txtPercentage"),
        btnSave: $("#btnSave"),
        btnRefresh: $("#btnRefresh"),
        clstbl: $(".clstbl")
    };
    var objServerSide = {
        GetMethod: function (URL, callback) {
            $.ajax({
                type: "get",
                contentType: "application/json;charset=utf-8;",
                data: null,
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    callback(responce);
                },
                error: function () {
                    callback("error");
                }
            });
        },
        PostMehtod: function (URL, Data, callback) {
            $.ajax({
                type: "post",
                contentType: "application/json;charset=utf-8;",
                data: JSON.stringify(Data),
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    callback(responce);
                },
                error: function () {
                    callback("error");
                }
            });
        }
    };
    var objClientSide = {
        Initialization: function () {
            objClientSide.Event.onLoad();
            objClientSide.Event.onClick();
            objClientSide.Event.onKeydown();
            objClientSide.Event.onBlur();
            objClientSide.Event.onChange();
        },
        Event: {
            onLoad: function () {
                objClientSide.Method.LoadFinYear();
                objClientSide.Method.GetFieldName();
                objClientSide.Method.GetData();
                objClientSide.Method.GetGender();
            },
            onClick: function () {
                Selector.btnSave.click(function () {
                    objClientSide.Method.Save();
                });
                Selector.btnRefresh.click(function () {
                    objClientSide.Method.Refresh();
                });
                $(document).on("click", ".clsUpdate", function () {
                    var ID = $(this).closest(".row").find(".clsID").text();
                    var FinYear = $(this).closest(".row").find(".clsFinYear").text();
                    var GenderID = $(this).closest(".row").find(".clsGenderID").text();
                    var AddAmount = $(this).closest(".row").find(".clsAddAmount").text();
                    var AmountFrom = $(this).closest(".row").find(".clsAmountFrom").text();
                    var AmountTo = $(this).closest(".row").find(".clsAmountTo").text();
                    var Percentage = $(this).closest(".row").find(".clsPercentage").text();
                    Selector.hdnRateSlapID.val(ID);
                    Selector.txtAddAmount.val(AddAmount);
                    Selector.txtAmountFrom.val(AmountFrom);
                    Selector.txtAmountTo.val(AmountTo);
                    Selector.txtPercentage.val(Percentage);
                    Selector.ddlGender.val(GenderID);
                    Selector.btnSave.val(Update);
                });
                $(document).on("click", ".clsDelete", function () {
                    var ID = $(this).closest(".row").find(".clsID").text();
                    if (Selector.hdnRateSlapID.val() == ID) {
                        alert("this Record in Delete Mode. You cannt Delete this record now..");
                        return false;
                    }

                    if (confirm("Are you sure want to Delete this Record ??")) {

                        objClientSide.Method.Delete(ID);
                    }
                });
            },
            onKeydown: function () {
                $(document).on('keypress', '.money', function (e) {
                    if (!(e.keyCode > 45 && e.keyCode < 58) || e.keyCode == 47 || (e.keyCode == 46 && $(this).val().indexOf('.') != -1)) {
                                return false;
                            }
                });               
            },
            onBlur: function () {
                Selector.txtAccFinYear.blur(function () {
                    objClientSide.Method.LoadFinYear();
                    objClientSide.Method.GetItexMaxAmount();
                });
            },
            onChange: function () {
                Selector.ddlGender.change(function () {
                    var genderID = $(this).val();
                    objClientSide.Method.Search(genderID);
                });
            }
        },
        Method: {
            LoadFinYear: function () {
                Selector.txtFinYear.val(Selector.txtAccFinYear.val().trim());
            },
            GetFieldName: function () {
                var URL = "/Administration/Master/ItaxSlapPercentage/Get_FieldName";
                objServerSide.GetMethod(URL, function (responce) {
                    var jsdata = responce.Data;
                    $.each(jsdata, function (index, value) {
                        index = index + 1;
                        var star = value.IsMand == 1 ? '*' : "";
                        $("#" + index + "").text(value.FieldName);
                        $("#10" + index + "").text(star);
                        if (value.FieldID == 7) {
                            Selector.btnSave.val(value.FieldName);
                        }
                        if (value.FieldID == 8) {
                            Edit = value.FieldName;
                        }
                        if (value.FieldID == 9) {
                            Update = value.FieldName;
                        }
                        if (value.FieldID == 10) {
                            Delete = value.FieldName;
                        }
                    });
                });
            },
            GetGender: function () {
                var URL = "/Administration/Master/ItaxSlapPercentage/Get_Gender";
                objServerSide.GetMethod(URL, function (responce) {
                    var jsdata = responce.Data;
                    Selector.ddlGender.empty();
                    Selector.ddlGender.append("<option value=''>Select Gender</option>")
                    $.each(jsdata, function (index, value) {
                        Selector.ddlGender.append("<option value='" + value.GenderID + "'>" + value.Gender + "</option>");
                    });
                });
            },
            GetData: function () {
                var Data = { finyear: Selector.txtAccFinYear.val().trim() };
                var url = "/Administration/Master/ItaxSlapPercentage/Get_Data";
                console.log(Data);
                objServerSide.PostMehtod(url, Data, function (responce) {
                    var jsdata = responce.Data;
                    console.log(jsdata);
                    objClientSide.Method.AppendTable(jsdata);
                });
            },
            AppendTable: function (jsdata) {
                Selector.clstbl.empty();
                $.each(jsdata, function (index, value) {
                    index = index + 1;
                    Selector.clstbl.append("<div class='row border-top'>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsID hide'>" + value.RateSlabID + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1'>" + index + "</div>"
                        + "<div class='col-lg-2 col-md-2 col-sm-2 clsFinYear'>" + value.FinYear + "</div>"
                        + "<div class='col-lg-0 col-md-0 col-sm-0 hide clsGenderID'>" + value.GenderID + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsGender'>" + value.Gender + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsAddAmount'>" + value.AddAmount + "</div>"
                        + "<div class='col-lg-2 col-md-2 col-sm-2 clsAmountFrom'>" + value.AmountFrom + "</div>"
                        + "<div class='col-lg-2 col-md-2 col-sm-2 clsAmountTo'>" + value.AmountTo + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsPercentage'>" + value.Percentage + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsUpdate text-center text-info' style='cursor:pointer' >" + Edit + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsDelete text-center text-default' style='cursor:pointer'>" + Delete + "</div>"
                        + "</div>");
                });
            },
            Save: function () {
                var Data = {
                    RateSlabID: Selector.hdnRateSlapID.val(), FinYear: Selector.txtAccFinYear.val().trim(),
                    GenderID: Selector.ddlGender.val(), Gender: "", AddAmount: Selector.txtAddAmount.val().trim(),
                    AmountFrom: Selector.txtAmountFrom.val(), AmountTo: Selector.txtAmountTo.val().trim(),
                    Percentage:Selector.txtPercentage.val().trim()
                };
                console.log(Data);
                var URL = "/Administration/Master/ItaxSlapPercentage/Save";
                objServerSide.PostMehtod(URL, Data, function (responce) {
                    var Message = responce.Message;
                    var code = responce.Data;
                    alert(Message);
                    if (code.IsSuccess == 1) {
                        objClientSide.Method.Refresh();
                    } else {
                        objClientSide.Method.Focus(code.FocusCode);
                    }
                });
            },
            Delete: function (ID) {
                var Data = { RateSlabID: ID };
                var URL = "/Administration/Master/ItaxSlapPercentage/Delete";
                objServerSide.PostMehtod(URL, Data, function (responce) {
                    var Message = responce.Message;
                    alert(Message);
                    objClientSide.Method.Refresh();
                });
            },
            Refresh: function () {
                Selector.txtAddAmount.val("");
                Selector.txtAmountFrom.val("");
                Selector.txtAmountTo.val("");
                Selector.txtPercentage.val("");
                Selector.hdnRateSlapID.val("");
                objClientSide.Method.GetFieldName();
                objClientSide.Method.GetGender();
                objClientSide.Method.LoadFinYear();
                objClientSide.Method.GetData();
            },
            Focus: function (num) {
                if (num==1) {
                    Selector.txtFinYear.focus();                           
                }
                if (num == 2) {
                    Selector.ddlGender.focus();
                }
                if (num == 3) {
                    Selector.txtAmountFrom.focus();
                }
                if (num == 4) {
                    Selector.txtAmountTo.focus();
                }
                if (num == 5) {
                    Selector.txtAddAmount.focus();
                }
                if (num == 6) {
                    Selector.txtPercentage.focus();
                }
            },
            Search: function (GenderID) {           
              Selector.clstbl.find('.row').find('.clsGenderID:contains(' + GenderID + ')').closest('.row').show();              
              Selector.clstbl.find('.row').find('.clsGenderID:not(:contains(' + GenderID + '))').closest('.row').hide();
            }
        }
    };

    objClientSide.Initialization();
});