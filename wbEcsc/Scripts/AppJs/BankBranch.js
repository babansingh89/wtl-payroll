﻿$(function () {
    $("#btnRefresh").click(function () {        
        window.location.reload();
    });
    $("#txtBankName").autocomplete({
        source: function (request, responce) {
            var data = { strprefix: $("#txtBankName").val().trim() };
            $.ajax({
                type: 'post',
                contentType: 'application/json;charset=utf-8;',
                data: JSON.stringify(data),
                dataType: 'json',
                url: "/Administration/Master/BankBranch/Get_Bank",
                success: function (data) {
                    console.log(data);

                    responce($.map(data, function (item) {
                        return { value: item.BankID, label: item.BankName };
                    }));
                },
                error: function (responce) {
                    alert(responce.d);
                }
            });
        },
        select: function (e, i) {
            e.preventDefault();
            $('#txtBankName').val(i.item.label);
            $('#hdnBankID').val(i.item.value);
        },
        focus: function (e, i) {
            e.preventDefault();
            $('#txtBankName').val(i.item.label);
            $('#hdnBankID').val(i.item.value);
        }
    });
//  ---------------------------------------
    var EditButton = "";

    var Control = {
        txtBankName: $("#txtBankName"),
        hdnBankID: $("#hdnBankID"),
        txtBranchName: $("#txtBranchName"),
        hdnBranchID: $("#hdnBranchID"),
        txtIFSC: $("#txtIFSC"),
        txtMICR:$("#txtMICR"),
        btnSubmit: $("#btnSubmit"),
        btnRefresh: $("#btnRefresh"),
        clstbl: $(".clstbl")
    };

    var objServer = {
        GetFieldName: function (callback) {
            $.ajax({
                type: 'post',
                contentType: 'application/json;churset=utf-8;',
                data: null,
                dataType: 'json',
                url: "/Administration/Master/BankBranch/GetFieldName",
                success: function (responce) {
                    var js = responce.Data;
                    callback(js);
                },
                error: function (responce) {
                    callback("error");
                }
            });
        },
        GetBankBranch: function (callback) {
            $.ajax({
                type: 'post',
                contentType: 'application/json;charset=utf-8;',
                data: null,
                dataType: 'json',
                url: "/Administration/Master/BankBranch/GetBankBranch",
                success: function (response) {
                    var jsdata = response.Data;
                    callback(jsdata);
                },
                error: function (response) {
                    callback("error");
                }
            });
        },        
        SaveData: function (data, callback) {
            $.ajax({
                type: 'post',
                contentType: 'application/json',
                data: JSON.stringify(data),
                dataType: 'json',
                url: "/Administration/Master/BankBranch/Save",
                success: function (responce) {
                    callback(responce);
                },
                error: function () {
                    callback("error");
                }
            });
        }
    };

    var objClient = {
        Initialization: function () {
            objClient.Event.onLoad();
            objClient.Event.onClick();
            objClient.Event.onBlur();
            objClient.Event.onChange();
        },
        Event: {
            onLoad: function () {
                objClient.Method.GetFieldName();
                objClient.Method.GetBankBranch();
            },
            onClick: function () {
                Control.btnSubmit.click(function () {
                    objClient.Method.SaveData();
                });
                $(document).on('click', '.clsEdit', function () {
                    var hdnBankID = $(this).closest(".clsrow").find(".clsBankID").text();
                    var txtBankName = $(this).closest(".clsrow").find(".clsBankName").text();
                    var hdnBranchID = $(this).closest(".clsrow").find(".clsBranchID").text();
                    var txtBranchName = $(this).closest(".clsrow").find(".clsBranch").text();
                    var txtIFSC = $(this).closest(".clsrow").find(".clsIFSC").text();
                    var txtMICR = $(this).closest(".clsrow").find(".clsMICR").text();

                    Control.hdnBankID.val(hdnBankID);
                    Control.txtBankName.val(txtBankName);
                    Control.hdnBranchID.val(hdnBranchID);
                    Control.txtBranchName.val(txtBranchName);
                    Control.txtIFSC.val(txtIFSC);
                    Control.txtMICR.val(txtMICR);

                    Control.btnSubmit.val("Update");
                });
            },
            onBlur: function () {

            },
            onChange: function () {
                //Control.ddlEmployeeType.change(function () {
                //    Control.clstbl.empty();
                //    var emptp = $(this).val();
                //    if (emptp == "") {
                //        return false;
                //    }
                //    objClient.Method.GetPayByEmployeeType(emptp);
                //});
            }
        },
        Method: {
            GetFieldName: function () {
                objServer.GetFieldName(function (jsdata) {
                    $.each(jsdata, function (index, value) {
                        index = index + 1;
                        var star = value.IsMand == 1 ? '*' : "";
                        $("#" + index + "").text(value.FieldName);
                        $("#" + index + "0").text(star);
                        if (value.FieldID == 6) {
                            EditButton = value.FieldName;
                        }
                    });
                });
            },
            GetBankBranch: function () {
                objServer.GetBankBranch(function (jsdata) {
                    objClient.Method.PopulateTable(jsdata);
                });               
            },
            PopulateTable: function (jsdata) {
                Control.clstbl.empty();                     
                $.each(jsdata, function (index, value) {
                    index = index + 1;
                    var MICR = value.MICR == null ? "" : value.MICR;
                    Control.clstbl
                           .append("<div class='row border-top clsrow'>"
                                 + "<div class='col-md-1 col-sm-1'>" + index + "</div>"                               
                                 + "<div class='col-md-0 col-sm-0 clsBankID text-center' style='display:none;'>" + value.BankID + "</div>"
                                 + "<div class='col-md-3 col-sm-3 clsBankName'>" + value.BankName + "</div>"
                                 + "<div class='col-md-0 col-sm-0 clsBranchID' style='display:none;'>" + value.BranchID + "</div>"
                                 + "<div class='col-md-3 col-sm-3 clsBranch'>" + value.Branch + "</div>"
                                 + "<div class='col-md-2 col-sm-2 clsIFSC'>" + value.IFSC + "</div>"
                                 + "<div class='col-md-2 col-sm-2 clsMICR'>" + MICR + "</div>"
                                 + "<div class='col-md-1 col-sm-1 clsEdit text-center text-info' style='cursor:pointer;' title='click to Edit " + value.Branch + " Branch'> " + EditButton + " </div>"
                                 + "</div>");
                });
            },           
            SaveData: function () {
                var objpaymd = {
                    BranchID: Control.hdnBranchID.val() == "" ? null : parseInt(Control.hdnBranchID.val()),
                    BankID: Control.hdnBankID.val(),
                    Branch: Control.txtBranchName.val().trim(),
                    BankName: Control.txtBankName.val().trim(),
                    IFSC: Control.txtIFSC.val().trim(),
                    MICR: Control.txtMICR.val().trim()
                };

                console.log(objpaymd);
                objServer.SaveData(objpaymd, function (responce) {
                    if (responce == "error") {
                        alert("Internal server error");
                        return false;
                    }
                    alert(responce.Message);
                    if (responce.Data == 1) {
                        window.location.reload();
                    }
                });
            }
        }
    };

    objClient.Initialization();
});

function SearchTable() {
    var bankprefix = $("#txtBankName").val().trim().toUpperCase();
    var branchprefix = $("#txtBranchName").val().trim().toUpperCase();
    var IFSC = $("#txtIFSC").val().trim().toUpperCase();
    var tablerow = $('#myTable').find('.clsrow');
    $.each(tablerow, function (index, value) {
        var bankname = $(this).find('.clsBankName').text().toUpperCase();
        var Branchname = $(this).find('.clsBranch').text().toUpperCase();
        var tblIFSC = $(this).find('.clsIFSC').text().toUpperCase();
        if (bankname.indexOf(bankprefix) > -1 && Branchname.indexOf(branchprefix) > -1 && tblIFSC.indexOf(IFSC) > -1) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}