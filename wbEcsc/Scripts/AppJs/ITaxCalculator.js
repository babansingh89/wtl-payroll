﻿var istrueFinYear = false; var _thisOtherSourceDetails = ""; _thisDeduction6A = ""; _thisChallan = ""; _thisSavingType = ""; _thisGrossHead = ""; _thisAllowanceHead = ""; _thisDeductionHead = ""; _thisReliefHead = "";

var IncomeFrmOtherSource = "";
IncomeFrmOtherSource += "<tr style='background-color: azure;height:40px; display:none;' class='A1'>";
IncomeFrmOtherSource += "<td colspan='4' align='right'>";
IncomeFrmOtherSource += "<table style='width:56%' id='tblSourceFromOtherIncome'>";
IncomeFrmOtherSource += "<tr>";
IncomeFrmOtherSource += "<td colspan='4' width='100%'>";
IncomeFrmOtherSource += "<table id='Grid_OtherIncomeSource' class='Grid'>";
IncomeFrmOtherSource += "<tr>";
IncomeFrmOtherSource += "<td><input type='text' id='txtOtherDesc' class='form-control' style='width:420px;' /><input type='hidden' id='hdnOtherDescID' /></td>";
//IncomeFrmOtherSource += "<td></td>"//"<td><input type='text' id='txtotherIncAmount' class='form-control allownumericwithoutdecimal' style='text-align: right;' /></td>";
IncomeFrmOtherSource += "<td><input type='button' id='btnOtherIncSrc' value='Add' class='btn btn-primary' onclick='SaveOtherSourceIncome(this, \"" + 'Grid_OtherIncomeSource' + "\", \"" + 'trclass1' + "\", \"" + 'txtOtherDesc' + "\", \"" + 'clssOtherIncomeSourceAmt' + "\")' /></td>";
IncomeFrmOtherSource += "</tr>";
IncomeFrmOtherSource += "<thead>";
IncomeFrmOtherSource += "<tr>";
IncomeFrmOtherSource += "<th style='border: 1px solid;'>Description</th>";
//IncomeFrmOtherSource += "<th style='border: 1px solid;'>Amount</th>";
IncomeFrmOtherSource += "<th style='border: 1px solid;'></th>";
IncomeFrmOtherSource += "</tr>";
IncomeFrmOtherSource += "</thead>";
IncomeFrmOtherSource += "<tbody></tbody>";
IncomeFrmOtherSource += "</table>";
IncomeFrmOtherSource += "</td>";
IncomeFrmOtherSource += "</tr>";
IncomeFrmOtherSource += "</table>";
IncomeFrmOtherSource += "</td>";
IncomeFrmOtherSource += "</tr>";

var Deduction6A = "";
Deduction6A += "<tr style='border-bottom: 1px solid gray; background-color: azure; display: none;' class='A2'>";
Deduction6A += "<td colspan='4' align='right'>";
Deduction6A += "<div>";
Deduction6A += "<table id='GridTaxDetails' class='Grid'>";
Deduction6A += "<thead>";
Deduction6A += "<tr>";
Deduction6A += "<th style='border: 1px solid;'>SlNo.</th>";
Deduction6A += "<th style='border: 1px solid;'>Tax Type</th>";
//Deduction6A += "<th style='border: 1px solid;'></th>";
Deduction6A += "<th style='border: 1px solid; text-align:center;'>Amount</th>";
Deduction6A += "</tr>";
Deduction6A += "</thead>";
Deduction6A += "<tbody></tbody>";
Deduction6A += "</table>";
Deduction6A += "</div>";
Deduction6A += "</td>";
Deduction6A += "</tr>";

Deduction6A += "<tr class='A2' style='border-bottom: 1px solid gray; display: none; background-color: lightblue; height:40px'>";
Deduction6A += "<td colspan='6'>";
Deduction6A += "<table>";
Deduction6A += "<tr align='right'>";
Deduction6A += "<td style='padding: 4px;' align='right'><span class='headFont'>Add New Section&nbsp;&nbsp;</span><span class='require'>*</span> </td>";
Deduction6A += "<td style='padding: 4px;'>";
Deduction6A += "<input type='text' ID='txtNewSection' style='text-transform: uppercase;' class='form-control' />";
Deduction6A += "</td>";
Deduction6A += "<td style='padding: 4px;' align='right'><span class='headFont'>Type&nbsp;&nbsp;</span><span class='require'>*</span> </td>";
Deduction6A += "<td style='padding: 4px; margin-right: 35px'>";
Deduction6A += "<label id='pdf' class='radio-inline'><input type='radio' id='rbValue' style='zoom:1.2;' name='optradio' onchange='myFunction()'>Value</label>";
Deduction6A += "<label id='excel' class='radio-inline'><input type='radio' id='rbPercent' style='zoom:1.2;' name='optradio' onchange='myFunction()'>Percentage</label>";
Deduction6A += "</td>";
Deduction6A += "<td style='padding: 4px; display:none;' align='right;' class='Max'><span class='headFont '>Percent(%)&nbsp;&nbsp;</span><span class='require'>*</span> </td>";
Deduction6A += "<td style='padding: 4px; display:none;' class='Max'>";
Deduction6A += "<input type='text' ID='txtNewPercentage' style='text-align: right' class='form-control allownumericwithoutdecimal' />";
Deduction6A += "</td>";
Deduction6A += "<td style='padding: 4px;' align='right'><span class='headFont'>Max Amount&nbsp;&nbsp;</span><span class='require'>*</span> </td>";
Deduction6A += "<td style='padding: 4px;'>";
Deduction6A += "<input type='text' ID='txtNewMaxAmount' style='text-align: right' class='form-control allownumericwithoutdecimal' />";
Deduction6A += "</td>";
Deduction6A += "<td style='padding: 4px;' align='center'>";
Deduction6A += "<button type='button' id='btnSectionAdd' class='btn btn-primary' onclick='AddNewSection(this)'>Add</button>";
Deduction6A += "</td>";
Deduction6A += "</tr>";
Deduction6A += "</table>";
Deduction6A += "</td>";
Deduction6A += "</tr>";

var Deduction6Asub = "";
Deduction6Asub += "<tr style='border-bottom: 1px solid gray; display: none' class='A5'>";
Deduction6Asub += "<td colspan='4' align='right'>";
Deduction6Asub += "<div>";
Deduction6Asub += "<table id='tblclassDeducChapterVIA' class='Grid'>";
Deduction6Asub += "<thead>";
Deduction6Asub += "<tr>";
Deduction6Asub += "<th style='border: 1px solid;'>SlNo.</th>";
Deduction6Asub += "<th style='border: 1px solid;'>Sub Saving Type</th>";
Deduction6Asub += "<th style='border: 1px solid; text-align:center;'>Amount</th>";
Deduction6Asub += "</tr>";
Deduction6Asub += "</thead>";
Deduction6Asub += "<tbody></tbody>";
Deduction6Asub += "</table>";
Deduction6Asub += "</div>";
Deduction6Asub += "</td>";
Deduction6Asub += "</tr>";

Deduction6Asub += "<tr class='A5' style='border-bottom: 1px solid gray; display: none; background-color: lightblue; height:40px'>";
Deduction6Asub += "<td colspan='6'>";
Deduction6Asub += "<table>";
Deduction6Asub += "<tr align='right'>";
Deduction6Asub += "<td style='padding: 4px;' align='right'><span class='headFont'>Add New Section&nbsp;&nbsp;</span><span class='require'>*</span> </td>";
Deduction6Asub += "<td style='padding: 4px;'>";
Deduction6Asub += "<input type='text' ID='txtNewSection' style='text-transform: uppercase;' class='form-control' />";
Deduction6Asub += "</td>";
Deduction6Asub += "<td style='padding: 4px;' align='right'><span class='headFont'>Type&nbsp;&nbsp;</span><span class='require'>*</span> </td>";
Deduction6Asub += "<td style='padding: 4px; margin-right: 35px'>";
Deduction6Asub += "<label id='pdf' class='radio-inline'><input type='radio' id='rbValue' name='optradio' onchange='myFunction()'>Value</label>";
Deduction6Asub += "<label id='excel' class='radio-inline'><input type='radio' id='rbPercent' name='optradio' onchange='myFunction()'>Percentage</label>";
Deduction6Asub += "</td>";
Deduction6Asub += "<td style='padding: 4px;' align='right'><span class='headFont Max'>Max Amount&nbsp;&nbsp;</span><span class='require'>*</span> </td>";
Deduction6Asub += "<td style='padding: 4px;'>";
Deduction6Asub += "<input type='text' ID='txtMaxAmount' style='text-align: right' class='form-control allownumericwithoutdecimal' />";
Deduction6Asub += "</td>";
Deduction6Asub += "<td style='padding: 4px;' align='center'>";
Deduction6Asub += "<button type='button' id='btnSectionAdd' class='btn btn-primary'>Add</button>";
Deduction6Asub += "</td>";
Deduction6Asub += "</tr>";
Deduction6Asub += "</table>";
Deduction6Asub += "</td>";
Deduction6Asub += "</tr>";

var Challan = "";
Challan += "<tr style='background-color: azure;height:40px; display:none;' class='A3'>";
Challan += "<td colspan='4' align='right'>";
Challan += "<table style='width:80%' id='tblChallan' class='Grid'>";
Challan += "<tr>";
Challan += "<td></td>";
Challan += "<td><input type='text' id='txtChallanNo' class='form-control' style='width:250px;' /></td>";
Challan += "<td><input type='text' id='txtChallanDate' class='form-control' /></td>";
Challan += "<td><input type='text' id='txtBSRNo' class='form-control' /></td>";
Challan += "<td><input type='text' id='txtChallanAmt' class='form-control allownumericwithoutdecimal' style='text-align: right;'/></td>";
Challan += "<td><input type='button' id='btnChallanAdd' value='Add' class='btn btn-primary' onclick='AddChallanIndividual()' /></td>";
Challan += "</tr>";
Challan += "<thead>";
Challan += "<tr>";
Challan += "<th style='border: 1px solid;'>SlNo</th>";
Challan += "<th style='border: 1px solid;'>ChallanNo</th>";
Challan += "<th style='border: 1px solid;'>ChallanDate</th>";
Challan += "<th style='border: 1px solid;'>BSRNo</th>";
Challan += "<th style='border: 1px solid;'>TaxAmount</th>";
Challan += "<th style='border: 1px solid;'></th>";
Challan += "</tr>";
Challan += "</thead>";
Challan += "<tbody></tbody>";
Challan += "</table>";
Challan += "</td>";
Challan += "</tr>";

var GrossPay = "";
GrossPay += "<tr style='background-color: azure;height:40px; display:none;' class='A4'>";
GrossPay += "<td colspan='4' align='right'>";
GrossPay += "<table style='width:56%' id='tblGrossPay'>";
GrossPay += "<tr>";
GrossPay += "<td colspan='4' width='100%'>";
GrossPay += "<table id='Grid_GrossPay' class='Grid'>";
GrossPay += "<tr>";
GrossPay += "<td><input type='text' id='txtGrossPayDesc' class='form-control' style='width:420px;' /><input type='hidden' id='hdnGrossPayDescID' /></td>";
//GrossPay += "<td></td>"//"<td><input type='text' id='txtGrossPayAmount' class='form-control allownumericwithoutdecimal' style='text-align: right;' /></td>";
GrossPay += "<td><input type='button' id='btnGrossPay' value='Add' class='btn btn-primary' onclick='SaveOtherSourceIncome(this, \"" + 'Grid_GrossPay' + "\", \"" + 'trclass' + "\", \"" + 'txtGrossPayDesc' + "\", \"" + 'clssGrossPayAmt' + "\")' /></td>";
GrossPay += "</tr>";
GrossPay += "<thead>";
GrossPay += "<tr>";
GrossPay += "<th style='border: 1px solid;'>Description</th>";
//GrossPay += "<th style='border: 1px solid;'>Amount</th>";
GrossPay += "<th style='border: 1px solid;'></th>";
GrossPay += "</tr>";
GrossPay += "</thead>";
GrossPay += "<tbody></tbody>";
GrossPay += "</table>";
GrossPay += "</td>";
GrossPay += "</tr>";
GrossPay += "</table>";
GrossPay += "</td>";
GrossPay += "</tr>";

var Allowance = "";
Allowance += "<tr style='background-color: azure;height:40px; display:none;' class='A5'>";
Allowance += "<td colspan='4' align='right'>";
Allowance += "<table style='width:56%' id='tblAllowance'>";
Allowance += "<tr>";
Allowance += "<td colspan='4' width='100%'>";
Allowance += "<table id='Grid_Allowance' class='Grid'>";
Allowance += "<tr>";
Allowance += "<td><input type='text' id='txtAllowanceDesc' class='form-control' style='width:420px;' /><input type='hidden' id='hdnAllowanceID' /></td>";
//Allowance += "<td></td>"//"<td><input type='text' id='txtAllowanceAmount' class='form-control allownumericwithoutdecimal' style='text-align: right;' /></td>";
Allowance += "<td><input type='button' id='btnAllowance' value='Add' class='btn btn-primary' onclick='SaveOtherSourceIncome(this, \"" + 'Grid_Allowance' + "\", \"" + 'trclass' + "\", \"" + 'txtAllowanceDesc' + "\", \"" + 'clssAllowanceAmt' + "\")' /></td>";
Allowance += "</tr>";
Allowance += "<thead>";
Allowance += "<tr>";
Allowance += "<th style='border: 1px solid;'>Description</th>";
//Allowance += "<th style='border: 1px solid;'>Amount</th>";
Allowance += "<th style='border: 1px solid;'></th>";
Allowance += "</tr>";
Allowance += "</thead>";
Allowance += "<tbody></tbody>";
Allowance += "</table>";
Allowance += "</td>";
Allowance += "</tr>";
Allowance += "</table>";
Allowance += "</td>";
Allowance += "</tr>";

var Deduction = "";
Deduction += "<tr style='background-color: azure;height:40px; display:none;' class='A6'>";
Deduction += "<td colspan='4' align='right'>";
Deduction += "<table style='width:56%' id='tblDeduction'>";
Deduction += "<tr>";
Deduction += "<td colspan='4' width='100%'>";
Deduction += "<table id='Grid_Deduction' class='Grid'>";
Deduction += "<tr>";
Deduction += "<td><input type='text' id='txtDeductionDesc' class='form-control' style='width:420px;' /><input type='hidden' id='hdnDeductionID' /></td>";
//Deduction += "<td></td>"//"<td><input type='text' id='txtDeductionAmount' class='form-control allownumericwithoutdecimal' style='text-align: right;' /></td>";
Deduction += "<td><input type='button' id='btnDeduction' value='Add' class='btn btn-primary' onclick='SaveOtherSourceIncome(this, \"" + 'Grid_Deduction' + "\", \"" + 'trclass' + "\", \"" + 'txtDeductionDesc' + "\", \"" + 'clssDeductionAmt' + "\")' /></td>";
Deduction += "</tr>";
Deduction += "<thead>";
Deduction += "<tr>";
Deduction += "<th style='border: 1px solid;'>Description</th>";
//Deduction += "<th style='border: 1px solid;'>Amount</th>";
Deduction += "<th style='border: 1px solid;'></th>";
Deduction += "</tr>";
Deduction += "</thead>";
Deduction += "<tbody></tbody>";
Deduction += "</table>";
Deduction += "</td>";
Deduction += "</tr>";
Deduction += "</table>";
Deduction += "</td>";
Deduction += "</tr>";

var Relief = "";
Relief += "<tr style='background-color: azure;height:40px; display:none;' class='A7'>";
Relief += "<td colspan='4' align='right'>";
Relief += "<table style='width:56%' id='tblRelief'>";
Relief += "<tr>";
Relief += "<td colspan='4' width='100%'>";
Relief += "<table id='Grid_Relief' class='Grid'>";
Relief += "<tr>";
Relief += "<td><input type='text' id='txtReliefDesc' class='form-control' style='width:420px;' /><input type='hidden' id='hdnReliefID' /></td>";
//Relief += "<td></td>"//"<td><input type='text' id='txtDeductionAmount' class='form-control allownumericwithoutdecimal' style='text-align: right;' /></td>";
Relief += "<td><input type='button' id='btnRelief' value='Add' class='btn btn-primary' onclick='SaveOtherSourceIncome(this, \"" + 'Grid_Relief' + "\", \"" + 'trclass' + "\", \"" + 'txtReliefDesc' + "\")' /></td>";
Relief += "</tr>";
Relief += "<thead>";
Relief += "<tr>";
Relief += "<th style='border: 1px solid;'>Description</th>";
//Relief += "<th style='border: 1px solid;'>Amount</th>";
Relief += "<th style='border: 1px solid;'></th>";
Relief += "</tr>";
Relief += "</thead>";
Relief += "<tbody></tbody>";
Relief += "</table>";
Relief += "</td>";
Relief += "</tr>";
Relief += "</table>";
Relief += "</td>";
Relief += "</tr>";


$(document).ready(function () {
    SalFinYearKeyUp();
    AutocompleteEmp();

    $('#tbl_1').on('focus', '#txtOtherDesc', function () {
        //AutocompleteOtherSourceIncome(this);
    });
    $('#tbl_1').on('focus', '#btnOtherIncSrc', function () {
        //OtherSourceIncomeAdd();
    });


    $('#tbl_1').on('focus', '#txtChallanDate', function () {
        $(this).mask("99/99/9999", { placeholder: "_" });
        $(this).datepicker({
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow',
            dateFormat: 'dd/mm/yy'
        });
    });
    $('#tbl_1').on('focus', '.allownumericwithoutdecimal', function () {
        $(this).on('copy paste cut', function (event) {
            event.preventDefault();
        });
        $(this).on('keypress keyup blur', function () {
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
    });

    $('#btnSaveMedicalAllowance').on('click', function () {
        MedicalGrossClick('', 'Save');
    });

    $("#divEmployee").on("hidden.bs.modal", function () {
        AfterSelectEmp();
    });

    $('#tbl_1').on('keyup', '.clssOtherIncomeSourceAmt', function () {
        CalculateOnTableInputKeyUp(this, 'Grid_OtherIncomeSource', 'trclass1');
    });
    $('#tbl_1').on('keyup', '.clssGrossPayAmt', function () {
        CalculateOnTableInputKeyUp(this, 'Grid_GrossPay', 'trclassGrossPay');
    });
    $('#tbl_1').on('keyup', '.clssAllowanceAmt', function () {
        CalculateOnTableInputKeyUp(this, 'Grid_Allowance', 'trclassAllowance');
    });
    $('#tbl_1').on('keyup', '.clssDeductionAmt', function () {
        CalculateOnTableInputKeyUp(this, 'Grid_Deduction', 'trclassDeduction');
    });
    $('#tbl_1').on('keyup', '.clssReliefAmt', function () {
        CalculateOnTableInputKeyUp(this, 'Grid_Relief', 'trclassRelief');
    });
})

function ValidateFinYear(SalFinYear) {
    var validation = "/^$|^[0-9]{4}-[0-9]{4}$/g";
    var startIndex = validation.indexOf('/') + 1
    var lastIndex = validation.lastIndexOf('/');
    var pattern = validation.substring(startIndex, lastIndex);
    var options = validation.substring(lastIndex + 1);
    var regExp = new RegExp(pattern, options);
    return regExp.test(SalFinYear);
}

function SalFinYearKeyUp() {
    var SalFinYear = $.trim($('#txtFrmSalFinYear').val());
    if (SalFinYear != '') {
        var istrueFinYear = ValidateFinYear(SalFinYear);
        if (istrueFinYear == true) {
            var Year1 = SalFinYear.split('-')[0];
            var Year2 = SalFinYear.split('-')[1];

            $('#txtAssessmentYear').val((parseInt(Year1) + 1) + '-' + (parseInt(Year2) + 1));
            ItaxData_OnPageLoad();
        }
        else { $('#txtAssessmentYear').val(''); }
    }
    else { $('#txtAssessmentYear').val(''); }
}

//1
function ItaxData_OnPageLoad() {
    var TransType = "pageLoad";
    var SalFinYear= $.trim($('#txtFrmSalFinYear').val());
    var EmpNo = "";
    var SectorID = "";

    var E = "{TransType:'" + TransType + "', SalFinYear:'" + SalFinYear + "', EmpNo:'" + EmpNo + "', SectorID:'" + SectorID + "'}";
    $("#overlay_Common").css('display', 'block');
    $.ajax({
        type: "POST",
        url: "/Administration/Master/ITaxCalculator/Get_ITaxData_onPageLoad",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var xmlDoc = $.parseXML(data.Data);
            var xml = $(xmlDoc);

            var slNo = 1;   //ITCalcID

            $("#tbl_1 tr.trclass").remove();
            $(xml).find("Table").each(function () {
                var rowcolor = slNo % 2 != 0 ? 'background-color:lightblue;' : '';
                var imgShowHideDetails = $(this).find("isDetail").text() == 0 ? '' : "<img src='/Content/Images/down.gif' style='float:right; cursor:pointer;' title='Show Details' onclick='ShowHideDetails(this)' />";
                var isEditable = $(this).find("isEditable").text() == 'false' ? "disabled" : '';
                var keyupOtherRelief = $(this).find("ITCalcID").text() == '14' ? "onkeyup='keyupOtherRelief(this)'" : '';

                var isDetail = "";
                if ($(this).find("isDetail").text() == 'A1') { isDetail = IncomeFrmOtherSource; }
                if ($(this).find("isDetail").text() == 'A2') { isDetail = Deduction6A; }
                if ($(this).find("isDetail").text() == 'A3') { isDetail = Challan; }
                if ($(this).find("isDetail").text() == 'A4') { isDetail = GrossPay; }
                if ($(this).find("isDetail").text() == 'A5') { isDetail = Allowance; }
                if ($(this).find("isDetail").text() == 'A6') { isDetail = Deduction; }
                if ($(this).find("isDetail").text() == 'A7') { isDetail = Relief; }
                
                $("#tbl_1").append("<tr class='trclass' style='padding-top:8px; height:40px; " + rowcolor + "'><td style='display:none;'>" +
                    $(this).find("ITCalcID").text() + "</td><td style='display:none;'>" +
                    $(this).find("isVisible").text() + "</td><td style='display:none;'>" +
                    $(this).find("isType").text() + "</td><td style='display:none;'>" +
                    $(this).find("isCalculative").text() + "</td><td style='padding: 4px; background-color: #6ad3f5; text-align:center; border-bottom:2px dotted yellow'>" +
                    slNo + "</td><td style='padding-left:10px'>" +
                    $(this).find("ITCalcDescription").text() + imgShowHideDetails + "</td><td> " +
                    "<span style='display:none;'>" + $(this).find("isDetail").text() + "</span>" +
                    "<input type='text' class='form-control allownumericwithoutdecimal' style='text-align: right' " + isEditable + " " + keyupOtherRelief + "/>" + "</td></tr>" +
                    isDetail
                    );
                slNo++;

                //GetOtherIncomeData($(this).find("ITCalcID").text());
            });//<a class='fa fa-hand-o-down' style='float:right; cursor:pointer;' ></a><img src='/Content/Images/UP.gif' />

            //CalculateTaxableIncome7();
            CalculationAll();
            $("#overlay_Common").css('display', 'none');
        },
        error: function (data, status, e) {
            alert(e);
            $("#overlay_Common").css('display', 'none');
        }
    });
}

//2
function AutocompleteEmp() {
    $("#txtEmployeeNo").autocomplete({
        source: function (request, response) {
            var TransType = "empNameAutoCom";
            var SalFinYear = $.trim($('#txtFrmSalFinYear').val());;
            var EmpNo = $.trim($("#txtEmployeeNo").val());
            var SectorID = "";

            var E = "{TransType:'" + TransType + "', SalFinYear:'" + SalFinYear + "', EmpNo:'" + EmpNo + "', SectorID:'" + SectorID + "'}";
            $.ajax({
                type: "POST",
                url: "/Administration/Master/ITaxCalculator/Get_ITaxData_onPageLoad",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var xmlDoc = $.parseXML(data.Data);
                    var xml = $(xmlDoc);
                    var DataAutoComplete = [];

                    $(xml).find("Table").each(function () {
                        DataAutoComplete.push({
                            label: $(this).find("EmpNo").text(),
                            EmpID: $(this).find("EmployeeID").text(),
                            Gender: $(this).find("gender").text()
                        });
                    });
                    response(DataAutoComplete);
                }
            });
        },
        select: function (e, i) {
            var EmpNoID = i.item.EmpID;
            //var a = EmpNoID.split('#');
            //var EmployeeID = a[0];
            //var EmpNo = a[1];

            $("#hdnEmpID").val(i.item.EmpID);
            $("#txtGender").val(i.item.Gender);

            var TransType1 = "empNameAutoComSet";
            var SalFinYear1 = $.trim($('#txtFrmSalFinYear').val());
            var EmpNo1 = $.trim(i.item.EmpID);
            var SectorID1 = $.trim($("#ddlSector").val());

            var X = "{TransType:'" + TransType1 + "', SalFinYear:'" + SalFinYear1 + "', EmpNo:'" + EmpNo1 + "', SectorID:'" + SectorID1 + "'}";
            $("#overlay_Common").css('display', 'block');
            $.ajax({
                type: "POST",
                url: "/Administration/Master/ITaxCalculator/Get_ITaxData_onPageLoad",
                data: X,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var xmlDoc = $.parseXML(data.Data);
                    var xml = $(xmlDoc);
                    var Table2 = xml.find("Table2");

                    if ($(Table2).find("FINAL").text() == "Y") {
                        alert("Please select correct Sector. !!");
                        $('#txtEmployeeNo').val(''); $('#hdnEmpID').val('');
                        $("#ddlSector").focus(); return false;
                    }
                    if ($(Table2).find("FINAL").text() == "N") {
                        alert("You have no permission to see this Employee Data. !!");
                        $('#txtEmployeeNo').val(''); $('#hdnEmpID').val('');
                        $('#txtEmployeeNo').focus(); return false;
                    }
                    if ($(Table2).find("FINAL").text() == "") {
                        AfterSelectEmp();
                    }

                    $("#overlay_Common").css('display', 'none');
                },
                error: function (data, status, e) {
                    alert(e);
                    $("#overlay_Common").css('display', 'none');
                }
            });
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtEmployeeNo').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtEmployeeNo').val('');
            $('#hdnEmpID').val('');
            $('#txtGender').val('');
        }
        if (iKeyCode == 46) {
            $('#txtEmployeeNo').val('');
            $('#hdnEmpID').val('');
            $('#txtGender').val('');
        }
    });
}

//3
function AfterSelectEmp() {

    var TransType = "empNameAutoComSetValid";
    var SalFinYear = $.trim($('#txtFrmSalFinYear').val());
    var EmpNo = $.trim($("#hdnEmpID").val());
    var SectorID = $.trim($("#ddlSector").val());

    var X = "{TransType:'" + TransType + "', SalFinYear:'" + SalFinYear + "', EmpNo:'" + EmpNo + "', SectorID:'" + SectorID + "'}";
    $("#overlay_Common").css('display', 'block');
    $.ajax({
        type: "POST",
        url: "/Administration/Master/ITaxCalculator/Get_ITaxData_onPageLoad",
        data: X,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var xmlDoc = $.parseXML(data.Data);
            var xml = $(xmlDoc);

            var sumGrossPay = 0;
            var sumPTax = 0;
            var sumItax = 0;
            var sumPF = 0;
            var sumNetPay = 0;
            var sumHBL = 0;
            var sumHRA = 0;
            var sumOtherDedu = 0;

            $("#tbl_2 tr.trclass").remove();
            $("#tbl_2 tr.trclass1").remove();
            $(xml).find("Table1").each(function () {
                sumGrossPay += (isNaN(parseFloat($.trim($(this).find("GrossPay").text()))) ? 0 : parseFloat($.trim($(this).find("GrossPay").text())));
                sumPTax += (isNaN(parseFloat($.trim($(this).find("PTax").text()))) ? 0 : parseFloat($.trim($(this).find("PTax").text())));
                sumItax += (isNaN(parseFloat($.trim($(this).find("Itax").text()))) ? 0 : parseFloat($.trim($(this).find("Itax").text())));
                sumPF += (isNaN(parseFloat($.trim($(this).find("PF").text()))) ? 0 : parseFloat($.trim($(this).find("PF").text())));
                sumNetPay += (isNaN(parseFloat($.trim($(this).find("NetPay").text()))) ? 0 : parseFloat($.trim($(this).find("NetPay").text())));
                sumHBL += (isNaN(parseFloat($.trim($(this).find("HBL").text()))) ? 0 : parseFloat($.trim($(this).find("HBL").text())));
                sumHRA += (isNaN(parseFloat($.trim($(this).find("HRA").text()))) ? 0 : parseFloat($.trim($(this).find("HRA").text())));
                sumOtherDedu += (isNaN(parseFloat($.trim($(this).find("OtherDedu").text()))) ? 0 : parseFloat($.trim($(this).find("OtherDedu").text())));

                var backgroundcolor = $(this).find("sStatus").text() == 'History' ? 'background-color:#ccbaba;' : $(this).find("sStatus").text() == 'Current' ? 'background-color:#b7deb0;' : "";
                var grossp = (isNaN(parseFloat($.trim($(this).find("GrossPay").text()))) ? 0 : parseFloat($.trim($(this).find("GrossPay").text())));
                var gross = $(this).find("sStatus").text() == 'Projection'
                    ? '<a style="cursor:pointer;color:blue;" onclick="ProjectionGrossClick(this)" onmouseover="ChangeColorRed(this)" onmouseout="ChangeColorBlue(this)">' + (grossp).toFixed(2) + '</a>'
                    : $(this).find("sStatus").text() == 'Medical'
                    ? '<a style="cursor:pointer;color:blue;" onclick="MedicalGrossClick(this,\'' + "Load" + '\')" onmouseover="ChangeColorRed(this)" onmouseout="ChangeColorBlue(this)">' + (grossp).toFixed(2) + '</a>'
                    : (grossp).toFixed(2);//,\'' + 'Load' + '
                var printButton = $(this).find("sStatus").text() == 'History' ? "<a href='javascript:void(0);'><img src='/Content/Images/Print-icon2.png' onclick='PrintHistoryPayslip(this)' title='Print Payslip' /></a>" : "";
                    //

                $("#tbl_2").append("<tr class='trclass' style=" + backgroundcolor + "><td style='display:none;'>" +
                    $(this).find("EmployeeID").text() + "</td><td style=''>" +
                    $(this).find("SalMonth").text() + "</td><td style='text-align:right;'>" +
                    gross + "</td><td style='text-align:right;'>" +
                    $(this).find("PTax").text() + "</td><td style='text-align:right;'>" +
                    $(this).find("Itax").text() + "</td><td style='text-align:right;'>" +
                    $(this).find("PF").text() + "</td><td style='text-align:right;'>" +
                    $(this).find("NetPay").text() + "</td><td style='text-align:right;'>" +
                    $(this).find("HBL").text() + "</td><td style='text-align:right;'>" +
                    $(this).find("HRA").text() + "</td><td style='text-align:right;'>" +
                    $(this).find("OtherDedu").text() + "</td><td style='text-align:center;'>" +
                    $(this).find("sStatus").text() +
                    printButton + "</td></tr>");

            });

            $("#tbl_2").append("<tr class='trclass1' style='font-weight:bold; background-color:#acdef6; color:brown'><td style='display:none;'>" +
                    "" + "</td><td style=''>" +
                    "Total" + "</td><td style='text-align:right;'>" +
                    sumGrossPay + "</td><td style='text-align:right;'>" +
                    sumPTax + "</td><td style='text-align:right;'>" +
                    sumItax + "</td><td style='text-align:right;'>" +
                    sumPF + "</td><td style='text-align:right;'>" +
                    sumNetPay + "</td><td style='text-align:right;'>" +
                    sumHBL + "</td><td style='text-align:right;'>" +
                    sumHRA + "</td><td style='text-align:right;'>" +
                    sumOtherDedu + "</td><td style='text-align:center;'>" +
                    "" + "</td></tr>");

            GetOtherIncomeData(data);
            BindDeductionUnder(data);
            BindChallan(data);

            Deduction6A = ""; Deduction6Asub = "";

            var TotalLabelAmt = 0;
            $.each($('#GridTaxDetails tr.trclass2'), function (index, value) {
                TotalLabelAmt += (isNaN(parseFloat($.trim($(this).find("td:eq(5) label").text()))) ? 0 : parseFloat($.trim($(this).find("td:eq(5) label").text())));
            });
            var xx = $("#GridTaxDetails").parent().closest('tr').attr('class');
            $.each($('#tbl_1 tr.trclass'), function (index, value) {
                var classname = $(this).closest('tr').find('span').text();
                if (classname == xx) {
                    $(this).closest('tr').find('input').val(TotalLabelAmt);
                }
            });

            $.each($('#tbl_1 input.allownumericwithoutdecimal'), function (index, value) {
                var name = $(this).closest('tr').find('td:eq(1)').text();
                var ITaxname = $(this).closest('tr').find('td:eq(2)').text();
                if (name == "GrossPay") {
                    $(this).closest('tr').find('td:eq(3) input').val(sumGrossPay);
                    CalculateOnTableInputKeyUp('', 'Grid_GrossPay', 'trclassGrossPay');
                }
                if (name == "HRA") {
                    $(this).closest('tr').find('td:eq(3) input').val(sumHRA);
                    CalculateOnTableInputKeyUp(this, 'Grid_Allowance', 'trclassAllowance');
                }
                if (name == "PTax") {
                    $(this).closest('tr').find('td:eq(3) input').val(sumPTax);
                    CalculateOnTableInputKeyUp(this, 'Grid_Deduction', 'trclassDeduction');
                }
                if (ITaxname == "ITax") {
                    $(this).closest('tr').find('td:eq(6) input').val(sumItax);
                    CalculationAll();
                }
            });

            $("#overlay_Common").css('display', 'none');
        },
        error: function (data, status, e) {
            alert(e);
            $("#overlay_Common").css('display', 'none');
        }
    });
}

//4
function BindDeductionUnder(data) {
    var xmlDoc = $.parseXML(data.Data);
    var xml = $(xmlDoc);
    var backgroundcolor = "";
    var slNo = 1;

    $("#overlay_Common").css('display', 'block');

    $("#GridTaxDetails tr.trclass2").remove();
    $(xml).find("Table5").each(function () {
        var svAmt = (isNaN(parseFloat($.trim($(this).find("SavingAmount").text()))) ? 0 : parseFloat($.trim($(this).find("SavingAmount").text())));
        var subTypeCNT = (isNaN(parseInt($.trim($(this).find("cnt").text()))) ? 0 : parseInt($.trim($(this).find("cnt").text())));
        var labelAmt = (isNaN(parseFloat($.trim($(this).find("LabelAmount").text()))) ? 0 : parseFloat($.trim($(this).find("LabelAmount").text())));

        var isDisabled = subTypeCNT > 0 ? "disabled='disabled'" : "";
        var trcolor = slNo % 2 == 0 ? "style='background-color: #e0e0b8;'" : "style='background-color: #ccbaba;'";

        var ItaxSaveID = $(this).find("ItaxSaveID").text();  //cbc0e4   ccbaba  b7deb0

        $("#GridTaxDetails").append("<tr class='trclass2' " + trcolor + "><td style='display:none;'>" +
                    $(this).find("ItaxSaveID").text() + "</td><td style='display:none;'>" +
                    $(this).find("MaxType").text() + "</td><td style='display:none;'>" +
                    $(this).find("MaxAmount").text() + "</td><td style='text-align:center;'>" +
                    //$(this).find("LabelAmount").text() + "</td><td style=''>" +
                    slNo + "</td><td style=''>" +
                    $(this).find("SavingTypeDesc").text() + "</td><td style='text-align:right;'>" +
                    "<img src='/Content/Images/down.gif' style='float:right; cursor:pointer;' title='Show Details' onclick='ShowHideDeduction(this, \"" + $(this).find("ItaxSaveID").text() + "\", \"" + slNo + "\")' />" +
                    "<label>" + labelAmt.toFixed(2) + "</label>" +
                    "<input type='text' class='allownumericwithoutdecimal' value=" + svAmt.toFixed(2) + " " + isDisabled + " onkeyup='CalculateSavingType(this)' style='text-align:right;'/>" + "</td></tr>" +

                    "<tr style='display:none; background-color:rgb(154, 154, 154)' class='trDeducChapterVIA" + slNo + "'>" +
                    "<td colspan='100%' align='right'><div style='max-height: 200px; overflow: auto; width: 95%;'>" +
                    "<table id='tblclassDeducChapterVIA" + slNo + "' class='tblclassDeducChapterVIA Grid'  ><thead><tr>" +
                    "<th style='border: 1px solid; width:110px'>Sl.No.</th>" + "<th style='border: 1px solid; text-align:center;'>Saving Type</th>" + "<th style='border: 1px solid; text-align:center;'>Amount</th>" +
                    "</tr></thead><tbody></tbody></table></div>" +

                    "<table style='width:95%;'><tbody><tr style='background-color: #b5d0da;'>" +
                    "<td style='padding: 1px;'><span style='font-weight:bold;'>Abbv</span><span style='color:red'>*</span><input class='form-control' style='text-transform: uppercase;' class='form-control' id='txtAbbreviation'/></td>" +
                    "<td style='padding: 4px;'><span style='font-weight:bold;'>Desc</span><span style='color:red'>*</span><input class='form-control' id='txtDescription'/></td>" +
                    "<td style='padding: 4px; width: 150px;'><input type='radio' name='rbSubType' class='rdSavingsValue' onclick='ClickRadioGridofGrid(this)' style='zoom:1.2;'/>Value<br/><input type='radio' name='rbSubType' class='rdSavingsPercentage' onclick='ClickRadioGridofGrid(this)' style='zoom:1.2;'/>Percentage</td>" +
                    "<td style='padding: 4px; display:none;' class='tdPercntGridofGrid'><span style='font-weight:bold;'>Percentage(%)</span><span style='color:red'>*</span><input id='txtPercentage' class='allownumericwithoutdecimal form-control' style='text-align:right'/></td>" +
                    "<td style='padding: 4px;'><span style='font-weight:bold;'>Max Amount</span><span style='color:red'>*</span><input id='txtMaxAmountsaving' class='allownumericwithoutdecimal form-control' style='text-align:right'/></td>" +
                    "<td style='padding: 4px;'><button type='button' class='btn btn-primary GridBtnAdd ' onclick='AddNewItaxSaveTpe(this, \"" + $(this).find("ItaxSaveID").text() + "\", \"" + slNo + "\")'>Add</button></td>" +
                    "</tr></tbody></table></div></td></tr>");

        var slno1 = 1;

        $("#tblclassDeducChapterVIA" + slNo + " tr.trclass3").remove();
            $(xml).find("Table6").each(function () {
                var ItaxSaveID1 = $(this).find("ItaxSavingTypeID").text();
                if (ItaxSaveID == ItaxSaveID1) {
                    var svAmt1 = (isNaN(parseFloat($.trim($(this).find("SavingAmount").text()))) ? 0 : parseFloat($.trim($(this).find("SavingAmount").text())));
                    var labelAmt = (isNaN(parseFloat($.trim($(this).find("LabelAmount").text()))) ? 0 : parseFloat($.trim($(this).find("LabelAmount").text())));

                    $("#tblclassDeducChapterVIA" + slNo).append("<tr class='trclass3' ><td style='display:none;'>" +
                            $(this).find("ItaxSavingTypeID").text() + "</td><td style='display:none;'>" +
                            $(this).find("ItaxSaveWiseID").text() + "</td><td style='display:none;'>" +
                            $(this).find("MaxType").text() + "</td><td style='display:none;'>" +
                            $(this).find("MaxAmount").text() + "</td><td style='text-align:center;'>" +
                             //+ "</td><td style='text-align:right;'>" +
                            slno1 + "</td><td style='text-align:center; width:500px;'>" +
                            $(this).find("SubSavingTypeDesc").text() + "</td><td style='text-align:right;'>" +
                            "<label>" + labelAmt.toFixed(2) + "</label>" +
                            "<input type='text' class='allownumericwithoutdecimal' value=" + svAmt1.toFixed(2) + " onkeyup='CalculateSubSavingType(this, \"" + slNo + "\")' style='text-align:right;'/>" + "</td></tr>");
                    slno1++;
                }
            });

        slNo++;
    });
    $("#overlay_Common").css('display', 'none');
}

//5
function BindChallan(data) {
    var xmlDoc = $.parseXML(data.Data);
    var xml = $(xmlDoc);
    var backgroundcolor = "";
    var slNo = 1;
    var TotalChallanAmt = 0;

    $("#overlay_Common").css('display', 'block');
    $("#tblChallan tr.trclass4").remove();
    $(xml).find("Table4").each(function () {
        var taxamt = (isNaN(parseFloat($.trim($(this).find("TaxAmount").text()))) ? 0 : parseFloat($.trim($(this).find("TaxAmount").text())));
        $("#tblChallan").append("<tr class='trclass4' ><td style='text-align:center;'>" +
                slNo + "</td><td style='text-align:left;'>" +
                $(this).find("ChallanNo").text() + "</td><td style='text-align:center;'>" +
                $(this).find("ChallanDate").text() + "</td><td style='text-align:center;'>" +
                $(this).find("BSRNo").text() + "</td><td style='text-align:right;'>" +
                taxamt.toFixed(2) + "</td><td style='text-align:center;'>" +
                "<img src='/Content/Images/Delete.png' style='cursor:pointer;' width='30px' height='25px' onclick='DeleteChallan(this)'>" + "</td></tr>");
        slNo++;
        TotalChallanAmt += taxamt;
    });
    var aa = $("#tblChallan").parent().closest('tr').attr('class');
    $.each($('#tbl_1 tr.trclass'), function (index, value) {
        var classname = $(this).closest('tr').find('span').text();
        if (classname == aa) {
            $(this).closest('tr').find('input').val(TotalChallanAmt);
        }
    });

    $("#overlay_Common").css('display', 'none');
}

//6
function AddChallanIndividual() {
    if ($.trim($('#txtChallanNo').val()) == "") { alert("Please enter Challan No."); $('#txtChallanNo').focus(); return false; }
    if ($.trim($('#txtChallanDate').val()) == "") { alert("Please enter Challan Date."); $('#txtChallanDate').focus(); return false; }
    if ($.trim($('#txtBSRNo').val()) == "") { alert("Please enter BSR No."); $('#txtBSRNo').focus(); return false; }
    if ($.trim($('#txtChallanAmt').val()) == "") { alert("Please enter Challan Amount."); $('#txtChallanAmt').focus(); return false; }

    var slNo = (isNaN(parseInt($('#tblChallan tr.trclass4:last').find('td:eq(0)').text())) ? 0 : parseInt($('#tblChallan tr.trclass4:last').find('td:eq(0)').text()));

        var taxamt = (isNaN(parseFloat($.trim($('#txtChallanAmt').val()))) ? 0 : parseFloat($.trim($('#txtChallanAmt').val())));
        $("#tblChallan").append("<tr class='trclass4' ><td style='text-align:center;'>" +
                (slNo + 1) + "</td><td style='text-align:left;'>" +
                $.trim($('#txtChallanNo').val()) + "</td><td style='text-align:center;'>" +
                $.trim($('#txtChallanDate').val()) + "</td><td style='text-align:center;'>" +
                $.trim($('#txtBSRNo').val()) + "</td><td style='text-align:right;'>" +
                taxamt.toFixed(2) + "</td><td style='text-align:center;'>" +
                "<img src='/Content/Images/Delete.png' style='cursor:pointer;' width='30px' height='25px' onclick='DeleteChallan(this)'>" + "</td></tr>");

        $('#txtChallanNo').val(''); $('#txtChallanDate').val(''); $('#txtBSRNo').val(''); $('#txtChallanAmt').val('');

        var TotalChallanAmt = 0;
        $.each($('#tblChallan tr.trclass4'), function (index, value) {
            TotalChallanAmt += (isNaN(parseFloat($.trim($(this).closest('tr').find("td:eq(4)").text()))) ? 0 : parseFloat($.trim($(this).closest('tr').find("td:eq(4)").text())));
        });
        $(_thisChallan).closest('tr').find('input').val(TotalChallanAmt);

    //CalculateTaxableIncome7();
        CalculationAll();
}

function CalculateDeductionUnder6A() {

}

function ShowHideDeduction(t, ItaxSaveID, slNo) {
    if ($(t).attr('src') == '/Content/Images/down.gif') {
        $(t).attr('src', '/Content/Images/UP.gif');
        $(t).attr('title', 'Hide Details');
        $('.trDeducChapterVIA' + slNo).css('display', '');
        _thisSavingType = t;
    }
    else {
        $(t).attr('src', '/Content/Images/down.gif');
        $(t).attr('title', 'Show Details');
        $('.trDeducChapterVIA' + slNo).css('display', 'none');
        _thisSavingType = t;
    }
}

function ShowHideDetails(t) {
    if ($(t).attr('src') == '/Content/Images/down.gif') {
        $(t).attr('src', '/Content/Images/UP.gif');
        $(t).attr('title', 'Hide Details');
        if ($(t).closest('tr').find('span').text() == 'A1') {
            $('.A1').css('display', '');
            _thisOtherSourceDetails = t;
            //GetOtherIncomeData(t);
        }
        if ($(t).closest('tr').find('span').text() == 'A2') {
            $('.A2').css('display', '');
            _thisDeduction6A = t;
        }
        if ($(t).closest('tr').find('span').text() == 'A3') {
            $('.A3').css('display', '');
            _thisChallan = t;
        }
        if ($(t).closest('tr').find('span').text() == 'A4') {
            $('.A4').css('display', '');
            _thisGrossHead = t;
            //GetOtherIncomeData(t);
        }
        if ($(t).closest('tr').find('span').text() == 'A5') {
            $('.A5').css('display', '');
            _thisAllowanceHead = t;
            //GetOtherIncomeData(t);
        }
        if ($(t).closest('tr').find('span').text() == 'A6') {
            $('.A6').css('display', '');
            _thisDeductionHead = t;
            //GetOtherIncomeData(t);
        }
        if ($(t).closest('tr').find('span').text() == 'A7') {
            $('.A7').css('display', '');
            _thisReliefHead = t;
        }
    }
    else {
        $(t).attr('src', '/Content/Images/down.gif');
        $(t).attr('title', 'Show Details');
        if ($(t).closest('tr').find('span').text() == 'A1') {
            $('.A1').css('display', 'none');
            _thisOtherSourceDetails = t;
        }
        if ($(t).closest('tr').find('span').text() == 'A2') {
            $('.A2').css('display', 'none');
            _thisDeduction6A = t;
        }
        if ($(t).closest('tr').find('span').text() == 'A3') {
            $('.A3').css('display', 'none');
            _thisChallan = t;
        }
        if ($(t).closest('tr').find('span').text() == 'A4') {
            $('.A4').css('display', 'none');
            _thisGrossHead = t;
        }
        if ($(t).closest('tr').find('span').text() == 'A5') {
            $('.A5').css('display', 'none');
            _thisAllowanceHead = t;
        }
        if ($(t).closest('tr').find('span').text() == 'A6') {
            $('.A6').css('display', 'none');
            _thisDeductionHead = t;
        }
        if ($(t).closest('tr').find('span').text() == 'A7') {
            $('.A7').css('display', 'none');
            _thisReliefHead = t;
        }
    }
}

function DeleteChallan(t) {
    $(t).closest('tr').remove();
    var TotalChallanAmt = 0; var cnt = 0;
    $.each($('#tblChallan tr.trclass4'), function (index, value) {
        TotalChallanAmt += (isNaN(parseFloat($.trim($(this).closest('tr').find("td:eq(4)").text()))) ? 0 : parseFloat($.trim($(this).closest('tr').find("td:eq(4)").text())));
        cnt++;
    });
    $(_thisChallan).closest('tr').find('input').val(TotalChallanAmt);

    //$(_thisChallan).closest('tr').find('input').attr('disabled', cnt > 0);
    //CalculateTaxableIncome7();
    CalculationAll();
}

function CalculateSubSavingType(t, slno1) {
    var maxAmt = (isNaN(parseFloat($.trim($(t).closest('tr').find("td:eq(3)").text()))) ? 0 : parseFloat($.trim($(t).closest('tr').find("td:eq(3)").text())));
    var inputValue = (isNaN(parseFloat($.trim($(t).val()))) ? 0 : parseFloat($.trim($(t).val())));

    if (maxAmt > 0) {
        if (inputValue <= maxAmt) {
            $(t).closest('tr').find('td:eq(6) label').text(inputValue.toFixed());
        }

        if (inputValue > maxAmt) {
            $(t).closest('tr').find('td:eq(6) label').text(maxAmt.toFixed());
        }
    }
    else {
        $(t).closest('tr').find('td:eq(6) label').text(inputValue.toFixed());
    }

    var TotalLabelAmt = 0;

    $.each($('#tblclassDeducChapterVIA' + slno1 + ' tr.trclass3'), function (index, value) {
        TotalLabelAmt += (isNaN(parseFloat($.trim($(this).find("td:eq(6) label").text()))) ? 0 : parseFloat($.trim($(this).find("td:eq(6) label").text())));
    });
    $(_thisSavingType).closest('tr').find('input').val(TotalLabelAmt);

    CalculateSavingType(_thisSavingType);
    //CalculateTaxableIncome7();
    CalculationAll();


}

function CalculateSavingType(t) {
    var maxAmt = (isNaN(parseFloat($.trim($(t).closest('tr').find("td:eq(2)").text()))) ? 0 : parseFloat($.trim($(t).closest('tr').find("td:eq(2)").text())));
    var inputValue = (isNaN(parseFloat($.trim($(t).closest('tr').find("td:eq(5) input").val()))) ? 0 : parseFloat($.trim($(t).closest('tr').find("td:eq(5) input").val())));

    if (maxAmt > 0) {
        if (inputValue <= maxAmt) {
            $(t).closest('tr').find('td:eq(5) label').text(inputValue.toFixed());
        }

        if (inputValue > maxAmt) {
            $(t).closest('tr').find('td:eq(5) label').text(maxAmt.toFixed());
        }
    }
    else {
        $(t).closest('tr').find('td:eq(5) label').text(inputValue.toFixed());
    }

    var TotalLabelAmt = 0;

    $.each($('#GridTaxDetails tr.trclass2'), function (index, value) {
        TotalLabelAmt += (isNaN(parseFloat($.trim($(this).find("td:eq(5) label").text()))) ? 0 : parseFloat($.trim($(this).find("td:eq(5) label").text())));
    });
    $(_thisDeduction6A).closest('tr').find('input').val(TotalLabelAmt);

    //CalculateTaxableIncome7();
    CalculationAll();
}

function keyupOtherRelief(t) {
    //CalculateTaxableIncome7();
}

function ClickRadioGridofGrid(t) {
    if ($(t).hasClass("rdSavingsValue")) {
        $(t).closest('tr').find('td:eq(3)').hide();
        $(t).closest('tr').find('td:eq(3)').find('input').val('');
        $(t).closest('tr').find('.rdSavingsPercentage').prop('checked', false);

    }
    if ($(t).hasClass("rdSavingsPercentage")) {
        $(t).closest('tr').find('td:eq(3)').show();
        $(t).closest('tr').find('.rdSavingsValue').prop('checked', false);
    }
}

function AddNewItaxSaveTpe(t, TaxTypeId, SlNo) {
    var ItaxSaveID = 0;
    var Abbre = $(t).parent().parent().find('td').find("#txtAbbreviation").val();
    var txtDescription = $(t).parent().parent().find('td').find("#txtDescription").val();
    var MaxAmount = $(t).parent().parent().find('td').find("#txtMaxAmountsaving").val();
    var Percentage = $(t).parent().parent().find('td').find("#txtPercentage").val();
    var hdnEmpID = $('#hdnEmpID').val();
    var Type = "";
    if ($(t).parent().parent().find('td').find(".rdSavingsPercentage:checked").length == 0 && $(t).parent().parent().find('td').find(".rdSavingsValue:checked").length == 0) {
        alert("Please Select Type !");
        //document.getElementById("rdSavingsValue").focus();
        return false;
    }
    if ($(t).parent().parent().find('td').find(".rdSavingsPercentage:checked").length > 0) { Type = 'P' } else { Type = 'V'; }
    if (Abbre == "") {
        $(t).parent().parent().find('td').find("#txtAbbreviation").focus();
        alert("Please Select Abbreviation !");
        return false;
    }
    if (txtDescription == "") {
        alert("Please Select Description !");
        $(t).parent().parent().find('td').find("#txtDescription").focus();
        return false;
    }
    if (MaxAmount == "") {
        alert("Please enter Max Amount !");
        $(t).parent().parent().find('td').find("#txtMaxAmountsaving").focus();
        return false;
    }
    if (Type == "P") {
        if (Percentage == "") {
            alert("Please enter Percentage !");
            $(t).parent().parent().find('td').find("#txtPercentage").focus();
            return false;
        }
        else if (parseInt(Percentage) > 100) {
            alert("Please enter right Percentage !");
            $(t).parent().parent().find('td').find("#txtPercentage").val(100);
            return false;
        }
    }
    var _data = "{ ItaxSaveID:'" + ItaxSaveID + "', Description: '" + txtDescription + "', TaxTypeId: '" + TaxTypeId +
        "', Abbre: '" + Abbre + "', MaxAmount: '" + MaxAmount + "', type: '" + Type + "', Percentage: '" + Percentage + "' }";
    //alert(_data); return false;
    $.ajax({
        type: "POST",
        url: "/Administration/Master/ITaxCalculator/SaveNewSubSavingType",
        data: _data,
        //async:false,
        //dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.Status == 200) {
                if (data.Data != null) {
                    if (data.Data != '<NewDataSet />') {

                        var xmlDoc = $.parseXML(data.Data);
                        var xml = $(xmlDoc);
                        var Table = xml.find("Table");

                        if ($(Table).find("ErrorCode").text() == "0") {
                            alert("Record not inserted !"); return false;
                        }
                        if ($(Table).find("ErrorCode").text() == "1") {

                            var slNo1 = (isNaN(parseInt($('#tblclassDeducChapterVIA' + SlNo + ' tr.trclass3:last').find('td:eq(4)').text())) ? 0 : parseInt($('#tblclassDeducChapterVIA' + SlNo + ' tr.trclass3:last').find('td:eq(4)').text()));

                            $("#tblclassDeducChapterVIA" + SlNo).append("<tr class='trclass3' ><td style='display:none;'>" +
                                    TaxTypeId + "</td><td style='display:none;'>" +
                                    $(Table).find("ItaxSaveID").text() + "</td><td style='display:none;'>" +//$(this).find("ItaxSaveWiseID").text()
                                    Type + "</td><td style='display:none;'>" +
                                    MaxAmount + "</td><td style='text-align:center;'>" +
                                    (slNo1 + 1) + "</td><td style='text-align:center; width:500px;'>" +
                                    txtDescription + "</td><td style='text-align:right;'>" +
                                    "<label>" + (0.00).toFixed(2) + "</label>" +
                                    "<input type='text' class='allownumericwithoutdecimal' value=" + (0.00).toFixed(2) + " onkeyup='CalculateSubSavingType(this, \"" + SlNo + "\")' style='text-align:right;'/>" + "</td></tr>");

                            $(t).closest('tr').find('td:eq(0)').find('input').val('');
                            $(t).closest('tr').find('td:eq(1)').find('input').val('');
                            $(t).closest('tr').find('td:eq(3)').find('input').val('');
                            $(t).closest('tr').find('td:eq(4)').find('input').val('');
                            $(t).closest('tr').find('td:eq(2)').find('.rdSavingsValue').prop('checked', false);
                            $(t).closest('tr').find('td:eq(2)').find('.rdSavingsPercentage').prop('checked', false);

                            $('#GridTaxDetails tr.trclass2').each(function (evt) {
                                var _slno = $(this).find('td:eq(3)').text();
                                if (SlNo == _slno) {
                                    $(this).find('td:eq(5) input').attr('disabled', true);
                                }
                            });
                        }

                        //PopulateGridOfGid(TaxTypeId, SlNo);
                        //$("#txtAbbreviation").val(''); $("#txtDescription").val(''); $("#txtPercentage").val(''); $("#txtMaxAmountsaving").val('');
                        //$('.rdSavingsValue').prop('checked', false); $('.rdSavingsPercentage').prop('checked', false);

                    }
                    else {
                        alert("No Record found !");
                    }
                }
                else {
                    alert("No Record found !");
                }
            }
            else {
                alert(data.Message);
            }
        },
        error: function (data) {
            alert(data.Message);
        }
    });
}

function myFunction() {
    if (document.getElementById('rbPercent').checked == true) {
        $('.Max').css('display', '');
    }
    if (document.getElementById('rbValue').checked == true) {
        $('.Max').css('display', 'none');
        $('#txtNewPercentage').val('');
    }
}

function AddNewSection(t) {
    var Gender = "";
    var Type = "";
    if (document.getElementById("rbValue").checked == false && document.getElementById("rbPercent").checked == false) {
        alert("Please Select Type !");
        document.getElementById("rbValue").focus();
        return false;
    }
    
    if (document.getElementById("rbValue").checked == true) { Type = 'V' } else { Type = 'P'; }
    if (Type == 'P') {
        if ($('#txtNewPercentage').val() == '') { alert("Please enter percentage !"); $('#txtNewPercentage').focus(); return false; }
        if (parseInt($('#txtNewPercentage').val()) > 100) {
            alert("Percent should not be greater than 100 !");
            $('#txtNewPercentage').val(100);
            return false;
        }
    } //alert(Type); return false;
    if ($('#txtGender').val() == '') { alert("Please select proper Employee from given list !"); $('#txtEmployeeNo').focus(); return false; }
    if ($('#txtGender').val() == 'Male') { Gender = 'M'; } else { Gender = 'F'; }
    if ($('#txtNewSection').val() == '') { alert("Please Enter Section!"); $('#txtNewSection').focus(); return false; } else {
        if ($('#txtNewMaxAmount').val() == '') { alert("Please Enter Max Amount!"); $('#txtNewMaxAmount').focus(); return false; } else {
            $.ajax({
                type: "POST",
                url: "/Administration/Master/ITaxCalculator/AddNewSection",
                data: "{Section:'" + $('#txtNewSection').val() + "', MaxAmount:'" + $('#txtNewMaxAmount').val() +
                    "', FinYear:'" + $('#txtFrmSalFinYear').val() + "',Gender:'" + Gender + "',Type:'" + Type +
                    "', Percentage:'" + $('#txtNewPercentage').val() + "'}",
                cache: false,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    if (data.Status == 200) {
                        if (data.Data != null) {
                            if (data.Data != '<NewDataSet />') {

                                var xmlDoc = $.parseXML(data.Data);
                                var xml = $(xmlDoc);
                                var Table = xml.find("Table");

                                if (parseInt($(Table).find("strID").text()) > 0) {

                                    var slNo1 = (isNaN(parseInt($('#GridTaxDetails tr.trclass2:last').find('td:eq(3)').text())) ? 0 : parseInt($('#GridTaxDetails tr.trclass2:last').find('td:eq(3)').text()));
                                    var trcolor = (slNo1 +1) % 2 == 0 ? "style='background-color: #e0e0b8;'" : "style='background-color: #ccbaba;'";

                                    $("#GridTaxDetails").append("<tr class='trclass2' " + trcolor + "><td style='display:none;'>" +
                                            $(Table).find("strID").text() + "</td><td style='display:none;'>" +
                                            Type + "</td><td style='display:none;'>" +
                                            $('#txtNewMaxAmount').val() + "</td><td style='text-align:center;'>" +
                                            (slNo1 + 1) + "</td><td style=''>" +
                                            $('#txtNewSection').val() + "</td><td style='text-align:right;'>" +
                                            "<img src='/Content/Images/down.gif' style='float:right; cursor:pointer;' title='Show Details' onclick='ShowHideDeduction(this, \"" + $(Table).find("strID").text() + "\", \"" + (slNo1 + 1) + "\")' />" +
                                            "<label>" + (0.00).toFixed(2) + "</label>" +
                                            "<input type='text' class='allownumericwithoutdecimal' value=" + (0.00).toFixed(2) + " onkeyup='CalculateSavingType(this)' style='text-align:right;'/>" + "</td></tr>" +

                                            "<tr style='display:none; background-color:rgb(154, 154, 154)' class='trDeducChapterVIA" + (slNo1 + 1) + "'>" +
                                            "<td colspan='100%' align='right'><div style='max-height: 200px; overflow: auto; width: 95%;'>" +
                                            "<table id='tblclassDeducChapterVIA" + (slNo1 + 1) + "' class='tblclassDeducChapterVIA Grid'  ><thead><tr>" +
                                            "<th style='border: 1px solid; width:110px'>Sl.No.</th>" + "<th style='border: 1px solid; text-align:center;'>Saving Type</th>" + "<th style='border: 1px solid; text-align:center;'>Amount</th>" +
                                            "</tr></thead><tbody></tbody></table></div>" +

                                            "<table style='width:95%;'><tbody><tr style='background-color: #b5d0da;'>" +
                                            "<td style='padding: 1px;'><span style='font-weight:bold;'>Abbv</span><span style='color:red'>*</span><input class='form-control' style='text-transform: uppercase;' class='form-control' id='txtAbbreviation'/></td>" +
                                            "<td style='padding: 4px;'><span style='font-weight:bold;'>Desc</span><span style='color:red'>*</span><input class='form-control' id='txtDescription'/></td>" +
                                            "<td style='padding: 4px; width: 150px;'><input type='radio' name='rbSubType' class='rdSavingsValue' onclick='ClickRadioGridofGrid(this)' style='zoom:1.2;'/>Value<br/><input type='radio' name='rbSubType' class='rdSavingsPercentage' onclick='ClickRadioGridofGrid(this)' style='zoom:1.2;'/>Percentage</td>" +
                                            "<td style='padding: 4px; display:none;' class='tdPercntGridofGrid'><span style='font-weight:bold;'>Percentage(%)</span><span style='color:red'>*</span><input id='txtPercentage' class='allownumericwithoutdecimal form-control' style='text-align:right'/></td>" +
                                            "<td style='padding: 4px;'><span style='font-weight:bold;'>Max Amount</span><span style='color:red'>*</span><input id='txtMaxAmountsaving' class='allownumericwithoutdecimal form-control' style='text-align:right'/></td>" +
                                            "<td style='padding: 4px;'><button type='button' class='btn btn-primary GridBtnAdd ' onclick='AddNewItaxSaveTpe(this, \"" + $(Table).find("strID").text() + "\", \"" + (slNo1 + 1) + "\")'>Add</button></td>" +
                                            "</tr></tbody></table></div></td></tr>");



                                    document.getElementById('rbValue').checked = true;
                                    $('#txtNewSection').val('');
                                    $('#txtNewMaxAmount').val('');
                                    $('.Max').css('display', 'none');
                                    $('#txtNewPercentage').val('');
                                }
                            }
                        }
                    }
                }
            });
        }
    }
}

function FinalSaveRecord() {
    if ($('#txtEmployeeNo').val() == "") { alert("Please select proper Employee from given list !"); $('#txtEmployeeNo').focus(); return false; }
    if ($('#hdnEmpID').val() == "") { alert("Please select proper Employee from given list !"); $('#txtEmployeeNo').focus(); return false; }
    if ($('#txtFrmSalFinYear').val() == "") { alert("Please enter proper Salary Financial year !"); $('#txtFrmSalFinYear').focus(); return false; }
    if ($('#txtAssessmentYear').val() == "") { alert("Please enter proper Salary Financial year !"); $('#txtAssessmentYear').focus(); return false; }

    var TransType = "finalSave";
    var EmpID = $.trim($('#hdnEmpID').val());
    var SalFinYear = $.trim($('#txtFrmSalFinYear').val());
    var AssessmentYear = $.trim($('#txtAssessmentYear').val());

    var arrMainTable = []; var cntMainTable = 0;
    $.each($('#tbl_1 tr.trclass'), function (index, value) {
        cntMainTable++;
        var ITCalcID = $.trim($(this).closest('tr').find('td:eq(0)').text());
        var Amount = $.trim($(this).closest('tr').find("td:eq(6) input").val());

        arrMainTable.push({ 'ITCalcID': ITCalcID, 'Amount': Amount });
    });

    var arrChallan = [];
    $.each($('#tblChallan tr.trclass4'), function (index, value) {
        var SubDetailID = $.trim($(this).closest('tr').find('td:eq(0)').text());
        var ChallanNo = $.trim($(this).closest('tr').find('td:eq(1)').text());
        var ChallanDate = $.trim($(this).closest('tr').find("td:eq(2)").text());
        var BSRNo = $.trim($(this).closest('tr').find('td:eq(3)').text());
        var TaxAmount = $.trim($(this).closest('tr').find("td:eq(4)").text());

        arrChallan.push({ 'SubDetailID': SubDetailID, 'ChallanNo': ChallanNo, 'ChallanDate': ChallanDate, 'BSRNo': BSRNo, 'TaxAmount': TaxAmount });
    });

    var arrOtherIncome = [];
    $.each($('#Grid_GrossPay tr.trclassGrossPay'), function (index, value) {
        var ITCalcID = $.trim($(this).closest('tr').find('td:eq(3) span').text());
        var OtherSourceId = $.trim($(this).closest('tr').find('td:eq(0)').text());
        var OtherIncome = $.trim($(this).closest('tr').find('td:eq(3) input').val());

        arrOtherIncome.push({ 'ITCalcID': ITCalcID, 'OtherSourceId': OtherSourceId, 'OtherIncome': OtherIncome });
    });
    $.each($('#Grid_Allowance tr.trclassAllowance'), function (index, value) {
        var ITCalcID = $.trim($(this).closest('tr').find('td:eq(3) span').text());
        var OtherSourceId = $.trim($(this).closest('tr').find('td:eq(0)').text());
        var OtherIncome = $.trim($(this).closest('tr').find('td:eq(3) input').val());

        arrOtherIncome.push({ 'ITCalcID': ITCalcID, 'OtherSourceId': OtherSourceId, 'OtherIncome': OtherIncome });
    });
    $.each($('#Grid_Deduction tr.trclassDeduction'), function (index, value) {
        var ITCalcID = $.trim($(this).closest('tr').find('td:eq(3) span').text());
        var OtherSourceId = $.trim($(this).closest('tr').find('td:eq(0)').text());
        var OtherIncome = $.trim($(this).closest('tr').find('td:eq(3) input').val());

        arrOtherIncome.push({ 'ITCalcID': ITCalcID, 'OtherSourceId': OtherSourceId, 'OtherIncome': OtherIncome });
    });
    $.each($('#Grid_OtherIncomeSource tr.trclass1'), function (index, value) {
        var ITCalcID = $.trim($(this).closest('tr').find('td:eq(3) span').text());
        var OtherSourceId = $.trim($(this).closest('tr').find('td:eq(0)').text());
        var OtherIncome = $.trim($(this).closest('tr').find('td:eq(3) input').val());

        arrOtherIncome.push({ 'ITCalcID': ITCalcID, 'OtherSourceId': OtherSourceId, 'OtherIncome': OtherIncome });
    });
    $.each($('#Grid_Relief tr.trclassRelief'), function (index, value) {
        var ITCalcID = $.trim($(this).closest('tr').find('td:eq(3) span').text());
        var OtherSourceId = $.trim($(this).closest('tr').find('td:eq(0)').text());
        var OtherIncome = $.trim($(this).closest('tr').find('td:eq(3) input').val());

        arrOtherIncome.push({ 'ITCalcID': ITCalcID, 'OtherSourceId': OtherSourceId, 'OtherIncome': OtherIncome });
    });

    var arrParentDeduction6 = [];
    $.each($('#GridTaxDetails tr.trclass2'), function (index, value) {
        var ItaxEmpDetailsID = $.trim($(this).closest('tr').find('td:eq(3)').text());
        var ITaxTypeID = $.trim($(this).closest('tr').find('td:eq(0)').text());
        var LabelAmount = $.trim($(this).closest('tr').find('td:eq(5) label').text());
        var SavingAmount = $.trim($(this).closest('tr').find('td:eq(5) input').val());

        arrParentDeduction6.push({ 'ItaxEmpDetailsID': ItaxEmpDetailsID, 'ITaxTypeID': ITaxTypeID, 'LabelAmount': LabelAmount, 'SavingAmount': SavingAmount });
    });

    var arrChildDeduction6 = [];
    $.each($('.tblclassDeducChapterVIA tr.trclass3'), function (index, value) {
        var ItaxEmpDetailsID = $.trim($(this).closest('tr').find('td:eq(4)').text());
        var ITaxTypeID = $.trim($(this).closest('tr').find('td:eq(0)').text());;
        var ItaxSubSavingTypeID = $.trim($(this).closest('tr').find('td:eq(1)').text());
        var LabelAmount = $.trim($(this).closest('tr').find('td:eq(6) label').text());
        var SavingAmount = $.trim($(this).closest('tr').find('td:eq(6) input').val());

        arrChildDeduction6.push({ 'ItaxEmpDetailsID': ItaxEmpDetailsID, 'ITaxTypeID': ITaxTypeID, 'ItaxSubSavingTypeID': ItaxSubSavingTypeID, 'LabelAmount': LabelAmount, 'SavingAmount': SavingAmount });
    });

    //console.log(arrChildDeduction6); return false;

    
    if (cntMainTable == 0) { alert("No Data Found"); return false; }

    var ArrayMainTable = JSON.stringify(arrMainTable);
    var ArrayChallan = JSON.stringify(arrChallan);
    var ArrayOtherIncome = JSON.stringify(arrOtherIncome);
    var ArrayParentDeduction6 = JSON.stringify(arrParentDeduction6);
    var ArrayChildDeduction6 = JSON.stringify(arrChildDeduction6);

    var S = "{TransType:'" + TransType + "', EmpID:'" + EmpID + "', SalFinYear:'" + SalFinYear + "', AssessmentYear:'" + AssessmentYear +
        "', ArrayMainTable:'" + ArrayMainTable + "', ArrayChallan:'" + ArrayChallan + "', ArrayOtherIncome:'" + ArrayOtherIncome +
        "', ArrayParentDeduction6:'" + ArrayParentDeduction6 + "', ArrayChildDeduction6:'" + ArrayChildDeduction6 + "'}";

    //console.log(S); return false;

    var conf = confirm("Are you sure want to Save ?");
    if (conf == true) {
        $("#overlay_Common").css('display', 'block');
        
        $.ajax({
            type: "POST",
            url: "/Administration/Master/ITaxCalculator/FinalSave",
            data: S,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.Status == 200) {
                    if (data.Data != null) {
                        if (data.Data != '<NewDataSet />') {
                            var xmlDoc = $.parseXML(data.Data);
                            var xml = $(xmlDoc);
                            var Table = xml.find("Table");
                            if (parseInt($(Table).find("strID").text()) > 0) {
                                alert("Record Inserted successfully !");
                                FinalRefresh();
                            }
                            else {
                                alert(data.Message);
                            }

                            $("#overlay_Common").css('display', 'block');
                        }
                    }
                }
            },
            error: function (data, status, e) {
                alert(e);
                $("#overlay_Common").css('display', 'block');
            }
        });
    }
}

function FinalRefresh() {
    window.location.href = "";
}

function ChangeColorRed(t) {
    $(t).css('color', 'red');
}

function ChangeColorBlue(t) {
    $(t).css('color', 'blue');
}

function ProjectionGrossClick(_this) {
    var EmpNoID = $.trim($("#hdnEmpID").val());
    var a = EmpNoID.split('#');
    var EmpNo = a[1];

    var SectorID = $("#ddlSector").val();
    if (SectorID == "") {
        alert('Please Select Sector !'); $('#txtSearchEmpNo').val(''); $("#hdnEmployeeNo").val(''); $("#ddlSector").focus(); return false;
    }

    var SalMonth = $.trim($(_this).closest('tr').find('td:eq(1)').text());

    if (EmpNo != '') {
        var E = "{EmployeeNo: '" + EmpNo + "', SectorID: '" + SectorID + "', SalMonth:'" + SalMonth + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/ITaxCalculator/Get_Employee",
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                var t = D.Data;
                var table = t.Table;
                var table1 = t.Table1;
                var table2 = t.Table2;

                if (table.length > 0) {
                    Bind_Table(table);

                    $('#lblSalMonth').text($.trim($(_this).closest('tr').find('td:eq(1)').text()));
                    $('#divEmployee').modal('show');
                    $( "#tabs" ).tabs({ active: 2 });
                }

            }
        });
    }

    
}

function MedicalGrossClick(_this, TransactionType) {
    var EmpNoID = $.trim($("#hdnEmpID").val());
    var a = EmpNoID.split('#');
    var EmployeeID = a[0];

    var FinYear = $.trim($('#txtSalFinYear').val());
    var MedicalAllowance = $.trim($('#txtMedicalAllowance').val());

    if (EmployeeID != '') {

       // TransactionType,  EmployeeID,  FinYear,  MedicalAllowance

        var E = "{TransactionType: '" + TransactionType + "', EmployeeID: '" + EmployeeID + "', FinYear: '" + FinYear + "', MedicalAllowance:'" + MedicalAllowance + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/ITaxCalculator/MedicalAllowance",
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var xmlDoc = $.parseXML(data.Data);
                var xml = $(xmlDoc);
                var Table = xml.find("Table");
                
                if (TransactionType == 'Load') {
                    var MedicalAllowanceAmt = (isNaN(parseFloat($.trim($(Table).find("MedicalAllowance").text()))) ? 0 : parseFloat($.trim($(Table).find("MedicalAllowance").text())));
                    $('#txtMedicalAllowance').val((MedicalAllowanceAmt).toFixed(2));
                    $('#divEmployeeMedical').modal('show');
                }
                if (TransactionType == 'Save') {
                    if ($(Table).find("MSG").text() == '1') {
                        alert("Record Inserted Successfully !");
                        AfterSelectEmp();
                        $('#divEmployeeMedical').modal('hide');
                    }
                }
            }
        });
    }
}

function PrintHistoryPayslip(t) {
    //EmpID, SalMonth, status
    var EmpID = $.trim($('#hdnEmpID').val()) != "" ? $.trim($('#hdnEmpID').val()).split("#")[0] : 0;
    var SalMonth = $(t).closest('tr').find('td:eq(1)').text();
    var status = 'H';

    var FormName = '';
    var ReportName = '';
    var ReportType = '';
    var StrFinYr = '';
    var StrSecID;
    var StrSalMonth;
    var StrSalMonthID;
    var StrCenID;
    var StrReportTyp = '';
    ReportName = "PaySlip_new_LocationWISE_A5";//"History_PaySlip_new_LocationWISE_A5";
    var EmployeeID = EmpID;
    var SalMonth = SalMonth;
    var Status = status;
    var SecID = $('#ddlSector').val()
    FormName = "EmployeeWisePayReport.aspx";

    ReportType = "PaySlipReport";

    var E = '';
    E = "{EmployeeID:'" + EmployeeID + "',SalMonth:'" + SalMonth + "',Status:'" + Status + "',SecID:'" + SecID + "', FormName:'" + FormName + "', ReportName:'" + ReportName + "',ReportType:'" + ReportType + "'}";

    $.ajax({

        type: "POST",
        url: "/Administration/Master/ProjectedSalary/Report_Paravalue_Payslip",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (msg) {
            var left = ($(window).width() / 2) - (900 / 2),
           top = ($(window).height() / 2) - (600 / 2),
           popup = window.open("/ReportView.aspx?ReportTypeID=" + "8" + "&ReportID=" + "8" + "&SalMonth=" + $("#txtSalMonth").val(), "popup", "width=900, height=600, top=" + top + ", left=" + left);
            //reporttypeid and reportid see from table mst_reportconfig and dat_reportconfig
            popup.focus();
        }
    });
}

function ReGenerate() {
    var TransType = "empNameAutoComSetValidRegen";
    var SalFinYear = $.trim($('#txtFrmSalFinYear').val());
    var EmpNo = $.trim($("#hdnEmpID").val());
    var SectorID = $.trim($("#ddlSector").val());

    var X = "{TransType:'" + TransType + "', SalFinYear:'" + SalFinYear + "', EmpNo:'" + EmpNo + "', SectorID:'" + SectorID + "'}";
    $("#overlay_Common").css('display', 'block');
    $.ajax({
        type: "POST",
        url: "/Administration/Master/ITaxCalculator/Get_ITaxData_onPageLoad",
        data: X,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var xmlDoc = $.parseXML(data.Data);
            var xml = $(xmlDoc);

            var sumGrossPay = 0;
            var sumPTax = 0;
            var sumItax = 0;
            var sumPF = 0;
            var sumNetPay = 0;
            var sumHBL = 0;
            var sumHRA = 0;
            var sumOtherDedu = 0;

            $("#tbl_2 tr.trclass").remove();
            $("#tbl_2 tr.trclass1").remove();
            $(xml).find("Table1").each(function () {
                sumGrossPay += (isNaN(parseFloat($.trim($(this).find("GrossPay").text()))) ? 0 : parseFloat($.trim($(this).find("GrossPay").text())));
                sumPTax += (isNaN(parseFloat($.trim($(this).find("PTax").text()))) ? 0 : parseFloat($.trim($(this).find("PTax").text())));
                sumItax += (isNaN(parseFloat($.trim($(this).find("Itax").text()))) ? 0 : parseFloat($.trim($(this).find("Itax").text())));
                sumPF += (isNaN(parseFloat($.trim($(this).find("PF").text()))) ? 0 : parseFloat($.trim($(this).find("PF").text())));
                sumNetPay += (isNaN(parseFloat($.trim($(this).find("NetPay").text()))) ? 0 : parseFloat($.trim($(this).find("NetPay").text())));
                sumHBL += (isNaN(parseFloat($.trim($(this).find("HBL").text()))) ? 0 : parseFloat($.trim($(this).find("HBL").text())));
                sumHRA += (isNaN(parseFloat($.trim($(this).find("HRA").text()))) ? 0 : parseFloat($.trim($(this).find("HRA").text())));
                sumOtherDedu += (isNaN(parseFloat($.trim($(this).find("OtherDedu").text()))) ? 0 : parseFloat($.trim($(this).find("OtherDedu").text())));

                var backgroundcolor = $(this).find("sStatus").text() == 'History' ? 'background-color:#ccbaba;' : $(this).find("sStatus").text() == 'Current' ? 'background-color:#b7deb0;' : "";
                var grossp = (isNaN(parseFloat($.trim($(this).find("GrossPay").text()))) ? 0 : parseFloat($.trim($(this).find("GrossPay").text())));
                var gross = $(this).find("sStatus").text() == 'Projection'
                    ? '<a style="cursor:pointer;color:blue;" onclick="ProjectionGrossClick(this)" onmouseover="ChangeColorRed(this)" onmouseout="ChangeColorBlue(this)">' + (grossp).toFixed(2) + '</a>'
                    : $(this).find("sStatus").text() == 'Medical'
                    ? '<a style="cursor:pointer;color:blue;" onclick="MedicalGrossClick(this,\'' + "Load" + '\')" onmouseover="ChangeColorRed(this)" onmouseout="ChangeColorBlue(this)">' + (grossp).toFixed(2) + '</a>'
                    : (grossp).toFixed(2);
                var printButton = $(this).find("sStatus").text() == 'History' ? "<a href='javascript:void(0);'><img src='/Content/Images/Print-icon2.png' onclick='PrintHistoryPayslip(this)' title='Print Payslip' /></a>" : "";

                $("#tbl_2").append("<tr class='trclass' style=" + backgroundcolor + "><td style='display:none;'>" +
                    $(this).find("EmployeeID").text() + "</td><td style=''>" +
                    $(this).find("SalMonth").text() + "</td><td style='text-align:right;'>" +
                    gross + "</td><td style='text-align:right;'>" +
                    $(this).find("PTax").text() + "</td><td style='text-align:right;'>" +
                    $(this).find("Itax").text() + "</td><td style='text-align:right;'>" +
                    $(this).find("PF").text() + "</td><td style='text-align:right;'>" +
                    $(this).find("NetPay").text() + "</td><td style='text-align:right;'>" +
                    $(this).find("HBL").text() + "</td><td style='text-align:right;'>" +
                    $(this).find("HRA").text() + "</td><td style='text-align:right;'>" +
                    $(this).find("OtherDedu").text() + "</td><td style='text-align:center;'>" +
                    $(this).find("sStatus").text() +
                    printButton + "</td></tr>");

            });

            $("#tbl_2").append("<tr class='trclass1' style='font-weight:bold; background-color:#acdef6; color:brown'><td style='display:none;'>" +
                    "" + "</td><td style=''>" +
                    "Total" + "</td><td style='text-align:right;'>" +
                    sumGrossPay + "</td><td style='text-align:right;'>" +
                    sumPTax + "</td><td style='text-align:right;'>" +
                    sumItax + "</td><td style='text-align:right;'>" +
                    sumPF + "</td><td style='text-align:right;'>" +
                    sumNetPay + "</td><td style='text-align:right;'>" +
                    sumHBL + "</td><td style='text-align:right;'>" +
                    sumHRA + "</td><td style='text-align:right;'>" +
                    sumOtherDedu + "</td><td style='text-align:center;'>" +
                    "" + "</td></tr>");

            GetOtherIncomeData(data);
            BindDeductionUnder(data);
            BindChallan(data);

            var TotalLabelAmt = 0;
            $.each($('#GridTaxDetails tr.trclass2'), function (index, value) {
                TotalLabelAmt += (isNaN(parseFloat($.trim($(this).find("td:eq(5) label").text()))) ? 0 : parseFloat($.trim($(this).find("td:eq(5) label").text())));
            });
            var xx = $("#GridTaxDetails").parent().closest('tr').attr('class');
            $.each($('#tbl_1 tr.trclass'), function (index, value) {
                var classname = $(this).closest('tr').find('span').text();
                if (classname == xx) {
                    $(this).closest('tr').find('input').val(TotalLabelAmt);
                }
            });

            $.each($('#tbl_1 input.allownumericwithoutdecimal'), function (index, value) {
                var name = $(this).closest('tr').find('td:eq(1)').text();
                var ITaxname = $(this).closest('tr').find('td:eq(2)').text();
                if (name == "GrossPay") {
                    $(this).closest('tr').find('td:eq(3) input').val(sumGrossPay);
                    CalculateOnTableInputKeyUp('', 'Grid_GrossPay', 'trclassGrossPay');
                }
                if (name == "HRA") {
                    $(this).closest('tr').find('td:eq(3) input').val(sumHRA);
                    CalculateOnTableInputKeyUp(this, 'Grid_Allowance', 'trclassAllowance');
                }
                if (name == "PTax") {
                    $(this).closest('tr').find('td:eq(3) input').val(sumPTax);
                    CalculateOnTableInputKeyUp(this, 'Grid_Deduction', 'trclassDeduction');
                }
                if (ITaxname == "ITax") {
                    $(this).closest('tr').find('td:eq(6) input').val(sumItax);
                }
            });
            $("#overlay_Common").css('display', 'none');
        },
        error: function (data, status, e) {
            alert(e);
            $("#overlay_Common").css('display', 'none');
        }
    });
}





//******************************************************************************************************************************************************************************


function GetOtherIncomeData(data) {
    var xmlDoc = $.parseXML(data.Data);
    var xml = $(xmlDoc);
    var TotalAmt = 0;

    //console.log(data);
    $("#Grid_GrossPay tr.trclassGrossPay").remove();
    $("#Grid_Allowance tr.trclassAllowance").remove();
    $("#Grid_Deduction tr.trclassDeduction").remove();
    $("#Grid_OtherIncomeSource tr.trclass1").remove();
    $("#Grid_Relief tr.trclassRelief").remove();

    $(xml).find("Table7").each(function () {
        var isEditable = $(this).find("isEditable").text() == 'false' ? "disabled" : '';
        var OtherIncome = (isNaN(parseFloat($.trim($(this).find("OtherIncome").text()))) ? 0 : parseFloat($.trim($(this).find("OtherIncome").text())));

        if ($(this).find("ITaxCalcID").text() == '1') {
            $("#Grid_GrossPay").append("<tr class='trclassGrossPay'><td style='display:none;'>" +
                    $.trim($(this).find("OtherSourceId").text()) + "</td><td style='display:none;'>" +
                    $.trim($(this).find("isType").text()) + "</td><td>" +
                    $.trim($(this).find("OtherSourceName").text()) + "</td><td style='text-align:right'>" +
                    "<input type='text' class='clssGrossPayAmt allownumericwithoutdecimal' style='text-align:right;' " + isEditable + " value=" + OtherIncome + " />" +
                    "<span style='display:none;'>" + $(this).find("ITaxCalcID").text() + "</span></td></tr>");
        }
        if ($(this).find("ITaxCalcID").text() == '2') {
            $("#Grid_Allowance").append("<tr class='trclassAllowance'><td style='display:none;'>" +
                    $.trim($(this).find("OtherSourceId").text()) + "</td><td style='display:none;'>" +
                    $.trim($(this).find("isType").text()) + "</td><td>" +
                    $.trim($(this).find("OtherSourceName").text()) + "</td><td style='text-align:right'>" +
                    "<input type='text' class='clssAllowanceAmt allownumericwithoutdecimal' style='text-align:right;' " + isEditable + " value=" + OtherIncome + " />" +
                    "<span style='display:none;'>" + $(this).find("ITaxCalcID").text() + "</span></td></tr>");
        }
        if ($(this).find("ITaxCalcID").text() == '4') {
            $("#Grid_Deduction").append("<tr class='trclassDeduction'><td style='display:none;'>" +
                    $.trim($(this).find("OtherSourceId").text()) + "</td><td style='display:none;'>" +
                    $.trim($(this).find("isType").text()) + "</td><td>" +
                    $.trim($(this).find("OtherSourceName").text()) + "</td><td style='text-align:right'>" +
                    "<input type='text' class='clssDeductionAmt allownumericwithoutdecimal' style='text-align:right;' " + isEditable + " value=" + OtherIncome + " />" +
                    "<span style='display:none;'>" + $(this).find("ITaxCalcID").text() + "</span></td></tr>");
        }
        if ($(this).find("ITaxCalcID").text() == '6') {
            $("#Grid_OtherIncomeSource").append("<tr class='trclass1'><td style='display:none;'>" +
                    $.trim($(this).find("OtherSourceId").text()) + "</td><td style='display:none;'>" +
                    $.trim($(this).find("isType").text()) + "</td><td>" +
                    $.trim($(this).find("OtherSourceName").text()) + "</td><td style='text-align:right'>" +
                    "<input type='text' class='clssOtherIncomeSourceAmt allownumericwithoutdecimal' style='text-align:right;' " + isEditable + " value=" + OtherIncome + " />" +
                    "<span style='display:none;'>" + $(this).find("ITaxCalcID").text() + "</span></td></tr>");
            TotalAmt += OtherIncome;
        }
        if ($(this).find("ITaxCalcID").text() == '15') {
            $("#Grid_Relief").append("<tr class='trclassRelief'><td style='display:none;'>" +
                    $.trim($(this).find("OtherSourceId").text()) + "</td><td style='display:none;'>" +
                    $.trim($(this).find("isType").text()) + "</td><td>" +
                    $.trim($(this).find("OtherSourceName").text()) + "</td><td style='text-align:right'>" +
                    "<input type='text' class='clssReliefAmt allownumericwithoutdecimal' style='text-align:right;' " + isEditable + " value=" + OtherIncome + " />" +
                    "<span style='display:none;'>" + $(this).find("ITaxCalcID").text() + "</span></td></tr>");
        }
    });
    var aa1 = $("#Grid_OtherIncomeSource").parent().closest('tr').parent().closest('tr').attr('class');
    $.each($('#tbl_1 tr.trclass'), function (index, value) {
        var classname = $(this).closest('tr').find('span').text();
        if (classname == aa1) {
            $(this).closest('tr').find('input').val(TotalAmt);
        }
    });
}

function GetOtherIncomeData1(ITaxCalcID) {
    //$(t).closest('tr').find('td:eq(0)').text()
    var E = "{ITaxCalcID:'" + ITaxCalcID + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/ITaxCalculator/Get_AllOtherSourceIncome",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var xmlDoc = $.parseXML(data.Data)
            var xml = $(xmlDoc);

            if (ITaxCalcID == '1') {
                $("#Grid_GrossPay tr.trclassGrossPay").remove();
                $(xml).find("Table").each(function () {
                    if (ITaxCalcID == $(this).find("ITaxCalcID").text()) {
                        var isEditable = $(this).find("isEditable").text() == 'false' ? "disabled" : '';
                        $("#Grid_GrossPay").append("<tr class='trclassGrossPay'><td style='display:none;'>" +
                                $.trim($(this).find("OtherSourceId").text()) + "</td><td style='display:none;'>" +
                                $.trim($(this).find("isType").text()) + "</td><td>" +
                                $.trim($(this).find("OtherSourceName").text()) + "</td><td style='text-align:right'>" +
                                "<input type='text' class='clssGrossPayAmt allownumericwithoutdecimal' style='text-align:right;' " + isEditable + " value=" + $(this).find("OtherIncome").text() + " />" + "</td></tr>");
                    }
                });
            }
            if (ITaxCalcID == '2') {
                $("#Grid_Allowance tr.trclassAllowance").remove();
                $(xml).find("Table").each(function () {
                    if (ITaxCalcID == $(this).find("ITaxCalcID").text()) {
                        var isEditable = $(this).find("isEditable").text() == 'false' ? "disabled" : '';
                        $("#Grid_Allowance").append("<tr class='trclassAllowance'><td style='display:none;'>" +
                                $.trim($(this).find("OtherSourceId").text()) + "</td><td style='display:none;'>" +
                                $.trim($(this).find("isType").text()) + "</td><td>" +
                                $.trim($(this).find("OtherSourceName").text()) + "</td><td style='text-align:right'>" +
                                "<input type='text' class='clssAllowanceAmt allownumericwithoutdecimal' style='text-align:right;' " + isEditable + " value=" + $(this).find("OtherIncome").text() + " />" + "</td></tr>");
                    }
                });
            }
            if (ITaxCalcID == '4') {
                $("#Grid_Deduction tr.trclassDeduction").remove();
                $(xml).find("Table").each(function () {
                    if (ITaxCalcID == $(this).find("ITaxCalcID").text()) {
                        var isEditable = $(this).find("isEditable").text() == 'false' ? "disabled" : '';
                        $("#Grid_Deduction").append("<tr class='trclassDeduction'><td style='display:none;'>" +
                                $.trim($(this).find("OtherSourceId").text()) + "</td><td style='display:none;'>" +
                                $.trim($(this).find("isType").text()) + "</td><td>" +
                                $.trim($(this).find("OtherSourceName").text()) + "</td><td style='text-align:right'>" +
                                "<input type='text' class='clssDeductionAmt allownumericwithoutdecimal' style='text-align:right;' " + isEditable + " value=" + $(this).find("OtherIncome").text() + " />" + "</td></tr>");
                    }
                });
            }
            if (ITaxCalcID == '6') {
                $("#Grid_OtherIncomeSource tr.trclass1").remove();
                $(xml).find("Table").each(function () {
                    if (ITaxCalcID == $(this).find("ITaxCalcID").text()) {
                        var isEditable = $(this).find("isEditable").text() == 'false' ? "disabled" : '';
                        $("#Grid_OtherIncomeSource").append("<tr class='trclass1'><td style='display:none;'>" +
                                $.trim($(this).find("OtherSourceId").text()) + "</td><td style='display:none;'>" +
                                $.trim($(this).find("isType").text()) + "</td><td>" +
                                $.trim($(this).find("OtherSourceName").text()) + "</td><td style='text-align:right'>" +
                                "<input type='text' class='clssOtherIncomeSourceAmt allownumericwithoutdecimal' style='text-align:right;' " + isEditable + " value=" + $(this).find("OtherIncome").text() + " />" + "</td></tr>");
                    }
                });
                var TotalOtherIncomeAmount = 0;
                $.each($('#Grid_OtherIncomeSource tr.trclass1'), function (index, value) {
                    TotalOtherIncomeAmount += (isNaN(parseFloat($.trim($(this).closest('tr').find("td:eq(2) input").val()))) ? 0 : parseFloat($.trim($(this).closest('tr').find("td:eq(2) input").val())));
                });
                $(_thisOtherSourceDetails).closest('tr').find('input').val(TotalOtherIncomeAmount);
            }
            if (ITaxCalcID == '15') {
                $("#Grid_Relief tr.trclassRelief").remove();
                $(xml).find("Table").each(function () {
                    if (ITaxCalcID == $(this).find("ITaxCalcID").text()) {
                        var isEditable = $(this).find("isEditable").text() == 'false' ? "disabled" : '';
                        $("#Grid_Relief").append("<tr class='trclassRelief'><td style='display:none;'>" +
                                $.trim($(this).find("OtherSourceId").text()) + "</td><td style='display:none;'>" +
                                $.trim($(this).find("isType").text()) + "</td><td>" +
                                $.trim($(this).find("OtherSourceName").text()) + "</td><td style='text-align:right'>" +
                                "<input type='text' class='clssReliefAmt allownumericwithoutdecimal' style='text-align:right;' " + isEditable + " value=" + $(this).find("OtherIncome").text() + " />" + "</td></tr>");
                    }
                });
            }
        }
    });
}

function SaveOtherSourceIncome(t, tblid, trclass, txt, inputclass) {
    var ITaxCalcID = "";
    var zz = $("#" + tblid).parent().closest("table").parent().closest('tr').attr('class');
    $.each($('#tbl_1 tr.trclass'), function (index, value) {
        var classname = $(this).closest('tr').find('span').text();
        if (classname == zz) {
            ITaxCalcID = $(this).closest('tr').find('td:eq(0)').text();
        }
    });

    if ($.trim($('#' + txt).val()) == '') { alert("Please enter Input field !"); $('#' + txt).focus(); return false; }

    var E = "{ITaxCalcID:'" + $.trim(ITaxCalcID) + "', OtherSourceName:'" + $.trim($('#' + txt).val()) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/ITaxCalculator/Save_OtherSourceIncome",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var xmlDoc = $.parseXML(data.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");

            if (parseInt($(Table).find("OtherSourceId").text()) > 0) {
                $("#" + tblid).append("<tr class='" + trclass + "'><td style='display:none;'>" +
                            $.trim($(Table).find("OtherSourceId").text()) + "</td><td style='display:none;'>" +
                            $.trim($(Table).find("isType").text()) + "</td><td>" +
                            $.trim($(Table).find("OtherSourceName").text()) + "</td><td style='text-align:right'>" +
                            "<input type='text' class='" + inputclass + " allownumericwithoutdecimal' style='text-align:right;' />" +
                            "<span style='display:none;'>" + $.trim(ITaxCalcID) + "</span></td></tr>");
                $('#' + txt).val(''); $('#' + txt).focus();

            }
            //CalculateOnTableInputKeyUp('', tblid, trclass);
        },
        error: function (data, status, e) {
            alert(e);
        }
    });

    /*
    var ITaxCalcID = $(_thisOtherSourceDetails).closest('tr').find('td:eq(0)').text();

    if ($.trim($('#txtOtherDesc').val()) == '') { alert("Please select Other Source Income form given list !"); $('#txtOtherDesc').focus(); return false; }

    var E = "{ITaxCalcID:'" + $.trim(ITaxCalcID) + "', OtherSourceName:'" + $.trim($('#txtOtherDesc').val()) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/ITaxCalculator/Save_OtherSourceIncome",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var xmlDoc = $.parseXML(data.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");

            if (parseInt($(Table).find("OtherSourceId").text()) > 0) {
                $("#Grid_OtherIncomeSource").append("<tr class='trclass1'><td style='display:none;'>" +
                            $.trim($(Table).find("OtherSourceId").text()) + "</td><td>" +
                            $.trim($(Table).find("OtherSourceName").text()) + "</td><td style='text-align:right'>" +
                            "<input type='text' class='clssOtherIncomeSourceAmt allownumericwithoutdecimal' style='text-align:right;' />" + "</td></tr>");
                $('#txtOtherDesc').val(''); $('#txtOtherDesc').focus();
                
            }
            var TotalOtherIncomeAmount = 0;
            $.each($('#Grid_OtherIncomeSource tr.trclass1'), function (index, value) {
                TotalOtherIncomeAmount += (isNaN(parseFloat($.trim($(this).closest('tr').find("td:eq(2) input").val()))) ? 0 : parseFloat($.trim($(this).closest('tr').find("td:eq(2) input").val())));
            });
            $(_thisOtherSourceDetails).closest('tr').find('input').val(TotalOtherIncomeAmount);

            //CalculateTaxableIncome7();
        },
        error: function (data, status, e) {
            alert(e);
        }
    });
    */

}

function CalculateOnTableInputKeyUp(t, tblid, trclass) {
    var Total = 0;
    $.each($('#' + tblid + ' tr.' + trclass), function (index, value) {
        Total += (isNaN(parseFloat($.trim($(this).closest('tr').find("td:eq(3) input").val()))) ? 0 : parseFloat($.trim($(this).closest('tr').find("td:eq(3) input").val())));
    });
    //alert(Total);
    var zz = $("#" + tblid).parent().closest("table").parent().closest('tr').attr('class');

    $.each($('#tbl_1 tr.trclass'), function (index, value) {
        var classname = $(this).closest('tr').find('span').text();
        if (classname == zz) {
            $(this).closest('tr').find('input').val(Total);
        }
    });

    CalculationAll();
}

function CalculationAll() {
    $("#overlay_Common").css('display', 'block');

    var inputITCalcID = 0; inputID1 = ""; inputID2 = ""; inputID4 = ""; inputID6 = ""; inputID8 = "";
    var input3 = ""; input5 = "";
    var input7 = ""; input10 = ""; input11 = ""; input12 = ""; input13 = ""; input14 = ""; input15 = ""; input16 = ""; input17 = ""; input18 = ""; input19 = ""; label19 = "";

    $.each($('#tbl_1 tr.trclass'), function (index, value) {
        inputITCalcID = $.trim($(this).find('td:eq(0)').text());
        
        if (inputITCalcID == '1') { inputID1 = (isNaN(parseFloat($.trim($(this).find("td:eq(6) input").val()))) ? 0 : parseFloat($.trim($(this).find("td:eq(6) input").val()))); }
        if (inputITCalcID == '2') { inputID2 = (isNaN(parseFloat($.trim($(this).find("td:eq(6) input").val()))) ? 0 : parseFloat($.trim($(this).find("td:eq(6) input").val()))); }
        if (inputITCalcID == '4') { inputID4 = (isNaN(parseFloat($.trim($(this).find("td:eq(6) input").val()))) ? 0 : parseFloat($.trim($(this).find("td:eq(6) input").val()))); }
        if (inputITCalcID == '8') { inputID8 = (isNaN(parseFloat($.trim($(this).find("td:eq(6) input").val()))) ? 0 : parseFloat($.trim($(this).find("td:eq(6) input").val()))); }
        if (inputITCalcID == '6') { inputID6 = (isNaN(parseFloat($.trim($(this).find("td:eq(6) input").val()))) ? 0 : parseFloat($.trim($(this).find("td:eq(6) input").val()))); }
        if (inputITCalcID == '3') { input3 = $(this).find("td:eq(6) input"); }
        if (inputITCalcID == '5') { input5 = $(this).find("td:eq(6) input"); }
        if (inputITCalcID == '7') { input7 = $(this).find("td:eq(6) input"); }
        //if (inputITCalcID == '8') { input8 = $(this).find("td:eq(6) input"); }
        if (inputITCalcID == '9') { input9 = $(this).find("td:eq(6) input"); }
        if (inputITCalcID == '10') { input10 = $(this).find("td:eq(6) input"); }
        if (inputITCalcID == '11') { input11 = $(this).find("td:eq(6) input"); }
        if (inputITCalcID == '12') { input12 = $(this).find("td:eq(6) input"); }
        if (inputITCalcID == '13') { input13 = $(this).find("td:eq(6) input"); }
        if (inputITCalcID == '14') { input14 = $(this).find("td:eq(6) input"); }
        if (inputITCalcID == '15') { input15 = $(this).find("td:eq(6) input"); }
        if (inputITCalcID == '16') { input16 = $(this).find("td:eq(6) input"); }
        if (inputITCalcID == '17') { input17 = $(this).find("td:eq(6) input"); }
        if (inputITCalcID == '18') { input18 = $(this).find("td:eq(6) input"); }
        if (inputITCalcID == '19') { input19 = $(this).find("td:eq(6) input"); label19 = $(this).find("td:eq(5)"); }

    });

    input3.val((
                inputID1 -
                inputID2
                ).toFixed(2)
               );

    input5.val((
                input3.val() -
                inputID4
                ).toFixed(2)
               );

    input7.val((
                 parseFloat(input5.val()) +
                 inputID6
                 ).toFixed(2)
                );

    input9.val((
                 parseFloat(input7.val()) -
                 inputID8
                 ).toFixed(2)
                );
    
    var TransType = "CalcuTaxableIncome";
    var SalFinYear = $.trim($('#txtFrmSalFinYear').val());
    var EmpNo = $.trim($("#hdnEmpID").val());
    var SectorID = $.trim($("#ddlSector").val());
    var AssesmentYear = $.trim($('#txtAssessmentYear').val());;
    var TaxableIncome = input9.val();

    var X = "{TransType:'" + TransType + "', SalFinYear:'" + SalFinYear + "', EmpNo:'" + EmpNo +
        "', SectorID:'" + SectorID + "', AssesmentYear:'" + AssesmentYear + "', TaxableIncome:'" + TaxableIncome + "'}";
    
    $.ajax({
        type: "POST",
        url: "/Administration/Master/ITaxCalculator/Get_ITaxDataCalculation",
        data: X,
        dataType: "json",
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var xmlDoc = $.parseXML(data.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");

            input10.val($.trim($(Table).find("ITaxLi").text()));
            input11.val($.trim($(Table).find("Surcharge").text()));
            input12.val($.trim($(Table).find("EduCess").text()));
            input13.val($.trim($(Table).find("SecCess").text()));

            var _input10 = (isNaN(parseFloat($.trim(input10.val()))) ? 0 : parseFloat($.trim(input10.val())));
            var _input11 = (isNaN(parseFloat($.trim(input11.val()))) ? 0 : parseFloat($.trim(input11.val())));
            var _input12 = (isNaN(parseFloat($.trim(input12.val()))) ? 0 : parseFloat($.trim(input12.val())));
            var _input13 = (isNaN(parseFloat($.trim(input13.val()))) ? 0 : parseFloat($.trim(input13.val())));

            input14.val((
                        _input10 +
                        _input11 +
                        _input12 +
                        _input13
                        ).toFixed(2)
                       );

            $.each($('#tbl_1 input.allownumericwithoutdecimal'), function (index, value) {
                var name = $(this).closest('tr').find('td:eq(1)').text();
                if (name == "Rebate") {
                    $(this).closest('tr').find('td:eq(3) input').val($.trim($(Table).find("Rebate").text()));
                }
            });

            var Total = 0;
            $.each($('#Grid_Relief tr.trclassRelief'), function (index, value) {
                Total += (isNaN(parseFloat($.trim($(this).closest('tr').find("td:eq(3) input").val()))) ? 0 : parseFloat($.trim($(this).closest('tr').find("td:eq(3) input").val())));
            });
            var zz = $("#Grid_Relief").parent().closest("table").parent().closest('tr').attr('class');

            $.each($('#tbl_1 tr.trclass'), function (index, value) {
                var classname = $(this).closest('tr').find('span').text();
                if (classname == zz) {
                    $(this).closest('tr').find('input').val(Total);
                }
            });

            input16.val((
                 parseFloat(input14.val()) -
                 input15.val()
                 ).toFixed(2)
                );

            input19.val((
                 parseFloat(input16.val()) -
                 input17.val() -
                 input18.val()
                 ).toFixed(2)
                );

            var _input19 = (isNaN(parseFloat($.trim(input19.val()))) ? 0 : parseFloat($.trim(input19.val())));

            if (_input19 > 0) {
                input19.css('font-weight', 'bold');
                input19.css('font-size', 'x-large');
                label19.css('font-weight', 'bold');
                label19.css('font-size', 'x-large');
                input19.css('color', 'green');
                label19.css('color', 'green');
                label19.text("Balance Deductable (16-17-18)");
            }
            if (_input19 < 0) {
                input19.css('font-weight', 'bold');
                input19.css('font-size', 'x-large');
                label19.css('font-weight', 'bold');
                label19.css('font-size', 'x-large');
                input19.css('color', 'red');
                label19.css('color', 'red');
                label19.text("Balance Refundable (16-17-18)");
            }


            $("#overlay_Common").css('display', 'none');

        },
        error: function (data, status, e) {
            alert(e);
            $("#overlay_Common").css('display', 'none');
        }
    });
   

}


function AutocompleteOtherSourceIncome(t) {
    $(t).autocomplete({
        source: function (request, response) {
            var E = "{OtherDesc:'" + $(t).val() + "'}";
            $.ajax({
                type: "POST",
                url: "/Administration/Master/ITaxCalculator/Get_AllOtherSourceIncome",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var xmlDoc = $.parseXML(data.Data)
                    var xml = $(xmlDoc);

                    //var t = D.Data;
                    var DataAutoComplete = [];
                    //if (t.length > 0) {
                        $(xml).find("Table").each(function () {
                            DataAutoComplete.push({
                                label: $(this).find("OtherSourceName").text(),
                                ID: $(this).find("OtherSourceId").text()
                            });
                        });
                        response(DataAutoComplete);
                    //}
                }
            });
        },
        //appendTo: "#AutoComplete-District",
        select: function (e, i) {
            $("#hdnOtherDescID").val(i.item.ID);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}

function OtherSourceIncomeAdd() {
    if ($.trim($('#txtOtherDesc').val()) == '') { alert("Please select Other Source Income form given list !"); $('#txtOtherDesc').focus(); return false; }
    //if ($.trim($('#txtotherIncAmount').val()) == '') { alert("Please enter Other Source Income Amount !"); $('#txtotherIncAmount').focus(); return false; }

    var isduplicate = 0;
    $.each($('#Grid_OtherIncomeSource tr.trclass1'), function (index, value) {
        if ($.trim($('#hdnOtherDescID').val()) == $.trim($(this).closest('tr').find("td:eq(0)").text())) {
            isduplicate++;
        }
    });
    if (isduplicate > 0) { alert("This is already exist in grid !"); $('#txtotherIncAmount').focus(); return false; }

    var E = "{OtherSourceId:'" + $.trim($('#hdnOtherDescID').val()) + "', OtherSourceName:'" + $.trim($('#txtOtherDesc').val()) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/ITaxCalculator/Save_OtherSourceIncome",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var xmlDoc = $.parseXML(data.Data);
            var xml = $(xmlDoc);
            var Table = xml.find("Table");

            if ($(Table).find("OtherSourceId").text() == 0) {
                $("#Grid_OtherIncomeSource").append("<tr class='trclass1'><td style='display:none;'>" +
                        $.trim($('#hdnOtherDescID').val()) + "</td><td>" +
                        $.trim($('#txtOtherDesc').val()) + "</td><td style='text-align:right'>" +
                        $.trim($('#txtotherIncAmount').val()) + "</td><td style='text-align:center'>" +
                        "<img src='/Content/Images/Delete.png' style='cursor:pointer;' width='30px' height='25px' onclick='DeleteOtherSourceIncome(this)'/>" + "</td></tr>");

                        $('#hdnOtherDescID').val(''); $('#txtOtherDesc').val(''); $('#txtotherIncAmount').val('');
                        $('#txtOtherDesc').focus();
            }
            if ($(Table).find("OtherSourceId").text() > 0) {
                $("#Grid_OtherIncomeSource").append("<tr class='trclass1'><td style='display:none;'>" +
                        $.trim($(Table).find("OtherSourceId").text()) + "</td><td>" +
                        $.trim($('#txtOtherDesc').val()) + "</td><td style='text-align:right'>" +
                        $.trim($('#txtotherIncAmount').val()) + "</td><td style='text-align:center'>" +
                        "<img src='/Content/Images/Delete.png' style='cursor:pointer;' width='30px' height='25px' onclick='DeleteOtherSourceIncome(this)'/>" + "</td></tr>");

                        $('#hdnOtherDescID').val(''); $('#txtOtherDesc').val(''); $('#txtotherIncAmount').val('');
                        $('#txtOtherDesc').focus();
            }
            var TotalOtherIncomeAmount = 0;
            $.each($('#Grid_OtherIncomeSource tr.trclass1'), function (index, value) {
                TotalOtherIncomeAmount += (isNaN(parseFloat($.trim($(this).closest('tr').find("td:eq(2)").text()))) ? 0 : parseFloat($.trim($(this).closest('tr').find("td:eq(2)").text())));
            });
            $(_thisOtherSourceDetails).closest('tr').find('input').val(TotalOtherIncomeAmount);

            //CalculateTaxableIncome7();
        },
        error: function (data, status, e) {
            alert(e);
        }
    });

    
}

function DeleteOtherSourceIncome(t) {
    $(t).closest('tr').remove();
    var TotalOtherIncomeAmount = 0; var cnt = 0;
    $.each($('#Grid_OtherIncomeSource tr.trclass1'), function (index, value) {
        TotalOtherIncomeAmount += (isNaN(parseFloat($.trim($(this).closest('tr').find("td:eq(2)").text()))) ? 0 : parseFloat($.trim($(this).closest('tr').find("td:eq(2)").text())));
        cnt++;
    });
    $(_thisOtherSourceDetails).closest('tr').find('input').val(TotalOtherIncomeAmount);
    
    //$(_thisOtherSourceDetails).closest('tr').find('input').attr('disabled', cnt > 0);
    //CalculateTaxableIncome7();
}




