﻿

var isLeaveTour = "1"; var isChecked = "1";
var minDates = "";
$(document).ready(function () {

    FieldNameData();
    Bind_Tour();
    Get_Date_minmax();
    $('#btnDelete').hide();


    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
    $('#radioLeave').click(function () {
        isLeaveTour = "1";
    });
    $('#radioTour').click(function () {
        isLeaveTour = "0";
    });
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
    $('#chkIshalf').click(function () {
        if ($(this).prop("checked") == true) {
            isChecked = 0;
        }
        else if ($(this).prop("checked") == false) {
            isChecked = 1;
        }
        OnTo_Date();
    });
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
    $('#radioTour').click(function () {
        $('#txtBalanceLeave').val('');
        Bind_Tour();
    });
    $('#radioLeave').click(function () {
        $('#txtBalanceLeave').val('');
        Bind_Tour();
    });
    $('#btnSave').click(function () {
        Save();
    });

    $('#ddlLeave').change(function () {
        if ($('#ddlLeave').val() != '')
            Get_LeaveBalance();
        else
            $('#txtBalanceLeave').val('');

    });
    $('#btnRefresh').click(function () {
        location.reload();
    });

    $('#btnDelete').click(function () {
        var result = confirm('Are you sure want to delete this Leave Details ??');
        if(result == true)
        {
            var E = "{ApplicationNo : '" + $('#txtLVAppNo').val() + "', EmployeeID : '" + $('#hdnEmpID').val() + "', " +
                    "FromDate : '" + $('#txtLVFromDate').val() + "', ToDate : '" + $('#txtLVToDate').val() + "'}";

            $.ajax({
                type: "POST",
                url: "/Administration/Master/LeaveTransaction/Delete_LeaveDetails",
                data: E,
                dataType: "json",
                async: false,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data; 

                    var errCode = t[0].ErrorCode; 
                    var msg = t[0].Messege;
                    if(errCode  == 1)
                    {
                        alert(msg); location.reload(); return false;
                    }
                    //var dates = minDates = t[0].Vdate;
                }
            });
        }
    });

    $('#txtLVAppDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
    $('#txtLVFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        minDate:minDates,
        onSelect: function () {
            //if ($('#txtLVAppDate').val() == "") {
            //    alert('Please Enter Application Date !'); $('#txtLVFromDate').val(''); $('#txtLVAppDate').focus(); return false;
            //}
            //if ($('#txtEmployee').val() == "" || $('#hdnEmpID').val() == "") {
            //    alert('Please Select Employee Name !'); $('#txtLVFromDate').val(''); $('#txtEmployee').focus(); return false;
            //}
            OnFrom_Date(); $('#txtLVToDate').val(''); $('#txtLVDaysCount').val('');
        }
    });
    $('#txtLVToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            //if ($('#txtLVAppDate').val() == "") {
            //    alert('Please Enter Application Date !'); $('#txtLVToDate').val(''); $('#txtLVAppDate').focus(); return false;
            //}
            //if ($('#txtEmployee').val() == "" || $('#hdnEmpID').val() == "") {
            //    alert('Please Select Employee Name !'); $('#txtLVToDate').val(''); $('#txtEmployee').focus(); return false;
            //}
            OnTo_Date();
        }
    });


    //***************************  Search By EmpName ************************************
    $("#txtEmpNameNo").autocomplete({
        source: function (request, response) {
            //, string EmpId, string SalMonthId, string LvFlag
            var EmpId = '';
            var SalMonthId = '';

            if ($("#hdnlblEmpID").text() == '' || $("#hdnlblEmpID").text() == null || $("#hdnlblEmpID").text() == 'undefined' || $("#hdnlblEmpID").text() == 'NaN') {
                EmpId = 0;
            } else { EmpId = $("#hdnlblEmpID").text(); }

            if ($("#hdnlblSalMonthID").text() == '' || $("#hdnlblSalMonthID").text() == null || $("#hdnlblSalMonthID").text() == 'undefined' || $("#hdnlblSalMonthID").text() == 'NaN') {
                SalMonthId = 0;
            } else { SalMonthId = $("#hdnlblSalMonthID").text(); }
            

            var E = "{EmpName: '" + $.trim($("#txtEmpNameNo").val()) + "', SectorID: '" + $.trim($("#ddlSector").val()) +
                "', EmpId:'" + EmpId + "', SalMonthId:'" + SalMonthId + "'}";

            $.ajax({
                type: "POST",
                url: "/Administration/Master/LeaveTransaction/Search_EmpNameAutoComplete_Search",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data; 
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.EmpName,
                                EmpNo: item.EmployeeID,
                                AppNo: item.LtNo,
                                AppDate: item.LtDate,
                                DisEmpName: item.DisEmpName,
                                isLeave: item.IsLeave,
                                lvid: item.LvId,
                                bal: item.LBal,
                                isRound: item.IsRound,
                                fromDate: item.FromDate,
                                toDate: item.ToDate,
                                leaveCount: item.NoOfDays,
                                remarks: item.Remarks,
                                ReasonID: item.RCode
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            var EmpID = i.item.EmpNo;
            var EmployeeID = EmpID; $("#hdnEmployeeID").val(EmployeeID);

           
            $("#txtLVAppNo").val(i.item.AppNo);
            $("#txtLVAppDate").val(i.item.AppDate); $("#txtLVAppDate").prop("disabled", true);
            $("#txtEmployee").val(i.item.DisEmpName); $("#txtEmployee").prop("disabled", true);
            $("#hdnEmpID").val(EmployeeID);

            //i.item.isLeave == 1 ? $('#radioLeave').attr("checked", true) : $('#radioTour').attr("checked", true);
            if(i.item.isLeave ==1)
            {
                $('#radioLeave').attr("checked", true); $('#radioTour').attr("checked", false);
                isLeaveTour = 1;
                Bind_Tour();
            }
            else
            {
                $('#radioTour').attr("checked", true); $('#radioLeave').attr("checked", false);
                isLeaveTour = 0;
                Bind_Tour();
            }
            $("#ddlLeave").val(i.item.lvid);
            $("#txtBalanceLeave").val(i.item.bal);
          
            i.item.isRound == 1 ? ($('#chkIshalf').prop("checked", false)) : ($('#chkIshalf').prop("checked", true));
            isChecked = i.item.isRound;

            //==================================================== From Date ====================================================
            $("#txtLVFromDate").val(i.item.fromDate);
            var fromDate = ""; var chk_minDate = "";
            if (i.item.fromDate != '' ) {
                var a = i.item.fromDate.split('/');
                var day = a[0];
                var month = a[1];
                var year = a[2];
                fromDate = new Date(parseInt(year), parseInt(month) - 1, day); 
            }

          
            if (minDates != '' && minDates != null ) {
                var b = minDates.split('/');
                var day = b[0];
                var month = b[1];
                var year = b[2];
                chk_minDate = new Date(parseInt(year), parseInt(month) - 1, day);
            }

 
            if (fromDate < chk_minDate) {
                
                $("#txtLVFromDate").prop("disabled", true);
               
                $('#chkIshalf').prop("disabled", true);
            }
            else {
               
                $("#txtLVFromDate").prop("disabled", false);

                $('#chkIshalf').prop("disabled", false);
            }

            //==================================================== To Date ====================================================
            $("#txtLVToDate").val(i.item.toDate);
            var toDate = ""; 
            if (i.item.toDate != '') {
                var c = i.item.toDate.split('/');
                var day = c[0];
                var month = c[1];
                var year = c[2];
                toDate = new Date(parseInt(year), parseInt(month) - 1, day);
            }
            if (toDate < chk_minDate)
                $("#txtLVToDate").prop("disabled", true);
            else
                $("#txtLVToDate").prop("disabled", false);


            $("#txtLVDaysCount").val(i.item.leaveCount);
            $("#txtLVRemarks").val(i.item.remarks);

           
            $("#ddlReason").val(i.item.ReasonID);

            $("#btnSave").html("Update");
            $('#btnDelete').show();
        },
        minLength: 0
    }).click(function () {
        $("#txtEmpNameNo").val(''); $("#hdnEmployeeID").val('');
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtEmpNameNo').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtEmpNameNo').val('');
            $('#hdnEmployeeID').val('');
        }
        if (iKeyCode == 46) {
            $('#txtEmpNameNo').val('');
            $('#hdnEmployeeID').val('');
        }
    });

    //***************************  Search for EmpName ************************************
    $("#txtEmployee").autocomplete({
        source: function (request, response) {
            var EmpId = '';
            var SalMonthId = '';

            if ($("#hdnlblEmpID").text() == '' || $("#hdnlblEmpID").text() == null || $("#hdnlblEmpID").text() == 'undefined' || $("#hdnlblEmpID").text() == 'NaN') {
                EmpId = 0;
            } else { EmpId = $("#hdnlblEmpID").text(); }

            if ($("#hdnlblSalMonthID").text() == '' || $("#hdnlblSalMonthID").text() == null || $("#hdnlblSalMonthID").text() == 'undefined' || $("#hdnlblSalMonthID").text() == 'NaN') {
                SalMonthId = 0;
            } else { SalMonthId = $("#hdnlblSalMonthID").text(); }

            var E = "{EmpName: '" + $.trim($("#txtEmployee").val()) + "', SectorID: '" + $.trim($("#ddlSector").val()) +
                "', EmpNoName: '" + EmpId + "', SalMonthId:'" + SalMonthId + "'}";
            //alert(E);
            $.ajax({
                type: "POST",
                url: "/Administration/Master/LeaveTransaction/Search_EmpNameAutoComplete",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.EmpName,
                                EmpNo: item.EmployeeID
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            var EmpID = i.item.EmpNo;
            var EmployeeID = EmpID;

            $("#hdnEmpID").val(EmployeeID);

            
        },
        minLength: 0
    }).click(function () {
        $("#txtSearchEmpNo").val(''); $("#hdnEmployeeID").val('');
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtEmployee').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtEmployee').val('');
            $('#hdnEmpID').val('');
            Blank_Field();
        }
        if (iKeyCode == 46) {
            $('#txtEmployee').val('');
            $('#hdnEmpID').val('');
            Blank_Field();
        }
    });
});

function FieldNameData() {
    $.ajax({
        type: "POST",
        url: "/Administration/Master/LeaveTransaction/Get_FieldName",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                $.each(D.Data, function (index, value) {
                    var MandatoryStyle = '*';
                    var Id = '#' + value.FieldID;
                    $(Id).text(value.FieldName);
                    $('#spn' + value.FieldID).text(value.IsMand == 1 ? '   ' + MandatoryStyle : '');
                });
            }
        }
    });
}
function Bind_Tour() {
   
    var E = "{isTourLeave : " + isLeaveTour + "}"; 
    $.ajax({
        type: "POST",
        url: "/Administration/Master/LeaveTransaction/Bind_TourLeave",
        data: E,
        dataType: "json",
        async:false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                $("#ddlLeave").empty();
                $("#ddlLeave").append("<option value=''>-- Select --</option>")
                    $.each(t, function (index, value) {
                        var label = value.LeaveAbbv;
                        var val = value.LvId;
                        $("#ddlLeave").append($("<option></option>").val(val).html(label));
                    });
            }
        }
    });
}
function Get_LeaveBalance() {

    var E = "{ApplicationNo : '" + $('#txtLVAppNo').val().trim() + "', ApplicationDate : '" + $('#txtLVAppDate').val() + "', EmpID : '" + $('#hdnEmpID').val() + "', LeaveID : '" + $('#ddlLeave').val() + "', isTourLeave : " + isLeaveTour + "}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/LeaveTransaction/Get_LeaveBalance",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                if (t[0].ErrorCode == "1") {
                    $('#txtBalanceLeave').val(t[0].Lbal);
                }
                if (t[0].ErrorCode == "0") {
                    var id = ".txt_" + t[0].FId;
                    alert(t[0].Messege);
                    $("#ddlLeave").val('');
                    $(id).focus(); return false;
                    //$('#txtBalanceLeave').val(t[0].Lbal);
                }
                //else {
                //    if (t[0].FId == "6")
                //    {
                //        $("#ddlLeave").val('');
                //        $("#ddlLeave").focus();
                //        return false;
                //    }
                //}
            }
        }
    });
}
function Save() {
    var LvFlag = $('#hdnlblLvFlag').text() != 0 ? 1 : 0;

    var E = "{LVAppNo : '" + $('#txtLVAppNo').val() + "', LVAppDate : '" + $('#txtLVAppDate').val() + "', EmployeeID : '" + $('#hdnEmpID').val() + "', " +
            "LeaveID : '" + $('#ddlLeave').val() + "', FromDate : '" + $('#txtLVFromDate').val() + "', ToDate : '" + $('#txtLVToDate').val() + "', " +
            "LeaveBal : '" + $('#txtBalanceLeave').val() + "', isRound : '" + isChecked + "', isLeaveTour : '" + isLeaveTour + "', " +
            "NoofDays : '" + $('#txtLVDaysCount').val() + "', Remarks : '" + $('#txtLVRemarks').val() + "', SalMonthID : '" + $('#txtSalMonthID').val() +
            "', ReasonID : '" + $('#ddlReason').val() + "', LvFlag:'" + LvFlag + "'}";
   // alert(E);
    $.ajax({
        type: "POST",
        url: "/Administration/Master/LeaveTransaction/Save_LeaveTransaction",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                if (t[0].ErrorCode == "1") {
                    alert(t[0].Messege); location.reload(); return false;
                }
                else {
                    var id = ".txt_" + t[0].FId;
                    alert(t[0].Messege);
                    $(id).focus(); return false;

                    //if (t[0].FId == "2") { alert(t[0].Messege); $('#txtLVAppDate').focus(); return false; }
                    //if (t[0].FId == "3") { alert(t[0].Messege); $('#txtEmployee').focus(); return false; }
                    //if (t[0].FId == "6") { alert(t[0].Messege); $('#ddlLeave').focus(); return false; }
                    //if (t[0].FId == "8") { alert(t[0].Messege); $('#chkIshalf').focus(); return false; }
                    //if (t[0].FId == "9") { alert(t[0].Messege); $('#txtLVFromDate').focus(); return false; }
                    //if (t[0].FId == "10") { alert(t[0].Messege); $('#txtLVToDate').focus(); return false; }
                }
            }
        }
    });
}

function OnFrom_Date()
{
    if ($('#txtLVFromDate').val() != "" ) {

        var E = "{ApplicationNo : '" + $('#txtLVAppNo').val().trim() + "', ApplicationDate : '" + $('#txtLVAppDate').val() + "', EmpID : '" + $('#hdnEmpID').val() + "', FromDate : '" + $('#txtLVFromDate').val() + "', ToDate : '" + $('#txtLVToDate').val() + "'}";

        $.ajax({
            type: "POST",
            url: "/Administration/Master/LeaveTransaction/Check_From_Date",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data;
                if(t.length >0)
                {
                    if(t[0].ErrorCode == "0")
                    {
                        alert(t[0].Messege); $('#txtLVFromDate').val(''); return false;
                    }
                }
            }
        });
    }
}
function OnTo_Date() {
    if ($('#txtLVToDate').val() != "") {

        var E = "{ApplicationNo : '" + $('#txtLVAppNo').val().trim() + "', ApplicationDate : '" + $('#txtLVAppDate').val() + "', EmpID : '" + $('#hdnEmpID').val() + "', FromDate : '" + $('#txtLVFromDate').val() + "'," +
                "ToDate : '" + $('#txtLVToDate').val() + "', LeaveBal : '" + $('#txtBalanceLeave').val() + "', isRound : '" + isChecked + "', isLeave : '" + isLeaveTour + "', LeaveID : '" + $('#ddlLeave').val() + "'}";

        $.ajax({
            type: "POST",
            url: "/Administration/Master/LeaveTransaction/Check_To_Date",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0) {
                    if (t[0].ErrorCode == "0") {
                        var id = ".txt_" + t[0].FId;
                        alert(t[0].Messege);
                         $(id).focus(); return false;
                       
                        //alert(t[0].Messege); $('#txtLVFromDate').focus(); $('#txtLVToDate').val(''); return false;
                    }
                    if (t[0].ErrorCode == "1") {
                        $('#txtLVDaysCount').val(t[0].NDays); return false;
                    }
                  
                }
            }
        });
    }
}

function Get_Date_minmax()
{
    $.ajax({
        type: "POST",
        url: "/Administration/Master/LeaveTransaction/Get_Date_MinMax_Date",
        data: "",
        dataType: "json",
        async:false,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data; 
            var dates = minDates = t[0].Vdate;
        }
    });
}

function Blank_Field()
{
    
    $("#txtLVAppDate").val('');
    $("#txtEmployee").val(''); $("#hdnEmpID").val('');

   
    $('#radioLeave').prop("checked", true); $("#radioLeave").trigger("click");
    $('#radioTour').prop("checked", false);
    //Bind_Tour();

    $("#ddlLeave").val('');
    $("#txtBalanceLeave").val('');

    $('#chkIshalf').prop("checked", false);
    isChecked = 1;

    $("#txtLVFromDate").val('');
    $("#txtLVToDate").val('');
    $("#txtLVDaysCount").val('');
    $("#txtLVRemarks").val('');

}