﻿$(document).ready(function () {

    LoadData();

});


function LoadData() {

    $.ajax({
        type: "POST",
        url: "/Administration/Master/Form16Download/Load",
        data: {},
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            if (D.Status == 200) {

                $("#tbl tr.myData").empty(); var i = 0, color = '';
                $.each(D.Data, function (index, value) {
                    i = parseInt(i) + 1;

                    $('#tbl').append("<tr class='myData' >" +
                        "<td style='text-align:center;'>" + i + "</td>" +
                        "<td style='display:none;' class='clsEmpID'>" + value.EmployeeID + "</td>" +
                        "<td style='text-align:center; display:none;' class='clsFilePath'>" + value.FilePath + "</td>" +
                        "<td style='text-align:center;'>" + value.FinYear + "</td>" +
                       
                        "<td onclick='DownloadFile(this)' style='text-align:center; pointer-events:" + ((value.FilePath == null || value.FilePath == "null") ? "none" : "") + "'><span style='text-align:center; cursor:pointer; color:blue; display:" + ((value.FilePath == null || value.FilePath == "null") ? "none" : "") + "'>Download </span></td>" +
                        "<td onclick='ViewFile(this)' style='text-align:center; pointer-events:" + ((value.FilePath == null || value.FilePath == "null") ? "none" : "") + "'><span style='text-align:center; cursor:pointer; color:blue; display:" + ((value.FilePath == null || value.FilePath == "null") ? "none" : "") + "'>View </span></td>" +
                        "</tr > "
                    );
                });
            }
            else {
                alert('No data found');
            }
        }
    });

}

function ViewFile(ID) {
    var filepath = $(ID).closest('tr').find('.clsFilePath').text();
    var url = 'http://wtlhr.co.in/' + filepath; 

    $('#imgview').attr('src', url);
    $('#dialog').modal('show');

}

function DownloadFile(ID) {
    var filepath = $(ID).closest('tr').find('.clsFilePath').text();
    var url = 'http://wtlhr.co.in/' + filepath;

    window.open(url, '_blank'); 


    //var result = url.replace(/(^\w+:|^)\/\//, '');
    //$('.clsDownload').attr('href', "http://" + result);
    //$(".clsDownload")[0].click();

    //<a class="clsDownload" style="display:none;" href="" download></a>
}

