﻿$(document).ready(function () {
    var SERVER_OBJECT = function () {
        this.MakeAdministrativeLogin = function (data, callBack) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/LoginManager/AdministrativeLogin",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                  
                    callBack(data);

                }
            });
        };
    };
    var APP_OBJECT = function (serverObject) {
      
        var DomControls = {
            ctrl_txt_UserName: $(".txtUserName"),
            ctrl_txt_Password: $(".txtPassword"),
            ctrl_btn_Submit: $(".btn-administrative-login"),
            ctrl_frm_Form: $("#frm_AdministrativeLogin"),
            ctrl_msg_Span:$(".spn_frm_msg")
            //ctrl_login_loader: $(".linear_loader")
        };



        var GetFormData = function () {
            return {
                UserName: DomControls.ctrl_txt_UserName.val(),
                Password: DomControls.ctrl_txt_Password.val(),
            };
        };

        var Validators =  {
            ValidateForm: function () {
                var vldObj = {
                    rules: {},
                    messages: {},
                    errorPlacement: {}
                };
                //enable hidden field vallidation
                vldObj.ignore = [];

                //set rules
                vldObj.rules[DomControls.ctrl_txt_UserName.prop('name')] = { required: true };
                vldObj.rules[DomControls.ctrl_txt_Password.prop('name')] = { required: true };

                //set error messages
                vldObj.messages[DomControls.ctrl_txt_UserName.prop('name')] = { required: "Enter Username." };
                vldObj.messages[DomControls.ctrl_txt_Password.prop('name')] = { required: "Please Password" };
               
                //place errors in position
                vldObj.errorPlacement = function (error, element) {
                    error.insertAfter(element);
                    error.css('color', 'red');
                }

                //push validator object to tergate form
                //DomControls.ctrl_frm_Form.validate(vldObj);
                $("#frm_AdministrativeLogin").validate(vldObj);
                //validate form and return true and false
                //return DomControls.ctrl_frm_Form.valid();
                return $("#frm_AdministrativeLogin").valid();
            }
        };
        var ResetMessageControl = function () {
            DomControls.ctrl_msg_Span.css('color', 'Green').html('');
        }
        var ShowSuccessMessage = function (message) {
            DomControls.ctrl_msg_Span.css('color', 'Green').html(message);
        };
        var ShowErrorMessage = function (message) {
            DomControls.ctrl_msg_Span.css('color', 'red').html(message);
        };
        var EventHandlers = {
            OnLoginSubmit: function () { },
        };
        var BindEvents = function () {
           
            DomControls.ctrl_btn_Submit.on('click', function () {
                $("#llll").show();
                if (Validators.ValidateForm() == false)
                {
                    return;
                }

               
                var formData = GetFormData();
                serverObject.MakeAdministrativeLogin(formData, function (response) {
                   
                    if (response.Status === 200) {
                       
                        ShowSuccessMessage('Successfully Logged in');
                       

                        setTimeout(function () {
                            location.href = response.Data.LandingPageUrl;
                        }, 1000);
                        
                    } else {
                        ShowErrorMessage(response.Message);
                    }
                });
            });
            DomControls.ctrl_txt_Password.on('keydown', function (e) {
                if (e.keyCode == 13) {
                    DomControls.ctrl_btn_Submit.click();
                }
            });
        };

        this.InitApp = function () {
           
            BindEvents();
        };
    };
    var INIT_OBJECT = function () {
       
        (function () {
            var serverObject = new SERVER_OBJECT();
            var appObject = new APP_OBJECT(serverObject);
            appObject.InitApp();
        }());
    };
    (function () {
        new INIT_OBJECT();
    }());
});