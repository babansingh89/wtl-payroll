﻿var _globalthis = '';

//for print
var _prEmployeeName = ''; var _FromDate = ''; var _ToDate = ''; var _this = ''; var LRank = ''; var _VSancDtls = '';


var _SalMonthID = ''; var _EmployeeID = ''; var _EmpName = ''; var RepMonth = '';
$(document).ready(function () {
    PopulateGrid();


    $("#ddlYear").change(function () {
        PopulateGrid();
    });

    /*
    $(document).on('click', '#PrintEmp', function () {
        var rptHeader = 'Leave Balance as on ' + $.datepicker.formatDate('dd/mm/yy', new Date());
        Print(_EmpName, 'divtblEmpName', rptHeader);
    });
    $(document).on('click', '#PrintEmpDetails', function () {
        var rptHeader = 'Leave Taken as on ' + $.datepicker.formatDate('dd/mm/yy', new Date());
        Print(_EmpName, 'divtblEmpDetails', rptHeader);
    });
    $(document).on('click', '#PrintLeave', function () {
        var rptHeader = 'Leave Taken for ' + RepMonth;
        Print(_EmpName, 'divtblLeave', rptHeader);
    });
    $(document).on('click', '#PrintLvDetails', function () {
        var rptHeader = 'Leave Details for ' + RepMonth;
        Print(_EmpName, 'divtblLeaveDetails', rptHeader);
    });
    $(document).on('click', '#PrintTour', function () {
        var rptHeader = 'Tour Taken for ' + RepMonth;
        Print(_EmpName, 'divtblTourDetails', rptHeader);
    });
    $(document).on('click', '#PrintLWP', function () {
        var rptHeader = 'LWP Details for ' + RepMonth;
        Print(_EmpName, 'divtblLWPDetails', rptHeader);
    });
    $(document).on('click', '#PrintLate', function () {
        var rptHeader = 'Leave deducted due to Late Entry for ' + RepMonth;
        Print(_EmpName, 'divtblLateDetails', rptHeader);
    });
    */

    $(document).on('click', '#PrintEmp', function () {
        var rptHeader = 'Leave Balance as on ' + $.datepicker.formatDate('dd/mm/yy', new Date());
        PrintNew(_EmpName, 'divtblEmpName', 'tblEmpName', rptHeader);
    });
    $(document).on('click', '#PrintEmpDetails', function () {
        var rptHeader = 'Leave Taken as on ' + $.datepicker.formatDate('dd/mm/yy', new Date());
        PrintNew(_EmpName, 'divtblEmpDetails', 'tblEmpDetails', rptHeader);
    });
    $(document).on('click', '#PrintLeave', function () {
        var rptHeader = 'Leave Taken for ' + RepMonth;
        PrintNew(_EmpName, 'divtblLeave', 'tblLeave', rptHeader);
    });
    $(document).on('click', '#PrintLvDetails', function () {
        var rptHeader = 'Leave Details for ' + RepMonth;
        PrintNew(_EmpName, 'divtblLeaveDetails', 'tblLeaveDetails', rptHeader);
    });
    $(document).on('click', '#PrintTour', function () {
        var rptHeader = 'Tour Taken for ' + RepMonth;
        PrintNew(_EmpName, 'divtblTourDetails', 'tblTourDetails', rptHeader);
    });
    $(document).on('click', '#PrintHolidayDetails', function () {
        var rptHeader = 'Holiday for ' + RepMonth;
        PrintNew(_EmpName, 'divtblHolidayDetails', 'tblHolidayDetails', rptHeader);
    });
    $(document).on('click', '#PrintLWP', function () {
        var rptHeader = 'LWP Details for ' + RepMonth;
        PrintNew(_EmpName, 'divtblLWPDetails', 'tblLWPDetails', rptHeader);
    });
    $(document).on('click', '#PrintLate', function () {
        var rptHeader = 'Leave deducted due to Late Entry for ' + RepMonth;
        PrintNew(_EmpName, 'divtblLateDetails', 'tblLateDetails', rptHeader);
    });

    $(document).on('click', '#ChangeValueOff', function () {
        Change_CalculateOffDays();
    });

    $(document).on('click', '#ChangeValue', function () {
        Change_CalculateHoliDays();
    });

    //$(document).on('click', '#btnSanction', function () {
    //    ButtonSanction();
    //});

    $(document).bind("cut copy paste", 'input[type=text]', function (e) {
        return false;
    });

    function PopulateGrid() {

        $("#accordion-container").empty().append("<div id='accordion'/>");
        var acc = $("#accordion-container").find("#accordion");

        var _data = "{TransactionType:'" + 'SELECTGRID' + "', Year:'" + $.trim($('#ddlYear').val()) + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/MonthlyAttendence/SelectMonthYearData",
            data: _data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                if (D.Status == 200) {
                    if (D.Data != null) {

                        var xmlDoc = $.parseXML(D.Data);
                        var xml = $(xmlDoc);

                        var Table1 = xml.find("Table1");
                        var Table2 = xml.find("Table2");

                        $('#SanctionErrorCode').val($(Table1).find("ErrorCode").text());
                        $('#SanctionReturnCode').val($(Table1).find("ReturnCode").text());
                        $('#Sanction_LastSanc').val($(Table1).find("LastSanc").text());
                        $('#Sanction_LUser').val($(Table1).find("LUser").text());
                        $('#Sanction_Messege').val($(Table1).find("Messege").text());
                        $('#SanctionSalMonthID').val($(Table1).find("SalMonId").text());
                        $('#ReGenerate').val($(Table2).find("Regenerate").text());
                        $('#ReGenerateSalMonID').val($(Table2).find("SalMon").text());

                        var requiredIndex = -1;

                        $(xml).find("Table").each(function (index, value) {
                            var getColorCode = randomColor({
                                luminosity: 'light',
                                hue: 'blue'
                            });;//background:" + getColorCode + " !important; 

                            var label = $(this).find("VOpen").text() == 1 ? "<span style='color:green; text-align:right'>Can Open</span>" : "<span style='color:red; text-align:right'>Could not Open</span>";
                            var background = $(this).find("VSancDtls").text() == 'C' ? "background:#b74306  !important; color:#d0d1d2 !important" : "";

                            //acc.append("<h3 class='placeholder' style='text-align:center; " + background + "' onclick='BindDetailData(this, " + index + ")'>" + $(this).find("MNYR").text() +
                            acc.append("<h3 class='placeholder' style='text-align:center; " + background + "' >" + $(this).find("MNYR").text() +
                                "<label style='display:none'>" + $(this).find("SalMonthId").text() + "</label><span hidden='hidden'>" + $(this).find("RepMonth").text() +
                                "</span><span id='spnFrmDt' hidden='hidden'>" + $(this).find("ProccFrmDt").text() + "</span><span id='spnToDt' hidden='hidden'>" + $(this).find("ProccToDt").text() +
                                "</span><span id='spnVSancDtls' hidden='hidden'>" + $(this).find("VSancDtls").text() + "</span></h3>");
                            //acc.append($("<h3 class='my_accordion' style='text-align:center' onclick='BindDetailData(this)'>").append($("<a >").attr("href", "#").html($(this).find("MNYR").text())));
                            //acc.append($("<div id='divSanction'>").append($("<div style='height:250px'>").append($("<table id='tbl' class='Grid' style='border-collapse:collapse; width:100%;'>"))));  //.html(detaildata)
                            acc.append($("<div><div id='divSanction'>").append($("<table id='tbl' class='Grid' style='border-collapse:collapse; width:100%;'>")));

                            //if($(this).find("VSancDtls").text() == 'C')
                            //{
                            //    requiredIndex = index;
                            //}
                        });
                        //------------------------------------------------------------------------------------------------------------------------

                        //------------------------------------------------------------------------------------------------------------------------
                        acc.accordion({
                            collapsible: true,
                            //header: "h3",
                            active: false
                        });
                    }
                    else {
                        alert("No Record found !");
                    }
                }
                else {
                    alert(D.Message);
                }
            },
            error: function (D) {
                alert(D.Message);
            }
        });
    }
});

$(document).on('click',"#accordion-container #accordion", function () {
    $("#accordion-container #accordion").accordion({
        activate: function (e, ui) {
            //console.log(ui);
            if (ui.newHeader.length > 0) {
                //alert('opened');
                BindDetailData($(ui.newHeader.get()))
            } else {
                // alert('closed');
            }
        }
    });
});

function BindDetailData(t) {
    _globalthis = t;
    //$(t).closest('div').css('height', 'unset !important');
    var tableborder = 'style = "border: 1px solid; text-align:center"';

    var SalMonthId = $(t).find('label').text(); _SalMonthID = SalMonthId;
    RepMonth = $(t).find('span').text();

    var FromDate = $(t).find('#spnFrmDt').text(); var ToDate = $(t).find('#spnToDt').text();
    _FromDate = FromDate; _ToDate = ToDate;

    var VSancDtls = $(t).find('#spnVSancDtls').text(); _VSancDtls = VSancDtls;

    var preventCharacter = 'onkeydown = "return !((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) && event.keyCode!=8 && event.keyCode!=46);"';

        var _data = "{TransactionType:'" + 'SELECTCLICK' + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthId:'" + $.trim(SalMonthId) + "', FromDate:'" + $.trim(FromDate) + "', ToDate:'" + $.trim(ToDate) + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/MonthlyAttendence/SelectDetailData",
            data: _data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                if (D.Status == 200) {
                    var tergatePanel = $(t).parent('div').find("div[aria-labelledby='" + $(t).attr("id") + "']");
                    //console.log(tergatePanel.attr('aria-labelledby'));
                    //console.log(tergatePanel);
                    if (D.Data != null) {
                        var xmlDoc = $.parseXML(D.Data);
                        var xml = $(xmlDoc);
                        
                        var SanctionField; var ShowViewSanction = ''; //alert($('#SanctionSalMonthID').val()); alert(SalMonthId);

                        if (VSancDtls == 'H' || VSancDtls == 'C') {
                            SanctionField = '<div class="row"><div class="col-md-3"><button id="btnViewSanction" onclick="ViewSanction()" type="button" class="btn btn-info" data-dismiss="modal">View Sanction Details</button></div>';
                            SanctionField += '<div class="col-md-3"><a id="btnPrintMainReport" target="_blank" onclick="PrintMainReport()" class="btn btn-info" data-dismiss="modal">Print</a></div>';
                            if ($('#SanctionSalMonthID').val() == SalMonthId) {
                                SanctionField += '<div class="col-md-3"><span style="font-weight:bold;">' + $('#Sanction_LastSanc').val() + '</span></div>';
                                if ($('#ReGenerateSalMonID').val() == SalMonthId) {
                                    if ($('#ReGenerate').val() == 1) {
                                        SanctionField += '<div class="col-md-2"><button id="btnReGenerate" onclick="ReGenerate(this)" type="button" class="btn btn-info" data-dismiss="modal">ReGenerate</button></div></div>';
                                    }
                                }
                                if (($('#SanctionErrorCode').val() == 1 || $('#SanctionReturnCode').val() == 1)) {
                                    SanctionField += '<br/><input id="txtSanctionRemarks" class="form-control" placeholder="-- Remarks --"  /><br/>';
                                    SanctionField += '<div class="row">';

                                    if ($('#SanctionErrorCode').val() == 1) {
                                        SanctionField += '<div class="col-md-2"><button id="btnSanction" onclick="ButtonSanction(this)" type="button" class="btn btn-info" data-dismiss="modal">Sanction</button></div>';
                                    }
                                    if ($('#SanctionReturnCode').val() == 1) {
                                        SanctionField += '<div class="col-md-2"><button id="btnReturn" onclick="ButtonReturn(this)" type="button" class="btn btn-danger" data-dismiss="modal">Return</button></div>';
                                    }
                                    SanctionField += '</div>'
                                }
                            }

                            tergatePanel.find('#divSanction').html(SanctionField);
                        }
                        else {
                            tergatePanel.find('#divSanction').html('');
                        }


                        var slNo = 1;
                        $(t).closest('div').find('table').html('');
                        var detaildata = "<thead><tr><th " + tableborder + ">SlNo</th><th style='border: 1px solid; text-align:center'>EmpName<input id='EmpSearch' class='form-control EmpSearch' style='width:250px' placeholder='Search' onkeyup='EmpSearchGrid(this)' /></th>"
                            + "<th " + tableborder + ">AttDays</th><th " + tableborder + ">TourDays</th><th " + tableborder + ">LeaveDays</th><th " + tableborder + ">OffDays</th>"
                            + "<th " + tableborder + ">Holidays</th><th " + tableborder + ">LWPDays</th><th " + tableborder + ">Tiffin Days</th><th " + tableborder + ">Adj Tiffin Days</th>"
                            + "<th " + tableborder + ">Tffin Allowance</th><th " + tableborder + ">Late Attendence</th><th " + tableborder + ">Action</th></tr></thead>";
                        detaildata += "<tbody>";

                        $(xml).find("Table").each(function () {

                            var rowcolor = slNo % 2 != 0 ? 'e2e2e2' : 'ffffff';

                            detaildata += "<tr class='trclass' style='background-color:#" + rowcolor + "' onmouseover='trHover(this)' onmouseout='trMouseOut(this, \"" + rowcolor + "\")'>";

                            detaildata += "<td>" + slNo + "</td>";
                            detaildata += "<td style='display:none'>" + $(this).find("EmployeeID").text() + "</td>";
                            detaildata += "<td style='display:none'>" + $(this).find("SalMonthID").text() + "</td>";
                            detaildata += "<td><a href='javascript:void(0)' onclick='EmpPopupDatas(this)' title='Click for Details' style='color='black'; cursor:pointer' >" + $(this).find("EmpName").text() + "</a></td>";
                            detaildata += "<td>" + $(this).find("AttDays").text() + "</td>";
                            detaildata += "<td><a href='javascript:void(0)' onclick='TourPopupDatas(this)' title='Click for Details' style='color:blue; cursor:pointer' >" + $(this).find("TourDays").text() + "  " + "</a><img onclick='RedirectLeave(this)' style='cursor:pointer' title='Leave Entry' src='/Content/Images/Modify1.png' /></td>";
                            detaildata += "<td><a href='javascript:void(0)' onclick='LvPopupDatas(this)' title='Click for Details' style='color:#0fc7c7; cursor:pointer' >" + $(this).find("TotLvDays").text() + "  " + "</a><img onclick='RedirectLeave(this)' style='cursor:pointer' title='Leave Entry' src='/Content/Images/Modify1.png' /></td>";
                            //detaildata += "<td><input type='text' id='ActOffDays' onkeyup='CalculateAttDays(this)' style='text-align:right; width:50px' class='form-control preventpaste' value=" + $(this).find("ActOffDays").text() + " /></td>";
                            //detaildata += "<td><input type='text' id='ActHoliday' onkeyup='CalculateHoliDays(this)' style='text-align:right; width:70px' class='form-control preventpaste' value=" + $(this).find("ActHoliday").text() + " /></td>";
                            detaildata += "<td><span>" + $(this).find("ActOffDays").text() + "</span>" + "         " + "<img onclick='CalculateOffDays(this)' style='cursor:pointer' title='Leave Entry' src='/Content/Images/hand.png' /></td>";
                            detaildata += "<td><a href='javascript:void(0)' onclick='HolidayPopupDatas(this)' title='Click for Details' style='color:#ff0202; cursor:pointer' >" + $(this).find("ActHoliday").text() + "</a>" + "         " + "<img onclick='RedirectHoliDays(this)' style='cursor:pointer' title='Leave Entry' src='/Content/Images/Modify1.png' /><img onclick='CalculateHoliDays(this)' style='cursor:pointer' title='Leave Entry' src='/Content/Images/hand.png' /></td>";
                            detaildata += "<td><a href='javascript:void(0)' onclick='LWPPopupDatas(this)' title='Click for Details' style='color:#24b336; cursor:pointer' >" + $(this).find("LwpDays").text() + "  " + "</a><img onclick='RedirectLeave(this)' style='cursor:pointer' title='Leave Entry' src='/Content/Images/Modify1.png' /></td>";
                            detaildata += "<td>" + $(this).find("TiffinDays").text() + "</td>";
                            detaildata += "<td><input type='text' id='AdjustTiffinDays' onkeyup='AdjustTiffinDays(this)' " + preventCharacter + " style='text-align:right;' class='form-control preventpaste' value=" + $(this).find("AdjustTiffinDays").text() + " /></td>";
                            detaildata += "<td>" + $(this).find("TiffinAllowance").text() + "</td>";
                            detaildata += "<td style='display:none'>" + $(this).find("ClDays").text() + "</td>";
                            detaildata += "<td style='display:none'>" + $(this).find("MlDays").text() + "</td>";
                            detaildata += "<td style='display:none'>" + $(this).find("ElDays").text() + "</td>";
                            detaildata += "<td style='display:none'>" + $(this).find("OthLvDays").text() + "</td>";
                            detaildata += "<td><a href='javascript:void(0)' onclick='LatePopupDatas(this)' title='Click for Details' style='color:#eb8f00; cursor:pointer' >" + $(this).find("LateAtt").text() + "  " + "</a><img onclick='RedirectLeave(this)' style='cursor:pointer' title='Leave Entry' src='/Content/Images/Modify1.png' /></td>";
                            detaildata += "<td style='display:none'>" + $(this).find("ActOffDays").text() + "</td>";
                            detaildata += "<td style='display:none'>" + $(this).find("ActHoliday").text() + "</td>";
                            detaildata += "<td><a onclick='SaveEachDetail(this)' class='btn btn-info'>Save</a></td>";
                            detaildata += "</tr>";

                            slNo++;
                        });
                        detaildata += "</tbody>";
                        tergatePanel.find('table').html(detaildata);
                        tergatePanel.css({ 'height': '500px', 'overflow': 'scroll' });
                    }
                    else {
                        //alert("No Record found !");
                        //$(t).closest('div').find('table').html(''); background-color: #f0f2f7;" 
                        tergatePanel.find('table').html('<span style="text-align:center;">No Record found</span>');
                        tergatePanel.find('table').css({ 'background-color': '#f0f2f7', 'border-left': 'unset', 'border-right': 'unset', 'border-bottom': 'unset', 'text-align': 'center' });

                        tergatePanel.find('#divSanction').html('');
                        tergatePanel.css("height", '60px');
                    }
                }
                else {
                    alert(D.Message);
                }
            },
            error: function (D) {
                alert(D.Message);
                //OverLayContainer.css('display', 'none');
            }
        });
    //}
}

function BindTableAfterReGenerate(t) {
    
}

function EmpSearchGrid(t) {
    //if ($.trim($(t).val()) == "") {
    //    $(t).closest('#tbl').find("tbody tr").show();
    //    return;
    //}

    //var value = $(t).val();
    //var s = $(t).closest('#tbl').find("tbody tr").not(".trclass:contains('" + $(t).val().toUpperCase() + "')");
    //var s1 = $(t).closest('#tbl').find("tbody tr").find(".trclass:contains('" + $(t).val().toUpperCase() + "')");
    //s.hide();
    //s1.parent().show();

    if ($.trim($(t).val()) == "") {
        $(t).closest('#tbl').find("tbody tr").show();
        return;
    }

    var s = $(t).closest('#tbl').find("tbody tr").not("td:eq(3):contains('" + $(t).val().toUpperCase() + "')");
    var s1 = $(t).closest('#tbl').find("tbody tr").find("td:eq(3):contains('" + $(t).val().toUpperCase() + "')");
    s.hide();
    s1.parent().show();
}

function SaveEachDetail(t) {
    var conf = confirm("Are you sure want to save?");
    if (conf) {
        var TransactionType = 'UPDATE';
        var SalMonthID = $.trim($(t).closest('tr').find('td:eq(2)').text());
        var EmployeeID = $.trim($(t).closest('tr').find('td:eq(1)').text());
        var AttDays = $.trim($(t).closest('tr').find('td:eq(4)').text());
        var TourDays = $.trim($(t).closest('tr').find('td:eq(5)').text());
        var TotLvDays = $.trim($(t).closest('tr').find('td:eq(6)').text());
        var ActOffDays = $.trim($(t).closest('tr').find('td:eq(7)').find('span').text());
        var ActHoliday = $.trim($(t).closest('tr').find('td:eq(8)').find('span').text());
        var LwpDays = $.trim($(t).closest('tr').find('td:eq(9)').text());
        var AdjustTiffinDays = $.trim($(t).closest('tr').find('td:eq(11)').find('input').val());
        var TiffinAllowance = $.trim($(t).closest('tr').find('td:eq(12)').text());
        var ClDays = $.trim($(t).closest('tr').find('td:eq(13)').text());
        var MlDays = $.trim($(t).closest('tr').find('td:eq(14)').text());
        var ElDays = $.trim($(t).closest('tr').find('td:eq(15)').text());
        var OthLvDays = $.trim($(t).closest('tr').find('td:eq(16)').text());
        var LateAtt = $.trim($(t).closest('tr').find('td:eq(17)').text());

        var _data = "{TransactionType:'" + TransactionType + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + SalMonthID +
            "', EmployeeID:'" + EmployeeID + "', AttDays:'" + AttDays + "', TourDays:'" + TourDays +
            "', ClDays:'" + ClDays + "', MlDays:'" + MlDays + "', ElDays:'" + ElDays + "', OthLvDays:'" + OthLvDays +
            "', ActOffDays:'" + ActOffDays + "', ActHoliday:'" + ActHoliday + "', LwpDays:'" + LwpDays + "', AdjustTiffinDays:'" + AdjustTiffinDays +
            "', TiffinAllowance:'" + TiffinAllowance + "', LateAtt:'" + LateAtt + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/MonthlyAttendence/UpdateDetailData",
            data: _data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                if (D.Status == 200) {
                    if (D.Data != null) {
                        var xmlDoc = $.parseXML(D.Data);
                        var xml = $(xmlDoc);
                        var Table = xml.find("Table");
                        if ($(Table).find("ErrorCode").text() == 1) {
                            alert($(Table).find("Messege").text());
                        }
                        else {
                            alert($(Table).find("Messege").text());
                            //$('.txt' + $(Table).find("FId").text()).focus();
                        }
                    }
                    else {
                        alert("No Record found !");
                    }
                }
                else {
                    alert(D.Message);
                }
            },
            error: function (D) {
                alert(D.Message);
            }
        });
    }
}

function RedirectLeave(t) {
    var EmployeeID = $.trim($(t).closest('tr').find('td:eq(1)').text());
    var SalMonthID = $.trim($(t).closest('tr').find('td:eq(2)').text());
    var yr = $('#ddlYear').val(); //alert(yr);

    var _data = "{TransactionType:'" + 'LEAVEEDIT' + "', SalMonthID:'" + $.trim(SalMonthID) + "', EmployeeID:'" + $.trim(EmployeeID) + "', yr:'" + yr + "'}"; //alert(_data);
    $.ajax({
        type: "POST",
        url: "/Administration/Master/MonthlyAttendence/LeaveEdit",
        data: _data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.Status == 200) {
                if (D.Data != null) {
                    var xmlDoc = $.parseXML(D.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");

                    var emp = $(Table).find("EmployeeID").text();
                    var flag = $(Table).find("LvFlag").text();
                    

                    window.location.href = '/LeaveTransaction/Index?emp=' + emp + '&sal=' + SalMonthID + '&flag=' + flag + '&yr=' + yr;
                }
                else {
                    alert("No Record found !");
                }
            }
            else {
                alert(D.Message);
            }
        },
        error: function (D) {
            alert(D.Message);
        }
    });
}

function EmpPopupDatas(t) {
    var EmployeeID = $.trim($(t).closest('tr').find('td:eq(1)').text());
    var SalMonthID = $.trim($(t).closest('tr').find('td:eq(2)').text());

    _SalMonthID = SalMonthID; _EmployeeID = EmployeeID; _EmpName = $.trim($(t).closest('tr').find('td:eq(3)').text());

    var _data = "{TransactionType:'" + 'LEAVEBALANCE' + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + $.trim(SalMonthID) + "', EmployeeID:'" + $.trim(EmployeeID) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/MonthlyAttendence/LeaveBalance",
        data: _data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.Status == 200) {
                if (D.Data != null) {
                    var xmlDoc = $.parseXML(D.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");

                    if ($(Table).find("EmpName").text() == '') {
                        alert("No Record found !"); return false;
                    }

                    $('#lblEmpName').text($.trim($(t).closest('tr').find('td:eq(3)').text()));
                    $('#lblEmpID').text($.trim($(t).closest('tr').find('td:eq(1)').text()));

                    $("#tblEmpName tr.trclass").empty();
                    $(xml).find("Table").each(function () {
                        $("#tblEmpName").append("<tr class='trclass'><td style='display:none;'>" +   // 
                            $(this).find("LvId").text() + "</td><td style='text-align:center'>" +
                            $(this).find("LeaveType").text() + "</td><td style='text-align:center'>" +
                            $(this).find("Opening").text() + "</td><td style='text-align:center'><a href='javascript:void(0)' onclick='EmpDetailsDatas(this)' title='Click for Details' style='color:blue; cursor:pointer' >" +
                            $(this).find("Taken").text() + "</a></td><td style='text-align:center'>" +
                            $(this).find("Closing").text() + "</td></tr>");
                    });

                    //jQuery.noConflict();
                    $('#divEmployee').modal('show');
                }
                else {
                    alert("No Record found !");
                }
            }
            else {
                alert(D.Message);
            }
        },
        error: function (D) {
            alert(D.Message);
        }
    });
}

function EmpDetailsDatas(t) {
    var EmployeeID = _EmployeeID;
    var SalMonthID = _SalMonthID;
    var LvId = $.trim($(t).closest('tr').find('td:eq(0)').text());


    var _data = "{TransactionType:'" + 'LEAVEBALANCEDTLS' + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + $.trim(SalMonthID) + "', EmployeeID:'" + $.trim(EmployeeID) + "', LvId:'" + $.trim(LvId) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/MonthlyAttendence/LeaveTypeDetails",
        data: _data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.Status == 200) {
                if (D.Data != null) {
                    var xmlDoc = $.parseXML(D.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");

                    if ($(Table).find("EmpName").text() == '') {
                        alert("No Record found !"); return false;
                    }

                    $('#lblEmpDetailsEmpName').text(_EmpName);
                    $('#lblEmpDetailsEmpID').text(_EmployeeID);

                    $("#tblEmpDetails tr.trclass").empty();
                    $(xml).find("Table").each(function () {
                        $("#tblEmpDetails").append("<tr class='trclass'><td style='display:none;'>" +   // 
                            $(this).find("LvId").text() + "</td><td style='text-align:center'>" +
                            $(this).find("LeaveType").text() + "</td><td style='text-align:center'>" +
                            $(this).find("LeaveDate").text() + "</td><td style='text-align:center'>" +
                            $(this).find("NoOfDays").text() + "</td></tr>");
                    });

                    //jQuery.noConflict();
                    $('#divEmpDetails').modal('show');
                }
                else {
                    alert("No Record found !");
                }
            }
            else {
                alert(D.Message);
            }
        },
        error: function (D) {
            alert(D.Message);
        }
    });
}

function LvPopupDatas(t) {

    var EmployeeID = $.trim($(t).closest('tr').find('td:eq(1)').text());
    var SalMonthID = $.trim($(t).closest('tr').find('td:eq(2)').text());

    _SalMonthID = SalMonthID; _EmployeeID = EmployeeID; _EmpName = $.trim($(t).closest('tr').find('td:eq(3)').text());

    var _data = "{TransactionType:'" + 'LEAVECLICK' + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + $.trim(SalMonthID) + "', EmployeeID:'" + $.trim(EmployeeID) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/MonthlyAttendence/LeaveBalance",
        data: _data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.Status == 200) {
                if (D.Data != null) {
                    var xmlDoc = $.parseXML(D.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");

                    if ($(Table).find("EmpName").text() == '') {
                        alert("No Record found !"); return false;
                    }

                    $('#lblLvEmpName').text($.trim($(t).closest('tr').find('td:eq(3)').text()));
                    $('#lblLvEmpID').text($.trim($(t).closest('tr').find('td:eq(1)').text()));

                    $("#tblLeave tr.trclass").empty();
                    $(xml).find("Table").each(function () {
                        $("#tblLeave").append("<tr class='trclass' onclick='LeaveDetails(this)'  title='Click for Details'><td style='display:none;'>" +   // 
                            $(this).find("LvId").text() + "</td><td style='text-align:center'>" +
                            $(this).find("LeaveType").text() + "</td><td style='text-align:center'>" +
                            $(this).find("NoOfDays").text() + "</td></tr>");
                    });

                    //jQuery.noConflict();
                    $('#divLeave').modal('show');
                }
                else {
                    alert("No Record found !");
                }
            }
            else {
                alert(D.Message);
            }
        },
        error: function (D) {
            alert(D.Message);
        }
    });
}

function LeaveDetails(t) {
    var chkall = $('#chkAllLeave').prop('checked') == true ? 0 : '';
    //alert(chkall);
    var EmployeeID = _EmployeeID;
    var SalMonthID = _SalMonthID;
    var LvId = chkall == '0' ? 0 : $.trim($(t).closest('tr').find('td:eq(0)').text());// alert($.trim($(t).closest('tr').find('td:eq(0)').text())); alert(LvId);

    //_EmployeeID = ''; _SalMonthID = '';

    var _data = "{TransactionType:'" + 'LEAVETYPECLICK' + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + $.trim(SalMonthID) + "', EmployeeID:'" + $.trim(EmployeeID) + "', LvId:'" + $.trim(LvId) + "'}";
    //alert(_data);
    $.ajax({
        type: "POST",
        url: "/Administration/Master/MonthlyAttendence/LeaveTypeDetails",
        data: _data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.Status == 200) {
                if (D.Data != null) {
                    var xmlDoc = $.parseXML(D.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");

                    if ($(Table).find("EmpName").text() == '') {
                        alert("No Record found !"); return false;
                    }

                    $('#lblLvDetailsEmpName').text(_EmpName);
                    $('#lblLvDetailsEmpID').text(_EmployeeID);

                    $("#tblLeaveDetails tr.trclass").empty();
                    $(xml).find("Table").each(function () {
                        $("#tblLeaveDetails").append("<tr class='trclass'><td style='display:none;'>" +   // 
                            $(this).find("LvId").text() + "</td><td style='text-align:center'>" +
                            $(this).find("LeaveType").text() + "</td><td style='text-align:center'>" +
                            $(this).find("LeaveDate").text() + "</td><td style='text-align:center'>" +
                            $(this).find("NoOfDays").text() + "</td></tr>");
                    });

                    //jQuery.noConflict();
                    $('#divLeaveDetails').modal('show');
                }
                else {
                    alert("No Record found !");
                }
            }
            else {
                alert(D.Message);
            }
        },
        error: function (D) {
            alert(D.Message);
        }
    });
}

function HolidayPopupDatas(t) {

    var EmployeeID = $.trim($(t).closest('tr').find('td:eq(1)').text());
    var SalMonthID = $.trim($(t).closest('tr').find('td:eq(2)').text());

    _SalMonthID = SalMonthID; _EmployeeID = EmployeeID; _EmpName = $.trim($(t).closest('tr').find('td:eq(3)').text());

    var _data = "{TransactionType:'" + 'HOLIDAYCLICK' + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + $.trim(SalMonthID) + "', EmployeeID:'" + $.trim(EmployeeID) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/MonthlyAttendence/LeaveBalance",
        data: _data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.Status == 200) {
                if (D.Data != null) {
                    var xmlDoc = $.parseXML(D.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");

                    $('#lblHolidayDetailsEmpName').text($.trim($(t).closest('tr').find('td:eq(3)').text()));
                    $('#lblHolidayDetailsEmpID').text($.trim($(t).closest('tr').find('td:eq(1)').text()));

                    $("#tblHolidayDetails tr.trclass").empty();
                    $(xml).find("Table").each(function () {
                        $("#tblHolidayDetails").append("<tr class='trclass'><td style='text-align:center'>" +   // 
                            $(this).find("HDate").text() + "</td><td style='text-align:center'>" +
                            $(this).find("HDesc").text() + "</td></tr>");
                    });

                    //jQuery.noConflict();
                    $('#divHoliday').modal('show');
                }
                else {
                    alert("No Record found !");
                }
            }
            else {
                alert(D.Message);
            }
        },
        error: function (D) {
            alert(D.Message);
        }
    });
}

function TourPopupDatas(t) {
    var EmployeeID = $.trim($(t).closest('tr').find('td:eq(1)').text());
    var SalMonthID = $.trim($(t).closest('tr').find('td:eq(2)').text());

    var _data = "{TransactionType:'" + 'TOURTYPECLICK' + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + $.trim(SalMonthID) + "', EmployeeID:'" + $.trim(EmployeeID) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/MonthlyAttendence/LeaveBalance",
        data: _data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.Status == 200) {
                if (D.Data != null) {
                    var xmlDoc = $.parseXML(D.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");

                    if ($(Table).find("EmpName").text() == '') {
                        alert("No Record found !"); return false;
                    }

                    $('#lblTourDetailsEmpName').text($.trim($(t).closest('tr').find('td:eq(3)').text()));
                    $('#lblTourDetailsEmpID').text($.trim($(t).closest('tr').find('td:eq(1)').text()));

                    $("#tblTourDetails tr.trclass").empty();
                    $(xml).find("Table").each(function () {
                        $("#tblTourDetails").append("<tr class='trclass'><td style='display:none;'>" +   // 
                            $(this).find("LvId").text() + "</td><td style='text-align:center'>" +
                            $(this).find("LeaveType").text() + "</td><td style='text-align:center'>" +
                            $(this).find("LeaveDate").text() + "</td><td style='text-align:center'>" +
                            $(this).find("NoOfDays").text() + "</td></tr>");
                    });

                    //jQuery.noConflict();
                    $('#divTourDetails').modal('show');
                }
                else {
                    alert("No Record found !");
                }
            }
            else {
                alert(D.Message);
            }
        },
        error: function (D) {
            alert(D.Message);
        }
    });
}

function LWPPopupDatas(t) {
    var EmployeeID = $.trim($(t).closest('tr').find('td:eq(1)').text());
    var SalMonthID = $.trim($(t).closest('tr').find('td:eq(2)').text());

    var _data = "{TransactionType:'" + 'LWPTYPECLICK' + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + $.trim(SalMonthID) + "', EmployeeID:'" + $.trim(EmployeeID) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/MonthlyAttendence/LeaveBalance",
        data: _data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.Status == 200) {
                if (D.Data != null) {
                    var xmlDoc = $.parseXML(D.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");

                    if ($(Table).find("EmpName").text() == '') {
                        alert("No Record found !"); return false;
                    }

                    $('#lblLWPDetailsEmpName').text($.trim($(t).closest('tr').find('td:eq(3)').text()));
                    $('#lblLWPDetailsEmpID').text($.trim($(t).closest('tr').find('td:eq(1)').text()));

                    $("#tblLWPDetails tr.trclass").empty();
                    $(xml).find("Table").each(function () {
                        $("#tblLWPDetails").append("<tr class='trclass'><td style='display:none;'>" +   // 
                            $(this).find("LvId").text() + "</td><td style='text-align:center'>" +
                            $(this).find("LeaveType").text() + "</td><td style='text-align:center'>" +
                            $(this).find("LeaveDate").text() + "</td><td style='text-align:center'>" +
                            $(this).find("NoOfDays").text() + "</td></tr>");
                    });

                    //jQuery.noConflict();
                    $('#divLWPDetails').modal('show');
                }
                else {
                    alert("No Record found !");
                }
            }
            else {
                alert(D.Message);
            }
        },
        error: function (D) {
            alert(D.Message);
        }
    });
}

function LatePopupDatas(t) {
    var EmployeeID = $.trim($(t).closest('tr').find('td:eq(1)').text());
    var SalMonthID = $.trim($(t).closest('tr').find('td:eq(2)').text());

    var _data = "{TransactionType:'" + 'LATETYPECLICK' + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + $.trim(SalMonthID) + "', EmployeeID:'" + $.trim(EmployeeID) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/MonthlyAttendence/LeaveBalance",
        data: _data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.Status == 200) {
                if (D.Data != null) {
                    var xmlDoc = $.parseXML(D.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");

                    if ($(Table).find("EmpName").text() == '') {
                        alert("No Record found !"); return false;
                    }

                    $('#lblLateDetailsEmpName').text($.trim($(t).closest('tr').find('td:eq(3)').text()));
                    $('#lblLateDetailsEmpID').text($.trim($(t).closest('tr').find('td:eq(1)').text()));

                    $("#tblLateDetails tr.trclass").empty();
                    $(xml).find("Table").each(function () {
                        $("#tblLateDetails").append("<tr class='trclass'><td style='display:none;'>" +   // 
                            $(this).find("LvId").text() + "</td><td style='text-align:center'>" +
                            $(this).find("LeaveType").text() + "</td><td style='text-align:center'>" +
                            $(this).find("LeaveDate").text() + "</td><td style='text-align:center'>" +
                            $(this).find("NoOfDays").text() + "</td></tr>");
                    });

                    //jQuery.noConflict();
                    $('#divLateDetails').modal('show');
                }
                else {
                    alert("No Record found !");
                }
            }
            else {
                alert(D.Message);
            }
        },
        error: function (D) {
            alert(D.Message);
        }
    });
}

function trHover(t) {
    t.style.backgroundColor = '#bfb8b8';
}
function trMouseOut(t, rowcolor) {
    t.style.backgroundColor = '#' + rowcolor;
}

function AdjustTiffinDays(t) {
    var changevalue = $(t).val();

    var TransactionType = 'ADJUSTTIFFINDAYS';
    var SalMonthID = $.trim($(t).closest('tr').find('td:eq(2)').text());
    var EmployeeID = $.trim($(t).closest('tr').find('td:eq(1)').text());
    var AttDays = $.trim($(t).closest('tr').find('td:eq(4)').text());
    var TourDays = $.trim($(t).closest('tr').find('td:eq(5)').text());
    var TotLvDays = $.trim($(t).closest('tr').find('td:eq(6)').text());
    var ActOffDays = $.trim($(t).closest('tr').find('td:eq(7)').find('input').val());
    var ActHoliday = $.trim($(t).closest('tr').find('td:eq(8)').find('input').val());
    var LwpDays = $.trim($(t).closest('tr').find('td:eq(9)').text());
    var AdjustTiffinDays = $.trim(changevalue);
    var TiffinAllowance = $.trim($(t).closest('tr').find('td:eq(12)').text());
    var ClDays = $.trim($(t).closest('tr').find('td:eq(13)').text());
    var MlDays = $.trim($(t).closest('tr').find('td:eq(14)').text());
    var ElDays = $.trim($(t).closest('tr').find('td:eq(15)').text());
    var OthLvDays = $.trim($(t).closest('tr').find('td:eq(16)').text());
    var LateAtt = $.trim($(t).closest('tr').find('td:eq(17)').text());

    var _data = "{TransactionType:'" + TransactionType + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + SalMonthID +
        "', EmployeeID:'" + EmployeeID + "', AttDays:'" + AttDays + "', TourDays:'" + TourDays +
        "', ClDays:'" + ClDays + "', MlDays:'" + MlDays + "', ElDays:'" + ElDays + "', OthLvDays:'" + OthLvDays +
        "', ActOffDays:'" + ActOffDays + "', ActHoliday:'" + ActHoliday + "', LwpDays:'" + LwpDays + "', AdjustTiffinDays:'" + AdjustTiffinDays +
        "', TiffinAllowance:'" + TiffinAllowance + "', LateAtt:'" + LateAtt + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/MonthlyAttendence/UpdateDetailData",
        data: _data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.Status == 200) {
                if (D.Data != null) {
                    var xmlDoc = $.parseXML(D.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");
                    if ($(Table).find("ErrorCode").text() == 1) {
                        $.trim($(t).closest('tr').find('td:eq(12)').text($(Table).find("TiffinAllowance").text()));
                    }
                    else {
                        alert($(Table).find("Messege").text());
                        //$('.txt' + $(Table).find("FId").text()).focus();
                    }
                }
                else {
                    alert("No Record found !");
                }
            }
            else {
                alert(D.Message);
            }
        },
        error: function (D) {
            alert(D.Message);
        }
    });
}

function CalculateOffDays(t) {
    $('#lblSalMonthIDOff').text('');
    $('#lblEmployeeIDOff').text('');
    $('#lblAttDaysOff').text('');
    $('#lblOffDays').text('');
    $('#txtChangeValueOff').val('');


    $('#lblSalMonthIDOff').text($.trim($(t).closest('tr').find('td:eq(1)').text()));
    $('#lblEmployeeIDOff').text($.trim($(t).closest('tr').find('td:eq(2)').text()));
    $('#lblAttDaysOff').text($.trim($(t).closest('tr').find('td:eq(4)').text()));
    $('#lblOffDays').text($(t).closest('tr').find('td:eq(18)').text());
    $('#txtChangeValueOff').val($(t).closest('tr').find('td:eq(7)').find('span').text());

    _this = t;

    //jQuery.noConflict();
    $('#divChangeOff').modal('show');
}

function Change_CalculateOffDays(t) {
    var SalMonthID = $.trim($('#lblSalMonthIDOff').text());
    var EmployeeID = $.trim($('#lblEmployeeIDOff').text());
    var AttDays = $.trim($('#lblAttDaysOff').text());
    var OffDays = $.trim($('#lblOffDays').text());
    var ChangeOffDays = $.trim($('#txtChangeValueOff').val());

    var _data = "{TransactionType:'" + 'CHANGEOFFDAY' + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + SalMonthID +
        "', EmployeeID:'" + EmployeeID + "', AttDays:'" + AttDays + "', ActOffDays:'" + OffDays + "', ChOffDays:'" + ChangeOffDays +
        "', FromDate:'" + _FromDate + "', ToDate:'" + _ToDate + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/MonthlyAttendence/ChangeOffDays",
        data: _data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.Status == 200) {
                if (D.Data != null) {
                    var xmlDoc = $.parseXML(D.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");
                    if ($(Table).find("ErrorCode").text() == 1) {
                        $(_this).closest('tr').find('td:eq(4)').text($(Table).find("Attendance").text());
                        $(_this).closest('tr').find('td:eq(7)').find('span').text($(Table).find("ChOffdays").text());
                        $(_this).closest('tr').find('td:eq(18)').text($(Table).find("ChOffdays").text());
                        //jQuery.noConflict();
                        $('#divChangeOff').modal('hide');
                    }
                    else {
                        alert($(Table).find("Messege").text());
                        $(_this).text($(Table).find("ChOffdays").text());
                        $(_this).closest('tr').find('td:eq(7)').find('span').text($(Table).find("ChOffdays").text());
                        //jQuery.noConflict();
                        //$('#divChange').modal('show');
                    }
                }
                else {
                    alert("No Record found !");
                }
            }
            else {
                alert(D.Message);
            }
        },
        error: function (D) {
            alert(D.Message);
        }
    });
}

function RedirectHoliDays(t) {

    window.location.href = '/HolidayExcpMaster/Index?red=1';
}

function CalculateHoliDays(t) {
    $('#lblSalMonthID').text('');
    $('#lblEmployeeID').text('');
    $('#lblAttDays').text('');
    $('#lblActHoliday').text('');
    $('#txtChangeValue').val('');


    $('#lblEmployeeID').text($.trim($(t).closest('tr').find('td:eq(1)').text()));
    $('#lblSalMonthID').text($.trim($(t).closest('tr').find('td:eq(2)').text()));
    $('#lblAttDays').text($.trim($(t).closest('tr').find('td:eq(4)').text()));
    //var ChangeHoliday = $.trim($('#txtChangeValue').val());
    $('#lblActHoliday').text($(t).closest('tr').find('td:eq(19)').text());
    $('#txtChangeValue').val($(t).closest('tr').find('td:eq(8)').find('a').text());

    _this = t;

    //jQuery.noConflict();
    $('#divChange').modal('show');

    //window.location.href = '/HolidayExcpMaster/Index?red=1';
}

function Change_CalculateHoliDays(t) {
    var SalMonthID = $.trim($('#lblSalMonthID').text());
    var EmployeeID = $.trim($('#lblEmployeeID').text());
    var AttDays = $.trim($('#lblAttDays').text());
    var ActHoliday = $.trim($('#lblActHoliday').text());
    var ChangeHoliday = $.trim($('#txtChangeValue').val());

    var _data = "{TransactionType:'" + 'CHANGEHOLIDAY' + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + SalMonthID +
        "', EmployeeID:'" + EmployeeID + "', AttDays:'" + AttDays + "', ActHoliday:'" + ActHoliday + "', ChangeHoliday:'" + ChangeHoliday +
        "', FromDate:'" + _FromDate + "', ToDate:'" + _ToDate + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/MonthlyAttendence/ChangeHoliday",
        data: _data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.Status == 200) {
                if (D.Data != null) {
                    var xmlDoc = $.parseXML(D.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");
                    if ($(Table).find("ErrorCode").text() == 1) {
                        $(_this).closest('tr').find('td:eq(4)').text($(Table).find("Attendance").text());
                        $(_this).closest('tr').find('td:eq(8)').find('span').text($(Table).find("ChHolidays").text());
                        $(_this).closest('tr').find('td:eq(19)').text($(Table).find("ChHolidays").text());
                        //jQuery.noConflict();
                        $('#divChange').modal('hide');
                    }
                    else {
                        alert($(Table).find("Messege").text());
                        $(_this).text($(Table).find("ChHolidays").text());
                        $(_this).closest('tr').find('td:eq(8)').find('span').text($(Table).find("ChHolidays").text());
                        //jQuery.noConflict();
                        //$('#divChange').modal('show');
                    }
                }
                else {
                    alert("No Record found !");
                }
            }
            else {
                alert(D.Message);
            }
        },
        error: function (D) {
            alert(D.Message);
        }
    });
}

function ButtonSanction(t) {
    var conf = confirm("Are you sure want to Sanction?");
    if (conf) {

        var LevelRemarks = $(t).parent('div').parent('div').parent('div').find('input').val(); //alert(LevelRemarks); return false;

        var _data = "{TransactionType:'" + "SANCTIONED" + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + _SalMonthID +
            "', LevelRemarks:'" + LevelRemarks + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/MonthlyAttendence/UpdateSanction",
            data: _data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                if (D.Status == 200) {
                    if (D.Data != null) {
                        var xmlDoc = $.parseXML(D.Data);
                        var xml = $(xmlDoc);
                        var Table = xml.find("Table");

                        var Table1 = xml.find("Table1");
                        var Table2 = xml.find("Table2");



                        if ($(Table).find("ErrorCode").text() == 1) {
                            alert($(Table).find("Messege").text());
                            //window.location.href = '/MonthlyAttendence/Index';

                            $('#SanctionErrorCode').val($(Table1).find("ErrorCode").text());
                            $('#SanctionReturnCode').val($(Table1).find("ReturnCode").text());
                            $('#Sanction_LastSanc').val($(Table1).find("LastSanc").text());
                            $('#Sanction_LUser').val($(Table1).find("LUser").text());
                            $('#Sanction_Messege').val($(Table1).find("Messege").text());
                            $('#SanctionSalMonthID').val($(Table1).find("SalMonId").text());
                            $('#ReGenerate').val($(Table2).find("Regenerate").text());
                            $('#ReGenerateSalMonID').val($(Table2).find("SalMon").text());

                            var SanctionField; var ShowViewSanction = ''; //alert($('#SanctionSalMonthID').val()); alert(SalMonthId);

                            if (_VSancDtls == 'H' || _VSancDtls == 'C') {
                                SanctionField = '<div class="row"><div class="col-md-3"><button id="btnViewSanction" onclick="ViewSanction()" type="button" class="btn btn-info" data-dismiss="modal">View Sanction Details</button></div>';
                                if ($('#SanctionSalMonthID').val() == _SalMonthID) {
                                    SanctionField += '<div class="col-md-6"><span style="font-weight:bold;">' + $('#Sanction_LastSanc').val() + '</span></div>';
                                    if ($('#ReGenerateSalMonID').val() == _SalMonthID) {
                                        if ($('#ReGenerate').val() == 1) {
                                            SanctionField += '<div class="col-md-2"><button id="btnReGenerate" onclick="ReGenerate(this)" type="button" class="btn btn-info" data-dismiss="modal">ReGenerate</button></div></div>';
                                        }
                                    }
                                    if (($('#SanctionErrorCode').val() == 1 || $('#SanctionReturnCode').val() == 1)) {
                                        SanctionField += '<br/><input id="txtSanctionRemarks" class="form-control" placeholder="-- Remarks --"  /><br/>';
                                        SanctionField += '<div class="row">';

                                        if ($('#SanctionErrorCode').val() == 1) {
                                            SanctionField += '<div class="col-md-2"><button id="btnSanction" onclick="ButtonSanction(this)" type="button" class="btn btn-info" data-dismiss="modal">Sanction</button></div>';
                                        }
                                        if ($('#SanctionReturnCode').val() == 1) {
                                            SanctionField += '<div class="col-md-2"><button id="btnReturn" onclick="ButtonReturn(this)" type="button" class="btn btn-danger" data-dismiss="modal">Return</button></div>';
                                        }
                                        SanctionField += '</div>'
                                    }
                                }

                                $(_globalthis).closest('div').children('div').find('#divSanction').html(SanctionField);
                            }
                            else {
                                $(_globalthis).closest('div').children('div').find('#divSanction').html('');
                            }
                        }
                        else {
                            alert($(Table).find("Messege").text());
                        }
                    }
                    else {
                        alert("No Status found !");
                    }
                }
                else {
                    alert(D.Message);
                }
            },
            error: function (D) {
                alert(D.Message);
            }
        });
    }
}

function ButtonReturn(t) {
    var conf = confirm("Are you sure want to Return?");
    if (conf) {
        var LevelRemarks = $(t).parent('div').parent('div').parent('div').find('input').val(); //alert(LevelRemarks); return false;

        var _data = "{TransactionType:'" + "RETURN" + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + _SalMonthID +
            "', LevelRemarks:'" + LevelRemarks + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/MonthlyAttendence/UpdateSanction",
            data: _data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                if (D.Status == 200) {
                    if (D.Data != null) {
                        var xmlDoc = $.parseXML(D.Data);
                        var xml = $(xmlDoc);
                        var Table = xml.find("Table");
                        var Table1 = xml.find("Table1");
                        var Table2 = xml.find("Table2");

                        if ($(Table).find("ErrorCode").text() == 1) {
                            alert($(Table).find("Messege").text());
                            //window.location.href = '/MonthlyAttendence/Index';

                            $('#SanctionErrorCode').val($(Table1).find("ErrorCode").text());
                            $('#SanctionReturnCode').val($(Table1).find("ReturnCode").text());
                            $('#Sanction_LastSanc').val($(Table1).find("LastSanc").text());
                            $('#Sanction_LUser').val($(Table1).find("LUser").text());
                            $('#Sanction_Messege').val($(Table1).find("Messege").text());
                            $('#SanctionSalMonthID').val($(Table1).find("SalMonId").text());
                            $('#ReGenerate').val($(Table2).find("Regenerate").text());
                            $('#ReGenerateSalMonID').val($(Table2).find("SalMon").text());

                            var SanctionField; var ShowViewSanction = ''; //alert($('#SanctionSalMonthID').val()); alert(SalMonthId);

                            if (_VSancDtls == 'H' || _VSancDtls == 'C') {
                                SanctionField = '<div class="row"><div class="col-md-3"><button id="btnViewSanction" onclick="ViewSanction()" type="button" class="btn btn-info" data-dismiss="modal">View Sanction Details</button></div>';
                                if ($('#SanctionSalMonthID').val() == _SalMonthID) {
                                    SanctionField += '<div class="col-md-6"><span style="font-weight:bold;">' + $('#Sanction_LastSanc').val() + '</span></div>';
                                    if ($('#ReGenerateSalMonID').val() == _SalMonthID) {
                                        if ($('#ReGenerate').val() == 1) {
                                            SanctionField += '<div class="col-md-2"><button id="btnReGenerate" onclick="ReGenerate(this)" type="button" class="btn btn-info" data-dismiss="modal">ReGenerate</button></div></div>';
                                        }
                                    }
                                    if (($('#SanctionErrorCode').val() == 1 || $('#SanctionReturnCode').val() == 1)) {
                                        SanctionField += '<br/><input id="txtSanctionRemarks" class="form-control" placeholder="-- Remarks --"  /><br/>';
                                        SanctionField += '<div class="row">';

                                        if ($('#SanctionErrorCode').val() == 1) {
                                            SanctionField += '<div class="col-md-2"><button id="btnSanction" onclick="ButtonSanction(this)" type="button" class="btn btn-info" data-dismiss="modal">Sanction</button></div>';
                                        }
                                        if ($('#SanctionReturnCode').val() == 1) {
                                            SanctionField += '<div class="col-md-2"><button id="btnReturn" onclick="ButtonReturn(this)" type="button" class="btn btn-danger" data-dismiss="modal">Return</button></div>';
                                        }
                                        SanctionField += '</div>'
                                    }
                                }

                                $(_globalthis).closest('div').children('div').find('#divSanction').html(SanctionField);
                            }
                            else {
                                $(_globalthis).closest('div').children('div').find('#divSanction').html('');
                            }
                        }
                        else {
                            alert($(Table).find("Messege").text());
                        }
                    }
                    else {
                        alert("No Status found !");
                    }
                }
                else {
                    alert(D.Message);
                }
            },
            error: function (D) {
                alert(D.Message);
            }
        });
    }
}

function ViewSanction() {
    var LevelRemarks = '';

    var _data = "{TransactionType:'" + "VIEWDETAILS" + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + _SalMonthID +
        "', LevelRemarks:'" + LevelRemarks + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/MonthlyAttendence/UpdateSanction",
        data: _data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.Status == 200) {
                if (D.Data != null) {
                    var xmlDoc = $.parseXML(D.Data);
                    var xml = $(xmlDoc);
                    var Table1 = xml.find("Table1");

                    if ($(Table1).find("ErrorCode").text() == 1) {
                        $("#tblView tr.trclass").empty();
                        $(xml).find("Table").each(function () {
                            $("#tblView").append("<tr class='trclass'><td style='text-align:center'>" +   // style='display:none;'
                                $(this).find("UseName").text() + "</td><td style='text-align:center'>" +
                                $(this).find("SanctionOn").text() + "</td><td style='text-align:center'>" +
                                $(this).find("ReturnOn").text() + "</td><td>" +
                                $(this).find("ReturnBy").text() + "</td><td>" +
                                $(this).find("LevelRemarks").text() + "</td></tr>");
                        });

                        //jQuery.noConflict();
                        $('#divView').modal('show');
                    }
                    else {
                        alert($(Table1).find("Messege").text());
                        //jQuery.noConflict();
                        $('#divView').modal('hide');
                    }
                }
                else {
                    alert("No Status found !");
                    //jQuery.noConflict();
                    $('#divView').modal('hide');
                }
            }
            else {
                alert(D.Message);
                //jQuery.noConflict();
                $('#divView').modal('hide');
            }
        },
        error: function (D) {
            alert(D.Message);
            //jQuery.noConflict();
            $('#divView').modal('hide');
        }
    });
}

function ReGenerate(t) {
    var conf = confirm("Are you sure want to ReGenerate?");
    if (conf) {
        var EmployeeID = '';

        var tableborder = 'style = "border: 1px solid; text-align:center"';
        var preventCharacter = 'onkeydown = "return !((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) && event.keyCode!=8 && event.keyCode!=46);"';

        var _data = "{TransactionType:'" + "REGENERATE" + "', Year:'" + $.trim($('#ddlYear').val()) + "', SalMonthID:'" + _SalMonthID +
            "', EmployeeID:'" + EmployeeID + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/MonthlyAttendence/LeaveBalance",
            data: _data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                if (D.Status == 200) {
                    if (D.Data != null) {
                        var xmlDoc = $.parseXML(D.Data);
                        var xml = $(xmlDoc);
                        var Table = xml.find("Table");


                        var slNo = 1;
                        $(_globalthis).closest('div').find('table').html('');
                        var detaildata = "<thead><tr><th " + tableborder + ">SlNo</th><th style='border: 1px solid; text-align:center'>EmpName<input id='EmpSearch' class='form-control EmpSearch' style='width:250px' placeholder='Search' onkeyup='EmpSearchGrid(this)' /></th>"
                            + "<th " + tableborder + ">AttDays</th><th " + tableborder + ">TourDays</th><th " + tableborder + ">LeaveDays</th><th " + tableborder + ">OffDays</th>"
                            + "<th " + tableborder + ">Holidays</th><th " + tableborder + ">LWPDays</th><th " + tableborder + ">Tiffin Days</th><th " + tableborder + ">Adj Tiffin Days</th>"
                            + "<th " + tableborder + ">Tffin Allowance</th><th " + tableborder + ">Late Attendence</th><th " + tableborder + ">Action</th></tr></thead>";
                        detaildata += "<tbody>";

                        $(xml).find("Table").each(function () {

                            var rowcolor = slNo % 2 != 0 ? 'e2e2e2' : 'ffffff';

                            detaildata += "<tr class='trclass' style='background-color:#" + rowcolor + "' onmouseover='trHover(this)' onmouseout='trMouseOut(this, \"" + rowcolor + "\")'>";

                            detaildata += "<td>" + slNo + "</td>";
                            detaildata += "<td style='display:none'>" + $(this).find("EmployeeID").text() + "</td>";
                            detaildata += "<td style='display:none'>" + $(this).find("SalMonthID").text() + "</td>";
                            detaildata += "<td><a href='javascript:void(0)' onclick='EmpPopupDatas(this)' title='Click for Details' style='color='black'; cursor:pointer' >" + $(this).find("EmpName").text() + "</a></td>";
                            detaildata += "<td>" + $(this).find("AttDays").text() + "</td>";
                            detaildata += "<td><a href='javascript:void(0)' onclick='TourPopupDatas(this)' title='Click for Details' style='color:blue; cursor:pointer' >" + $(this).find("TourDays").text() + "  " + "</a><img onclick='RedirectLeave(this)' style='cursor:pointer' title='Leave Entry' src='/Content/Images/Modify1.png' /></td>";
                            detaildata += "<td><a href='javascript:void(0)' onclick='LvPopupDatas(this)' title='Click for Details' style='color:#0fc7c7; cursor:pointer' >" + $(this).find("TotLvDays").text() + "  " + "</a><img onclick='RedirectLeave(this)' style='cursor:pointer' title='Leave Entry' src='/Content/Images/Modify1.png' /></td>";
                            //detaildata += "<td><input type='text' id='ActOffDays' onkeyup='CalculateAttDays(this)' style='text-align:right; width:50px' class='form-control preventpaste' value=" + $(this).find("ActOffDays").text() + " /></td>";
                            //detaildata += "<td><input type='text' id='ActHoliday' onkeyup='CalculateHoliDays(this)' style='text-align:right; width:70px' class='form-control preventpaste' value=" + $(this).find("ActHoliday").text() + " /></td>";
                            detaildata += "<td><span>" + $(this).find("ActOffDays").text() + "</span>" + "         " + "<img onclick='CalculateOffDays(this)' style='cursor:pointer' title='Leave Entry' src='/Content/Images/Modify1.png' /></td>";
                            detaildata += "<td><span>" + $(this).find("ActHoliday").text() + "</span>" + "         " + "<img onclick='CalculateHoliDays(this)' style='cursor:pointer' title='Leave Entry' src='/Content/Images/Modify1.png' /></td>";
                            detaildata += "<td><a href='javascript:void(0)' onclick='LWPPopupDatas(this)' title='Click for Details' style='color:#24b336; cursor:pointer' >" + $(this).find("LwpDays").text() + "  " + "</a><img onclick='RedirectLeave(this)' style='cursor:pointer' title='Leave Entry' src='/Content/Images/Modify1.png' /></td>";
                            detaildata += "<td>" + $(this).find("TiffinDays").text() + "</td>";
                            detaildata += "<td><input type='text' id='AdjustTiffinDays' onkeyup='AdjustTiffinDays(this)' " + preventCharacter + " style='text-align:right;' class='form-control preventpaste' value=" + $(this).find("AdjustTiffinDays").text() + " /></td>";
                            detaildata += "<td>" + $(this).find("TiffinAllowance").text() + "</td>";
                            detaildata += "<td style='display:none'>" + $(this).find("ClDays").text() + "</td>";
                            detaildata += "<td style='display:none'>" + $(this).find("MlDays").text() + "</td>";
                            detaildata += "<td style='display:none'>" + $(this).find("ElDays").text() + "</td>";
                            detaildata += "<td style='display:none'>" + $(this).find("OthLvDays").text() + "</td>";
                            detaildata += "<td><a href='javascript:void(0)' onclick='LatePopupDatas(this)' title='Click for Details' style='color:#eb8f00; cursor:pointer' >" + $(this).find("LateAtt").text() + "  " + "</a><img onclick='RedirectLeave(this)' style='cursor:pointer' title='Leave Entry' src='/Content/Images/Modify1.png' /></td>";
                            detaildata += "<td style='display:none'>" + $(this).find("ActOffDays").text() + "</td>";
                            detaildata += "<td style='display:none'>" + $(this).find("ActHoliday").text() + "</td>";
                            detaildata += "<td><a onclick='SaveEachDetail(this)' class='btn btn-info'>Save</a></td>";
                            detaildata += "</tr>";

                            slNo++;
                        });
                        detaildata += "</tbody>";

                        $(_globalthis).closest('div').find('table').html(detaildata);

                    }
                    else {
                        alert("No Status found !");
                    }
                }
                else {
                    alert(D.Message);
                }
            },
            error: function (D) {
                alert(D.Message);
            }
        });
    }
}

//Do code here for Main report calling
function PrintMainReport() {
   
    var res = confirm('Are you sure want to Print the details ??');
    if (res == true) {
        var SalMonthID = _SalMonthID;
        var EmployeeID = 0;
        var SectorID = $("#ddlSector").val();

        //var _data = "{SalMonthID:'" + _SalMonthID + "', EmployeeID:'" + EmployeeID + "', SectorID:'" + SectorID + "'}";
        //$.ajax({
        //    type: "POST",
        //    url: "/MonthlyAttendence/Print",
        //    data: _data,
        //    contentType: "application/json; charset=utf-8",
        //    dataType: "json",
        //    success: function (D) {

        //    }
        //});
        $("#btnPrintMainReport").attr('href', "/Administration/Master/MonthlyAttendence/Print?SalMonthID=" + _SalMonthID + "&EmployeeID=" + EmployeeID + "&SectorID=" + SectorID);

       
    }
}


//for Demo
function Print(EmpName, tblID, rptHeader) {//innerHTML
    $.ajax({
        type: "POST",
        url: "/Administration/Master/MonthlyAttendence/CompanyDetails",
        data: "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.Status == 200) {
                if (D.Data != null) {
                    var xmlDoc = $.parseXML(D.Data);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");

                    var divToPrint = document.getElementById(tblID);
                    //var aaaa = '<div><table id="tblPrint" style="border-collapse: collapse;"><thead>';
                    //aaaa += '<tr>';
                    //aaaa += '<th colspan="2" style="text-align:left"><img src=' + $(Table).find("Logo").text() + ' /></th>';
                    //aaaa += '<th colspan="4" style="text-align:left; font-weight:bold; font-size:25px">' + $(Table).find("OfficeName").text() +
                    //    '<tr><th rowspan="4">' + $(Table).find("Address").text() + '</th></tr>'
                    //    '</th>';
                    //aaaa += '</tr>';

                    var aaaa = '<div><table id="tblPrint" ><thead>';
                    aaaa += '<tr>';
                    aaaa += '<th rowspan="2" style="text-align:left"><img src=' + $(Table).find("Logo").text() + ' /></th>';
                    aaaa += '<th><span style="text-align:left; font-weight:bold; font-size:23px">' + $(Table).find("OfficeName").text() + '</span><br/><span style="text-align:center">' + $(Table).find("DepartmentName").text() + '</span></th></tr>';
                    aaaa += '<tr><th>' + $(Table).find("Address").text() + '</th></tr>';
                    aaaa += '<tr><th colspan="6">========================================================================================================</th></tr>';
                    aaaa += '<tr><th colspan="6">' + rptHeader + '</th></tr>';
                    aaaa += '<tr><th colspan="6">========================================================================================================</th></tr>';
                    aaaa += '<tr><th style="width:250px"><span style="font-weight: bold; width:20px">Employee Name -(Employee No) :</span></th><th colspan="4" style="text-align:left;"><span>' + EmpName + '</span></th></tr>';
                    aaaa += '<tr><th colspan="6" >' + divToPrint.innerHTML + '</th></tr></thead></table></div>';


                    var popupWin = window.open('', '_blank', 'width=800,height=600,location=no,left=200px');
                    popupWin.document.open();
                    popupWin.document.write('<html><table><tr><td></td></tr></table><body onload="window.print()">' + aaaa + '</html>');
                    popupWin.document.close();

                }
                else {
                    alert("No Record found !");
                }
            }
            else {
                alert(D.Message);
            }
        },
        error: function (D) {
            alert(D.Message);
        }
    });



}

//for Using
function PrintNew(EmpName, tblID, tblclassID, rptHeader) {

    var table = $("#" + tblclassID);
    var tblCount = (table.find('tbody tr.trclass')).length;
    if (tblCount > 0) {
        var res = confirm('Are You Sure Want to Print Report ??');
        if (res == true) {
            var contents = $("#" + tblID).html();
            var frame1 = $('<iframe />');
            frame1[0].name = "frame1";
            frame1.css({ "position": "absolute", "top": "-1000000px" });
            $("body").append(frame1);
            var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
            frameDoc.document.open();
            //Create a new HTML document.
            frameDoc.document.write("<html><head><title></title>");
            frameDoc.document.write('</head><body>');
            //Append the external CSS file.
            //frameDoc.document.write('<link href="style.css" rel="stylesheet" type="text/css" />');
            //Append the DIV contents.
            frameDoc.document.write("<div style='width:100%'>" +
                "<div style='width:20%; float:left; text-align:right; padding-bottom:5px;'><img src='" + $("#lblLogo").text() + "' alt='Delete' style='width:85px;height:80px;cursor:pointer;' /></div>");
            frameDoc.document.write("<div style='width:90%; height:80px; text-align:center; '>" +
                "<span>" + $("#lblHeader").text() + "</span><br/>" +
                "<span>" + $("#lbladdress").text() + "</span></div>" +
                "<span>========================================================================================================</span><br/>" +
                "<div style='font-weight: bold; text-align:center;'><span>" + rptHeader + "</span></div><br/>" +
                "<div><span>========================================================================================================</span></div><br/>" +
                "<div><span style='font-weight: bold; width:20px'>Employee Name - (Employee No) :     </span><span>" + EmpName + "</span></div><br/>" +
                "</div>");


            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');
            frameDoc.document.close();
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
            //setTimeout(function () {
                
            //}, 500);
        }
    }
    else {
        alert('No Record found !'); return false;
    }
}


