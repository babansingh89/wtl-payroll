﻿


$(document).ready(function () {

    Load();

});


function Load() {

    $.ajax({
        type: "POST",
        url: "/VPF/Load",
        data: {},
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data; 
            var table = t["Table"];
            var table1 = t["Table1"]; 

            var isSaveButton = table1[0].isSaveButton;
            isSaveButton == 0 ? $('#btnSave').prop('disabled', false) : $('#btnSave').prop('disabled', true); 

            if (table.length > 0) {
                    $("#tbl tbody tr.myData").empty();
                    $.each(table, function (index, value) {
                        $("#tbl tbody").append("<tr class='myData'>" +
                            "<td style='text-align:center'>" + (index + 1) + "</td>" +
                            "<td class='EmployeeID' style='display:none;' >" + value.EmployeeID + "</td>" +
                            "<td class='EmpNo' style='text-align:center' >" + value.EmpNo + "</td>" +
                            "<td style='text-align:left'>" + value.EmpName + "</td>" +
                            "<td><input type='text' class='appgross' style='text-align:right' disabled value='" + value.ApplicableGross + "' /></td>" +
                            "<td><input type='text' class='perct'  value='" + value.Percentage + "' style='text-align:right' onkeyup='ChangeValue(this)' /></td>" +
                            "<td><input type='text' class='calamt' style='text-align:right' disabled value='" + value.CalcuAmt + "'  /></td>" +
                            "<td><input type='text' class='inputamt' style='text-align:right' " + (value.CheckBox == 1 ? '' : 'disabled') + " value= '" + value.InputAmount + "' /></td>" +
                            "<td style='text-align:center'><input type='checkbox' class='chk'  onclick='Enable(this)' " + (value.CheckBox == 1 ? 'checked' : '')+ "  /></td></tr>");
                    });
                
            }
        }
    });
}

function Enable(ID)
{
    var isChk = $(ID).prop('checked'); 
    $(ID).closest('tr').find('.inputamt').val(0);
    if (isChk)
    {
        $(ID).closest('tr').find('.inputamt').prop('disabled', false);
    }
    else
    {
        $(ID).closest('tr').find('.inputamt').prop('disabled', true);
    }
}

function ChangeValue(ID) {

    var perct = $(ID).val() == "" ? 0 : $(ID).val();
    var appGross = $(ID).closest('tr').find('.appgross').val();
    var calAmt = ( parseFloat(appGross) * parseFloat(perct)) / 100;
    $(ID).closest('tr').find('.calamt').val(calAmt);

}

function Save()
{
    var ArrList = [];
    var t = $('#tbl tbody tr.myData').length;
    if(t > 0)
    {
        $("#tbl tbody tr.myData").each(function (index, value) {
            var EmployeeID = $(this).closest('tr').find('.EmployeeID').text(); 
            var appgross = $(this).find('.appgross').val();
            var perct = $(this).find('.perct').val();
            var calamt = $(this).find('.calamt').val();
            var inputamt = $(this).find('.inputamt').val();
            var chk = $(this).find('.chk').prop('checked');

            ArrList.push({
                'EmployeeID': EmployeeID, 'appgross': appgross, 'perct': perct, 'calamt': calamt, 'inputamt': inputamt, 'chk': (chk == true ? 1 : 0)
            });
        });

        var Master = JSON.stringify(ArrList);
        var E = "{param: " + Master + "}";
        $.ajax({
            type: "POST",
            url: "/VPF/Save",
            data: E,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data;
                if (t == 'success')
                {
                    alert('Saved Successfully !');
                    location.reload();
                }
                else {
                    alert(D.Message);
                }
            }
        });
    }


}







