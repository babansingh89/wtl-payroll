﻿

$(document).ready(function () {
   // $('#overlay').show();
    $('#btnRefresh').click(function () {
        location.reload();
    });

    $('#btnSalGen').click(function () {


        var SectorID = $('#ddlSector').val();
        var SalMonthID = $('#txtSalMonthID').val();
        var SalMonth = $('#txtSalMonth').val();
        var SalFinYear = $('#txtSalFinYear').val();

        if (SectorID == '')
        {
            alert('Please Select Sector !'); return false;
        }

        var result = confirm("Do you want to Process Salary for the Month of " + SalMonth + " ?");
        if(result == true)
            Generate_SalGen(SalMonthID, SectorID, SalFinYear);
    });

    
});




function Generate_SalGen(SalMonthID, SectorID, SalFinYear) {
    var E = "{SalMonthID: " + $.trim(SalMonthID) + ", SectorID: " + $.trim(SectorID) + ", SalFinYear: '" + $.trim(SalFinYear) + "'}";
    // $('.loading-overlay').show();
    $.ajax({
        type: "POST",
        url: "/Administration/Master/SalaryGeneration/Salary_Process",
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            //$('.loading-overlay').hide();
            var t = D.Data;
            if (t == "success") {
                alert('Salary Generation has been Completed Successfully !!'); location.reload(); return false;
            }
            else if (t == "fail") {
                alert('There is Some Error !!'); return false;
            }
            else if (t == "encode") {
                alert('Sorry ! Your Salary has been already Processed. You Can not Generate Salary Process !'); return false;
            }
            else {
                alert(D.Message); return false;
            }
        }
    });
}


