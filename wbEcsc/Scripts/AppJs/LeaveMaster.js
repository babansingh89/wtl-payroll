﻿$(document).ready(function () {
    FieldNameData();
    GridData(0);
    
    $("#txtMaxInYear").val('0');
    $("#txtMaxSave").val('0');
    $("#txtMaxInServicePeriod").val('0');
    //, #txtMaxSave, #txtMaxInServicePeriod
    $('#txtMaxInYear, #txtMaxSave, #txtMaxInServicePeriod').keypress(function () {
        //event.which != 46 && 
        if (event.which == 58  || (event.which <= 47 || event.which >= 59)) {
            event.preventDefault();
            //if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            //    event.preventDefault();
            //}
        }
    });

    
    $('#btnSave').click(SaveUpdate);
    $('#btnRefresh').click(ClearAllFields);
});

function FieldNameData() {
    $.ajax({
        type: "POST",
        url: "/Administration/Master/LeaveMaster/Get_FieldName",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                $.each(D.Data, function (index, value) {
                    var MandatoryStyle = '*';
                    var Id = '#' + value.FieldId;
                    $(Id).text(value.FieldName);
                    $('#spn' + value.FieldId).text(value.IsMand == 1 ? '   ' + MandatoryStyle : '');
                });
            }
        }
    });
}

function GridData(leaveTypeId) {
    //var leaveTypeId = 0;
   
    //if ($('#hdnLeaveTypeID').val() > 0) {
    //    leaveTypeId = $('#hdnLeaveTypeID').val();
    //};
    
    var S = "{LVID:'" + leaveTypeId + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/LeaveMaster/Get_GridDATA",
        data: S,
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        success: function(D) {

            var t = D.Data;
            if (t.length > 0) {
                var gender = '';
                $("#tbl tr.trclass").empty();
                $.each(D.Data, function (index, value) {

                    if (value.Gender == 'B')
                        gender = 'Both';
                    else if (value.Gender == 'M')
                        gender = 'Male';
                    else
                        gender = 'Female';
                    $('#tbl').append("<tr class='trclass'> <td style='display:none'>" +
                            value.LvId +                 //0
                        "</td><td>" +
                            value.LeaveAbbv +            //1
                        "</td><td>" +
                            value.LeaveTypeDesc +        //2
                        "</td><td>" +
                            value.LvYearlyUpto +         //3
                        "</td><td>" +
                            value.MaxCfUpto +            //4
                        "</td><td>" +
                            value.MaxServUpto +          //5
                        "</td><td>" +
                            gender +                     //6
                        "</td><td>" +
                            value.ActiveFlag +           //7
                        "</td><td>" +
                            value.isRound +              //8
                        "</td><td>" +
                            value.isLeave +              //9
                        "</td><td style='text-align:center; cursor:pointer; color:blue' onclick='EditLeave(this)'>" +
                            "Edit" +
                        "</td><td style='text-align:center; cursor:pointer; color:blue' onclick='DeleteLeave(this)'>" +
                            "Delete" +
                        "</td></tr>"
                    );
                });
            }
            
           
        }
    });
};

function EditLeave(t) {
    
    //$('#divEffectiveTo').show();
    $('#btnSave').html('Update');
    $('#txtAbbv').val($(t).closest('tr').find('td:eq(1)').text().trim());
    $('#txtLeaveDesc').val($(t).closest('tr').find('td:eq(2)').text().trim());
    $('#txtMaxInYear').val($(t).closest('tr').find('td:eq(3)').text().trim());

    var gender = $(t).closest('tr').find('td:eq(6)').text().trim();
    if (gender == 'Both')
        $('#ddlGender').val('B');
    else if (gender == 'Male')
        $('#ddlGender').val('M');
    else
        $('#ddlGender').val('F');


    
    $('#txtMaxSave').val($(t).closest('tr').find('td:eq(4)').text().trim());
    $('#txtMaxInServicePeriod').val($(t).closest('tr').find('td:eq(5)').text().trim());


    //$('#chkIsActive').val($(t).closest('tr').find('td:eq(7)').text().trim());
    var activeflag = $(t).closest('tr').find('td:eq(7)').text().trim();
    
    if (activeflag == 1)
        $('#chkIsActive').prop("checked", true);
    else
        $('#chkIsActive').prop("checked", false);

    var roundflag = $(t).closest('tr').find('td:eq(8)').text().trim();

    if (roundflag == 1)
        $('#chkIsRound').prop("checked", true);
    else
        $('#chkIsRound').prop("checked", false);

    var leaveTourflag = $(t).closest('tr').find('td:eq(9)').text().trim();

    if (leaveTourflag == 1)
        $('#chkIsLeave').prop("checked", true);
    else
        $('#chkIsLeave').prop("checked", false);

    $('#hdnLVID').val($(t).closest('tr').find('td:eq(0)').text().trim());
    
}

function DeleteLeave(t) {
    var conf = confirm("Are you sure want to Delete this Record ?");
    if (conf == true) {
        var leaveID = $(t).closest('tr').find('td:eq(0)').text().trim();
        alert(leaveID);
        var E = "{LeaveTypeID:'" + leaveID + "'}"; alert(E);
        $.ajax({
            type: "POST",
            url: "/Administration/Master/LeaveMaster/Delete_LeaveMaster",
            data: E, 
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                
                var t = D.Data;
                if (t.length > 0) {
                    if (t[0].ErrorCode == 1) {
                        alert(t[0].Messege);
                        ClearAllFields();
                        
                    }
                    else {
                        alert(t[0].Messege);
                        
                    }
                }
            }
        });
    }
}

function SaveUpdate() {
    
    var isActiveValue = 0;
    if($("#chkIsActive").prop('checked') == true) {
        isActiveValue = 1;
    }
    var isRoundValue = 0;
    if ($("#chkIsRound").prop('checked') == true) {
        isRoundValue = 1;
    }
    var isLeaveValue = 0;
    if ($("#chkIsLeave").prop('checked') == true) {
        isLeaveValue = 1;
    }
    var S = "{LeaveTypeID:'" + $.trim($('#hdnLVID').val()) + "'," +
            "leaveDesc:'" + $.trim($('#txtLeaveDesc').val()) + "', " +
            "abbv:'" + $.trim($('#txtAbbv').val()) + "', " +
            "gender:'" + $.trim($('#ddlGender').val()) + "', " +
            "maxInYear:'" + $.trim($('#txtMaxInYear').val()) + "', " +
            "maxSave:'" + $.trim($('#txtMaxSave').val()) + "', " +
            "MaxInServicePeriod:'" + $.trim($('#txtMaxInServicePeriod').val()) + "'," +
            "isActive:'" + isActiveValue + "',"+
            "isRound:'" + isRoundValue + "'," +
            "isLeaveTour:'" + isLeaveValue + "'}";
   
    $.ajax({
        type: "POST",
        url: "/Administration/Master/LeaveMaster/SaveUpdate_LeaveMaster",
        data: S,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                if (t[0].ErrorCode == 1) {
                    alert(t[0].Messege);
                    ClearAllFields();
                    GridData(0);
                }
                else {
                    alert(t[0].Messege);
                }
            }
        }
    });
}

function ClearAllFields() {
    $("#hdnLVID").val('');
    $("#txtLeaveDesc").val('');
    $("#txtAbbv").val('');
    $("#ddlGender").val('');
    $("#txtMaxInYear").val('0');
    $("#txtMaxSave").val('0');
    $("#txtMaxInServicePeriod").val('0');
    document.getElementById('chkIsActive').checked = true;
    document.getElementById('chkIsRound').checked = true;
    document.getElementById('chkIsLeave').checked = true;
    $('#btnSave').html('Save');
}