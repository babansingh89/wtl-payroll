﻿$(document).ready(function (e) {
    $('#btnUpdate').click(function () {
        var Oldpassword = $('#OldPassword').val();
        var Newpassword = $('#NewPassword').val();
        var RNewpassword = $('#reNewPassword').val();
        if (Oldpassword != '' && Newpassword != '' && RNewpassword != '') {
            if (Newpassword == RNewpassword) {
                var E = "{OldPassword:'" + Oldpassword + "',NewPassword:'" + Newpassword + "'}";
                $.ajax({
                    type: "POST",
                    url: "/Administration/Master/PasswordChange/ChangePassword",
                    data: E,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        //console.log(D);
                        alert(D[0].MESSEGE);
                        EmptyField();
                    }
                });
            }
            else {
                alert("New Password and Confirmed Password are not same");
                $('#reNewPassword').focus();
            }
        }
        else {
            alert("All Field's Require");
        }
    })
    function EmptyField() {
        $('#OldPassword').val('');
        $('#NewPassword').val('');
        $('#reNewPassword').val('');
        $('#worng').hide();
        $('#perfect').hide();
    }
    $('#btnClear').click(function () {
        EmptyField();
    });
    $('#reNewPassword').keyup(function () {
        $('#worng').show();
        var Newpassword = $('#NewPassword').val();
        var RNewpassword = $('#reNewPassword').val();
        if (Newpassword == RNewpassword) {
            $('#perfect').show();
            $('#worng').hide();
        }
        else {
            $('#perfect').hide();
            $('#worng').show();
        }
    });
});
function blockSpecialChar(e) {
    var k = e.keyCode;
    return (k != 39 && k != 92);
}
