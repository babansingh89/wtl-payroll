﻿


$(document).ready(function () {
    
    $("#btnSearch").click(Show);

    $("#txtSearchEmpNo").on("keyup", function () {
        if ($(this).val().trim() == "") {
            $("#tbl tbody tr").show();
            return;
        }
        var value = $(this).val();
        var s = $("#tbl tbody tr").not(".EmpNo:contains('" + value.toUpperCase() + "')");
        var s1 = $("#tbl tbody tr").find(".EmpNo:contains('" + value.toUpperCase() + "')");
        s.hide();
        s1.parent().show();
    });

});



function Show()
{
    var LocationID = $('#ddlLocation').val();

    var S = "{SectorID:'" + $.trim($('#ddlSector').val()) + "', LocationID:'" + $.trim($('#ddlLocation').val()) + "', EDID : '" + $.trim($('#txtEDID').val()) + "' , SalFinYear : '" + $.trim($('#txtSalFinYear').val()) + "', SalMonthID : '" + $.trim($('#txtSalMonthID').val()) + "', MenuID : '" + $('#txtMenuID').val() + "'}";
    $.ajax({
        type: "POST",
        url: "/EDInput/Search_Details",
        data: S,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data; 
            if(t.length>0)
            {

                if ($.trim($('#txtEDID').val()) == 1) {    //This is only for ITax
                    $('#Itax').show();
                    $('#other_Itax').hide();
                    $("#tbl_ITax tbody tr.trclass").empty();
                    $.each(t, function (index, value) {
                        $("#tbl_ITax").append("<tr class='trclass'><td class='EmpNo' style='text-align:center'>" +
                            value.EmpNo + "</td><td style='text-align:left'>" +
                            value.EmpName + "</td><td style='text-align:center'>" +
                            "<input type='text' id='" + 'Itax_this_month_' + value.EmpNo + "' disabled style='text-align:center;'  value='" + value.ItaxForThisMonth + "' /></td><td>" +
                            "<input type='text' id='" + value.EmpNo + "'  style='text-align:center;'  value='" + value.EDAmount + "' onchange='return ChangeValue(this, \"" + value.BalanceItax + "\")' onkeydown = 'return (event.keyCode!=13)' /></td><td>" +
                            "<input type='text' id='" + 'BalanceItax_' + value.EmpNo + "' disabled style='text-align:center;'  value='" + value.BalanceItax + "'  /></td></tr>");
                    });
                }
                else
                {
                    $('#Itax').hide();
                    $('#other_Itax').show();
                    $("#tbl tbody tr.trclass").empty();
                    $.each(t, function (index, value) {
                        $("#tbl").append("<tr class='trclass'><td class='EmpNo' style='text-align:center'>" +
                            value.EmpNo + "</td><td style='text-align:left'>" +
                            value.EmpName + "</td><td style='text-align:center'>" +
                            "<input type='text' id='" + value.EmpNo + "' style='text-align:center;'  value='" + value.EDAmount + "' onchange='return ChangeValue(this, \"" + '' + "\")' onkeydown = 'return (event.keyCode!=13)' /></td></tr>");
                    });
                }

             
                $('#Div1').modal('show');
            }
        }
    });
}
function ChangeValue(txt, BalanceItax)
{
    var $this = $(txt);
    var id = $this.attr("id");
    var textBoxID = '#' + id;

    var Amount = ($(textBoxID).val());

    //if ($('#txtEDID').val() == 1) {
    //    if ((parseFloat(Amount)) > (parseFloat(BalanceItax))) {
    //        alert("Sorry ! You can not enter more than Balance ITax Amount " + BalanceItax); return false;
    //    }
    //}
   
    //var C = "{ EmpNo: '" + id + "',  EDID : '" + $.trim($('#txtEDID').val()) + "',  Amount: '" + Amount + "', SectorID: '" + $("#ddlSector").val() + "'}";
    var C = "{ EmpNo: '" + id + "',  EDID : '" + $.trim($('#txtEDID').val()) + "',  Amount: '" + Amount + "', SectorID: '" + $("#ddlSector").val() + "', FinYear: '" + $("#txtSalFinYear").val() + "'}";


    $.ajax({
        type: "POST",
        url: "/EDInput/Update_Details_RowWise",
        data: C,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var result = D.Data; 
            if (result == "encode") {
                alert('Sorry ! Your salary has been already Processed. \n You can not Save.');
                return false;
            }
            else if(result=="fail")
            {
                alert('Sorry ! There is some problem with database !');
                return false;
            }
            else {
                $(textBoxID).css("background-color", "lightgreen");
            }
        }
    });

}







