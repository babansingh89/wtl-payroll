﻿$(function () {
    var Edit = "";
    var Delete = "";
    var Update = "";

    var Selector = {
        txtAccFinYear: $("#txtAccFinYear"),
        hdnMaxSaveID: $("#hdnMaxSaveID"),
        txtFinYear: $("#txtFinYear"),
        txtDescription: $("#txtDescription"),
      //  rdoButton:$("input[name=value]:checked"),
        chckAmount: $("#chckAmount"),
        chckPercentage: $("#chckPercentage"),
        ddlGender: $("#ddlGender"),
        txtAmount: $("#txtAmount"),
        btnSave: $("#btnSave"),
        btnRefresh: $("#btnRefresh"),
        clstbl: $(".clstbl")
    };
    var objServerSide = {
        GetMethod: function (URL, callback) {
            $.ajax({
                type: "get",
                contentType: "application/json;charset=utf-8;",
                data: null,
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    callback(responce);
                },
                error: function () {
                    callback("error");
                }
            });
        },
        PostMehtod: function (URL, Data, callback) {
            $.ajax({
                type: "post",
                contentType: "application/json;charset=utf-8;",
                data: JSON.stringify(Data),
                dataType: "json",
                async: false,
                url: URL,
                success: function (responce) {
                    callback(responce);
                },
                error: function () {
                    callback("error");
                }
            });
        }
    }
    var objClientSide = {
        Initialization: function () {
            objClientSide.Event.onLoad();
            objClientSide.Event.onClick();
            objClientSide.Event.onKeydown();
            objClientSide.Event.onBlur();
        },
        Event: {
            onLoad: function () {
                objClientSide.Method.LoadFinYear();
                objClientSide.Method.GetFieldName();
                objClientSide.Method.GetItexSchame();
                objClientSide.Method.GetGender();
            },
            onClick: function () {
                Selector.btnSave.click(function () {
                    objClientSide.Method.Save();
                });
                Selector.btnRefresh.click(function () {
                    objClientSide.Method.Refresh();
                });
                $(document).on("click", ".clsUpdate", function () {
                    var ID = $(this).closest(".row").find(".clsID").text();
                    var FinYear = $(this).closest(".row").find(".clsFinYear").text();
                    var GenderID = $(this).closest(".row").find(".clsGenderID").text();
                    var IsAmount = $(this).closest(".row").find(".clsIsAmount").text();
                    var Description = $(this).closest(".row").find(".clsDescription").text();
                    var Amount = $(this).closest(".row").find(".clsAmount").text();
                    Selector.hdnMaxSaveID.val(ID);
                    Selector.txtAmount.val(Amount);
                    Selector.txtDescription.val(Description);
                    console.log(IsAmount);
                    $("input[name=value]").filter("[value=" + IsAmount + "]").prop('checked', true);                
                    Selector.ddlGender.val(GenderID);
                    Selector.btnSave.val(Update);
                });
                $(document).on("click", ".clsDelete", function () {
                    var ID = $(this).closest(".row").find(".clsID").text();
                    if (Selector.hdnMaxSaveID.val() == ID) {
                        alert("this Record in Delete Mode. You cannt Delete this record now..");
                        return false;
                    }

                    if (confirm("Are you sure want to Delete this Record ??")) {

                        objClientSide.Method.Delete(ID);
                    }
                });
            },
            onKeydown: function () {
                Selector.txtAmount.keypress(function (e) {
                    if (!(e.keyCode > 45 && e.keyCode < 58) || e.keyCode == 47 || (e.keyCode == 46 && $(this).val().indexOf('.') != -1)) {
                        return false;
                    }
                });
                Selector.txtDescription.keypress(function () {
                    objClientSide.Method.Search();
                });
            },
            onBlur: function () {
                Selector.txtAccFinYear.blur(function () {
                    objClientSide.Method.LoadFinYear();
                    objClientSide.Method.GetItexMaxAmount();
                });
                Selector.txtDescription.blur(function () {
                    objClientSide.Method.Search();
                });
                //Selector.txtAmount.on('keypress blur', function () {
                //    if(Selector.chckPercentage.is(':checked'))
                //    {
                //        Selector.txtAmount.prop('maxlength', '3');
                //    }
                //});
            }
        },
        Method: {
            LoadFinYear: function () {
                Selector.txtFinYear.val(Selector.txtAccFinYear.val().trim());
            },
            GetFieldName: function () {
                var URL = "/Administration/Master/ItaxScheme/Get_FieldName";
                objServerSide.GetMethod(URL, function (responce) {
                    var jsdata = responce.Data;
                    $.each(jsdata, function (index, value) {
                        index = index + 1;
                        var star = value.IsMand == 1 ? '*' : "";
                        $("#" + index + "").text(value.FieldName);
                        $("#" + index + "2").text(star);
                        if (value.FieldID == 7) {
                            Selector.btnSave.val(value.FieldName);
                        }
                        if (value.FieldID == 8) {
                            Edit = value.FieldName;
                        }
                        if (value.FieldID == 9) {
                            Update = value.FieldName;
                        }
                        if (value.FieldID == 10) {
                            Delete = value.FieldName;
                        }
                    });
                });
            },
            GetGender: function () {
                var URL = "/Administration/Master/ItaxScheme/Get_Gender";
                objServerSide.GetMethod(URL, function (responce) {
                    var jsdata = responce.Data;
                    Selector.ddlGender.empty();
                    Selector.ddlGender.append("<option value=''>Select Gender</option>")
                    $.each(jsdata, function (index, value) {
                        Selector.ddlGender.append("<option value='" + value.GenderID + "'>" + value.Gender + "</option>");
                    });
                });
            },
            GetItexSchame: function () {
                var Data = {
                    ID: Selector.hdnMaxSaveID.val(), FinYear: Selector.txtAccFinYear.val().trim(),
                    Description: Selector.txtDescription.val(), IsAmount: $("input[name='value']:checked").val(),
                    GenderID: Selector.ddlGender.val(), Gender: "", Amount: Selector.txtAmount.val().trim()
                };
                var url = "/Administration/Master/ItaxScheme/Get_Data";
              //  console.log(Data);
                objServerSide.PostMehtod(url, Data, function (responce) {
                    var jsdata = responce.Data;
                    console.log(jsdata);
                    objClientSide.Method.AppendTable(jsdata);
                });
            },
            AppendTable: function (jsdata) {
                console.log(jsdata);
                Selector.clstbl.empty();               
                $.each(jsdata, function (index, value) {
                    index = index + 1;
                    var amountxt = value.IsAmount == 'M' ? 'Rupees' : 'Percentage';
                    Selector.clstbl.append("<div class='row border-top'>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsID hide'>" + value.ID + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1'>" + index + "</div>"
                        + "<div class='col-lg-2 col-md-2 col-sm-2 clsFinYear'>" + value.FinYear + "</div>"
                         + "<div class='col-lg-2 col-md-2 col-sm-2 clsDescription'>" + value.Description + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 hide clsGenderID'>" + value.GenderID + "</div>"
                        + "<div class='col-lg-2 col-md-2 col-sm-2 clsGender'>" + value.Gender + "</div>"
                          + "<div class='col-lg-0 col-md-0 col-sm-0 clsIsAmount hide'>" + value.IsAmount + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsAmount'>" + value.Amount + "</div>"
                           + "<div class='col-lg-1 col-md-1 col-sm-1'>" + amountxt + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsUpdate text-center text-info' style='cursor:pointer' >" + Edit + "</div>"
                        + "<div class='col-lg-1 col-md-1 col-sm-1 clsDelete text-center text-default' style='cursor:pointer'>" + Delete + "</div>"
                        + "</div>");
                });
            },
            Save: function () {
                var Data = {
                    ID: Selector.hdnMaxSaveID.val(), FinYear: Selector.txtAccFinYear.val().trim(),
                    Description: Selector.txtDescription.val(), IsAmount: $("input[name='value']:checked").val(),
                    GenderID: Selector.ddlGender.val(), Gender: "", Amount: Selector.txtAmount.val().trim()
                };
                console.log(Data);
                var URL = "/Administration/Master/ItaxScheme/Save";
                objServerSide.PostMehtod(URL, Data, function (responce) {
                    var Message = responce.Message;
                    var code = responce.Data;
                    alert(Message);
                    console.log(code);
                    if (code.data2 == 0) {
                        objClientSide.Method.Focus(code.data1);
                    }
                    if (code.data2 == 1) {
                        objClientSide.Method.Refresh();
                    }
                });
            },
            Delete: function (ID) {
                var Data = {
                    ID: ID, FinYear: Selector.txtAccFinYear.val().trim(),
                    Description:Selector.txtDescription.val(),IsAmount:$("input[name='value']:checked").val(),
                    GenderID: Selector.ddlGender.val(), Gender: "", Amount: Selector.txtAmount.val().trim()
                };
                var URL = "/Administration/Master/ItaxScheme/Delete";
                objServerSide.PostMehtod(URL, Data, function (responce) {
                    var Message = responce.Message;
                    alert(Message);
                    objClientSide.Method.Refresh();
                });
            },
            Refresh: function () {
                Selector.txtAmount.val("");
                Selector.hdnMaxSaveID.val("");
                Selector.txtDescription.val("");                
                objClientSide.Method.GetFieldName();
                objClientSide.Method.GetGender();
                objClientSide.Method.LoadFinYear();
                objClientSide.Method.GetItexSchame();
            },
            Focus: function (num) {
                //if(num==2)
                //{
                //    Selector.txtFinYear.focus();
                //}
                if (num == 2) {
                    Selector.ddlGender.focus();
                }
                if (num == 3) {
                    Selector.txtDescription.focus();
                }
                if (num == 6) {
                    Selector.txtAmount.focus();
                }
            },
            Search: function () {
              var input = Selector.txtDescription.val().trim().toUpperCase();
              var tr = Selector.clstbl.find('.row');
                $.each(tr, function (index, value) {
                    var description = $(this).find('.clsDescription').text();
                    if (description.toUpperCase().indexOf(input) > -1) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            }
        }
    };

    objClientSide.Initialization();
});