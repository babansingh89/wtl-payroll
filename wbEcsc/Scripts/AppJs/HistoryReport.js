﻿$(document).ready(function () {

    Load_Finyear();

    $("#ddlSchSector").change(function () {
        Load_Finyear();
    });
    $("#txtSchFinYear").change(function () {
        //if ($(this).val() == '') { alert('please select Fin Year'); $("#txtSchFinYear").focus(); return false; }
        Populate_SalMonth();
    });
    $("#btnshow").click(function () {
        Show();
    });
});


function Load_Finyear() {

    var s = $('#ddlSchSector').val();
    var E = "{SectorID : '" + s + "'}";
    if (s != '') {
        $.ajax({
            type: "POST",
            url: "/All_Report/Show_FInyear",
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (D) {
                var a = D.Data.length;
                if (a > 0) {
                    $("#txtSchFinYear").empty();
                    $("#txtSchFinYear").append($("<option value=''>--Select--</option>"));
                    $.each(D.Data, function (index, value) {
                        var label = value.SalaryFinYear;
                        var val = value.SalaryFinYear;
                        //var t = val.split('#');
                        //if (index == 0)
                            //$("#txtSchFinYear").append($("<option selected></option>").val(val).html(label));
                        //else
                            $("#txtSchFinYear").append($("<option></option>").val(val).html(label));
                    });
                    //Populate_Center();
                }
            }
        });
    }
}

function Populate_SalMonth() {


    var E = "{ FinYear : '" + $("#txtSchFinYear").val() + "', SectorID : '" + $("#ddlSchSector").val() + "'}";
    $.ajax({
        type: "POST",
        url: "/All_Report/Show_SalMonth",
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            var a = D.Data.length;
            if (a > 0) {
                $("#ddlSchSalMonth").empty();
                $("#ddlSchSalMonth").append($("<option value=''>--Select--</option>"));
                $.each(D.Data, function (index, value) {
                    var label = value.SalaryMonth;
                    var val = value.Salmonthid;
                    var t = val.split('#');
                    if (t[1] == 'C')
                        $("#ddlSchSalMonth").append($("<option selected></option>").val(val).html(label));
                    else
                        $("#ddlSchSalMonth").append($("<option></option>").val(val).html(label));
                });


            }
        }
    });
}

function Show() {

    if ($("#ddlSchSector").val() == "") { $("#ddlSchSector").focus(); return false; }
    if ($("#txtSchFinYear").val() == "") { $("#txtSchFinYear").focus(); return false; }
    if ($("#ddlSchSalMonth").val() == "") { $("#ddlSchSalMonth").focus(); return false; }

    var t = $("#ddlSchSalMonth").val().split('#')[0];

    var Master = {
        SectorID: $("#ddlSchSector").val(), SalFinYear: $("#txtSchFinYear").val(), SalMonthID: t,
        SalMonth: $("#ddlSchSalMonth option:selected").text(), isPDFExcel: 1
    };

    var E = "{ ReportID : '" + $("#ddlReportList").val() + "', Params: " + JSON.stringify(Master) + "}";
    $.ajax({
        type: "POST",
        url: "/All_Report/SaveReportData",
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            var a = D.Data.length;
            if (a > 0) {

                var left = ($(window).width() / 2) - (900 / 2),
                    top = ($(window).height() / 2) - (600 / 2),
                    popup = window.open("/ReportView.aspx?ReportID=" + $("#ddlReportList").val() + "&SalMonth=" + $("#ddlSchSalMonth option:selected").text() + "&ReportTypeID=" + $("#ddlReportList").val(), "popup", "width=900, height=600, top=" + top + ", left=" + left);
                popup.focus();
            }
        }
    });
}

