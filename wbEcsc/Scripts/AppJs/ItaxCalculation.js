﻿

$(document).ready(function () {
    AssessementYear();
    $("input:text").focus(function () { if ($(this).val() == 0) { $(this).val('') }; });

    $('.numeric').each(function (evt) {
        if ($(this).text() == 0) { $(this).text(''); }

    });
});

function AssessementYear()
{
   
    var minYear = 0; var maxYear = 0; var minAssmtYear = 0; var maxAssmtYear = 0;
    var FinYear = $('#txtSalFinYear').val(); 

    var lines = FinYear.split('-'); 
    minYear = lines[0]; 
    maxYear = lines[1]; 

    minAssmtYear = parseInt(minYear) + 1;
    maxAssmtYear = parseInt(maxYear) + 1;
    var AssessmentYear = minAssmtYear + "-" + maxAssmtYear;
    $('#txtAssessmentYear').val(AssessmentYear);
}

function myFunction() {
    if (document.getElementById('rbPercent').checked == true) {
        $('.Max').text('Percentage  ');
    }
    if (document.getElementById('rbValue').checked == true) {
        $('.Max').text('Max  Amount  ');
    }
}

function deleteRow(Element) {
    var row = $(Element).closest('tr');
    $(row).remove();
    var Total = 0;
    if ($('#GridViewOther tr').length == 1) { $('#txtOtherSource').val(''); }
    $('.gridcls').each(function (evt) {
        Total = Total + parseInt($(this).text());
        $('#txtOtherSource').val(Total);
    });
    NetBalance();
}
function deleteOtherSrcRow(Element) {
    var row = $(Element).closest('tr');
    $(row).remove();
    var Total = 0;
    if ($('.grdOtherSrcInc').length == 0) { $('#txtOtherS').val(''); }
    $('.grdOtherSrcInc').each(function (evt) {
       
        Total = Total + parseInt($(this).text()); 
        $('#txtOtherS').val(Total);
    });
}

$(document).ready(function () {
    $('.show4').hide();
    $('#btnSectionAdd').hide();

    $("#txtEmpNo").autocomplete({
        source: function (request, response) {

           
            var E = "{EmpNo: '" + $.trim($("#txtEmpNo").val()) + "'}";

            $.ajax({
                type: "POST",
                url: "/Administration/Master/ItaxCalculation/Search_EmpNoAutoComplete",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.EmpNo,
                                EmpNo: item.EmployeeID
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            var EmpNoID = i.item.EmpNo;
            var a = EmpNoID.split('#');
            var EmployeeID = a[0];
            var EmpNo = a[1];

            $("#hdnEmpID").val(EmployeeID);
            var SectorID = $("#ddlSector").val();
            if (SectorID == "") {
                alert('Please Select Sector !'); $('#txtSearchEmpNo').val(''); $("#hdnEmployeeNo").val(''); $("#ddlSector").focus(); return false;
            }
            Populate_Grid(EmpNo);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtEmpNo').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtEmpNo').val('');
            $('#hdnEmpID').val('');
        }
        if (iKeyCode == 46) {
            $('#txtEmpNo').val('');
            $('#hdnEmpID').val('');
        }
    });



});




function Populate_Grid(EmpNo)
{
    var E = "{EmpNo: '" + EmpNo + "', SalFinYear: '" + $.trim($("#txtSalFinYear").val()) + "', SectorID: '" + $.trim($("#ddlSector").val()) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/ItaxCalculation/Load_Grid",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
          
            if (D.Message != '')
            {
                alert(D.Message); return false;
            }

            var table_1 = t.Table;
            var table_2 = t.Table1;
            var table_3 = t.Table2;
            var table_4 = t.Table3;

            if(table_1.length >0 )
            {
                var gen = table_1[0].Gender;
                if (gen == "M")
                    $("#txtGender").val('Male');
                else
                    $("#txtGender").val('FeMale');
            }

            if (table_4.length > 0) {
                $("#HiddenCheckGrid").val(1);
                var tGrossPay=0;   var tPtax=0;   var tItax=0;   var tGIS=0;   var tHRA=0;   var tGPF=0;   var tGPFRecov=0;   var tHBL=0; 
                var tIHBL = 0; var tOtherDedu = 0; var tNetPay = 0; var tArrearOT = 0;
                $("#tbl_1 tr.trclass").remove();
                $.each(table_4, function (index, value) {
                    var status = value.sStatus;

                    if (status == 'Projection') {
                        $("#tbl_1").append("<tr class='trclass' style='background-color:#D3F9FA;'> <td>" + value.SalMonth + "</td>" +
                                            "<td>" + value.GrossPay + "</td>" + "<td>" + value.Ptax + "</td>" + "<td>" + value.Itax + "</td>" + "<td>" + value.GIS + "</td>" +
                                            "<td>" + value.HRA + "</td>" + "<td>" + value.GPF + "</td>" + "<td>" + value.GPFRecov + "</td>" + "<td>" + value.HBL + "</td>" +
                                            "<td>" + value.IHBL + "</td>" + "<td>" + value.OtherDedu + "</td>" + "<td>" + value.NetPay + "</td>" + "<td>" + value.ArrearOT + "</td></tr>");
                    }
                    else if (status == "H") {
                        $("#tbl_1").append("<tr class='trclass' style='background-color:#F9F8D3;'> <td>" + value.SalMonth + "</td>" +
                                           "<td>" + value.GrossPay + "</td>" + "<td>" + value.Ptax + "</td>" + "<td>" + value.Itax + "</td>" + "<td>" + value.GIS + "</td>" +
                                           "<td>" + value.HRA + "</td>" + "<td>" + value.GPF + "</td>" + "<td>" + value.GPFRecov + "</td>" + "<td>" + value.HBL + "</td>" +
                                           "<td>" + value.IHBL + "</td>" + "<td>" + value.OtherDedu + "</td>" + "<td>" + value.NetPay + "</td>" + "<td>" + value.ArrearOT + "</td></tr>");
                    }
                    else if (status == "C") {
                        $("#tbl_1").append("<tr class='trclass' style='background-color:lightgreen;'> <td>" + value.SalMonth + "</td>" +
                                           "<td>" + value.GrossPay + "</td>" + "<td>" + value.Ptax + "</td>" + "<td>" + value.Itax + "</td>" + "<td>" + value.GIS + "</td>" +
                                           "<td>" + value.HRA + "</td>" + "<td>" + value.GPF + "</td>" + "<td>" + value.GPFRecov + "</td>" + "<td>" + value.HBL + "</td>" +
                                           "<td>" + value.IHBL + "</td>" + "<td>" + value.OtherDedu + "</td>" + "<td>" + value.NetPay + "</td>" + "<td>" + value.ArrearOT + "</td></tr>");
                    }
                    else if (status == "B") {
                        $("#tbl_1").append("<tr class='trclass' style='background-color:lightgreen;'> <td>" + value.SalMonth + "</td>" +
                                           "<td>" + value.GrossPay + "</td>" + "<td>" + value.Ptax + "</td>" + "<td>" + value.Itax + "</td>" + "<td>" + value.GIS + "</td>" +
                                           "<td>" + value.HRA + "</td>" + "<td>" + value.GPF + "</td>" + "<td>" + value.GPFRecov + "</td>" + "<td>" + value.HBL + "</td>" +
                                           "<td>" + value.IHBL + "</td>" + "<td>" + value.OtherDedu + "</td>" + "<td>" + value.NetPay + "</td>" + "<td>" + value.ArrearOT + "</td></tr>");
                    }

                    tGrossPay = tGrossPay + value.GrossPay;
                    tPtax = tPtax + value.Ptax;
                    tItax = tItax + value.Itax;
                    tGIS = tGIS + value.GIS;
                    tHRA = tHRA + value.HRA;
                    tGPF = tGPF + value.GPF;
                    tGPFRecov = tGPFRecov + value.GPFRecov;
                    tHBL = tHBL + value.HBL;
                    tIHBL = tIHBL + value.IHBL;
                    tOtherDedu = tOtherDedu + value.OtherDedu;
                    tNetPay = tNetPay + value.NetPay;
                    tArrearOT = tArrearOT + value.ArrearOT;

                    $("#txtIncome").val(tGrossPay + tArrearOT); $("#txtTotalPTax").val(tPtax); $("#txtTotHRA").val(tHRA);
                    $("#txtTotIHBL").val(tIHBL); 


                });
                $("#tbl_1").append("<tr class='trclass' style='background-color:#85C1E9;'> <td>" + 'Total' + "</td>" +
                    "<td>" + tGrossPay + "</td>" +
                    "<td>" + tPtax + "</td>" +
                    "<td>" + tItax + "</td>" +
                    "<td>" + tGIS + "</td>" +
                    "<td>" + tHRA + "</td>" +
                    "<td>" + tGPF + "</td>" +
                    "<td>" + tGPFRecov + "</td>" +
                    "<td>" + tHBL + "</td>" +
                    "<td>" + tIHBL + "</td>" +
                    "<td>" + tOtherDedu + "</td>" +
                    "<td>" + tNetPay + "</td>" +
                    "<td>" + tArrearOT + "</td></tr>");
                Populate_Other();
            }
            else
            {
                $("#HiddenCheckGrid").val(0);
            }
        }
    });
}
function Populate_Other()
{
    if ($('#HiddenCheckGrid').val() == 1) {
        var F = "{hdnEmpID:'" + $('#hdnEmpID').val() + "'}"; 
        $.ajax({
            type: "POST",
            url: "/Administration/Master/ItaxCalculation/GetTaxBalAndChallan",
            data: F,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                $('#btnSectionAdd').show();
                var t = D.Data;

                var Challan = t.Table;
                var ItaxDetails = t.Table1;

                $('#txtInterestFromSaving').val(Math.round($(ItaxDetails).find('OtherIncomeIntSaving').text()));
                $('#txtInterestFromFD').val(Math.round($(ItaxDetails).find('OtherIncomeIntFD').text()));
                $('#txtOtherS').val(Math.round($(ItaxDetails).find('OtherIncomeOther').text()));
                $('#txtSurcharge').val(Math.round($(ItaxDetails).find('Surcharge').text()));
                $('#txtUS89').val(Math.round($(ItaxDetails).find('ReliefUS89').text()));
                $('#txtDetails1').val(Math.round($(ItaxDetails).find('Total').text()));

                $.each(Challan, function () {
                    var customer = $(this);
                    $('#GridViewOther').append("<tr class='grv_Other'><td>" + $(this).find("ChallanNo").text() + "</td><td>" + $(this).find("BSRNo").text() + "</td><td>" + $(this).find("ChallanDate").text() + "</td><td style='text-align:right' class='gridcls'>" + Math.round($(this).find("TaxAmount").text()) + "</td><td style='text-align:right'><input id='btnDelete' onclick='deleteRow(this)' class='Btnclassname' type='button' value='Delete'/></td></tr>");
                });
                var Total = 0;
                $('.gridcls').each(function (evt) {
                    Total = Total + parseInt($(this).text());
                    $('#txtOtherSource').val(Total);
                });


            }
        });
        var F = "{hdnEmpID:'" + $('#hdnEmpID').val() + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/ItaxCalculation/Grid2ndGrid",
            data: F,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0)
                {
                   
                    var SlNo = 1; var isButton = ''; var isAmount = '';
                    $("#GridTaxDetails tr.trclass").remove();
                    $.each(t, function (index, value) {
                       
                        if (SlNo == 1) {
                            isButton = "<input type='button' value='Show Details' id='btnDetails4' onclick='Show_80CC_Grid(this)' class='btn btn-primary' />";
                            isAmount = "<input type='text' id='txtHAmount' style='text-align:right' onkeypress='return NumberOnly()' class='allownumericwithdecimal' disabled value='" + ((value.Amount) == null ? '0' : (value.Amount)) + "' />";
                        }
                        else {
                            isButton = "<input type='button' value='Show Details' id='btnDetails5'  class='btn btn-primary' />";  //onclick='Show_80CC_Grid(this)'
                            isAmount = "<input type='text' id='txtHAmount' style='text-align:right' onkeyup='CkeckAmount(this, \"" + value.MaxType + "\" , \"" + value.MaxAmount + "\" , \"" + 'lblchk' + SlNo + "\")' onkeypress='return NumberOnly()' class='allownumericwithdecimal' value='" + ((value.Amount) == null ? '' : (value.Amount)) + "' />";
                        }
                        $("#GridTaxDetails").append("<tr class='trclass tblTaxDetails'> <td>" + SlNo + "</td>" +
                                        "<td class='clsTaxDetailID' style='display:none;' >" + value.ItaxSavingTypeID + "</td>" +
                                        "<td >" + value.TypeDesc + "</td>" +
                                        "<td style='text-align:center;'>" + isButton + "</td>" +
                                        "<td style='display:none'>" + value.MaxType + "</td>" +
                                        "<td style='display:none'>" + value.MaxAmount + "</td>" +
                                        "<td style='text-align:center;'><label class='chklbl' style='text-align:left; width:200px' id=lblchk" + SlNo + " value='" + value.lblAmount + "' >" + "</label>" + isAmount + " </td></tr>"); 
                        SlNo++;
                    });
                }

                $('#tblBeneficary_CHDetail').html(D.d);

                Calcutate();
            }
        });
    }
    CalNetTaxPay();
}


//function 
function NetBalance() {
   
    var TotalTaxLi = 0;
    var OtherPaid = 0;
    if ($('#txtOtherSource').val() != "") { OtherPaid = $('#txtOtherSource').val(); }
   
    if ($('#txtNetTaxPay').val() != "") { TotalTaxLi = $('#txtNetTaxPay').val(); }
    var check = Math.sign(TotalTaxLi); 

    var BalanceMinus = Math.round(parseFloat(TotalTaxLi) + (parseFloat($('#txtTDSAmt').val()) + parseFloat(OtherPaid))); if (isNaN(BalanceMinus))  { BalanceMinus = 0; } //if (BalanceMinus === NaN) { BalanceMinus = 0; }
    var BalancePlus = Math.round(parseFloat(TotalTaxLi) - (parseFloat($('#txtTDSAmt').val()) + parseFloat(OtherPaid))); if (isNaN(BalancePlus)) { BalancePlus = 0; } //if (BalancePlus === NaN) { BalancePlus = 0; }
   

    if (check == -1) {
        $('.balance').text('Balance Deductible  ');
        //$('#txtBalTax').val(BalanceMinus);
    }
    else {
        $('.balance').text('Balance Refundable  ');
        //$('#txtBalTax').val(BalancePlus);

    }
    
    if (parseFloat($('#txtBalTax').val()) > 0) { $('#txtBalTax').val(BalancePlus); } else { $('#txtBalTax').val(BalanceMinus); }
}

function CalEduCess(Rebate) {
   
    var G = "";
    if (G == "")
    {
        G = 0;
    }
    else {
        G = Math.round(parseFloat($('#txtOnNetIncome').val()) - parseFloat($('#txtRabate').val()));;
    }
    //if ($('#txtTaxableIncome').val() <= 500000) { $('#txtRabate').val(5000); G = 5000; }
    

    //Educess = 
    $('#txtEduCess').val(Math.round((G * 2) / 100));
    $('#txtHDCess').val(Math.round((G * 1) / 100));
}

function CalTaxPay() {
    var US89 = 0;
    var TotalLi = 0;
    if ($('#txtUS89').val() != '') { US89 = $('#txtUS89').val(); }
    if ($('#txtTaxLi').val() != '') { TotalLi = $('#txtTaxLi').val(); }
    var Total = parseFloat(TotalLi) - parseFloat(US89);
    if (Total == "NaN") {
        $('#txtNetTaxPay').val() = 0;
    }
    else {
        $('#txtNetTaxPay').val(Math.round(parseFloat(Total)));
    }
    NetBalance();
}
function TotTaxLi() {
    var NetIncome = 0;
    var Rebate = 0;
    var SurCharge = 0;

    if ($('#txtOnNetIncome').val() == '' || $('#txtOnNetIncome').val() == 'NaN') { NetIncome = 0; } else { NetIncome = parseFloat($('#txtOnNetIncome').val()); }

    if ($('#txtRabate').val() == '' || $('#txtRabate').val() == 'NaN') { Rebate = 0; } else { Rebate = parseFloat($('#txtRabate').val()); }

    if ($('#txtSurcharge').val() == '' || $('#txtSurcharge').val() == 'NaN') { SurCharge = 0; } else { SurCharge = parseFloat($('#txtSurcharge').val()); }



    $('#txtTaxLi').val(Math.round(((parseFloat(NetIncome) - parseFloat(Rebate))) + (parseFloat(SurCharge) + parseFloat($('#txtEduCess').val()) + parseFloat($('#txtHDCess').val()))));
    NetBalance();
    CalTaxPay();
}
$(document).ready(function () {
    var Date = $('#txtSalFinYear').val().split('-');
    Date = Date[0];
    $('#txtDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true,
        duration: 'slow',
        minDate: '' + Date + '-03-01',
        maxDate: 'today',
        onClose: function () {
            $(this).focus();
        },
        dateFormat: 'yy-mm-dd'
    });
    $(".allownumericwithdecimal").on("keypress keyup blur", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $(".allownumericwithoutdecimal").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $("#GridViewOther tr:gt(0)").remove();
    $('#btnOtherSource').click(function () {
        load_Data();
        return false;
    });
    $('#btnOtherIncSrc').click(function () {
        Grid_OtherIncomeSource();
        return false;
    });
    $('#btnOtherRefresh').click(function () {
        $("#GridViewOther tr:gt(0)").remove();
        $('#txtOtherSource').val('');
        NetBalance();
        return false;
    });

    $('#btnRefresh').click(function () {
       location.reload();
    });
});

function load_Data() {
    var Total = '';
    if ($('#txtChallanNo').val() == '') { $('#txtChallanNo').focus(); return false; }
    if ($('#txtBSRNo').val() == '') { $('#txtBSRNo').focus(); return false; }
    if ($('#txtDate').val() == '') { $('#txtDate').focus(); return false; }
    if ($('#txtAmount').val() == '') { $('#txtAmount').focus(); return false; }
    // $('#GridViewOther').append("<tr><td>" + $('#txtChallanNo').val() + "</td><td>" + $('#txtBSRNo').val() + "</td><td>" + $('#txtDate').val() + "</td><td style='text-align:right'>" + $('#txtAmount').val() + "</td></tr>");

    $('#GridViewOther').append("<tr><td>" + $('#txtChallanNo').val() + "</td><td>" + $('#txtBSRNo').val() + "</td><td>" + $('#txtDate').val() +
      "</td><td style='text-align:right' >" + $('#txtAmount').val() + "</td><td style='width:50px'> <input id='btnDelete' onclick='deleteRow(this)' type='button' class='Btnclassname' value='Delete'/></td></tr>");

    $("#grdEmployee tr").filter(":even").addClass("GridRowEven");
    $("#grdEmployee tr").filter(":odd").addClass("GridRowOdd");

    if ($('#txtOtherSource').val() == "") { $('#txtOtherSource').val(0); }
    $('#txtOtherSource').val(Math.round(parseFloat($('#txtOtherSource').val()) + parseFloat($('#txtAmount').val())));
    NetBalance();
    $('#txtChallanNo').val('');
    $('#txtBSRNo').val('');
    $('#txtDate').val('');
    $('#txtAmount').val('');
    return false;

}
function Grid_OtherIncomeSource() {
    var Total = '';
    if ($('#txtotherDesc').val() == '') { $('#txtotherDesc').focus(); return false; }
    if ($('#txtotherIncAmount').val() == '') { $('#txtotherIncAmount').focus(); return false; }
    if ($('#txtotherIncAmount').val() <= 0) { $('#txtotherIncAmount').focus(); return false; }

    $('#Grid_OtherIncomeSource').append("<tr class='tbl_OtherIncomeSource'><td class='clsotherDesc'>" + $('#txtotherDesc').val() + "</td><td class='grdOtherSrcInc'>" + $('#txtotherIncAmount').val() +
      "</td><td > <input id='btnDelete' onclick='deleteOtherSrcRow(this)' type='button' class='Btnclassname' value='Delete'/></td></tr>");


    if ($('#txtOtherS').val() == "") { $('#txtOtherS').val(0); }
    $('#txtOtherS').val(Math.round(parseFloat($('#txtOtherS').val()) + parseFloat($('#txtotherIncAmount').val())));

    $('#txtotherDesc').val('');
    $('#txtotherIncAmount').val('');
    return false;

}


function CkeckAmountCESS(Amount, AmountType, Per, lblID) {
    if (AmountType == 'P') { $('#' + lblID + '').text((parseFloat(Amount) * parseFloat(Per)) / 100); }
    else {
        $('#' + lblID + '').text(Math.round(parseFloat(Amount)));
    }
    var total = 0;
    var TDS = 0;
    $('.g2').each(function (evt) {
        if ($(this).text() == '') { $(this).text(0); }
        total = parseFloat($(this).text()) + total;
        alert(total);
        if ($('#txtTDS').val() != '' || $('#txtTDS').val() != 'NaN') { TDS = $('#txtTDS').val(); }
        $('#txtNetTaxPay').val(Math.round(parseFloat($('#txtTDS').val()) - parseFloat(total)));
    });
}

function CkeckAmount(ID, TaxType, MaxAmount, LblID) {
 var   Amount = $(ID).val();
    if (TaxType == 'P') {
        var AmountVal = 0; if (Amount != '') { AmountVal = Amount; }
        var MaxAmountVal = 0; if (MaxAmount != '') { MaxAmountVal = MaxAmount; }
        $('#' + LblID + '').text(Math.round((parseFloat(AmountVal) * parseFloat(MaxAmountVal)) / 100));
        var total = 0;
        $('.chklbl').each(function (evt) {

            if ($(this).text() == '') { $(this).text(0); }
            total = parseFloat($(this).text()) + total;
            $('#txtTotalDeduction').val(total);
        });
    }
    if (TaxType == 'V') {

        if (parseInt(Amount) > parseInt(MaxAmount))
        {
            $('#' + LblID + '').text(MaxAmount);
            return false;
        }
        var AmountVal = 0; if (Amount != '') { AmountVal = Amount; }
       
      
        $('#' + LblID + '').text(AmountVal);
        var total = 0;
        $('.chklbl').each(function (evt) {
            //if ($(this).text() == '') { $(this).text(0); }
            var thisvalue = $(this).text(); if (thisvalue == '') { thisvalue = 0; }
            total = parseFloat($(this).text()) + total;
            $('#txtTotalDeduction').val(total);
        });
    }
    CalNetTaxPay();
}


function pageLoad(sender, args) {

    $(document).ready(function () {


        $('#btnDetails1').click(function () {
            $('.show1').toggle();
            if ($('#btnDetails1').val() == 'Hide Details') { $('#btnDetails1').val('Show Details'); return false; }
            if ($('#btnDetails1').val() == 'Show Details') { $('#btnDetails1').val('Hide Details'); }
        });
        $('#btnDetails2').click(function () {
            $('.show2').toggle();
            if ($('#btnDetails2').val() == 'Hide Details') { $('#btnDetails2').val('Show Details'); return false; }
            if ($('#btnDetails2').val() == 'Show Details') { $('#btnDetails2').val('Hide Details'); }
        });
        $('#btnDetails3').click(function () {
            $('.show3').toggle();
            if ($('#btnDetails3').val() == 'Hide Details') { $('#btnDetails3').val('Show Details'); return false; }
            if ($('#btnDetails3').val() == 'Show Details') { $('#btnDetails3').val('Hide Details'); }
        });
       
    });
}
function Show_80CC_Grid(ID)
{
    checkButton(ID);
    var btnText = $(ID).prop("value");

    if (btnText == 'Hide Details')
    {
        $(ID).prop("value", 'Hide Details');
        var lblTotalAmt = $('#lblTotal1').text();
        if (lblTotalAmt > 0) {
            $('.new80c').show();
        }
        else
            Populate_80CC_Grid(ID);

        return false;
    }
    if (btnText == 'Show Details')
    {
        $(ID).prop("value", 'Show Details');
        var lblTotalAmt = $('#lblTotal1').text();
        if (lblTotalAmt > 0)
        {
            $('.new80c').hide();
        }
        else
            $('.newrow').remove();

        return true;
    }

   
}
function checkButton(ID) {
    var btnText = $(ID).prop("value");
    if (btnText == 'Hide Details') { $(ID).prop("value", 'Show Details'); $('.show4').hide(); return false; }
    if (btnText == 'Show Details') { $(ID).prop("value", 'Hide Details'); $('.show4').show(); return true;  }
}
function Populate_80CC_Grid(ID)
{
    var result = "";
    var F = "{hdnEmpID:'" + $('#hdnEmpID').val() + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/ItaxCalculation/Load_AllSavingType",
        data: F,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;

            var $this = $(ID);
            $parentTR = $this.closest('tr');
            //$parentTR.after(row);

            var amtInput = ''; var SlNo = 1;
            //$(".trclass80").remove();
            if (t.length > 0) {
               
                result += "<tr class='newrow new80c'>"
                         + "<td colspan='5' style='width:100%'>" 
                         + "<table class='Grid' ><tr><th style='border: 1px solid; text-align:center'>SlNo.</th><th style='border: 1px solid; text-align:center'>Saving Type</th><th style='border: 1px solid; text-align:center'>Amount</th></tr>";
                         
                
                $.each(t, function (index, value) {
                    amtInput = "<input type='text' id='txtGAmount' style='text-align:right'  onkeypress='return NumberOnly()' onkeyup='Calcutate(event)' class='atextbox allownumericwithdecimal' value='" + ((value.Amount) == null ? '' : (value.Amount)) + "' />";
                    result += "<tr  class='newrow new80c gv80C80CCD' style='background-color:blanchedalmond'>" +
                             "<td>" + SlNo + "</td>" +
                             "<td style='display:none;' class='cls_gv80C80CCDID'>" + value.ItaxSaveWiseID + "</td>" +
                             "<td>" + value.SubSavingTypeDesc + "</td>" +
                             "<td style='text-align:right'>" + amtInput + "</td>" +
                             "</tr>";
                    SlNo++;
                });
                result += "<tr class='newrow new80c' style='background-color:blanchedalmond'>" +
                        "<td>" + '' + "</td>" +
                        "<td style='display:none;'>" + '' + "</td>" +
                        "<td>" + '' + "</td>" +
                        "<td style='text-align:right'>Total : <label id='lblTotal1' ></label></td>" +
                        "</tr>";
                result += "</table></td></tr>";
                $parentTR.after(result);
            }
        }
    });
 
}

function CalNetTaxPay() {

    if ($('#txtIncome').val() != '') {

        var total = parseFloat($('#txtIncome').val());
        total = total - parseFloat($('#txtTotalPTax').val());
        total = total - parseFloat($('#txtTotHRA').val());
        total = total - parseFloat($('#txtTotIHBL').val());
        total = total + parseFloat($('#txtDetails1').val());
        total = total - parseFloat($('#txtTotalDeduction').val());
        if (total == "NaN") {
            $('#txtTaxableIncome').val(0);
        }
        else {
            $('#txtTaxableIncome').val(total);
        }
        var Amount = "";
        var Gender = "";
        if ($('#txtTaxableIncome').val() == "NaN") { Amount = 0; } else { Amount = $('#txtTaxableIncome').val(); }
        if ($('#txtGender').val() == 'Male') { Gender = 'M'; } else { Gender = 'F'; }
        var Educess = '';

        var W = "{TaxableIncome:'" + Amount + "',FinYear:'" + $('#txtSalFinYear').val() + "',Gender:'" + Gender + "'}";

        $.ajax({
            type: "POST",
            url: "/Administration/Master/ItaxCalculation/TaxLiabilies",
            data: W,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data; 

                var TaxLi = t[0].ITaxLi; 
                if (TaxLi == 'NaN') {
                    $('#txtOnNetIncome').val(0);
                }
                else {  $('#txtOnNetIncome').val(Math.round(TaxLi)); }

                if ($('#txtOnNetIncome').val() != '') {
                    if ($('#txtTaxableIncome').val() <= 500000) {
                       
                        //$('#txtRabate').val(5000);
                        $('#txtRabate').val(0);
                        Educess = Math.round(parseFloat($('#txtOnNetIncome').val()) - parseFloat($('#txtRabate').val()));
                        CalEduCess(Educess);
                    }
                    else {
                       
                        $('#txtRabate').val(0);
                        Educess = Math.round(parseFloat($('#txtOnNetIncome').val()) - parseFloat($('#txtRabate').val()));
                        CalEduCess(Educess);
                    }
                }
                TotTaxLi();
            }
        });
    }
}


function NumberOnly() {
    var AsciiValue = event.keyCode
    if ((AsciiValue >= 48 && AsciiValue <= 57) || (AsciiValue == 8 || AsciiValue == 127))
        event.returnValue = true;
    else
        event.returnValue = false;
}


function Calcutate5() {
    var total = 0;
    $('.cal3').each(function (evt) {
        //this.value = this.value.replace(/[^0-9\.]/g, '');
        var ThisTotal = '';
        if ($(this).val() == '') { ThisTotal = 0; } else { ThisTotal = $(this).val(); }
        total = parseFloat(ThisTotal) + total;
        
        $('#txtDetails1').val(Math.round(total));
    });
    CalNetTaxPay();
    TotTaxLi();
}
function Calcutate(evt) {
    var total = 0;
    $('.atextbox').each(function (evt) {
        var ThisTotal = '';
        if ($(this).val() == '') { ThisTotal = 0; } else { ThisTotal = $(this).val(); }
        total = parseFloat(ThisTotal) + total;
        if (total > 150000) {
            $('#txtHAmount').val(150000);
            total = 150000;
        }
        else {
            $('#txtHAmount').val(Math.round(total));
        }
        TotTaxLi();
    });
    $('#lblchk1').html(Math.round(total));
    $('#lblTotal1').html(Math.round(total));
    var total = 0;
    var ThisVal = 0;
    $('.chklbl').each(function (evt) {
        if ($(this).text() == '') { $(this).text(0); } else {
            ThisVal = $(this).text();
        }
        if (ThisVal > 150000) {
            total = 150000 + total;
        }
        else {
            total = parseFloat(ThisVal) + total;
        }
        $('#txtTotalDeduction').val(Math.round(total));
    });
    CalNetTaxPay();
}

//This is for only NUMERIC VALUE Entry for All Grid TextBox.
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

$(document).ready(function () {


    $("#ddlSector").change(function () {
        if ($("#txtGender").val() != '') {
            location.href = location.href;
        }
    });

    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
    $(".txtpress").keypress(function (event) {
        if (event.keyCode === 13) {
            return false;
            event.preventDefault(); // Ensure it is only this code that rusn
        }
    });
    $(".textbox").keypress(function (event) {
        if (event.keyCode === 13) {
            return false;
            event.preventDefault(); // Ensure it is only this code that rusn
        }
    });
});

//This is add button click event
$(document).ready(function () {
    $('#btnDetails1').click(function () {
        $('.show1').toggle();
        if ($('#btnDetails1').val() == 'Hide Details') { $('#btnDetails1').val('Show Details'); return false; }
        if ($('#btnDetails1').val() == 'Show Details') { $('#btnDetails1').val('Hide Details'); }
    });
    $('#btnDetails2').click(function () {
        $('.show2').toggle();
        if ($('#btnDetails2').val() == 'Hide Details') { $('#btnDetails2').val('Show Details'); return false; }
        if ($('#btnDetails2').val() == 'Show Details') { $('#btnDetails2').val('Hide Details'); }
    });
    $('#btnDetails3').click(function () {
        $('.show3').toggle();
        if ($('#btnDetails3').val() == 'Hide Details') { $('#btnDetails3').val('Show Details'); return false; }
        if ($('#btnDetails3').val() == 'Show Details') { $('#btnDetails3').val('Hide Details'); }
    });

    $("#btnViewTotCalculation").hide();
    $("#cmdAdd").click(function (event) {
        var taxTypeVal = $("#ddlTaxType").val();
        var SavingTypeVal = $("#ddlSavingType").val();
        var SavingAmt = $("#txtSavingAmount").val();

        var taxType = $('#ddlTaxType').find('option:selected').text();
        var SavingType = $('#ddlSavingType').find('option:selected').text();

        var TotalGPF = $("#hdnTotalGPF").val();

        if (taxTypeVal == "")
            $("#ddlTaxType").focus();
        else if (SavingTypeVal == "")
            $("#ddlSavingType").focus();
        else if (SavingAmt == "")
            $("#txtSavingAmount").focus();
        else {
            var F = "{taxTypeVal: " + taxTypeVal + ", taxType: '" + taxType + "', SavingTypeVal: '" + SavingTypeVal + "', SavingType: '" + SavingType + "', SavingAmt: '" + SavingAmt + "', TotalGPF:'" + TotalGPF + "'}";
            //alert(F);
            $.ajax({
                type: "POST",
                url: "ITaxEmployeeDeductionDetails.aspx/AddDetails",
                data: F,
                contentType: "application/json; charset=utf-8",
                success: function (D) {

                    $("#ITaxEmployeeDetail").html(D.d);
                    $("#btnViewTotCalculation").show();
                    $("#ddlTaxType").val('');
                    $("#ddlSavingType").val('');
                    $("#txtSavingAmount").val('');

                    ShowTotalSumofITaxType(taxTypeVal, taxType, SavingTypeVal, SavingType, SavingAmt);

                }
            });
        }
    });
});
function ShowTotalSumofITaxType(taxTypeVal, taxType, SavingTypeVal, SavingType, SavingAmt) {
    var ItaxMaxAmount = $("#hdnITaxTypeMaxAmount").val();
    var TotalGPF = $("#hdnTotalGPF").val();
    var MaleFemale = $("#hdnEmpGender").val();
    var SalYear = $("#txtAssessmentYear").val();


    var F = "{taxTypeVal: " + taxTypeVal + ", taxType: '" + taxType + "', SavingTypeVal: '" + SavingTypeVal + "', SavingType: '" + SavingType + "', SavingAmt: '" + SavingAmt + "', ItaxMaxAmount:'" + ItaxMaxAmount + "', TotalGPF:'" + TotalGPF + "', MaleFemale:'" + MaleFemale + "', SalYear:'" + SalYear + "'}";
    $.ajax({
        type: "POST",
        url: "ITaxEmployeeDeductionDetails.aspx/ItaxTypewiseTotal",
        data: F,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var strmsg = jQuery.parseJSON(D.d); //alert(strmsg);
            $("#Itax").html(strmsg);
        }
    });
}


//=========This is for add section on button click========//
$(document).ready(function () {
    $('#btnSectionAdd').click(function () {
        var Gender = "";
        var Type = "";
        if (document.getElementById("rbValue").checked == false && document.getElementById("rbPercent").checked == false) {
            alert("Please Select Type !");
            document.getElementById("rbValue").focus();
            return false;
        }
        if (document.getElementById("rbValue").checked == true) { Type = 'V' } else { Type = 'P'; }
        if (Type == 'P') {
            if (parseInt($('#txtMaxAmount').val()) > 100) {
                alert("Percent should not be greater than 100 !");
                $('#txtMaxAmount').val(100);
                return false;
            }
        }
        if ($('#txtGender').val() == 'Male') { Gender = 'M'; } else { Gender = 'F'; }
        if ($('#txtNewSection').val() == '') { alert("Please Enter Section!"); $('#txtNewSection').focus(); return false; } else {
            if ($('#txtMaxAmount').val() == '') { alert("Please Enter Max Amount!"); $('#txtMaxAmount').focus(); return false; } else {
                $.ajax({
                    type: "POST",
                    url: "/Administration/Master/ItaxCalculation/AddNewSection",
                    data: "{Section:'" + $('#txtNewSection').val() + "', MaxAmount:" + $('#txtMaxAmount').val() + ", FinYear:'" + $('#txtSalFinYear').val() + "',Gender:'" + Gender + "',hdnEmpID:'" + $('#hdnEmpID').val() + "',Type:'" + Type + "'}",
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {

                        var t = D.Data;
                        if (t.length > 0)
                        {
                            var ID = 'lblchk' + t[0].SrNo; 
                            $('#GridTaxDetails').append(
                                "<tr style='width:30px' class='tblTaxDetails'><td>" + "<label id='lblSerial' runat='server'>" + t[0].SrNo + "</label>" +
                                //"</td><td style='display:none'>" + "<input runat='server' type='Text' id='txtCheck" + t[0].SrNo + "'  value= '0'></input>" +
                                "</td><td style='display:none' class='clsTaxDetailID'>" + "<input runat='server' type='Text' id='lblId'  value= " + t[0].ItaxSavingTypeID + "></input>" +
                                "</td><td align='left'>" + "<label id='lblDesc' runat='server'>" + t[0].TypeDesc + "</label>" +
                                "</td><td align='left'>" + "<label id='lblDesc' runat='server'>" + '' + "</label>" +
                                "</td><td style='display:none'>" + "<label id='lableMaxType' runat='server'>" + t[0].MaxType + "</label>" +
                                "</td><td style='display:none'>" + "<label id='lableMaxAmt' >" + t[0].MaxAmount + "</label>" +
                                "</td><td align='center' >" + "<label style='text-align:left; width:200px;' class='chklbl' id='" + ID + "' >" + t[0].lblAmount + "</label><input Style='text-align: right' runat='server' onkeypress='return NumberOnly()' Class='textbox allownumericwithdecimal txtHAmountChk" + t[0].SrNo +
                                "' MaxLength='10' type='Text' id='txtHAmount' onkeyup=CkeckAmount(this,'" + t[0].MaxType + "',\'" + t[0].MaxAmount + "',\'" + ID + "') value=''></input>" + "</td></tr>");// " + $(BindGrid).find('Amount').text() + "
                            $('#GridTaxDetails').addClass('Grid1');
                            document.getElementById('rbValue').checked = true;
                            $('#txtNewSection').val('');
                            $('#txtMaxAmount').val('');

                            //isAmount = "<input type='text' id='txtHAmount' style='text-align:right' onkeyup='CkeckAmount(this, \"" + value.MaxType + "\" , \"" + value.MaxAmount + "\" , \"" + 'lblchk' + SlNo + "\")' onkeypress='return NumberOnly()' class='allownumericwithdecimal' value='" + ((value.Amount) == null ? '' : (value.Amount)) + "' />";
                        }
                    }
                });
            }
        }

    });
});




//===================================================== -- Save Data -- =========================================================//
var Table = [];
var Table2 = [];
var Table3 = [];
var Table4 = [];
$(document).ready(function () {
    $("#btnSave").click(function (e) {

        e.preventDefault();
        if ($('#ddlSector').val() == 0) {
            alert("Please select Sector...!");
            $('#ddlSector').focus();
            return false;
        }
        if ($('#hdnEmpID').val() == '') {
            alert("Please Enter or Select Employee Number...!");
            $('#txtEmpNo').focus();
            return false;
        }
       
        var tbl_Grid_OtherIncomeSource = $("#Grid_OtherIncomeSource");   //inside SlNo. 5
        var tblParent = $("#GridTaxDetails");                            //inside SlNo. 6 main grid on button click
                                 
        
        var tblChallan = $("#GridViewOther");

        Table3 = [];
        var rowCount = tblChallan.find("tr").length;
        var i = 0;
        $.each(tblChallan.find("tr"), function (index, value) {
            if (index != 0) {
                var ChallanNo = $(this).find("td").eq(0).html();
                var BSRNo = $(this).find("td").eq(1).html();
                var ChallanDate = $(this).find("td").eq(2).html();
                var Amount = $(this).find("td").eq(3).html();

                Table3.push({ 'ChallanNo': ChallanNo, 'BSRNo': BSRNo, 'ChallanDate': ChallanDate, 'Amount': Amount });
                i++;
            }
        });

        getJsonObjectFromTable();
        getJsonObjectFromTable3(tbl_Grid_OtherIncomeSource);
        getJsonObjectFromTable2(tblParent);
        
    });
});






function getJsonObjectFromTable() {
    Table = [];    //inside SlNo. 6 main grid first row button on button click
    $(".gv80C80CCD").each(function () {
        var ChildID = $(this).find(".cls_gv80C80CCDID").text(); 
        var ChildAmt = $(this).find("input[id*=txtGAmount]").val(); 
        Table.push({ 'ChildID': ChildID, 'ChildAmt': ChildAmt });
    });
}

function getJsonObjectFromTable2(table2) {
    
    var Object = {};
    Table2 = [];
    $.each(table2.find(".tblTaxDetails"), function (index, value) {
        var ParentID = $(this).find(".clsTaxDetailID").text(); 
        var ParentAmt = $(this).find("input[id*=txtHAmount]").val(); 
        Table2.push({ 'ParentID': ParentID, 'ParentAmt': ParentAmt });
    });
    

    var hdnEmpID = $('#hdnEmpID').val();
    var HiddenCheckGrid = $('#HiddenCheckGrid').val();
    var hdnEmpGender = $('#hdnEmpGender').val();
    var hdnTotalGrossPay = $('#hdnTotalGrossPay').val();
    var hdnTotalPtax = $('#hdnTotalPtax').val();
    var hdnTotalITax = $('#hdnTotalITax').val();
    var hdnTotalGIS = $('#hdnTotalGIS').val();
    var hdnTotalGPF = $('#hdnTotalGPF').val();
    var hdnTotalHRA = $('#hdnTotalHRA').val();
    var hdnTotalGPFRec = $('#hdnTotalGPFRec').val();
    var hdnTotalIHBL = $('#hdnTotalIHBL').val();
    var hdnTotalHBL = $('#hdnTotalHBL').val();
    var hdnTotalOtherDe = $('#hdnTotalOtherDe').val();
    var hdnTotalNetPay = $('#hdnTotalNetPay').val();
    var hdnTotalArrearOT = $('#hdnTotalArrearOT').val();


    var EmpID = $('#hdnEmpID').val();
    var FinYear = $('#txtAccFinYear').val();
    var AssessmentYear = $('#txtAssessmentYear').val();
    var IncomeFromSalary = ''; if ($('#txtIncome').val() == '') { IncomeFromSalary = 0; } else { IncomeFromSalary = $('#txtIncome').val(); }
    var InterestFromSaving = ''; if ($('#txtInterestFromSaving').val() == '') { InterestFromSaving = 0; } else { InterestFromSaving = $('#txtInterestFromSaving').val(); }
    var InterestFromFD = ''; if ($('#txtInterestFromFD').val() == '') { InterestFromFD = 0; } else { InterestFromFD = $('#txtInterestFromFD').val(); }
    var InterestFromOther = ''; if ($('#txtOtherS').val() == '') { InterestFromOther = 0; } else { InterestFromOther = $('#txtOtherS').val(); }
    var TotDeduChapVIA = ''; if ($('#txtTotalDeduction').val() == '') { TotDeduChapVIA = 0; } else { TotDeduChapVIA = $('#txtTotalDeduction').val(); }
    var TotTaxableIncome = ''; if ($('#txtTaxableIncome').val() == '') { TotTaxableIncome = 0; } else { TotTaxableIncome = $('#txtTaxableIncome').val(); }
    var TaxOnNetIncome = ''; if ($('#txtOnNetIncome').val() == '') { TaxOnNetIncome = 0; } else { TaxOnNetIncome = $('#txtOnNetIncome').val(); }
    var RabateSec87A = ''; if ($('#txtRabate').val() == '') { RabateSec87A = 0; } else { RabateSec87A = $('#txtRabate').val(); }
    var Surcharge = ''; if ($('#txtSurcharge').val() == '') { Surcharge = 0; } else { Surcharge = $('#txtSurcharge').val(); }
    var EducationCess = ''; if ($('#txtEduCess').val() == '') { EducationCess = 0; } else { EducationCess = $('#txtEduCess').val(); }
    var HSEducationCess = ''; if ($('#txtHDCess').val() == '') { HSEducationCess = 0; } else { HSEducationCess = $('#txtHDCess').val(); }
    var TaxLiabilities = ''; if ($('#txtTaxLi').val() == '') { TaxLiabilities = 0; } else { TaxLiabilities = $('#txtTaxLi').val(); }
    var ReliefUS89 = ''; if ($('#txtUS89').val() == '') { ReliefUS89 = 0; } else { ReliefUS89 = $('#txtUS89').val(); }
    var TaxPayabl = ''; if ($('#txtNetTaxPay').val() == '') { TaxPayabl = 0; } else { TaxPayabl = $('#txtNetTaxPay').val(); }
    var OtherITAX = ''; if ($('#txtOtherSource').val() == '') { OtherITAX = 0; } else { OtherITAX = $('#txtOtherSource').val(); }
    var BalanceTax = ''; if ($('#txtBalTax').val() == '') { BalanceTax = 0; } else { BalanceTax = $('#txtBalTax').val(); }

    //var TableChallan = JSON.stringify(Table3); 
    //var TableChild = JSON.stringify(Table); 
    //var TableParent = JSON.stringify(Table2); 
    //var TableOtherIncome = JSON.stringify(Table4); 

    var obj = {
        TableChild: Table, TableParents: Table2, TableChallan: Table3, TableOtherIncome: Table4, FinYear: FinYear, AssessmentYear: AssessmentYear,
        EmpID: EmpID , hdnEmpGender: hdnEmpGender , hdnTotalGrossPay:  hdnTotalGrossPay , hdnTotalPtax: hdnTotalPtax , hdnTotalITax: hdnTotalITax , hdnTotalGIS: hdnTotalGIS,
        hdnTotalHRA: hdnTotalHRA , hdnTotalGPF: hdnTotalGPF , hdnTotalGPFRec: hdnTotalGPFRec, hdnTotalHBL: hdnTotalHBL , hdnTotalIHBL: hdnTotalIHBL , hdnTotalHBL: hdnTotalHBL,
        hdnTotalOtherDe: hdnTotalOtherDe , hdnTotalNetPay: hdnTotalNetPay , hdnTotalArrearOT: hdnTotalArrearOT , IncomeFromSalary:  IncomeFromSalary , InterestFromSaving: InterestFromSaving,
        IncomeFromSalary: IncomeFromSalary , InterestFromFD: InterestFromFD , InterestFromOther: InterestFromOther , TotDeduChapVIA: TotDeduChapVIA , TotDeduChapVIA:  TotDeduChapVIA,
        TotTaxableIncome: TotTaxableIncome , TaxOnNetIncome: TaxOnNetIncome , RabateSec87A: RabateSec87A , Surcharge: Surcharge, EducationCess: EducationCess , HSEducationCess: HSEducationCess,
        TaxLiabilities: TaxLiabilities , ReliefUS89: ReliefUS89 , TaxPayabl: TaxPayabl , OtherITAX: OtherITAX , BalanceTax: BalanceTax };
   
    var W = JSON.stringify(obj);

    $.ajax({
        type: "POST",
        url: "/Administration/Master/ItaxCalculation/SaveData",
        data: W,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (d) {
            Table = [];
            Table2 = [];
            Table3 = [];
            Table4 = [];
            $("#GridViewOther tr:gt(0)").remove();
            $('#txtOtherSource').val('');
            alert(d.Data);
            location.reload();
        },
        failure: function (response) {
            alert("Wrong");
        }
    });
}

function getJsonObjectFromTable3(table3) {

    Table4 = [];
    $.each(table3.find(".tbl_OtherIncomeSource"), function (index, value) {
        
        var Desc = $(this).find(".clsotherDesc").text();
        var DescAmt = $(this).find(".grdOtherSrcInc").text();
        Table4.push({ 'Description': Desc, 'Amount': DescAmt });
    });
}