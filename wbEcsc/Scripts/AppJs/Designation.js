﻿
$(function () {


});

function SearchTable() {
    var desPrefix = $('#txtDesignation').val().trim().toUpperCase();
    var tbl = $('.clstbl').find('.clsrow');
    $.each(tbl, function (index, value) {
        var desig = $(this).find('.clsDesignation').text().toUpperCase();
        if (desig.indexOf(desPrefix) > -1) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}