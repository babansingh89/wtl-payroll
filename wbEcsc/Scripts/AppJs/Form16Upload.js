﻿$(document).ready(function () {

    $('#btnsearch').click(Validation);

});

function Validation()
{
    var finyear = $('#txForm16tFinYear').val();
    if (finyear == "") { $('#txForm16tFinYear').focus(); return false; }
    var res = FinYear_Validation(finyear);
    if (res == true)
        LoadData();
    else
        alert('Please enter correct finyear.');
}

function LoadData() {
   
    var E = "{FinYear:'" + $.trim($("#txForm16tFinYear").val()) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/Form16Upload/Load",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            if (D.Status == 200) {

                $("#tbl tr.myData").empty(); var i = 0, color='';
                $.each(D.Data, function (index, value) {
                    i = parseInt(i) + 1;

                    if (value.FilePath != null && value.FilePath != "null")
                        color = "#0dda1d3b";
                    else
                        color = "";

                    $('#tbl').append("<tr class='myData' style='background-color:" + color + "'>"+
                        "<td>" + i + "</td>" +
                        "<td style='display:none;' class='clsEmpID'>" + value.EmployeeID + "</td>" +
                        "<td style='display:none;' class='clsFilePath'>" + value.FilePath + "</td>" +
                        "<td style='text-align:center;' class='clsEmpNo'>" + value.EmpNo + "</td>"  +        
                        "<td>" + value.EmpName + "</td>" +               
                        "<td style='text-align:center;'><div style='float:left; width:80%'><input type='file' class='form-control flUpload' accept='.pdf'  multiple id='flPic_" + i + "'></div><div style='float:right; width:20%'>   <button type='button'  class='btn btn-primary' onclick='Photo(this)'><span class='glyphicon glyphicon-save-file' aria-hidden='true' style='padding-right:4px'></span>Upload</button></div></td>" +
                        "<td onclick='ViewFile(this)' style='text-align:center; pointer-events:" + ((value.FilePath == null || value.FilePath == "null") ? "none" : "") + "'><span style='text-align:center; cursor:pointer; color:blue; display:" + ((value.FilePath == null || value.FilePath == "null") ? "none" : "") + "'>View </span></td>"+
                        "</tr > "
                        //style='text-align:center; cursor:pointer; color:blue; pointer-events:" + ((value.FilePath == null || value.FilePath == "null")  ? "none" : "") + "'
                    );
                });
            }
            else {
                alert('No data found');
            }
        }
    });

}

function Photo(ID) {

    var fileExtension = ['pdf', 'PDF'];

    var id = $(ID).closest('tr').find('.flUpload').attr('id');
    var empno = $(ID).closest('tr').find('.clsEmpNo').text();
    var empid = $(ID).closest('tr').find('.clsEmpID').text();
    var filename = $('#' + id).val();

    if (filename.length == 0) {
        alert("Please Select .pdf file.");
        $('#'+ id).val("");
        return false;
    }

    var extension = filename.replace(/^.*\./, '');
    if ($.inArray(extension, fileExtension) == -1) {
        alert("Please select only (.pdf) files.");
        $('#'+id).val("");
        return false;
    }
    Upload(id, empid, empno);
}

function Upload(ID, empid, empno) {
    var formData = new FormData();
    var totalFiles = document.getElementById(ID).files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById(ID).files[i];
        var fileName = file.name; //console.log(fileName);
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.')); 
        formData.append(ID, file, $('#txForm16tFinYear').val() + "#" + fileNameExt + "#" + empid + "#" + empno);
    }
   
    $.ajax({
        type: "POST",
        url: "/Administration/Master/Form16Upload/UploadPicture",
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            var t = response.Data;

            if (t == 0) {
                alert('Sorry ! There is some proble to upload file.'); return false;
            }
            else if (t == 1) {

                alert('File Uploaded Successfully !'); LoadData(); return false;
            }
            else {
                fileName = t;

                //Checking(t);
            }
        }
    });
}

function ViewFile(ID)
{
    var filepath = $(ID).closest('tr').find('.clsFilePath').text();
    var url = 'http://wtlhr.co.in/' + filepath;

        $('#imgview').attr('src', url);
        $('#dialog').modal('show');
    
}

function FinYear_Validation(year) {

    if (year != "") {

        var str1 = $.trim(year);
        var str2 = "-";
        var a1 = ""; var a2 = "";
        //Special Character Checking
        if (str1.indexOf(str2) == -1) {
            return false;
        }
        else {
            var a = year.split('-');
            a1 = a[0];
            a2 = a[1];
            //Length Checking
            if ($.trim(a1).length != 4)
                return false;
            if ($.trim(a2).length != 4)
                return false;

            //isNumeric Checking
            var s1 = $.isNumeric(a1);
            var s2 = $.isNumeric(a2);
            if (s1 == false)
                return false;
            if (s2 == false)
                return false;

            //Max and Min Year Checking
            var g = parseInt(a1) + 1;
            if (g != a2)
                return false;
        }

        return true;
    }
    else
        return false;

}