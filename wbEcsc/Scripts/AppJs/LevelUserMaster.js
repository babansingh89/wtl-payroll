﻿$(document).ready(function () {
    FieldNameData(); 

    $(document).on('click', '#chkActiveFlag', function () {
        if ($("#chkActiveFlag").prop('checked') == true) {
            $('#lblActiveInActive').text('Active');
            $('#lblActiveInActive').css({ 'color': 'green', 'font-weight': 'bold' });
        }
        else {
            $('#lblActiveInActive').text('InActive');
            $('#lblActiveInActive').css({ 'color': 'red', 'font-weight': 'bold' });
        }
    })

    $('#btnSave').click(Save);

    $("#txtLevelName").autocomplete({
        source: function (request, response) {

            var E = "{FormName: '" + $.trim($("#txtLevelName").val()) + "', TransactionType:'" + 'LIST' + "'}";
            $.ajax({
                type: "POST",
                url: "/Administration/Master/LevelUserMaster/Get_LevelName",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.FormName,
                                ConfID: item.ConfID
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $("#hdnLevelID").val(i.item.ConfID); //alert($("#hdnEmpNo").val());
        },
        minLength: 0
    }).click(function () {
        $("#txtLevelName").val(''); $("#hdnLevelID").val('');
        $(this).autocomplete('search', ($(this).val()));
    });

    $("#txtLevelNameSearch").autocomplete({
        source: function (request, response) {

            var E = "{FormName: '" + $.trim($("#txtLevelNameSearch").val()) + "', TransactionType:'" + 'SEARCH' + "'}";
            $.ajax({
                type: "POST",
                url: "/Administration/Master/LevelUserMaster/Get_LevelName",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.FormName,
                                ConfID: item.ConfID
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $("#hdnLevelSearchD").val(i.item.ConfID);

            PopulateGrid($.trim($("#hdnLevelSearchD").val()));
        },
        minLength: 0
    }).click(function () {
        $("#txtLevelNameSearch").val(''); $("#hdnLevelSearchD").val('');
        $(this).autocomplete('search', ($(this).val()));
    });

    $("#txtUserName").autocomplete({
        source: function (request, response) {

            var E = "{LevelID: '" + $.trim($("#hdnLevelID").val()) + "', UserName: '" + $.trim($("#txtUserName").val()) + "'}";
            $.ajax({
                type: "POST",
                url: "/Administration/Master/LevelUserMaster/Get_UserName",
                data: E,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.UserName,
                                UserID: item.UserID
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $("#hdnUserID").val(i.item.UserID); //alert($("#hdnEmpNo").val());
        },
        minLength: 0
    }).click(function () {
        $("#txtUserName").val(''); $("#hdnUserID").val('');
        $(this).autocomplete('search', ($(this).val()));
    });

    function FieldNameData() {
        $.ajax({
            type: "POST",
            url: "/Administration/Master/LevelUserMaster/Get_FieldName",
            data: "",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0) {
                    $.each(D.Data, function (index, value) {
                        var MandatoryStyle = '*';
                        var Id = '#' + value.FieldID;
                        $(Id).text(value.FieldName);
                        $('#spn' + value.FieldID).text(value.IsMand == 1 ? '   ' + MandatoryStyle : '');
                    });
                }
            }
        });
    }

    function PopulateGrid(ConfID) {
        var _data = "{ConfID:'" + ConfID + "', LevelSubID:'" + $.trim($('#hdnLevelSubID').val()) + "'}";// alert(_data);
        $.ajax({
            type: "POST",
            url: "/Administration/Master/LevelUserMaster/GridLevel",
            data: _data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (D) {
                if (D.Status == 200) {
                    if (D.Data != null) {
                        var xmlDoc = $.parseXML(D.Data);
                        var xml = $(xmlDoc);
                        $("#tbl tr.trclass").empty();
                        $(xml).find("Table").each(function () {
                            $("#tbl").append("<tr class='trclass'><td style='display:none;'>" +
                                $(this).find("LevelSubID").text() + "</td><td style='display:none;'>" +
                                $(this).find("LevelID").text() + "</td><td style='display:none;'>" +
                                $(this).find("LevelUser").text() + "</td><td>" +
                                $(this).find("FormName").text() + "</td><td>" +
                                $(this).find("UserName").text() + "</td><td style='text-align:center'>" +
                                $(this).find("LevelRank").text() + "</td><td style='text-align:center'>" +
                                $(this).find("IsActive").text() + "</td><td style='text-align:center'>" +
                                '<a style="color:blue; cursor:pointer" onclick="Edit(this)">Edit</a>' + "</td></tr>");
                        });
                    }
                    else {
                        alert("No Record found !");
                    }
                }
                else {
                    alert(D.Message);
                }
            },
            error: function (D) {
                alert(D.Message);
            }
        });
    }

    function Save() {
        var status = $('#hdnLevelSubID').val() == "" ? "Save" : "Update";
        var IsActive = $("#chkActiveFlag").prop('checked') == true ? 1 : 0;

        var conf = confirm("Are you sure want to " + status + "?");
        if (conf) {
            var ActiveFlag = document.getElementById('chkActiveFlag').checked == true ? 1 : 0;
            // LevelID,  LevelSubID,  LevelUser,  LevelRank,  IsActive
            var _data = "{LevelID:'" + $('#hdnLevelID').val() + "', LevelSubID:'" + $('#hdnLevelSubID').val() + "', LevelUser:'" + $('#hdnUserID').val() +
                    "', LevelRank:'" + $('#txtRank').val() + "', IsActive:'" + IsActive + "'}"

            $.ajax({
                type: "POST",
                url: "/Administration/Master/LevelUserMaster/SaveLevel",
                data: _data,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    if (D.Status == 200) {
                        if (D.Data != null) {
                            var xmlDoc = $.parseXML(D.Data);
                            var xml = $(xmlDoc);
                            var Table = xml.find("Table");
                            if ($(Table).find("ErrorCode").text() == 1) {
                                alert($(Table).find("Messege").text());
                                ResetData();
                                PopulateGrid($("#hdnLevelSearchD").val());
                            }
                            else {
                                alert($(Table).find("Messege").text());
                                $('.txt' + $(Table).find("FId").text()).focus();
                            }
                        }
                        else {
                            alert("No Record found !");
                        }
                    }
                    else {
                        alert(D.Message);
                    }
                },
                error: function (D) {
                    alert(D.Message);
                }
            });
        }
    }

    function ResetData() {
        $('#hdnLevelSubID').val('');
        $('#txtLevelName').val(''); $('#hdnLevelID').val('');
        $('#txtUserName').val(''); $('#hdnUserID').val('');
        $('#txtRank').val('');
        document.getElementById('chkActiveFlag').checked = true;
        $('#lblActiveInActive').text('Active');
        $('#lblActiveInActive').css({ 'color': 'green', 'font-weight': 'bold' });
        $('#btnSave').html('Save');
    }
});

function Edit(t) {
    
    $('#hdnLevelSubID').val($.trim($(t).closest('tr').find('td:eq(0)').text()));
    $('#txtLevelName').val($.trim($(t).closest('tr').find('td:eq(3)').text())); $('#hdnLevelID').val($.trim($(t).closest('tr').find('td:eq(1)').text()));
    $('#txtUserName').val($.trim($(t).closest('tr').find('td:eq(4)').text())); $('#hdnUserID').val($.trim($(t).closest('tr').find('td:eq(2)').text()));
    $('#txtRank').val($.trim($(t).closest('tr').find('td:eq(5)').text()));

    if ($.trim($(t).closest('tr').find('td:eq(6)').text()) == 'Active') {
        document.getElementById('chkActiveFlag').checked = true;
        $('#lblActiveInActive').text('Active');
        $('#lblActiveInActive').css({ 'color': 'green', 'font-weight': 'bold' });
    }
    else {
        document.getElementById('chkActiveFlag').checked = false;
        $('#lblActiveInActive').text('InActive');
        $('#lblActiveInActive').css({ 'color': 'red', 'font-weight': 'bold' });
    }
    $('#btnSave').html('Update');
}