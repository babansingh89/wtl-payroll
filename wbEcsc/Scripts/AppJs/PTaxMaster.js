﻿$(document).ready(function () {
    FieldNameData();
    Populate_Grid();
    $('#btnRefresh').click(ClearAllFields);
    $('#btnSave').click(Save);
});

function ClearAllFields() {
    $('#hdnPTaxID').val(''); $('#txtSalaryFrom').val(''); $('#txtSalaryTo').val(''); $('#txtPTax').val('');
    $('#btnSave').html('Save'); Populate_Grid(); FieldNameData();
}

function FieldNameData() {
    $.ajax({
        type: "POST",
        url: "/Administration/Master/PTaxMaster/Get_FieldName",
        data: "",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data;
            if (t.length > 0) {
                $.each(D.Data, function (index, value) {
                    var MandatoryStyle = '*';
                    var Id = '#' + value.FieldID;
                    $(Id).text(value.FieldName);
                    $('#spn' + value.FieldID).text(value.IsMand == 1 ? '   ' + MandatoryStyle : '');
                });
            }
        }
    });
}

function Populate_Grid()
{
    var E = "{hdnPTaxID:'" + $.trim($('#hdnPTaxID').val()) + "'}";
    $.ajax({
        type: "POST",
        url: "/Administration/Master/PTaxMaster/Bind_Grid",
        data: E,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var xmlDoc = $.parseXML(D.Data);
            var xml = $(xmlDoc);

            var t = D.Data;
            if (t.length > 0) {
                $("#tbl tr.trclass").empty();
                $(xml).find("Table").each(function () {
                    if ($(this).find("PTaxID").text() != null) {
                        $("#tbl").append("<tr class='trclass'><td style='text-align:center'>" +
                            $(this).find("SalFrom").text() + "</td><td style='text-align:center'>" +
                            $(this).find("SalTo").text() + "</td><td style='text-align:center'>" +
                            $(this).find("PTax").text() + "</td><td onclick='EditPTax(this)' style='text-align:center; cursor:pointer; color:blue'>" +
                            $(this).find("Edit").text() + "</td><td onclick='DeletePTax(this)' style='text-align:center; cursor:pointer; color:blue'>" +
                            $(this).find("Del").text() + "</td><td style='display:none;'>" +
                            $(this).find("PTaxID").text() + "</td></tr>");
                    }
                });
            }
        }
    });
}

function EditPTax(t)
{
    $('#txtSalaryFrom').val($(t).closest('tr').find('td:eq(0)').text().trim());
    $('#txtSalaryTo').val($(t).closest('tr').find('td:eq(1)').text().trim());
    $('#txtPTax').val($(t).closest('tr').find('td:eq(2)').text().trim());
    $('#hdnPTaxID').val($(t).closest('tr').find('td:eq(5)').text().trim());
    $('#btnSave').html('Update');
}

function DeletePTax(t) {
    var conf = confirm("Are sure want to delete this PTax ?");
    if (conf == true) {
        var S = "{PTaxID:'" + $(t).closest('tr').find('td:eq(5)').text() + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/PTaxMaster/Delete_PTax",
            data: S,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var xmlDoc = $.parseXML(D.Data);
                var xml = $(xmlDoc);
                var Table = xml.find("Table");
                if (Table.length > 0) {
                    if ($(Table).find("ErrorCode").text() == 1) {
                        alert($(Table).find("Messege").text());
                        ClearAllFields();
                    }
                    else {
                        alert($(Table).find("Messege").text());
                    }
                }
            }
        });
    }
}

function Save() {
    var SaveUpdate = "";
    if ($('#hdnPTaxID').val() == '') { SaveUpdate = "Save"; } else { SaveUpdate = "Update"; }
    var conf = confirm("Are sure want to " + SaveUpdate + " this PTax ?");
    if (conf == true) {
        var S = "{SecID:'" + $.trim($('#ddlSector').val()) + "', hdnPTaxID:'" + $.trim($('#hdnPTaxID').val()) +
            "', SalFrom:'" + $.trim($('#txtSalaryFrom').val()) + "', SalTo:'" + $.trim($('#txtSalaryTo').val()) +
            "', PTax:'" + $.trim($('#txtPTax').val()) + "'}";
        $.ajax({
            type: "POST",
            url: "/Administration/Master/PTaxMaster/Save_PTax",
            data: S,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var xmlDoc = $.parseXML(D.Data);
                var xml = $(xmlDoc);
                var Table = xml.find("Table");
                if (Table.length > 0) {
                    if ($(Table).find("ErrorCode").text() == 1) {
                        alert($(Table).find("Messege").text());
                        ClearAllFields();
                    }
                    else {
                        alert($(Table).find("Messege").text());
                    }
                }
            }
        });
    }
}