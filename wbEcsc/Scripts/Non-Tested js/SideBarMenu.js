﻿/**
* A Brif History: Now new theme is about to ready. Now, we need to develop dynamic menu. On first iteration we will develop a besic menu
* which can be expandable to tree layer, level 1(two layered tree). I have some dummy datasource for hiarachey making, I will use that
datasource found from 'Valueation Board' datasource. But the Data Schema of that datasource had a capability of multilevel hairarchey.

Now,    the questions!-
        1. how to create level 1 expandable menu.
        2. How can I plug it to my client side sysmtem(js). that will be very easy to readable and maintailnable.
        3. Upgrade to service method.
        4. Send it to /AppJs folder.

Follow me:
        1. Add a global object to APP_GLOBAL named - Menu/SideBarMenu
        

*/
/***
Data Source format: {
                              text: nodeText,
                              id: nodeID,
                              nodes: childrens,
                              hasChildren: childrens.length > 0,
                              isExpanded: isExpanded,
                              data: additionalData,
                              parentID: parentID
                     };
*/


$(document).ready(function () {
   
    APP_GLOBAL.Menu = {};
    var menu = APP_GLOBAL.Menu;
    menu.SideBarMenu = function () {
       
        var _parent = this;
        var _dataSource = null;
        this.inititalize = function (signedDataSource) {
            _dataSource = signedDataSource;
            var _loopCount = 0;
            var CreateTreeDom = function (data) {
                var parentMrkup_jq = $("<ul class='sidebar-nav nav-pills nav-stacked' id='menu'></ul>");
                $.each(data, function (index, value) {
                    _loopCount++;

                    var linkNode = "<a style='cursor:pointer'  class='leaf-node-link " + value.text+"-"+value.id + "' " + (value.url == "///" ? "" : (" href='" + value.url + "' ")) + ">" + value.text + "</a>";
                    if (value.hasChildren === false) {//no child
                        parentMrkup_jq.append("<li class='active'>" + linkNode + "</li>");
                    } else {
                        parentMrkup_jq.append($("<li class='active'></li>").append(CreateTreeDom(value.nodes)).prepend("<a class='node-header-text' href='#node" + value.id + "' data-toggle='collapse' aria-expanded='false'>" + value.text + "</a>")).addClass("collapse");
                        CreateTreeDom(value.nodes);
                    }
                    
                });
               
                return parentMrkup_jq;
            };
            console.log(_dataSource);
            var x = CreateTreeDom(_dataSource.nodes);
            var headerNodes = x.find('.node-header-text');

            //assing id to nearest ul of tergate '.node-header-text' [<a>]
            //add .collapse class to tergate <ul> and remove .component class from tergate <ul>
            $.each(headerNodes, function (index, value) {
                var uls = $(value).siblings('ul').first();
                var tergateId = $(value).attr("href").replace("#","");
                //uls.attr("id", tergateId).addClass("collapse").removeClass("components");
                uls.attr("id", tergateId).addClass("collapse").removeClass("components");
            });
            //remove .collapse class from parent ul of tree
            x.removeClass('collapse');
            $("#tree").append($('<div class="tree-view tree-menu"></div>').append(x));
            //append code
           
                //$.ajax({
                //    type: "POST",
                //    url: "/Administration/AdminHome/GetApplicationApprovalUrl",
                //    data: "",
                //    contentType: "application/json; charset=utf-8",
                //    dataType: "json",
                //    success: function (response) {
                //        //console.log(response);
                //        console.log(response);
                //        if (response.Status === 200) {
                //            $('#tree').find('.Approval-3').attr('href', response.Data.Url).attr("target","_blank");
                //        } else {
                //            alert("Error on Getting Approval Link: "+response.Message);
                //        }
                //    },
                //    failure: function (response) {
                //        alert(response);
                //    }
                //});
            
        };
        this.show = function () {
            throw 'not implemented exception';
        };
        this.hide = function () {
            throw 'not implemented exception';
        };
        this.reload = function () {
            throw 'not implemented exception';
        };

    };

   
    //Execute:-
    $.ajax({
        type: "POST",
        url: "/Administration/MenuManager/GetMenus",
        //url: "/LoginManager/AdministrativeLogin",
        data: null,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log(response);
            if (response.Status === 200)
            {
                //var signedDatasource = JSON.parse('[{"MenuID":1,"MenuName":"Collection","ParentMenuID":null,"ActionMethodID":null,"Rank":1,"Url":"///","IsActive":true,"List":[{"MenuID":4,"MenuName":"Property Tax","ParentMenuID":1,"ActionMethodID":16,"Rank":1,"Url":"/Municipality/Collection/Index/","IsActive":true,"List":[]}]},{"MenuID":2,"MenuName":"Configuration","ParentMenuID":null,"ActionMethodID":1,"Rank":2,"Url":"/Home/Index/","IsActive":true,"List":[{"MenuID":5,"MenuName":"Counters","ParentMenuID":2,"ActionMethodID":24,"Rank":1,"Url":"/Municipality/MnCounter/Index/","IsActive":true,"List":[]},{"MenuID":6,"MenuName":"Map Counter & User","ParentMenuID":2,"ActionMethodID":37,"Rank":2,"Url":"/Municipality/MnUserCounterAssociate/Index/","IsActive":true,"List":[]},{"MenuID":7,"MenuName":"Pay Head Config","ParentMenuID":2,"ActionMethodID":42,"Rank":3,"Url":"/Municipality/MnPayHead/Configure_PayHead/","IsActive":true,"List":[]},{"MenuID":8,"MenuName":"Map Counter & Payment Mode","ParentMenuID":2,"ActionMethodID":45,"Rank":4,"Url":"/Municipality/MnPaymentModeConfiguration/Index/","IsActive":true,"List":[]}]},{"MenuID":3,"MenuName":"Reporting","ParentMenuID":null,"ActionMethodID":null,"Rank":3,"Url":"///","IsActive":true,"List":[{"MenuID":14,"MenuName":"Collection Report","ParentMenuID":3,"ActionMethodID":54,"Rank":1,"Url":"/Municipality/ReportCalls/MnCollectionReport/Index/","IsActive":true,"List":[]},{"MenuID":15,"MenuName":"Demand Report","ParentMenuID":3,"ActionMethodID":58,"Rank":2,"Url":"/Municipality/ReportCalls/MnDemandReport/Index/","IsActive":true,"List":[]}]},{"MenuID":9,"MenuName":"User","ParentMenuID":null,"ActionMethodID":1,"Rank":4,"Url":"/Home/Index/","IsActive":true,"List":[{"MenuID":10,"MenuName":"New User","ParentMenuID":9,"ActionMethodID":34,"Rank":1,"Url":"/Municipality/MnUserRegistration/Index/","IsActive":true,"List":[]},{"MenuID":12,"MenuName":"Reset Password","ParentMenuID":9,"ActionMethodID":55,"Rank":2,"Url":"/Municipality/MnUserPassword/ResetPasswordUI/","IsActive":true,"List":[]},{"MenuID":11,"MenuName":"Change Password","ParentMenuID":9,"ActionMethodID":28,"Rank":3,"Url":"/Municipality/MnUserPassword/Index/","IsActive":true,"List":[]},{"MenuID":13,"MenuName":"Manage Role","ParentMenuID":9,"ActionMethodID":30,"Rank":4,"Url":"/Municipality/MnRoleUsersMapping/Index/","IsActive":true,"List":[]}]}]');
                //console.log('dummy data');
                //console.log(JSON.stringify(signedDatasource));
               var signedDatasource = response.Data;
                //console.log('server data');
                //console.log(JSON.stringify(signedDatasource));
                var test = new APP_GLOBAL.Menu.SideBarMenu();
                var ConvertToTreeFormat = function (data) {
                    var formattedSource = [];
                    var formattedClass = function (nodeText, nodeID, childrens, isExpanded, additionalData, parentID, url) {
                        if (childrens.constructor !== Array) {
                            throw 'childrens must be type of array';
                        }
                        return {
                            text: nodeText,
                            id: nodeID,
                            nodes: childrens,
                            hasChildren: childrens.length > 0,
                            isExpanded: isExpanded,
                            data: additionalData,
                            parentID: parentID,
                            url: url
                        };
                    };
                    var drillDownNodes = function (data) {
                        var d = [];
                        $.each(data, function (index, value) {
                            if (value.Menus == 0) {//no child
                                d.push(formattedClass(value.MenuName, value.MenuID, [], false, null, value.ParentMenuID === null ? 0 : value.ParentMenuID, value.Url));
                            } else {
                                d.push(formattedClass(value.MenuName, value.MenuID, drillDownNodes(value.Menus), false, null, value.ParentMenuID === null ? 0 : value.ParentMenuID, ''));
                            }
                        });
                        return d;
                    };
                    return (function () {
                        return formattedClass('Super Parent', 0, drillDownNodes(data), true, null, null);
                    }());
                };
                
                test.inititalize(ConvertToTreeFormat(signedDatasource));
            }
        },
        failure: function (response) {
            alert(response);
        }
    });
    

});


