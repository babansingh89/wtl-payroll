﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;


namespace wbEcsc.ViewModels
{
    public class DesignationViewModel
    {
        public List<Field> LsitField { get; set; }
        public Designation_MD SelectedDesignation { get; set; }
        public List<Designation_MD> Designation_list { get; set; }
    }
}