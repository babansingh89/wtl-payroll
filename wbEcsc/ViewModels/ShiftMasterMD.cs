﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.ViewModels
{
    public class ShiftMasterMD
    {
        public string TransactionType { get; set; }
        public int? ShiftId { get; set; }
        public string ShiftDesc { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string GraceIn { get; set; }
        public string GraceOut { get; set; }
        public string MinHalfDayHrs { get; set; }
        public string MinFullDayHrs { get; set; }
        public string Wef { get; set; }
        public int? ActiveFlag { get; set; }
        public int? NShift { get; set; }
        public int? UserID { get; set; }
        public int? SectorID { get; set; }

    }//ALL END
}