﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.ViewModels
{
    public class DA_HRA_MA_MaterVM
    {
        public int? EarningCode { get; set; }
        public string EarningType { get; set; }
        public string EmpType { get; set; }
        public string EmpTypeDesc { get; set; }
        public int? DAID { get; set; }
        public string DARate { get; set; }
        public int? HRAID { get; set; }
        public string HRA { get; set; }
        public int? MAID { get; set; }
        public string PayVal { get; set; }
        public int? IDS { get; set; }
        public string VALS { get; set; }
        public int? DA_HRA_MA_ID { get; set; }
        public string EmpNo { get; set; }
        public string EmpName { get; set; }
        public string CenterName { get; set; }
        public string OldDa { get; set; }
        public string NewDa { get; set; }
        public string Declined { get; set; }
        public int? ErrorCode { get; set; }
        public string Messege { get; set; }
    }
}