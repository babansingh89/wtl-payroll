﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels;

namespace wbEcsc.ViewModels
{
    public class BankBranchViewModel
    {
        public List<Field> lstField { get; set; }
        public Branch_MD SelectedBranch { get; set; }
        public List<Branch_MD> BranchList { get; set; }
    //    public List<Bank_MD> BankList { get; set; }
    }


}