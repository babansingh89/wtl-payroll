﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.ViewModels
{
    public class DAMasterVM
    {
        public int? DAID { get; set; }
        public string EmpType { get; set; }
        public string EmpTypeDesc { get; set; }
        public string DARate { get; set; }
        public string MaxAmount { get; set; }
        public string OrderNo { get; set; }
        public string OrderDate { get; set; }
        public string EffectiveFrom { get; set; }
        public string EffectiveTo { get; set; }
        public string Edit { get; set; }
        public string Del { get; set; }
        public int? ErrorCode { get; set; }
        public string Messege { get; set; }
    }
}