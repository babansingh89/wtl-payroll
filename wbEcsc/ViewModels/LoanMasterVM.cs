﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.ViewModels
{
    public class LoanMasterVM
    {
        public int? EmployeeID { get; set; }
        public string EmpNo { get; set; }
        public string EmpName { get; set; }
        public int? SectorID { get; set; }
        public int? LeftMonth { get; set; }
        public string LoanDeducDate { get; set; }
        public string Messege { get; set; }
        public int? ErrorCode { get; set; }
        public int? Install1 { get; set; }
        public string InstallAmt1 { get; set; }
        public int? Install2 { get; set; }
        public string InstallAmt2 { get; set; }
        public int? IsRound { get; set; }
        public int? Install3 { get; set; }
        public string InstallAmt3 { get; set; }
        public string LoanAmount { get; set; }
        public string LoanSancDate { get; set; }
        public int? LoanID { get; set; }
        public int FieldID { get; set; }
        public string FieldName { get; set; }
        public int IsMand { get; set; }
        public int? LoanTypeID { get; set; }
        public int? LoanDescID { get; set; }
        public int? TotLoanInstall { get; set; }
        public int? CurLoanInstall { get; set; }
        public string LoanAmountPaid { get; set; }
        public string AdjustDate { get; set; }
        public string AdjustAmount { get; set; }
        public string ReferenceNo { get; set; }
        public string Referencedate { get; set; }
        public string DisburseAmt { get; set; }
        public string SancOrderNo { get; set; }
        public string LoanType { get; set; }
        public string LoanDesc { get; set; }
    }
}
