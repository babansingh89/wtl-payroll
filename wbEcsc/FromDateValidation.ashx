﻿<%@ WebHandler Language="C#" Class="FromDateValidation" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;

public class FromDateValidation : IHttpHandler {

    public void ProcessRequest (HttpContext context) {
        string JSONVal = "";
        string LtNo = "";
        string LtDate = "";
        int EmployeeID = 0;
        string FromDate = "";
        string ToDate = "";
        ApiResponceData GER = new ApiResponceData();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        try
        {
            if (context.Request.QueryString.Count > 0)
            {
                LtNo = context.Request.QueryString["LtNo"].ToString();
                LtDate = context.Request.QueryString["LtDate"].ToString();
                EmployeeID = Convert.ToInt32(context.Request.QueryString["EmployeeID"]);
                FromDate = context.Request.QueryString["FromDate"].ToString();
                ToDate = context.Request.QueryString["ToDate"].ToString();
            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                LtNo = context.Request.Params["LtNo"].ToString();
                LtDate = context.Request.Params["LtDate"].ToString();
                EmployeeID = Convert.ToInt32(context.Request.Params["EmployeeID"]);
                FromDate = context.Request.Params["FromDate"].ToString();
                ToDate = context.Request.Params["ToDate"].ToString();
            }
            if (EmployeeID>0)
            {
                DataTable dt = new DataTable();
                using (var command = new SqlCommand("Get_LvFromDtApp", con)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    con.Open();
                    command.Parameters.AddWithValue("@LtNo", LtNo);
                    command.Parameters.AddWithValue("@LtDate", LtDate);
                    command.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                    command.Parameters.AddWithValue("@FromDate", FromDate);
                    command.Parameters.AddWithValue("@ToDate", ToDate);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    sda.Fill(dt);
                    con.Close();
                }
                if (dt.Rows.Count > 0)
                {
                    List<Dictionary<string, object>> rowss = new List<Dictionary<string, object>>();
                    if (dt.Rows.Count > 0)
                    {

                        Dictionary<string, object> row1;

                        foreach (DataRow r in dt.Rows)
                        {
                            row1 = new Dictionary<string, object>();
                            foreach (DataColumn col in dt.Columns)
                            {
                                row1.Add(col.ColumnName, r[col]);
                            }
                            rowss.Add(row1);
                        }
                    }


                    GER.Data = rowss;
                    GER.Status = "Y";
                    GER.Message = "SUCCESS";
                    JSONVal = GER.ToJSON();
                }
                else
                {
                    GER.Status = "N";
                    GER.Message = "No records found ";
                    JSONVal = GER.ToJSON();
                }
            }
            else
            {
                GER.Status = "N";
                GER.Message = "Provide all the parameters ";
                JSONVal = GER.ToJSON();
            }
        }
        catch (Exception ex)
        {
            GER.Status = "N";
            GER.Message = ex.Message;
            JSONVal = GER.ToJSON();
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(JSONVal);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}