﻿<%@ WebHandler Language="C#" Class="CheckDoLogin" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
public class CheckDoLogin : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
{

    public void ProcessRequest (HttpContext context) {
        string JSONVal = "";
        string UserName = "";
        string UPassword = "";
        //LoginDetails GLD = new LoginDetails();
        ApiResponceData GER = new ApiResponceData();
        //string conString = DataAccess.DBHandler.GetConnectionString();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        try
        {
            if (context.Request.QueryString.Count > 0)
            {
                UserName = context.Request.QueryString["UserName"];
                UPassword = context.Request.QueryString["UPassword"];
            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                UserName = context.Request.Params["UserName"];
                UPassword = context.Request.Params["UPassword"];

            }

            if (UserName != "" && UPassword != "")
            {
                DataTable dtSignInfo = new DataTable();
                List<Dictionary<string, object>> rowss = new List<Dictionary<string, object>>();
                //DataTable dtSignInfo = DBHandler.GetResult("payroll.GET_LoginDetails", UserName, UPassword);
                using (var command = new SqlCommand("GET_LoginDetails", con)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    con.Open();
                    command.Parameters.AddWithValue("@UserName", UserName);
                    command.Parameters.AddWithValue("@UPassword", UPassword);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    sda.Fill(dtSignInfo);
                    con.Close();
                }
                if (dtSignInfo.Rows.Count > 0)
                {
                        Dictionary<string, object> row1;
                        foreach (DataRow r in dtSignInfo.Rows)
                        {
                            row1 = new Dictionary<string, object>();
                            foreach (DataColumn col in dtSignInfo.Columns)
                            {
                                row1.Add(col.ColumnName, r[col]);
                            }
                            rowss.Add(row1);
                        }
                    
                    GER.Data = rowss;
                    GER.Status = dtSignInfo.Rows[0]["ErrorCode"].ToString() == "1" ? "Y" : "N";
                    GER.Message = dtSignInfo.Rows[0]["Messege"].ToString() == "" ? "SUCCESS":dtSignInfo.Rows[0]["Messege"].ToString();
                    JSONVal = GER.ToJSON();
                }
                else
                {
                    GER.Status = "N";
                    GER.Message = "No records found ";
                    JSONVal = GER.ToJSON();
                }
            }
            else
            {
                GER.Status = "N";
                GER.Message = "Provide all the parameters ";
                JSONVal = GER.ToJSON();
            }

        }
        catch (Exception ex)
        {
            
            GER.Status = "N";
            GER.Message = ex.Message;
            JSONVal = GER.ToJSON();
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(JSONVal);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}