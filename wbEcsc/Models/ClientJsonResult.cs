﻿using System.Collections.Generic;
namespace wbEcsc.Models
{
    public class ClientJsonResult
    {
        public object Data { get; set; }
        public ResponseStatus Status { get; set; }
        public string Message { get; set; }
        public List<YearList> YearList { get; set; }
        public List<HolidayResult> holidayMaster { get; set; }
        public List<EmployeeList> EmployeeList { get; set; }
        public List<DateList> DateList { get; set; }
        public List<HolidayExcpResult> holidayExcpMaster { get; set; }
    }
    public class YearList
    {
        public string Year { get; set; }
    }
    public class HolidayResult
    {
        public string HdNo { get; set; }
        public string FromHDate { get; set; }
        public string HDesc { get; set; }
        public string ToHDate { get; set; }
        public string HType { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedOn { get; set; }
        public string SectorID { get; set; }
    }

    public class EmployeeList
    {
        public string EmpId { get; set; }
        public string EmpName { get; set; }
    }
    public class DateList
    {
        public string HdNo { get; set; }
        public string Hdate { get; set; }
    }
    public class HolidayExcpResult
    {
        public string ExcepNo { get; set; }
        public string EmployeeID { get; set; }
        public string EmpName { get; set; }
        public string Description { get; set; }
        public string HDate { get; set; }
        public string HdNo { get; set; }
        public string InsertedBy { get; set; }
        public string InsertedOn { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedOn { get; set; }
        public string SectorID { get; set; }
    }
}