﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class Caste_MD
    {
            public int? CasteID { get; set; }
            public string CasteCode { get; set; }
            public string Caste { get; set; }
    }
}