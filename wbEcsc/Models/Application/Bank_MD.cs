﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class Bank_MD
    {
        public int? BankID { get; set; }
        public string BankName { get; set; }
    }
}