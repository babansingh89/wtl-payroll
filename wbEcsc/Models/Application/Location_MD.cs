﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class Location_MD
    {
        public int? CenterID { get; set; }
        public string CenterCode { get; set; }
        public string CenterName { get; set; }
    }
}