﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class Status_MD
    {
        public int? StatusID { get; set; }
        public string Status { get; set; }

        public string Abbv { get; set; }
    }
}