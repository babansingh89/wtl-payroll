﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class IR_MD
    {
        public int? IRID {get;set;}
        public decimal IRRate { get; set; }
    }
}