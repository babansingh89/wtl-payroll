﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class HolidayMaster_MD
    {

        public int? hdnHolidayID { get; set; }
        public string txtYear { get; set; }
        public string ddlHolidayType { get; set; }
        public string txtFromDate { get; set; }
        public string txtToDate { get; set; }
        public string txtDescription { get; set; }
        public string Edit { get; set; }
        public string Del { get; set; }
        public int? ErrorCode { get; set; }
        public string Messege { get; set; }
        public int? FieldId { get; set; }
        public string FieldName { get; set; }
        public int? IsMand { get; set; }
    }
}