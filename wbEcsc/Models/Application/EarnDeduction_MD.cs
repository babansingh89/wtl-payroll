﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class EarnDeduction_MD
    {
        public string EDID { get; set; }
        public string EarnDeduction { get; set; }
        public string EDAbbv { get; set; }
    }
}