﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class State_MD
    {
        public int? StateId { get; set; }
        public string State { get; set; }
    }
}