﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class Religion_MD
    {
        public int? ReligionID { get; set; }
        public string ReligionCode { get; set; }
        public string Religion { get; set; }
    }
}