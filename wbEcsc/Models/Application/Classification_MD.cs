﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class Classification_MD
    {
        public int? ClassificationID { get; set; }
        public string ClassificationCode { get; set; }
        public string Classification { get; set; }
    }
}