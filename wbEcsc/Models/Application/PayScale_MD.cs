﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class PayScale_MD
    {
        public int? PayScaleID { get; set; }
        public string EmpTypeID { get; set; }
        public string EmpType { get; set; }
        public string PayScale { get; set; }
        public string GradePay { get; set; }
        public string IncrementPercentage { get; set; }       

    }
}