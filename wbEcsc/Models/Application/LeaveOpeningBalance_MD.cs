﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class LeaveOpeningBalance_MD
    {
        public int? SectorID { get; set; }
        public int? FieldID { get; set; }
        public string OpeningDate { get; set; }
        public string EmployeeName { get; set; }
        public int? EmployeeID { get; set; }
        public int? AbbraviationID { get; set; }
        public string Abbraviation { get; set; }
        public decimal? OpeningAmount { get; set; }
    }
}