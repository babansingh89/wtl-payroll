﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.App_Codes.BLL
{
    public class Category_MD
    {
        public int? CategoryID { get; set; }
        public string CategoryCode { get; set; }
        public string Category { get; set; }
    }
}