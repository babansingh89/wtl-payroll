﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class Cooperative_MD
    {
        public int? CooperativeID { get; set; }
        public string CooperativeCode { get; set; }
        public string Cooperative { get; set; }
    }
}