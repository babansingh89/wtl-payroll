﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Application
{
    public class ItaxMaxAmount
    {
        public int? ID { get; set; }
        public string FinYear { get; set; }
        public string GenderID { get; set; }
        public string Gender { get; set; }
        public decimal? Amount { get; set; }

    }
}