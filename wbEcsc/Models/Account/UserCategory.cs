﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class UserCategory
    {
       public int UserTypeID  { get; set; }
        public string UserType { get; set; }
        public string Description { get; set; }
    }
}