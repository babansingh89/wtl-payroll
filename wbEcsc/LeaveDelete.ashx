﻿<%@ WebHandler Language="C#" Class="LeaveDelete" %>
using System;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
    public class LeaveDelete : IHttpHandler, IReadOnlySessionState, IRequiresSessionState {

    public void ProcessRequest (HttpContext context) {
        string JSONVal = "";
        string LtNo = "";
        string Transtype = "Delete";
        int InsertedBy =0;
        ApiResponceEmpReportPersonalInfo GER = new ApiResponceEmpReportPersonalInfo();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        try
        {
            if (context.Request.QueryString.Count > 0)
            {
                LtNo = context.Request.QueryString["LtNo"].ToString();
                InsertedBy =Convert.ToInt32(context.Request.QueryString["InsertedBy"]);
            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                LtNo = context.Request.Params["LtNo"].ToString();
                InsertedBy =Convert.ToInt32(context.Request.Params["InsertedBy"]);
                
            }

            if (LtNo !="" && InsertedBy>0)
            {
                DataSet ds = new DataSet();
                using (var command = new SqlCommand("check_leaveTransactionNew", con)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    con.Open();
                    command.Parameters.AddWithValue("@TransactionType", Transtype);
                    command.Parameters.AddWithValue("@LtNo", LtNo);
                    command.Parameters.AddWithValue("@InsertedBy", InsertedBy);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    sda.Fill(ds);
                    con.Close();
                }
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    List<Dictionary<string, object>> rowss = new List<Dictionary<string, object>>();
                    
                    if (dt.Rows.Count > 0)
                    {

                        Dictionary<string, object> row1;

                        foreach (DataRow r in dt.Rows)
                        {
                            row1 = new Dictionary<string, object>();
                            foreach (DataColumn col in dt.Columns)
                            {
                                row1.Add(col.ColumnName, r[col].ToString());
                            }
                            rowss.Add(row1);
                        }
                    }
                    
                    GER.Data = rowss;
                    GER.Status = "Y";
                    GER.Message = "SUCCESS";
                    JSONVal = GER.ToJSON();
                }
                else
                {
                    GER.Status = "N";
                    GER.Message = "No records found ";
                    JSONVal = GER.ToJSON();
                }
            }
            else
            {
                GER.Status = "N";
                GER.Message = "Provide all the parameters ";
                JSONVal = GER.ToJSON();
            }

        }
        catch (Exception ex)
        {
            GER.Status = "N";
            GER.Message = ex.Message;
            JSONVal = GER.ToJSON();
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(JSONVal);
    }


    public bool IsReusable {
        get {
            return false;
        }
    }

}
public class ApiResponceEmpReportPersonalInfo
{
    public string Status { get; set; }

    public string Message { get; set; }

    public object Data { get; set; }
}
