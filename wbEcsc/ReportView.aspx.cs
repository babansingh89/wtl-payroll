﻿using System;
using System.Configuration;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions;
using CrystalDecisions.Shared;
using System.Web.Services;
using wbEcsc.App_Codes;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace wbEcsc
{
    public partial class ReportView : System.Web.UI.Page 
    {
        string ReportTypeID = "";
        string ReportID = "";
        string SalMonth = "";
        string ExportFileName = "";
        string ReportFor = "";
        string PDFExcel = "";
  string BankID = "";


        ExportFormatType formatType = ExportFormatType.NoFormat;
        CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();
        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;

        IInternalUser user;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!SessionContext.IsAuthenticated)
            {
                Response.Redirect("/Error/Error401/Index", true);
                return;
            }

            user = SessionContext.CurrentUser as IInternalUser;

            if (HttpContext.Current.Request.QueryString["ReportTypeID"] != null)
                ReportTypeID = HttpContext.Current.Request.QueryString["ReportTypeID"].ToString();

            if (HttpContext.Current.Request.QueryString["ReportID"] != null)
                ReportID = HttpContext.Current.Request.QueryString["ReportID"].ToString();

            if (HttpContext.Current.Request.QueryString["SalMonth"] != null)
                SalMonth = HttpContext.Current.Request.QueryString["SalMonth"].ToString();

            if (HttpContext.Current.Request.QueryString["ReportFor"] != null)
                ReportFor = HttpContext.Current.Request.QueryString["ReportFor"].ToString();

            if (HttpContext.Current.Request.QueryString["PDFExcel"] != null)
                PDFExcel = HttpContext.Current.Request.QueryString["PDFExcel"].ToString();

 if (HttpContext.Current.Request.QueryString["BankID"] != null)
                BankID = HttpContext.Current.Request.QueryString["BankID"].ToString();


            BindReport(crystalReport);
        }
        private void BindReport(ReportDocument crystalReport)
        {
            string Formula = "";
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Get_Report", con);

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ParentID", ReportTypeID);
            cmd.Parameters.AddWithValue("@ReportID", ReportID.Equals("") ? DBNull.Value : (object)ReportID);
            cmd.Parameters.AddWithValue("@UserID", user.UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);
                if(dt.Rows.Count>0)
                {
                    string rptName = dt.Rows[0]["rptName"].ToString();
                    string ReportName = dt.Rows[0]["ReportName"].ToString();

                    SqlCommand cmds = new SqlCommand("Get_Find_Report", con);
                    cmds.CommandType = CommandType.StoredProcedure;
                    cmds.Parameters.AddWithValue("@UserID", user.UserID);
                    cmds.Parameters.AddWithValue("@ReportID", ReportID);
                    cmds.Parameters.AddWithValue("@ReportTypeID", ReportTypeID);
                    SqlDataAdapter das = new SqlDataAdapter(cmds);
                    DataTable dtt = new DataTable();
                    das.Fill(dtt);
                    if(dtt.Rows.Count>0)
                    {
                        Formula = dtt.Rows[0]["Formula"].ToString();
                    }
                    if (ReportID == "11")
                        Formula = "{tmp_payslip_monthly_EmpWise_History.inserted_By}=" + user.UserID + "";

                    if (BankID != "")
                        Formula = "{RPT_Temp_Bank_Slip_cum_List.BankID}=" + BankID + "";

                    crystalReport.Load(Server.MapPath("~/Reports/" + rptName.Trim()));
                    crystalReport.RecordSelectionFormula = Formula;
                    ExportFileName = ReportName + "_" + SalMonth;

                    crystalReport.Refresh();

                    string server = Connection_Details.ServerName();
                    string database = Connection_Details.DatabaseName();
                    string userid = Connection_Details.UserID();
                    string password = Connection_Details.Password();

                   
                    crystalReport.DataSourceConnections[0].SetConnection(server, database, userid, password);
                    crystalReport.DataSourceConnections[0].IntegratedSecurity = false;
                    crystalReport.SetDatabaseLogon(server, database, userid, password);

                    for (int i = 0; i < crystalReport.Subreports.Count; i++)
                    {
                        crystalReport.Subreports[i].DataSourceConnections[0].SetConnection(server, database, userid, password);
                        crystalReport.Subreports[i].DataSourceConnections[0].IntegratedSecurity = false;
                        crystalReport.Subreports[i].SetDatabaseLogon(server, database, userid, password);

                    }
                    crystalReport.Refresh();
                    crystalReport.VerifyDatabase();
                    CrystalReportViewer1.ReportSource = crystalReport;

                    if (PDFExcel == "1" || PDFExcel == "")
                        formatType = ExportFormatType.PortableDocFormat;
                    else
                        formatType = ExportFormatType.Excel;

                    //formatType = ExportFormatType.PortableDocFormat;
                    crystalReport.ExportToHttpResponse(formatType, Response, false, ExportFileName);


                }
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

       
            
        
    }
}