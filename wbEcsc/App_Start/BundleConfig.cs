﻿using System.Web;
using System.Web.Optimization;

namespace wbEcsc
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.

            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/site.css", "~/Content/jquery-ui.css"));
            bundles.Add(new StyleBundle("~/Content/overlay").Include(
                     "~/Content/OverLay.css"));

            bundles.Add(new ScriptBundle("~/bundles/utility").Include(
                      "~/Scripts/ConverterUtility.js",
                      "~/Scripts/date.format.js",
                      "~/Script/EventBus.js", 
                      "~/Script/randomColor.js"));
            bundles.Add(new ScriptBundle("~/bundles/jsSql").Include(
                      "~/Scripts/JSLINQ.js",
                      "~/Scripts/alasql.js"));

           
            bundles.Add(new ScriptBundle("~/bundles/GlobalAppJs").Include(
                     "~/Scripts/AppJs/AppGlobal.js", 
                    // "~/Scripts/AppJs/Menu.js", 
                     "~/Scripts/AppJs/autoAjaxLoader.js"));
        }
    }
}
