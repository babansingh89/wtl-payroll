﻿<%@ WebHandler Language="C#" Class="Insert_LeaveApp" %>
using System;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Data.SqlClient;
public class Insert_LeaveApp : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
{

    public void ProcessRequest (HttpContext context) {
        string JSONVal = "";
        //string LtNo = "";
        string LtDate = "";
        int EmployeeID = 0;
        int LvId = 0;
        string FromDate = "";
        string ToDate = "";
        //string Bal = "";
        int IsRound = 1;
        int IsLeave = 1;
        //int Sanctioned = 1;
        string NoOfDays = "";
        int InsertedBy = 0;
        string Remarks = "";
        //int RCode = 1;
        //int LvFlag = 1;
        ApiResponceData GER = new ApiResponceData();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        try
        {
            if (context.Request.QueryString.Count > 0)
            {
                LtDate = context.Request.QueryString["LtDate"].ToString();
                EmployeeID = Convert.ToInt32(context.Request.QueryString["EmpID"]);
                LvId = Convert.ToInt32(context.Request.QueryString["LvId"]);
                FromDate = context.Request.QueryString["FromDate"].ToString();
                ToDate = context.Request.QueryString["ToDate"].ToString();
                IsRound = Convert.ToInt32(context.Request.QueryString["IsRound"]);
                IsLeave = Convert.ToInt32(context.Request.QueryString["IsLeave"]);
                NoOfDays = context.Request.QueryString["NoOfDays"].ToString();
                InsertedBy = Convert.ToInt32(context.Request.QueryString["InsertedBy"]);
                Remarks = context.Request.QueryString["Remarks"].ToString();
            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                LtDate = context.Request.Params["LtDate"].ToString();
                EmployeeID = Convert.ToInt32(context.Request.Params["EmpID"]);
                LvId = Convert.ToInt32(context.Request.Params["LvId"]);
                FromDate = context.Request.Params["FromDate"].ToString();
                ToDate = context.Request.Params["ToDate"].ToString();
                IsRound = Convert.ToInt32(context.Request.Params["IsRound"]);
                IsLeave = Convert.ToInt32(context.Request.Params["IsLeave"]);
                NoOfDays = context.Request.Params["NoOfDays"].ToString();
                InsertedBy = Convert.ToInt32(context.Request.Params["InsertedBy"]);
                Remarks = context.Request.Params["Remarks"].ToString();
            }

            if (EmployeeID > 0)
            {
                DataTable dtSignInfo = new DataTable();
                List<Dictionary<string, object>> rowss = new List<Dictionary<string, object>>();
                using (var command = new SqlCommand("Insert_LeaveApp", con)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    con.Open();
                    //command.Parameters.AddWithValue("@LtNo",LtNo==""?DBNull.Value:(object)LtNo);
                    command.Parameters.AddWithValue("@LtDate", LtDate==""?DBNull.Value:(object)LtDate);
                    command.Parameters.AddWithValue("@EmployeeID",EmployeeID);
                    command.Parameters.AddWithValue("@LvId", LvId);
                    command.Parameters.AddWithValue("@FromDate",FromDate==""?DBNull.Value:(object)FromDate);
                    command.Parameters.AddWithValue("@ToDate", ToDate==""?DBNull.Value:(object)ToDate);
                    //command.Parameters.AddWithValue("@Bal",Bal==""?DBNull.Value:(object)Bal);
                    command.Parameters.AddWithValue("@IsRound", IsRound);
                    command.Parameters.AddWithValue("@IsLeave",IsLeave);
                    //command.Parameters.AddWithValue("@Sanctioned", Sanctioned);
                    command.Parameters.AddWithValue("@NoOfDays",NoOfDays==""?DBNull.Value:(object)NoOfDays);
                    command.Parameters.AddWithValue("@InsertedBy", InsertedBy);
                    command.Parameters.AddWithValue("@Remarks",Remarks==""?DBNull.Value:(object)Remarks);
                    //command.Parameters.AddWithValue("@RCode", RCode);
                    //command.Parameters.AddWithValue("@LvFlag", LvFlag);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    sda.Fill(dtSignInfo);
                    con.Close();
                }
                if (dtSignInfo.Rows.Count > 0)
                {
                    Dictionary<string, object> row1;
                    foreach (DataRow r in dtSignInfo.Rows)
                    {
                        row1 = new Dictionary<string, object>();
                        foreach (DataColumn col in dtSignInfo.Columns)
                        {
                            row1.Add(col.ColumnName, r[col].ToString());
                        }
                        rowss.Add(row1);
                    }

                    GER.Data = rowss;
                    GER.Status = "Y";
                    GER.Message = "SUCCESS";
                    JSONVal = GER.ToJSON();
                }
                else
                {
                    GER.Status = "N";
                    GER.Message = "No records found ";
                    JSONVal = GER.ToJSON();
                }

            }
            else
            {
                GER.Status = "N";
                GER.Message = "Provide all the parameters ";
                JSONVal = GER.ToJSON();
            }

        }
        catch (Exception ex)
        {
            GER.Status = "N";
            GER.Message = ex.Message;
            JSONVal = GER.ToJSON();
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(JSONVal);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}