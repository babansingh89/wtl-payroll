﻿<%@ WebHandler Language="C#" Class="MonthlySalaryMaster" %>
using System;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;

public class MonthlySalaryMaster : IHttpHandler, IReadOnlySessionState, IRequiresSessionState {

    public void ProcessRequest (HttpContext context) {
        string JSONVal = "";
        int employeeid = 0;
        string Transtype = "MonthlySalaryMaster";
        string SalaryFinYear = "";
        ApiResponceEmpReport GER = new ApiResponceEmpReport();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        try
        {
            if (context.Request.QueryString.Count > 0)
            {
                employeeid = Convert.ToInt32(context.Request.QueryString["EID"]);
                SalaryFinYear = context.Request.QueryString["SalaryFinYear"].ToString();
                
            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                employeeid = Convert.ToInt32(context.Request.Params["EID"]);
                SalaryFinYear = context.Request.Params["SalaryFinYear"].ToString();
                
            }

            if (employeeid > 0 && SalaryFinYear!="" )
            {
                DataSet ds = new DataSet();
                using (var command = new SqlCommand("ShowReport_ProfileSal", con)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    con.Open();
                    command.Parameters.AddWithValue("@Transtype", Transtype);
                    command.Parameters.AddWithValue("@employeeid", employeeid);
                    command.Parameters.AddWithValue("@SalaryFinYear", SalaryFinYear==""?DBNull.Value:(object)SalaryFinYear);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    sda.Fill(ds);
                    con.Close();
                }
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    List<Dictionary<string, object>> rowss = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> rowss1 = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> rowss2 = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> rowss3 = new List<Dictionary<string, object>>();
                    if (dt.Rows.Count > 0)
                    {

                        Dictionary<string, object> row1;

                        foreach (DataRow r in dt.Rows)
                        {
                            row1 = new Dictionary<string, object>();
                            foreach (DataColumn col in dt.Columns)
                            {
                                row1.Add(col.ColumnName, r[col]);
                            }
                            rowss.Add(row1);
                        }
                    }
                    if (ds.Tables.Count > 1)
                    {
                        DataTable dt1 = ds.Tables[1];
                        if (dt1.Rows.Count > 0)
                        {

                            if (dt1.Rows.Count > 0)
                            {

                                Dictionary<string, object> row1;

                                foreach (DataRow r in dt1.Rows)
                                {
                                    row1 = new Dictionary<string, object>();
                                    foreach (DataColumn col in dt1.Columns)
                                    {
                                        row1.Add(col.ColumnName, r[col].ToString());
                                    }
                                    rowss1.Add(row1);
                                }
                            }
                        }
                    }
                    if (ds.Tables.Count > 2)
                    {
                        DataTable dt2 = ds.Tables[2];
                        if (dt2.Rows.Count > 0)
                        {

                            if (dt2.Rows.Count > 0)
                            {

                                Dictionary<string, object> row1;

                                foreach (DataRow r in dt2.Rows)
                                {
                                    row1 = new Dictionary<string, object>();
                                    foreach (DataColumn col in dt2.Columns)
                                    {
                                        row1.Add(col.ColumnName, r[col]);
                                    }
                                    rowss2.Add(row1);
                                }
                            }
                        }
                    }
                    if (ds.Tables.Count > 3)
                    {
                    DataTable dt3 = ds.Tables[3];
                    if (dt3.Rows.Count > 0)
                    {

                        if (dt3.Rows.Count > 0)
                        {

                            Dictionary<string, object> row1;

                            foreach (DataRow r in dt3.Rows)
                            {
                                row1 = new Dictionary<string, object>();
                                foreach (DataColumn col in dt3.Columns)
                                {
                                    row1.Add(col.ColumnName, r[col]);
                                }
                                rowss3.Add(row1);
                            }
                        }
                    }
                }

                GER.Data = rowss;
                GER.Data1 = rowss1;
                GER.Data2 = rowss2;
                GER.Data3 = rowss3;
                GER.Status = "Y";
                GER.Message = "SUCCESS";
                JSONVal = GER.ToJSON();
            }
            else
            {
                GER.Status = "N";
                GER.Message = "No records found ";
                JSONVal = GER.ToJSON();
            }
        }
            else
            {
            GER.Status = "N";
            GER.Message = "Provide all the parameters ";
            JSONVal = GER.ToJSON();
        }

    }
        catch (Exception ex)
        {
            GER.Status = "N";
            GER.Message = ex.Message;
            JSONVal = GER.ToJSON();
        }
context.Response.ContentType = "application/json";
        context.Response.Write(JSONVal);
    }


    public bool IsReusable {
    get {
        return false;
    }
}

}