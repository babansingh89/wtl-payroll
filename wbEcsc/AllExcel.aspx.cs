﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Data.SqlClient;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;
using NPOI.HSSF.UserModel;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using System.Configuration;
using wbEcsc.App_Codes;
using wbEcsc.Models.Application;
using wbEcsc.Models.Account;

namespace wbEcsc
{
    public partial class AllExcel : System.Web.UI.Page
    {
        string ReportTypeID = "";
        string ReportID = "";
        string SalMonth = "";
        string SalMonthID = "";
        string SalFinYear = "";
        string SectorID = "";
        string ReportName = "";

        string conString = ConfigurationManager.ConnectionStrings["constring"].ConnectionString;

        List<wbEcsc.Models.Application.Report> reports = new List<wbEcsc.Models.Application.Report>();
        IInternalUser user;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!SessionContext.IsAuthenticated)
            {
                Response.Redirect("/Error/Error401/Index",true);
                return;
            }

            user = SessionContext.CurrentUser as IInternalUser;

            #region  IMPORTAT Variables
            if (HttpContext.Current.Request.QueryString["ReportTypeID"] != null)
                ReportTypeID = HttpContext.Current.Request.QueryString["ReportTypeID"].ToString();
            if (HttpContext.Current.Request.QueryString["ReportID"] != null)
                ReportID = HttpContext.Current.Request.QueryString["ReportID"].ToString();
            if (HttpContext.Current.Request.QueryString["SalMonth"] != null)
                SalMonth = HttpContext.Current.Request.QueryString["SalMonth"].ToString();
            if (HttpContext.Current.Request.QueryString["SalMonthID"] != null)
                SalMonthID = HttpContext.Current.Request.QueryString["SalMonthID"].ToString();
            if (HttpContext.Current.Request.QueryString["SalFinYear"] != null)
                SalFinYear = HttpContext.Current.Request.QueryString["SalFinYear"].ToString();
            if (HttpContext.Current.Request.QueryString["SectorID"] != null)
                SectorID = HttpContext.Current.Request.QueryString["SectorID"].ToString();
            if (HttpContext.Current.Request.QueryString["ReportName"] != null)
                ReportName = HttpContext.Current.Request.QueryString["ReportName"].ToString();
            #endregion

            if (ReportName == "EMP_Orginal_Sal_Statement")
                OrginalSalReports();
            else if (ReportName == "Salary_Projection_Report")
                ProjectedSalReports();
            else
                Bind_Report(ReportID);

        }
        public void Bind_Report(string ReportID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Get_Report", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ParentID", ReportTypeID);
            cmd.Parameters.AddWithValue("@ReportID", ReportID.Equals("") ? DBNull.Value : (object)ReportID);
            cmd.Parameters.AddWithValue("@UserID", user.UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);
                if(dt.Rows.Count>0)
                {
                    string rptName = dt.Rows[0]["rptName"].ToString();
                    string ReportName = dt.Rows[0]["ReportName"].ToString();

                    Get_Credentials(ReportName);

                }
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                con.Close();
            }
        }
        public void Get_Credentials(string ReportName)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("Load_reportDynamic", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SalmonthID", SalMonthID);
            cmd.Parameters.AddWithValue("@salmonth", SalMonth);
            cmd.Parameters.AddWithValue("@salaryFinYear", SalFinYear);
            cmd.Parameters.AddWithValue("@Secid", SectorID);
            cmd.Parameters.AddWithValue("@UserID", user.UserID);
            cmd.Parameters.AddWithValue("@ChildreportID", ReportID);

            cmd.Parameters.AddWithValue("@CenterID", DBNull.Value);
            cmd.Parameters.AddWithValue("@EmpType", DBNull.Value);
            cmd.Parameters.AddWithValue("@Status", DBNull.Value);
            cmd.Parameters.AddWithValue("@EDID", DBNull.Value);
            cmd.Parameters.AddWithValue("@SectorGroup", DBNull.Value);
            cmd.Parameters.AddWithValue("@CenterGroup", DBNull.Value);
            cmd.Parameters.AddWithValue("@EmpTypeGroup", DBNull.Value);
            cmd.Parameters.AddWithValue("@StatusGroup", DBNull.Value);
            cmd.Parameters.AddWithValue("@SchType", DBNull.Value);
            cmd.Parameters.AddWithValue("@isPDFExcel", 2);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);
                if(dt.Rows.Count>0)
                {
                    Choose_Function_forExcel(dt, ReportName);
                    
                }
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public void Choose_Function_forExcel(DataTable dt, string ReportName)
        {
            if (ReportName == "BankReport")
                Bank_Slip_Cum_List(dt, ReportName);
            else
            {
                string colums = "";
                GenerateExcels(dt, colums, ReportName);
            }
        }

        public void ProjectedSalReports()
        {
            string FinYr = ""; string SecID = ""; string EmpNo = "";
            SqlConnection con = new SqlConnection(conString);
            try
            {
                if (HttpContext.Current.Request.QueryString["SecID"] != null)
                    SecID = HttpContext.Current.Request.QueryString["SecID"].ToString();
                if (HttpContext.Current.Request.QueryString["EmpNo"] != null)
                    EmpNo = HttpContext.Current.Request.QueryString["EmpNo"].ToString();

                long UserID = user.UserID;
                int SectorID = Int32.Parse(SecID);

                SqlCommand cmd = new SqlCommand("Load_EmpWisePayReportExcel", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmpNo", EmpNo);
                cmd.Parameters.AddWithValue("@FinYR", SalFinYear);
                cmd.Parameters.AddWithValue("@SecID", SectorID);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    string Columns = "";
                    GenerateExcels(dt, Columns, "ProjectedSalReport");
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();


            }
        }

        public void OrginalSalReports()
        {
            SqlConnection con = new SqlConnection(conString);
            try
            {
                var Seid = "";
                var EmpNo = "";
               
                if (HttpContext.Current.Request.QueryString["SecID"] != null)
                    Seid = HttpContext.Current.Request.QueryString["SecID"].ToString();
                if (HttpContext.Current.Request.QueryString["EmpNo"] != null)
                    EmpNo = HttpContext.Current.Request.QueryString["EmpNo"].ToString();
                long UserID = user.UserID;
                int SectorID = Int32.Parse(Seid);

                
                SqlCommand cmd = new SqlCommand("Load_EmpWiseOrginalPayReportExcel", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmpNo", EmpNo);
                cmd.Parameters.AddWithValue("@FinYR", SalFinYear);
                cmd.Parameters.AddWithValue("@SecID", SectorID);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                con.Open();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    string Columns = "";
                    GenerateExcels(dt, Columns, "AnnualIncomeReport");
                }
            }
            catch(Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public void Bank_Slip_Cum_List(DataTable dt, string ReportName)
        {
                string colums = "NetPay";
                GenerateExcels(dt, colums, ReportName);
        }

        public void GenerateExcels(DataTable dt, string SumFieldName, string ReportName)
        {
            DataTable dtTotalPosition = new DataTable();

            decimal Total = 0; string Position = "";
            if (dt.Rows.Count > 0)
            {
                var workbook = new HSSFWorkbook();
                var sheet = workbook.CreateSheet();

                //Create a header row
                var headerRow = sheet.CreateRow(0);

                string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                for (int j = 0; j < columnNames.Length; j++)
                {
                    headerRow.CreateCell(j).SetCellValue(columnNames[j]);
                }
                int rowNumber = 1;
                var row = sheet.CreateRow(rowNumber);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //Create a new Row
                    row = sheet.CreateRow(rowNumber++);

                    string[] columnNamess = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                    for (int j = 0; j < columnNamess.Length; j++)
                    {
                        row.CreateCell(j).SetCellValue(dt.Rows[i][columnNamess[j]].ToString());
                    }
                }

                if (SumFieldName != "")
                {
                    string col = "";
                    string[] lines = SumFieldName.Split(',');
                    for (int i = 0; i < lines.Length; i++)
                    {
                        col = lines[i].Trim().ToString();
                        DataColumnCollection columns = dt.Columns;
                        if (columns.Contains(col))
                        {
                            Total = 0;
                            for (int k = 0; k < dt.Rows.Count; k++)
                            {
                                string value = dt.Rows[k][col].ToString();
                                Total = Total + Convert.ToDecimal(value);
                                Position = dt.Columns.IndexOf(col).ToString();
                            }
                            //Add Total amount and Column Position in a DataTable
                            if (dtTotalPosition != null)
                            {
                                if (dtTotalPosition.Rows.Count > 0)
                                {
                                    DataRow dtNewRow = dtTotalPosition.NewRow();
                                    dtNewRow["Position"] = Position;
                                    dtNewRow["Total"] = Total;
                                    dtTotalPosition.Rows.Add(dtNewRow);
                                }
                                else
                                {
                                    dtTotalPosition.Columns.Add("Position");
                                    dtTotalPosition.Columns.Add("Total");

                                    DataRow dtNewRow = dtTotalPosition.NewRow();
                                    dtNewRow["Position"] = Position;
                                    dtNewRow["Total"] = Total;
                                    dtTotalPosition.Rows.Add(dtNewRow);
                                }
                            }
                        }
                        else
                        {
                            if (dtTotalPosition != null)
                            {
                                if (dtTotalPosition.Rows.Count > 0)
                                {

                                }
                                else
                                {
                                    dtTotalPosition.Columns.Add("Position");
                                    dtTotalPosition.Columns.Add("Total");
                                }
                            }
                        }
                    }
                }

                if (SumFieldName != "")
                {
                    // Add a row for the detail row
                    row = sheet.CreateRow(dt.Rows.Count + 1);
                    //var cell = row.CreateCell(Convert.ToInt32(Position) - 1);
                    //cell.SetCellValue("Total:");

                    if (dtTotalPosition.Rows.Count > 0)
                    {
                        for (int j = 0; j < dtTotalPosition.Rows.Count; j++)
                        {
                            string FinalPosition = dtTotalPosition.Rows[j]["Position"].ToString();
                            string FinalAmt = dtTotalPosition.Rows[j]["Total"].ToString();

                            row.CreateCell(Convert.ToInt32(FinalPosition)).SetCellValue(FinalAmt);
                        }
                    }
                    else
                    {

                    }
                }


                using (var exportData = new MemoryStream())
                {
                    workbook.Write(exportData);
                    string saveAsFileName = string.Format(ReportName + "-{0:d}.xls", DateTime.Now).Replace("/", "-");
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
                    Response.Clear();
                    Response.BinaryWrite(exportData.GetBuffer());
                    Response.End();
                }
            }
        }


        //************************************************************************************************************************************************************
        //************************************************************************************************************************************************************
        

       
    }
}