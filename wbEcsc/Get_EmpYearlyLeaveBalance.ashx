﻿<%@ WebHandler Language="C#" Class="Get_EmpYearlyLeaveBalance" %>

using System;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Data.SqlClient;
public class Get_EmpYearlyLeaveBalance : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) {
        string JSONVal = "";
        int LYear = 0;
        int EmployeeID = 0;
        ApiResponceData GER = new ApiResponceData();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        SqlConnection con = new SqlConnection(conString);
        try
        {
            if (context.Request.QueryString.Count > 0)
            {
                EmployeeID = Convert.ToInt32(context.Request.QueryString["EmpID"]);
                LYear = Convert.ToInt32(context.Request.QueryString["YR"]);
                
            }
            else if (context.Request.HttpMethod.ToUpper() == "POST")
            {
                EmployeeID = Convert.ToInt32(context.Request.Params["EmpID"]);
                LYear = Convert.ToInt32(context.Request.Params["YR"]);
                

            }

            if (EmployeeID > 0 &&  LYear > 0)
            {
                DataTable dtSignInfo = new DataTable();
                List<Dictionary<string, object>> rowss = new List<Dictionary<string, object>>();
                using (var command = new SqlCommand("Get_EmplLeaveBalanceDetailByYear", con)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    con.Open();
                    command.Parameters.AddWithValue("@LYear",LYear);
                    command.Parameters.AddWithValue("@EmployeeID", EmployeeID);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    sda.Fill(dtSignInfo);
                    con.Close();
                }
                if (dtSignInfo.Rows.Count > 0)
                {
                        Dictionary<string, object> row1;
                        foreach (DataRow r in dtSignInfo.Rows)
                        {
                            row1 = new Dictionary<string, object>();
                            foreach (DataColumn col in dtSignInfo.Columns)
                            {
                                row1.Add(col.ColumnName, r[col].ToString());
                            }
                            rowss.Add(row1);
                        }
                    
                    GER.Data = rowss;
                    GER.Status = "Y";
                    GER.Message = "SUCCESS";
                    JSONVal = GER.ToJSON();
                }
                else
                {
                    GER.Status = "N";
                    GER.Message = "No records found ";
                    JSONVal = GER.ToJSON();
                }

            }
            else
            {
                GER.Status = "N";
                GER.Message = "Provide all the parameters ";
                JSONVal = GER.ToJSON();
            }

        }
        catch (Exception ex)
        {
            GER.Status = "N";
            GER.Message = ex.Message;
            JSONVal = GER.ToJSON();
        }
        context.Response.ContentType = "application/json";
        context.Response.Write(JSONVal);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}